package controlador;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dao.ClienteDAO;
import dao.InsumoDAO;
import dao.ItemPrendaDAO;
import dao.OrdenProduccionDAO;
import dao.PedidoClienteDAO;
import dao.PedidoInsumoDAO;
import dao.PrendaDAO;
import dao.ProveedorDAO;
import dao.SucursalDAO;
import dto.ClienteDTO;
import dto.InsumoDTO;
import dto.ItemPrendaDTO;
import dto.PedidoClienteDTO;
import dto.PrendaDTO;
import dto.SucursalDTO;
import enums.EstadoPedidoCliente;
import enums.MotivoRechazo;
import exceptions.ClienteException;
import exceptions.InsumoException;
import exceptions.ItemPrendaException;
import exceptions.OrdenProduccionException;
import exceptions.PedidoClienteException;
import exceptions.PedidoInsumoException;
import exceptions.PrendaException;
import exceptions.ProveedorException;
import exceptions.SucursalException;
import exceptions.UbicacionException;
import modelo.Almacen;
import modelo.Cliente;
import modelo.Despacho;
import modelo.Insumo;
import modelo.ItemPrenda;
import modelo.OrdenProduccion;
import modelo.PedidoCliente;
import modelo.PedidoInsumo;
import modelo.Prenda;
import modelo.Proveedor;
import modelo.Sucursal;

public class ControladorGral {
	private static ControladorGral instancia;

	private List<Cliente> clientes;
	private List<Insumo> insumos;
	private List<ItemPrenda> itemsPrenda;
	private List<OrdenProduccion> ordenesProduccion;
	private List<PedidoCliente> pedidosCliente;
	private List<PedidoInsumo> pedidosInsumo;
	private List<Prenda> prendas;
	private List<Proveedor> proveedores;
	private List<Sucursal> sucursales;

	private ControladorGral() {

		clientes = new ArrayList<Cliente>();
		insumos = new ArrayList<Insumo>();
		itemsPrenda = new ArrayList<ItemPrenda>();
		ordenesProduccion = new ArrayList<OrdenProduccion>();
		pedidosCliente = new ArrayList<PedidoCliente>();
		pedidosInsumo = new ArrayList<PedidoInsumo>();
		prendas = new ArrayList<Prenda>();
		proveedores = new ArrayList<Proveedor>();
		sucursales = new ArrayList<Sucursal>();

	}

	public static ControladorGral getInstancia() {
		if (instancia == null)
			instancia = new ControladorGral();
		return instancia;
	}

	/**************************************************************************************************************************/
	/** Getters y Setters */
	/**
	 *************************************************************************************************************************/

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Insumo> getInsumos() {
		return insumos;
	}

	public void setInsumos(List<Insumo> insumos) {
		this.insumos = insumos;
	}

	public List<ItemPrenda> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrenda> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	public List<OrdenProduccion> getOrdenesProduccion() {
		return ordenesProduccion;
	}

	public void setOrdenesProduccion(List<OrdenProduccion> ordenesProduccion) {
		this.ordenesProduccion = ordenesProduccion;
	}

	public List<PedidoCliente> getPedidosCliente() {
		return pedidosCliente;
	}

	public void setPedidosCliente(List<PedidoCliente> pedidosCliente) {
		this.pedidosCliente = pedidosCliente;
	}

	public List<PedidoInsumo> getPedidosInsumo() {
		return pedidosInsumo;
	}

	public void setPedidosInsumo(List<PedidoInsumo> pedidosInsumo) {
		this.pedidosInsumo = pedidosInsumo;
	}

	public List<Prenda> getPrendas() {
		return prendas;
	}

	public void setPrendas(List<Prenda> prendas) {
		this.prendas = prendas;
	}

	public List<Proveedor> getProveedores() {
		return proveedores;
	}

	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}

	public void setSucursales(List<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	// public void getSucursales() {
	// return sucursales;
	// }
	//
	/**************************************************************************************************************************/
	/** Clientes */
	/**
	 *************************************************************************************************************************/
	public List<Cliente> obtenerClientes() {
		List<Cliente> lista = new ArrayList<Cliente>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<Cliente> bd = ClienteDAO.getInstancia().obtenerClientes();
			// si los tengo en memoria ok, sino los agrego
			for (Cliente b : bd) {
				Cliente aux = clienteEnMemoria(b);
				if (aux == null) {
					lista.add(b);
					clientes.add(b);
				} else {
					lista.add(aux);
				}
			}
		} catch (ClienteException e) {
			e.printStackTrace();
		}
		return lista;
	}

	private Cliente clienteEnMemoria(Cliente bd) {
		for (Cliente p : clientes) {
			if ((bd.getNumeroCliente() == p.getNumeroCliente()) && (bd.getRazonSocial() == p.getRazonSocial())
					&& (bd.getCuit() == p.getCuit()) && (bd.getInscripcionAFIP() == p.getInscripcionAFIP())
					&& (bd.getDireccion() == p.getDireccion()) && (bd.getCondicionesPago() == p.getCondicionesPago())
					&& (bd.getLimiteCredito() == p.getLimiteCredito()) && (bd.getSaldo() == p.getSaldo())) {
				return p;
			}
		}
		return null;
	}

	public Cliente obtenerCliente(String cuit) {
		Cliente aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (Cliente s : clientes) {
				if (s.getCuit() == cuit) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = ClienteDAO.getInstancia().obtenerCliente(cuit);
			if (aux != null) {
				clientes.add(aux);
			}
		} catch (ClienteException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public void altaCliente(ClienteDTO dto) throws ClienteException {
		Cliente cliente = new Cliente();
		cliente.setCondicionesPago(dto.getCondicionesPago());
		cliente.setCuit(dto.getCuit());
		cliente.setDireccion(dto.getDireccion());
		cliente.setInscripcionAFIP(dto.getInscripcionAFIP());
		cliente.setLimiteCredito(dto.getLimiteCredito());
		cliente.setRazonSocial(dto.getRazonSocial());
		cliente.setSaldo(dto.getSaldo());
		ClienteDAO.getInstancia().guardarCliente(cliente);
		clientes.add(cliente);
	}

	public void eliminarCliente(String cuit) throws ClienteException {
		ClienteDAO.getInstancia().eliminarCliente(cuit);
		clientes.remove(obtenerCliente(cuit));
	}

	public void modificarCliente(ClienteDTO dto) throws ClienteException {
		clientes.remove(obtenerCliente(dto.getCuit()));
		Cliente cliente = new Cliente();
		cliente.setNumeroCliente(dto.getNumeroCliente());
		cliente.setCondicionesPago(dto.getCondicionesPago());
		cliente.setCuit(dto.getCuit());
		cliente.setDireccion(dto.getDireccion());
		cliente.setInscripcionAFIP(dto.getInscripcionAFIP());
		cliente.setLimiteCredito(dto.getLimiteCredito());
		cliente.setRazonSocial(dto.getRazonSocial());
		cliente.setSaldo(dto.getSaldo());
		ClienteDAO.getInstancia().modificarCliente(cliente);
		clientes.add(cliente);
	}

	/**************************************************************************************************************************/
	/** Prendas */
	/**
	 *************************************************************************************************************************/
	public List<Prenda> obtenerPrendas() throws PrendaException {
		List<Prenda> lista = new ArrayList<Prenda>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<Prenda> bd = PrendaDAO.getInstancia().obtenerPrendas();
			// si los tengo en memoria ok, sino los agrego
			for (Prenda b : bd) {
				Prenda aux = prendaEnMemoria(b);
				if (aux == null) {
					lista.add(b);
					prendas.add(b);
				} else {
					lista.add(aux);
				}
			}
		} catch (PrendaException e) {
			e.printStackTrace();
		}
		return lista;
	}

	private Prenda prendaEnMemoria(Prenda bd) throws PrendaException {
		for (Prenda p : prendas) {
			if ((bd.getCodigo() == p.getCodigo()) && (bd.getDescripcion() == p.getDescripcion())
					&& (bd.getMolde() == p.getMolde()) && (bd.getCostoActual() == p.getCostoActual())
					&& (bd.getPrecioVenta() == p.getPrecioVenta()) && (bd.getEstado() == p.getEstado())) {
				return p;
			}
		}
		return null;
	}

	public Prenda obtenerPrenda(int codigo) throws PrendaException {
		Prenda aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (Prenda s : prendas) {
				if (s.getCodigo() == codigo) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = PrendaDAO.getInstancia().obtenerPrenda(codigo);
			if (aux != null) {
				prendas.add(aux);
			}
		} catch (PrendaException e) {
			e.printStackTrace();
			System.out.println("Controlador General: Error b�squeda de prenda");
		}
		return aux;
	}

	public void altaPrenda(PrendaDTO dto) throws PrendaException {
		Prenda prenda = new Prenda();
		prenda.setCodigo(dto.getCodigo());
		prenda.setCostoActual(dto.getCostoActual());
		prenda.setDescripcion(dto.getDescripcion());
		prenda.setEstado(dto.getEstado());
		prenda.setMolde(dto.getMolde());
		prenda.setPrecioVenta(dto.getPrecioVenta());
		prenda.setTiempoProduccion(dto.getTiempoProduccion());
		prenda.setTemporada(dto.getTemporada());
		PrendaDAO.getInstancia().altaPrenda(prenda);
		prendas.add(prenda);
	}

	public void eliminarPrenda(int codigo) throws PrendaException {
		PrendaDAO.getInstancia().eliminarPrenda(codigo);
		prendas.remove(obtenerPrenda(codigo));
	}

	public void modificarPrenda(PrendaDTO dto) throws PrendaException {
		prendas.remove(obtenerPrenda(dto.getCodigo()));
		Prenda prenda = new Prenda();
		prenda.setCodigo(dto.getCodigo());
		prenda.setCostoActual(dto.getCostoActual());
		prenda.setDescripcion(dto.getDescripcion());
		prenda.setEstado(dto.getEstado());
		prenda.setMolde(dto.getMolde());
		prenda.setPrecioVenta(dto.getPrecioVenta());
		prenda.setTemporada(dto.getTemporada());
		prenda.setTiempoProduccion(dto.getTiempoProduccion());
		PrendaDAO.getInstancia().modificarPrenda(prenda);
		prendas.add(prenda);
	}

	/* ITEMPRENDA---------------------------------- */
	public List<ItemPrenda> obtenerItemsPrenda(int codigo) throws ItemPrendaException {
		List<ItemPrenda> lista = new ArrayList<ItemPrenda>();
		try {
			// obtengo todos los de ese estado de la base de datos
			Prenda prenda = new Prenda();
			prenda.setCodigo(codigo);
			List<ItemPrenda> bd = ItemPrendaDAO.getInstancia().obtenerItemsPrenda(prenda);
			// si los tengo en memoria ok, sino los agrego
			for (ItemPrenda i : bd) {
				ItemPrenda aux = itemPrendaEnMemoria(i);
				if (aux == null) {
					lista.add(i);
					itemsPrenda.add(i);
				} else {
					lista.add(aux);
				}
			}
		} catch (ItemPrendaException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public List<ItemPrenda> obtenerTodosItemsPrenda() throws ItemPrendaException {
		List<ItemPrenda> lista = new ArrayList<ItemPrenda>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<ItemPrenda> bd = ItemPrendaDAO.getInstancia().obtenerTodosItemsPrenda();
			// si los tengo en memoria ok, sino los agrego
			for (ItemPrenda i : bd) {
				ItemPrenda aux = itemPrendaEnMemoria(i);
				if (aux == null) {
					lista.add(i);
					itemsPrenda.add(i);
				} else {
					lista.add(aux);
				}
			}
		} catch (ItemPrendaException e) {
			e.printStackTrace();
			System.out.println("Controlador General: Error b�squeda de todos los items prenda");
		}
		return lista;
	}

	private ItemPrenda itemPrendaEnMemoria(ItemPrenda bd) throws ItemPrendaException {
		for (ItemPrenda p : itemsPrenda) {
			if (bd.getSubcodigo() == p.getSubcodigo()) {
				return p;
			}
		}
		return null;
	}

	public ItemPrenda obtenerItemPrenda(int subcodigo) throws ItemPrendaException {
		ItemPrenda aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (ItemPrenda i : itemsPrenda) {
				if (i.getSubcodigo() == subcodigo) {
					aux = i;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = ItemPrendaDAO.getInstancia().obtenerItemPrenda(subcodigo);
			if (aux != null) {
				itemsPrenda.add(aux);
			}
		} catch (ItemPrendaException e) {
			e.printStackTrace();
			System.out.println("Controlador General: Error b�squeda de item prenda");
		}
		return aux;
	}

	public void altaItemPrenda(ItemPrendaDTO dto) throws ItemPrendaException, SucursalException, UbicacionException {
		ItemPrendaDAO.getInstancia().guardarItemPrenda(new ItemPrenda(dto));
		ItemPrenda itemPrenda = ItemPrendaDAO.getInstancia().obtenerUltimoItemPrenda();
		if (Almacen.getInstancia().almacenar(itemPrenda) == true)
			System.out.println("Se pudo almacenar!");
		else
			System.out.println("Espacio insuficiente...!");
		itemsPrenda.add(new ItemPrenda(dto));
	}

	public void eliminarItemPrenda(int subcodigo) throws ItemPrendaException {
		ItemPrendaDAO.getInstancia().eliminarItemPrenda(subcodigo);
		// TODO
		// hacer que se elimine del almacen
		itemsPrenda.remove(obtenerItemPrenda(subcodigo));
	}

	public void modificarItemPrenda(ItemPrendaDTO dto) throws ItemPrendaException {
		itemsPrenda.remove(obtenerItemPrenda(dto.getSubcodigo()));
		ItemPrenda i = new ItemPrenda();
		i.setSubcodigo(dto.getSubcodigo());
		i.setColor(dto.getColor());
		i.setTalle(dto.getTalle());
		i.setCantidadStock(dto.getCantidadStock());
		i.setCantidadMinima(dto.getCantidadMinima());
		i.setCantidadProducir(dto.getCantidadProducir());
		ItemPrendaDAO.getInstancia().modificarItemPrenda(i);
		Prenda p = new Prenda();
		p.setCodigo(dto.getPrenda().getCodigo());
		p.setCostoActual(dto.getPrenda().getCostoActual());
		p.setDescripcion(dto.getPrenda().getDescripcion());
		p.setEstado(dto.getPrenda().getEstado());
		p.setMolde(dto.getPrenda().getMolde());
		p.setPrecioVenta(dto.getPrenda().getPrecioVenta());
		p.setTemporada(dto.getPrenda().getTemporada());
		p.setTiempoProduccion(dto.getPrenda().getTiempoProduccion());
		i.setPrenda(p);
		itemsPrenda.add(i);
	}

	/**************************************************************************************************************************/
	/** Pedidos Clientes */
	/**
	 *************************************************************************************************************************/
	public List<PedidoCliente> obtenerPedidosCliente() {
		List<PedidoCliente> lista = new ArrayList<PedidoCliente>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<PedidoCliente> bd = PedidoClienteDAO.getInstancia().obtenerPedidos();

			// si los tengo en memoria ok, sino los agrego
			for (PedidoCliente b : bd) {
				PedidoCliente aux = pedidoClienteEnMemoria(b);
				if (aux == null) {
					lista.add(b);
					pedidosCliente.add(b);
				} else {
					lista.add(aux);
				}
			}
		} catch (PedidoClienteException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public List<PedidoCliente> getPedidosCliente(String cuit) throws PedidoClienteException {
		List<PedidoCliente> bd = PedidoClienteDAO.getInstancia().obtenerPedidosCliente(cuit);
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		for (PedidoCliente p : bd) {
			PedidoCliente memo = pedidoClienteEnMemoria(p);
			if (memo == null) {
				pedidosCliente.add(p);
				resultado.add(p);
			} else {
				resultado.add(memo);
			}
		}
		return resultado;
	}

	public List<PedidoCliente> getPedidosClienteSucursal(int nrosucursal) {
		List<PedidoCliente> lista = new ArrayList<PedidoCliente>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<PedidoCliente> bd = PedidoClienteDAO.getInstancia().obtenerPedidosSucursal(nrosucursal);

			// si los tengo en memoria ok, sino los agrego
			for (PedidoCliente b : bd) {
				PedidoCliente aux = pedidoClienteEnMemoria(b);
				if (aux == null) {
					lista.add(b);
					pedidosCliente.add(b);
				} else {
					lista.add(aux);
				}
			}

		} catch (PedidoClienteException e) {
			e.printStackTrace();
		}
		return lista;
	}

	public List<PedidoCliente> obtenerPedidosClienteEstado(EstadoPedidoCliente estado) {
		List<PedidoCliente> lista = new ArrayList<PedidoCliente>();
		try {
			// obtengo todos los de ese estado de la base de datos
			List<PedidoCliente> bd = PedidoClienteDAO.getInstancia().obtenerPedidosEstado(estado);

			// si los tengo en memoria ok, sino los agrego
			for (PedidoCliente b : bd) {
				PedidoCliente aux = pedidoClienteEnMemoria(b);
				if (aux == null) {
					lista.add(b);
					pedidosCliente.add(b);
				} else {
					lista.add(aux);
				}
			}
		} catch (PedidoClienteException e) {
			e.printStackTrace();
		}
		return lista;
	}

	private PedidoCliente pedidoClienteEnMemoria(PedidoCliente bd) {
		for (PedidoCliente p : pedidosCliente) {
			if (bd.getNumeroPedido() == p.getNumeroPedido()) {
				return p;
			}
		}
		return null;
	}

	public PedidoCliente getPedidoCliente(int nro) {
		PedidoCliente aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (PedidoCliente s : pedidosCliente) {
				if (s != null && s.getNumeroPedido() != null && s.getNumeroPedido() == nro) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = PedidoClienteDAO.getInstancia().obtenerPedidoCliente(nro);
			if (aux != null) {
				pedidosCliente.add(aux);
			}
		} catch (PedidoClienteException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public void altaPedidoCliente(PedidoClienteDTO dto)
			throws PedidoClienteException, SucursalException, ItemPrendaException {
		PedidoCliente p = new PedidoCliente(dto);
		PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
		// pedidosCliente.add(p);
	}

	// TODO @LVOMMARO
	// public boolean procesarPedidoCliente(PedidoClienteDTO dto) throws
	// SucursalException, ItemPrendaException, PedidoClienteException,
	// ClienteException{
	public boolean procesarPedidoCliente(int numeroPedido) throws SucursalException, ItemPrendaException,
			PedidoClienteException, ClienteException, UbicacionException {
		PedidoCliente pc = new PedidoCliente(getPedidoCliente(numeroPedido));
		System.out.println("Gral: procesarPedidoCliente");
		if (Almacen.getInstancia().verificarPedido(pc)) {
			if (pc.getCliente().verificarSaldo(pc.getTotal())) {
				System.out.println("Gral: Existencias OK / Saldo OK");
				Almacen.getInstancia().procesarPedidoCliente(pc);
				Despacho.getInstancia().generarDespacho(pc);
				return true;
			} else {
				System.out.println("Gral: Fondos insuficientes");
				return false;
			}
		}
		System.out.println("Gral: No hay existencias");
		// GENERAR LA ORDEN DE PRODUCCI�N
		return false;
	}
	public void aprobarPedidoCliente(int nroPedido, EstadoPedidoCliente pendienteDeAceptacionCliente) throws PedidoClienteException {
		PedidoCliente p = getPedidoCliente(nroPedido);
		if (p != null) {
			p.setEstado(pendienteDeAceptacionCliente);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(p);
		} else {
			System.out.println("No se encontr� el pedido");
		}
	}

	public void aceptarPedidoCliente(int nroPedido, EstadoPedidoCliente aceptadoPorCliente) throws PedidoClienteException {
		PedidoCliente p = getPedidoCliente(nroPedido);
		if (p != null) {
			p.setEstado(aceptadoPorCliente);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(p);
		} else {
			System.out.println("No se encontr� el pedido");
		}
	}

	public void rechazarPedido(int nroPedido, MotivoRechazo motivo) throws PedidoClienteException {
		PedidoCliente p = getPedidoCliente(nroPedido);
		if (p != null) {
			p.setEstado(EstadoPedidoCliente.CHECKEO_RECHAZADO);
			p.setMotivo(motivo);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(p);
		} else {
			System.out.println("No se encontr� el pedido");
		}
	}

	public void cancelarPedidoCliente(int nroPedido, EstadoPedidoCliente canceladoEnProduccion) throws PedidoClienteException {
		PedidoCliente p = getPedidoCliente(nroPedido);
		if (p != null) {
			p.setEstado(canceladoEnProduccion);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(p);
		} else {
			System.out.println("No se encontr� el pedido");
		}
	}


//	public void cambiarEstadoPedidoCliente(int idPedido, EstadoPedidoCliente estado) throws PedidoClienteException,
//			SucursalException, ItemPrendaException, ClienteException, UbicacionException {
//		PedidoCliente p = getPedidoCliente(idPedido);
//		if (p == null) {
//			throw new PedidoClienteException("No se enconcuentra el pedido");
//		}
//		// si el cliente acepto el pedido se dispara la produccion
////		if (estado == EstadoPedidoCliente.ACEPTADO_POR_CLIENTE) {
//		if (estado == EstadoPedidoCliente.PENDIENTE_DE_ACEPTACION_CLIENTE) {
//			procesarPedidoCliente(idPedido);
//		}
//		p.setEstado(estado);
////		PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
//		PedidoClienteDAO.getInstancia().modificarPedidoCliente(p);
//	}

	// ALMACEN
	public void inicializarAlmacen() {
		try {
			Almacen.getInstancia().inicializar();
		} catch (UbicacionException e) {
			e.printStackTrace();
		}
	}

	/**************************************************************************************************************************/
	/** Produccion */
	/**
	 *************************************************************************************************************************/

	/**************************************************************************************************************************/
	/** Sucursales */
	/**
	 *************************************************************************************************************************/

	public List<Sucursal> getSucursales() {
		List<Sucursal> resultado = new ArrayList<Sucursal>();
		try {
			List<Sucursal> bd = SucursalDAO.getInstancia().obtenerSucursales();
			for (Sucursal p : bd) {
				Sucursal memo = sucursalEnMemoria(p);
				if (memo == null) {
					sucursales.add(p);
					resultado.add(p);
				} else {
					resultado.add(memo);
				}
			}
		} catch (SucursalException e) {
			System.out.println("Controlador General: Error b�squeda de sucursales");
			e.printStackTrace();
		}
		return resultado;
	}

	private Sucursal sucursalEnMemoria(Sucursal bd) {
		for (Sucursal p : sucursales) {
			if (bd.getNumero() == p.getNumero()) {
				return p;
			}
		}
		return null;
	}

	public Sucursal getSucursal(int nro) {
		Sucursal aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (Sucursal s : sucursales) {
				if (s.getNumero() == nro) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = SucursalDAO.getInstancia().obtenerSucursal(nro);
			if (aux != null) {
				sucursales.add(aux);
			}
		} catch (SucursalException e) {
			System.out.println("Controlador General: Error b�squeda de sucursal");
			e.printStackTrace();
		}
		return aux;
	}

	public void altaSucursal(SucursalDTO dto) throws SucursalException {
		Sucursal sucursal = new Sucursal();
		sucursal.setNumero(dto.getNumero());
		sucursal.setNombre(dto.getNombre());
		sucursal.setDireccion(dto.getDireccion());
		sucursal.setHorarios(dto.getHorarios());
		for (ClienteDTO cdto : dto.getClientes()) {
			sucursal.getClientes().add(new Cliente(cdto));
		}
		SucursalDAO.getInstancia().guardarSucursal(sucursal);
		sucursales.add(sucursal);
	}

	/**************************************************************************************************************************/
	/** ? */
	/**
	 *************************************************************************************************************************/

	public void descontarStockInsumo(int idInsumo, int cantidad) {
		for (Insumo i : insumos) {
			if (i.getCodigoInterno() == idInsumo) {
				i.setCantidadStock(i.getCantidadStock() - cantidad);
			}
		}
	}

	// Metodo para agregar d�as !!!
	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}

	public OrdenProduccion getOrdenProduccion(int lote) {
		OrdenProduccion aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (OrdenProduccion s : ordenesProduccion) {
				if (s.getLoteProduccion() == lote) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = OrdenProduccionDAO.getInstancia().obtenerOrden(lote);
			if (aux != null) {
				ordenesProduccion.add(aux);
			}
		} catch (OrdenProduccionException e) {
			e.printStackTrace();
		}

		return aux;
	}

	// INSUMOS

	public Insumo obtenerInsumo(int id) {
		Insumo aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (Insumo s : insumos) {
				if (s.getCodigoInterno() == id) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = InsumoDAO.getInstancia().obtenerInsumo(id);
			if (aux != null) {
				insumos.add(aux);
			}
		} catch (InsumoException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public Proveedor getProveedor(String rs) {
		Proveedor aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (Proveedor s : proveedores) {
				if (s.getRazonSocial() == rs) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = ProveedorDAO.getInstancia().obtenerProveedor(rs);
			if (aux != null) {
				proveedores.add(aux);
			}
		} catch (ProveedorException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public PedidoInsumo getPedidoInsumo(int lote) {
		PedidoInsumo aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (PedidoInsumo s : pedidosInsumo) {
				if (s.getLoteInsumo() == lote) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo
			// agrego a memoria
			aux = PedidoInsumoDAO.getInstancia().obtenerPedidoInsumo(lote);
			if (aux != null) {
				pedidosInsumo.add(aux);
			}
		} catch (PedidoInsumoException e) {
			e.printStackTrace();
		}
		return aux;
	}

	/**/public List<Insumo> obtenerInsumos() throws InsumoException {
		/**/ List<Insumo> lista = new ArrayList<Insumo>();
		/**/ try {
			/**/ // obtengo todos los de ese estado de la base de datos
			/**/ List<Insumo> bd = InsumoDAO.getInstancia().obtenerInsumos();
			/**/ // si los tengo en memoria ok, sino los agrego
			/**/ for (Insumo b : bd) {
				/**/ Insumo aux = insumoEnMemoria(b);
				/**/ if (aux == null) {
					/**/ lista.add(b);
					/**/ insumos.add(b);
				/**/ } else {
					/**/ lista.add(aux);
				/**/ }
			/**/ }
		/**/ } catch (InsumoException e) {
			/**/ e.printStackTrace();
		/**/ }
			return lista;
		}

	/**/private Insumo insumoEnMemoria(Insumo bd) throws InsumoException {
		/**/ for (Insumo i : insumos) {
			/**/ if (i.getCodigoInterno() == bd.getCodigoInterno()) {
				/**/ return i;
				/**/ }
			/**/ }
		/**/ return null;
		/**/ }

	public void altaInsumo(InsumoDTO dto) {
		Insumo i = new Insumo();
		try {
			i.setCodigoInterno(dto.getCodigoInterno());
			i.setNombre(dto.getNombre());
			i.setCantidadComprar(dto.getCantidadComprar());
			i.setCantidadMinima(dto.getCantidadComprar());
			i.setCantidadStock(dto.getCantidadStock());
			i.setCostoActual(dto.getCostoActual());
			InsumoDAO.getInstancia().guardarInsumo(i);
			insumos.add(i);
		} catch (InsumoException e) {
			e.printStackTrace();
		}
	}

	
	
	// ADMINISTRACION

	// DESPACHO

	// public List<UbicacionCantidad> generarDespacho(int idPedido) {
	// return Despacho.getInstancia().generarDespacho(idPedido);
	// }
	//
	// public Factura generarFactura(int nroPedido) {
	// return
	// Despacho.getInstancia().generarFactura(getPedidoCliente(nroPedido));
	// }
	//
	// public Remito generarRemito(int nroPedido) {
	// return
	// Despacho.getInstancia().generarRemito(getPedidoCliente(nroPedido));
	// }
	//

}
