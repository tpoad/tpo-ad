package remoto;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import controlador.ControladorGral;
import dto.ClienteDTO;
import dto.InsumoDTO;
import dto.ItemPrendaDTO;
import dto.OrdenProduccionDTO;
import dto.PedidoClienteDTO;
import dto.PrendaDTO;
import dto.SucursalDTO;
import enums.EstadoPedidoCliente;
import enums.MotivoRechazo;
import exceptions.ClienteException;
import exceptions.ItemPrendaException;
import exceptions.OrdenProduccionException;
import exceptions.PedidoClienteException;
import exceptions.PrendaException;
import exceptions.SucursalException;
import exceptions.UbicacionException;
import interfazRemota.InterfazRemota;
import modelo.Cliente;
import modelo.Insumo;
import modelo.ItemPrenda;
import modelo.OrdenProduccion;
import modelo.PedidoCliente;
import modelo.Prenda;
import modelo.Produccion;
import modelo.Sucursal;

public class ObjetoRemoto extends UnicastRemoteObject implements InterfazRemota {

	private static final long serialVersionUID = 1L;
	private static ObjetoRemoto instancia;
	private ControladorGral c;

	public ObjetoRemoto() throws RemoteException {
		super();
		c = ControladorGral.getInstancia();
	}

	public static ObjetoRemoto getInstancia() {
		if (instancia == null)
			try {
				instancia = new ObjetoRemoto();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		return instancia;
	}

	/**************************************************************************************************************************/
	/** Clientes */
	/**
	 *************************************************************************************************************************/
	public List<ClienteDTO> getClientes() throws RemoteException {
		List<Cliente> lista = c.obtenerClientes();
		List<ClienteDTO> resultado = new ArrayList<ClienteDTO>();
		for (Cliente c : lista) {
			resultado.add(c.toDTO());
		}
		return resultado;
	}

	public ClienteDTO obtenerCliente(String cuit) throws ClienteException {
		ClienteDTO cliente = null;
		Cliente c = ControladorGral.getInstancia().obtenerCliente(cuit);
		if (c != null) {
			cliente = c.toDTO();
		}
		return cliente;
	}

	public void altaCliente(ClienteDTO dto) throws ClienteException {
		c.altaCliente(dto);
	}

	public void eliminarCliente(String cuit) throws RemoteException {
		c.eliminarCliente(cuit);
	}

	public void modificarCliente(ClienteDTO dto) throws RemoteException {
		c.modificarCliente(dto);
	}

	/**************************************************************************************************************************/
	/** Prendas */
	/**
	 *************************************************************************************************************************/
	public List<PrendaDTO> obtenerPrendas() throws PrendaException {
		List<Prenda> lista = c.obtenerPrendas();
		List<PrendaDTO> resultado = new ArrayList<PrendaDTO>();
		for (Prenda p : lista) {
			resultado.add(p.toDTObobo());
		}
		return resultado;
	}

	public PrendaDTO getPrendaBoba(int id) throws RemoteException {
		PrendaDTO prenda = null;
		Prenda p = ControladorGral.getInstancia().obtenerPrenda(id);
		if (p != null)
			prenda = p.toDTObobo();
		return prenda;
	}

	public void altaPrenda(PrendaDTO dto) throws PrendaException {
		c.altaPrenda(dto);
	}

	public void eliminarPrenda(int id) throws RemoteException {
		c.eliminarPrenda(id);
	}

	public void modificarPrenda(PrendaDTO dto) throws RemoteException {
		c.modificarPrenda(dto);
	}

	/* ITEMPRENDA---------------------------------- */
	public List<ItemPrendaDTO> obtenerItemsPrenda(int codigo) throws RemoteException {
		List<ItemPrendaDTO> resultado = new ArrayList<ItemPrendaDTO>();
		List<ItemPrenda> lista = c.obtenerItemsPrenda(codigo);
		for (ItemPrenda i : lista) {
			resultado.add(i.toDTOsinPrendaSinInsumos());
		}
		return resultado;
	}

	public List<ItemPrendaDTO> obtenerTodosItemsPrenda() throws RemoteException {
		List<ItemPrendaDTO> resultado = new ArrayList<ItemPrendaDTO>();
		List<ItemPrenda> lista = c.obtenerTodosItemsPrenda();
		for (ItemPrenda i : lista) {
			resultado.add(i.toDTOsinPrendaSinInsumos());
		}
		return resultado;
	}

	public ItemPrendaDTO obtenerItemPrenda(int id) throws RemoteException {
		ItemPrendaDTO itemPrenda = null;
		ItemPrenda i = ControladorGral.getInstancia().obtenerItemPrenda(id);
		if (i != null)
			itemPrenda = i.toDTOsinPrendaSinInsumos();
		return itemPrenda;
	}

	public void altaItemPrenda(ItemPrendaDTO dto) throws ItemPrendaException, SucursalException, UbicacionException {
		c.altaItemPrenda(dto);
	}

	public void eliminarItemPrenda(int id) throws RemoteException {
		c.eliminarItemPrenda(id);
	}

	public void modificarItemPrenda(ItemPrendaDTO dto) throws RemoteException {
		c.modificarItemPrenda(dto);
	}

	/**************************************************************************************************************************/
	/** Pedidos Clientes */
	/**
	 *************************************************************************************************************************/
	public List<PedidoClienteDTO> obtenerPedidosCliente() {
		List<PedidoCliente> lista = c.obtenerPedidosCliente();
		List<PedidoClienteDTO> resultado = new ArrayList<PedidoClienteDTO>();
		for (PedidoCliente c : lista) {
			resultado.add(c.toDTO());
		}
		return resultado;
	}

	public List<PedidoClienteDTO> obtenerPedidosCliente(String cuit) throws PedidoClienteException {
		List<PedidoCliente> lista = c.getPedidosCliente(cuit);
		List<PedidoClienteDTO> resultado = new ArrayList<PedidoClienteDTO>();
		for (PedidoCliente c : lista) {
			resultado.add(c.toDTO());
		}
		return resultado;
	}

	public List<PedidoClienteDTO> obtenerPedidosClienteSucursal(int nrosucursal) throws RemoteException {
		List<PedidoClienteDTO> dtos = new ArrayList<PedidoClienteDTO>();
		List<PedidoCliente> pedidos = c.getPedidosClienteSucursal(nrosucursal);
		for (PedidoCliente s : pedidos) {
			dtos.add(s.toDTO());
		}
		return dtos;
	}

	public List<PedidoClienteDTO> obtenerPedidosClientePendienteDeCheckeo() throws PedidoClienteException {
		List<PedidoCliente> lista = c.obtenerPedidosClienteEstado(EstadoPedidoCliente.PENDIENTE_DE_CHECKEO);
		List<PedidoClienteDTO> resultado = new ArrayList<PedidoClienteDTO>();
		for (PedidoCliente c : lista) {
			resultado.add(c.toDTO());
		}
		return resultado;
	}

	public PedidoClienteDTO obtenerPedidoCliente(int intNroPedido) throws PedidoClienteException {
		PedidoClienteDTO pcdto = null;
		PedidoCliente pc = c.getPedidoCliente(intNroPedido);
		if (pc != null) {
			pcdto = pc.toDTO();
		}
		return pcdto;
	}

	public void altaPedidoCliente(PedidoClienteDTO pedidoCliente)
			throws PedidoClienteException, ItemPrendaException, SucursalException {
		c.altaPedidoCliente(pedidoCliente);
	}

	public void aprobarPedidoCliente(int nroPedido) throws PedidoClienteException, SucursalException,
			ItemPrendaException, ClienteException, UbicacionException {
		// c.cambiarEstadoPedidoCliente(nroPedido,
		// EstadoPedidoCliente.PENDIENTE_DE_ACEPTACION_CLIENTE);
		c.aprobarPedidoCliente(nroPedido, EstadoPedidoCliente.PENDIENTE_DE_ACEPTACION_CLIENTE);
	}

	public void aceptarPedidoCliente(int nroPedido) throws PedidoClienteException, SucursalException,
			ItemPrendaException, ClienteException, UbicacionException {
		// c.cambiarEstadoPedidoCliente(nroPedido,
		// EstadoPedidoCliente.ACEPTADO_POR_CLIENTE);
		c.aceptarPedidoCliente(nroPedido, EstadoPedidoCliente.ACEPTADO_POR_CLIENTE);
	}

	public void rechazarPedidoCliente(int nroPedido, MotivoRechazo motivo) throws PedidoClienteException,
			SucursalException, ItemPrendaException, ClienteException, UbicacionException {
		c.rechazarPedido(nroPedido, motivo);
	}

	public void cancelarPedidoCliente(int nroPedido) throws PedidoClienteException, SucursalException,
			ItemPrendaException, ClienteException, UbicacionException {
		// c.cambiarEstadoPedidoCliente(nroPedido,
		// EstadoPedidoCliente.CANCELADO_EN_PRODUCCION);
		c.cancelarPedidoCliente(nroPedido, EstadoPedidoCliente.CANCELADO_EN_PRODUCCION);
	}

	public void procesarPedidoCliente(int numeroPedido) throws SucursalException, ItemPrendaException,
			PedidoClienteException, ClienteException, UbicacionException {
		c.procesarPedidoCliente(numeroPedido);
	}

	// public void despacharPedidoCliente(int nroPedido) throws
	// PedidoClienteException, SucursalException,
	// ItemPrendaException, ClienteException, UbicacionException {
	// c.cambiarEstadoPedidoCliente(nroPedido, EstadoPedidoCliente.DESPACHADO);
	// }

	// public List<UbicacionCantidadDTO> despacharPedidoCliente(int nroPedido)
	// throws PedidoClienteException {
	// // List<UbicacionCantidad> lista = c.generarDespacho(nroPedido);
	// List<UbicacionCantidadDTO> resultado = new
	// ArrayList<UbicacionCantidadDTO>();
	// // for (UbicacionCantidad c : lista) {
	// // resultado.add(c.toDTO());
	// // }
	// return resultado;
	// }

	// public void procesarPedidoCliente(PedidoClienteDTO dto) throws
	// SucursalException, ItemPrendaException {
	// c.procesarPedidoCliente(dto);
	// }

	/**************************************************************************************************************************/
	/** Sucursales */
	/**
	 *************************************************************************************************************************/
	public List<SucursalDTO> getSucursales() throws RemoteException {
		List<SucursalDTO> dto = new ArrayList<SucursalDTO>();
		List<Sucursal> lista = c.getSucursales();
		for (Sucursal s : lista) {
			dto.add(s.toDTO());
		}
		return dto;
	}

	public SucursalDTO getSucursal(int nro) throws RemoteException {
		return c.getSucursal(nro).toDTO();
	}

	public void altaSucursal(SucursalDTO dto) throws SucursalException {
		c.altaSucursal(dto);
	}

	/**************************************************************************************************************************/
	/**
	 * PRODUCCION /**
	 *************************************************************************************************************************/
	public List<OrdenProduccionDTO> obtenerOrdenesArea(String area) throws OrdenProduccionException {
		List<OrdenProduccionDTO> dtos = new ArrayList<OrdenProduccionDTO>();
		for (OrdenProduccion o : Produccion.getInstancia().obtenerOrdenesArea(area)) {
			dtos.add(o.toDTO());
		}

		return dtos;
	}

	public void completarOrden(String area, int orden) throws RemoteException {
		Produccion.getInstancia().completarOrden(area, orden);
	}

	/**************************************************************************************************************************/
	/** ? */
	/**
	 *************************************************************************************************************************/
	/**/ public List<InsumoDTO> obtenerInsumos() throws RemoteException {
		/**/ List<Insumo> lista = c.obtenerInsumos();
		/**/
		/**/ List<InsumoDTO> resultado = new ArrayList<InsumoDTO>();
		/**/ for (Insumo i : lista) {
			/**/ resultado.add(i.toDTObobo());
			/**/ }
		/**/ return resultado;
	}

	/**/public InsumoDTO getInsumo(int id) throws RemoteException {
		/**/ InsumoDTO insumo = null;
		/**/Insumo i = ControladorGral.getInstancia().obtenerInsumo(id);
		/**/if (i != null)/**/ insumo = i.toDTObobo();
		/**/ return insumo;
	}

	/* MAL */ public List<InsumoDTO> obtenerInsumo() throws RemoteException {
		/**/ List<Insumo> lista = c.getInsumos();
		/**/
		/**/ List<InsumoDTO> resultado = new ArrayList<InsumoDTO>();
		/**/ for (Insumo i : lista) {
			/**/ resultado.add(i.toDTObobo());
			/**/ }
		/**/ return resultado;
	}

	public void altaInsumo(InsumoDTO dto) throws RemoteException {
		c.altaInsumo(dto);
	}

	// public FacturaDTO generarFactura(int nroPedido) throws FacturaException {
	// FacturaDTO dto = null;
	// return dto;
	// }
	//
	// public RemitoDTO generarRemito(int nroPedido) throws RemitoException {
	// RemitoDTO dto = null;
	// return dto;
	// }

}
