package server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import controlador.ControladorGral;
import hbt.HibernateUtil;
import interfazRemota.InterfazRemota;
import remoto.ObjetoRemoto;

public class Server {

	public Server() {
		try {
			LocateRegistry.createRegistry(1099);
			InterfazRemota objetoRemoto = new ObjetoRemoto();
			Naming.rebind("//localhost/ObjetoRemoto", objetoRemoto);
			System.out.println("Fijado en //localhost/ObjetoRemoto");
			
			HibernateUtil.getSessionFactory().openSession();
			
			//inicializar ubicaciones
			ControladorGral.getInstancia().inicializarAlmacen();
			//Produccion.getInstancia().InicializarAreasProduccion();
			
			
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		new Server();
		
	}

}
