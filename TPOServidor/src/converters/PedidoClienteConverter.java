package converters;

import java.io.Serializable;

import dto.PedidoClienteDTO;
import entities.PedidoClienteEntity;
//import entities.PedidoClienteEntity.MotivoEntity;

public class PedidoClienteConverter implements Serializable {

	private static final long serialVersionUID = 1L;

	public PedidoClienteDTO pedidoClienteToDTO(PedidoClienteEntity pedidoCliente) {
		PedidoClienteDTO pedidoClienteDTO = new PedidoClienteDTO();
		/*
		 * // pedidoClienteDTO.setCliente(ClienteConverter.clienteToDTO(
		 * pedidoCliente.getCliente()));
		 * pedidoClienteDTO.setEstado(pedidoCliente.getEstado());
		 * pedidoClienteDTO.setFechaGeneracion(pedidoCliente.getFechaGeneracion(
		 * ));
		 * pedidoClienteDTO.setFechaProbable(pedidoCliente.getFechaProbable());
		 * pedidoClienteDTO.setFechaReal(pedidoCliente.getFechaReal());
		 * List<ItemPedidoClienteDTO> listaPedidoClienteDTO = new
		 * ArrayList<ItemPedidoClienteDTO>(); ItemPedidoClienteDTO ipcDTO = new
		 * ItemPedidoClienteDTO(); /* for (ItemPedidoClienteEntity ipcE :
		 * pedidoCliente.getItemsPedidoCliente()) {
		 * ipcDTO.setCantidad(ipcE.getCantidad());
		 * ipcDTO.setItemPrenda(ItemPrendaConverter.itemPrendaToDTO(ipcE.
		 * getItemPrenda())); ipcDTO.setPrecio(ipcE.getPrecio());
		 * listaPedidoClienteDTO.add(ipcDTO); }
		 */
		/*
		 * pedidoClienteDTO.setItemsPedidoCliente(listaPedidoClienteDTO); switch
		 * (pedidoCliente.getMotivo()) { case ACEPTADO_OK:
		 * pedidoClienteDTO.setMotivo(MotivoDTO.ACEPTADO_OK); break; case
		 * RECHAZADO_CREDITO:
		 * pedidoClienteDTO.setMotivo(MotivoDTO.RECHAZADO_CREDITO); break; case
		 * RECHAZADO_DISCONTINUO:
		 * pedidoClienteDTO.setMotivo(MotivoDTO.RECHAZADO_DISCONTINUO); break; }
		 * pedidoClienteDTO.setNumeroPedido(pedidoCliente.getNumeroPedido());
		 * pedidoClienteDTO.setTotal(pedidoCliente.getTotal());
		 */
		return pedidoClienteDTO;
	}

	public PedidoClienteEntity pedidoClienteToEntity(PedidoClienteDTO pedidoCliente) {
		PedidoClienteEntity pedidoClienteEntity = new PedidoClienteEntity();

		/*
		 * //pedidoClienteEntity.setCliente(ClienteConverter.clienteToEntity(
		 * pedidoCliente.getCliente()));
		 * pedidoClienteEntity.setEstado(pedidoCliente.getEstado());
		 * pedidoClienteEntity.setFechaGeneracion(pedidoCliente.
		 * getFechaGeneracion());
		 * pedidoClienteEntity.setFechaProbable(pedidoCliente.getFechaProbable()
		 * ); pedidoClienteEntity.setFechaReal(pedidoCliente.getFechaReal());
		 * List<ItemPedidoClienteEntity> listaPedidoClienteDTO = new
		 * ArrayList<ItemPedidoClienteEntity>(); ItemPedidoClienteEntity ipcE =
		 * new ItemPedidoClienteEntity(); for (ItemPedidoClienteDTO ipcDTO :
		 * pedidoCliente.getItemsPedidoCliente()) {
		 * ipcE.setCantidad(ipcDTO.getCantidad());
		 * ipcE.setItemPrenda(ItemPrendaConverter.itemPrendaToEntity(ipcDTO.
		 * getItemPrenda())); ipcE.setPrecio(ipcDTO.getPrecio());
		 * listaPedidoClienteDTO.add(ipcE); } //
		 * pedidoClienteEntity.setItemsPedidoCliente(listaPedidoClienteDTO);
		 * switch (pedidoCliente.getMotivo()) { case ACEPTADO_OK: //
		 * pedidoClienteEntity.setMotivo(MotivoEntity.ACEPTADO_OK); break; case
		 * RECHAZADO_CREDITO: //
		 * pedidoClienteEntity.setMotivo(MotivoEntity.RECHAZADO_CREDITO); break;
		 * case RECHAZADO_DISCONTINUO: //
		 * pedidoClienteEntity.setMotivo(MotivoEntity.RECHAZADO_DISCONTINUO);
		 * break; }
		 * pedidoClienteEntity.setNumeroPedido(pedidoCliente.getNumeroPedido());
		 * pedidoClienteEntity.setTotal(pedidoCliente.getTotal());
		 */
		return pedidoClienteEntity;
	}

}
