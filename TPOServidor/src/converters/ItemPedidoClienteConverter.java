package converters;

import java.io.Serializable;

import dto.ItemPedidoClienteDTO;
import entities.ItemPedidoClienteEntity;

public class ItemPedidoClienteConverter implements Serializable {

	private static final long serialVersionUID = 1L;

	public static ItemPedidoClienteDTO itemPedidoClienteToDTO(ItemPedidoClienteEntity itemPedidoCliente) {
		ItemPedidoClienteDTO itemPedidoClienteDTO = new ItemPedidoClienteDTO();
		itemPedidoClienteDTO.setItemPrenda(ItemPrendaConverter.itemPrendaToDTO(itemPedidoCliente.getItemPrenda()));
		itemPedidoClienteDTO.setCantidad(itemPedidoCliente.getCantidad());
		itemPedidoClienteDTO.setPrecio(itemPedidoCliente.getPrecio());
		return itemPedidoClienteDTO;
	}

	public static ItemPedidoClienteEntity itemPedidoClienteToEntity(ItemPedidoClienteDTO itemPedidoCliente) {
		ItemPedidoClienteEntity itemPedidoClienteEntity = new ItemPedidoClienteEntity();
		itemPedidoClienteEntity
				.setItemPrenda(ItemPrendaConverter.itemPrendaToEntity(itemPedidoCliente.getItemPrenda()));
		itemPedidoClienteEntity.setCantidad(itemPedidoCliente.getCantidad());
		itemPedidoClienteEntity.setPrecio(itemPedidoCliente.getPrecio());
		return itemPedidoClienteEntity;
	}

}
