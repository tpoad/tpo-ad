package converters;

import java.io.Serializable;

import dto.ItemPrendaDTO;
import entities.ItemPrendaEntity;

public class ItemPrendaConverter implements Serializable {

	private static final long serialVersionUID = 1L;

	public static ItemPrendaDTO itemPrendaToDTO(ItemPrendaEntity itemPrenda) {
		ItemPrendaDTO itemPrendaDTO = new ItemPrendaDTO();
		/*
		 * itemPrendaDTO.setCantidadMinima(itemPrenda.getCantidadMinima());
		 * itemPrendaDTO.setCantidadProducir(itemPrenda.getCantidadProducir());
		 * itemPrendaDTO.setCantidadStock(itemPrenda.getCantidadStock()); switch
		 * (itemPrenda.getColor()) { case BLANCO:
		 * itemPrendaDTO.setColor(ColorDTO.BLANCO); break; case NEGRO:
		 * itemPrendaDTO.setColor(ColorDTO.NEGRO); break; case ROJO:
		 * itemPrendaDTO.setColor(ColorDTO.ROJO); break; case AZUL:
		 * itemPrendaDTO.setColor(ColorDTO.AZUL); break; case AMARILLO:
		 * itemPrendaDTO.setColor(ColorDTO.AMARILLO); break; }
		 * itemPrendaDTO.setSubcodigo(itemPrenda.getSubcodigo()); switch
		 * (itemPrenda.getTalle()) { case XS:
		 * itemPrendaDTO.setTalle(TalleDTO.XS); break; case S:
		 * itemPrendaDTO.setTalle(TalleDTO.S); break; case M:
		 * itemPrendaDTO.setTalle(TalleDTO.M); break; case L:
		 * itemPrendaDTO.setTalle(TalleDTO.L); break; case XL:
		 * itemPrendaDTO.setTalle(TalleDTO.XL); break; }
		 */
		return itemPrendaDTO;
	}

	public static ItemPrendaEntity itemPrendaToEntity(ItemPrendaDTO itemPrenda) {
		ItemPrendaEntity itemPrendaEntity = new ItemPrendaEntity();
		/*
		 * itemPrendaEntity.setCantidadMinima(itemPrenda.getCantidadMinima());
		 * itemPrendaEntity.setCantidadProducir(itemPrenda.getCantidadProducir()
		 * ); itemPrendaEntity.setCantidadStock(itemPrenda.getCantidadStock());
		 * switch (itemPrenda.getColor()) { case BLANCO:
		 * itemPrendaEntity.setColor(Color.BLANCO); break; case NEGRO:
		 * itemPrendaEntity.setColor(Color.NEGRO); break; case ROJO:
		 * itemPrendaEntity.setColor(Color.ROJO); break; case AZUL:
		 * itemPrendaEntity.setColor(Color.AZUL); break; case AMARILLO:
		 * itemPrendaEntity.setColor(Color.AMARILLO); break; }
		 * itemPrendaEntity.setSubcodigo(itemPrenda.getSubcodigo()); switch
		 * (itemPrenda.getTalle()) { case XS:
		 * itemPrendaEntity.setTalle(TalleEntity.XS); break; case S:
		 * itemPrendaEntity.setTalle(TalleEntity.S); break; case M:
		 * itemPrendaEntity.setTalle(TalleEntity.M); break; case L:
		 * itemPrendaEntity.setTalle(TalleEntity.L); break; case XL:
		 * itemPrendaEntity.setTalle(TalleEntity.XL); break; }
		 */
		return itemPrendaEntity;
	}

}
