package converters;

import java.util.List;

public class CuentaCorriente {
	private Integer numeroCliente;
	private List<Float> movimientos;
	private Float saldo;
	private Float limiteCredito;

	public CuentaCorriente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CuentaCorriente(Integer numeroCliente, List<Float> movimientos, Float saldo, Float limiteCredito) {
		super();
		this.numeroCliente = numeroCliente;
		this.movimientos = movimientos;
		this.saldo = saldo;
		this.limiteCredito = limiteCredito;
	}

	public Integer getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(Integer numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public List<Float> getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(List<Float> movimientos) {
		this.movimientos = movimientos;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public float getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(float limiteCredito) {
		this.limiteCredito = limiteCredito;
	}
}
