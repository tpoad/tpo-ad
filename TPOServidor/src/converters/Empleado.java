package converters;

public class Empleado {

	private String legajo;
	private String nombreCompleto;
	// private enum rol{ GERENTE, EMPLEADO_ADMINISTRATIVO, EMPLEADO_COMERCIAL };
	
	public Empleado(String legajo, String nombreCompleto) {
		super();
		this.legajo = legajo;
		this.nombreCompleto = nombreCompleto;
	}

	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getLegajo() {
		return legajo;
	}

	public void setLegajo(String legajo) {
		this.legajo = legajo;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
}
