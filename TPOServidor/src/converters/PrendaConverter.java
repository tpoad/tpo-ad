package converters;

import java.io.Serializable;

import dto.PrendaDTO;
import entities.PrendaEntity;

public class PrendaConverter implements Serializable {

	private static final long serialVersionUID = 1L;

	public static PrendaDTO prendaToDTO(PrendaEntity prenda) {
		PrendaDTO prendaDTO = new PrendaDTO();
		/*
		 * prendaDTO.setCodigo(prenda.getCodigo());
		 * prendaDTO.setCostoActual(prenda.getCostoActual());
		 * prendaDTO.setDescripcion(prenda.getDescripcion());
		 * prendaDTO.setEstado(prenda.getEstado()); List<ItemPrendaDTO>
		 * listItemPrendaDTO = new ArrayList<ItemPrendaDTO>(); ItemPrendaDTO
		 * ipDTO = new ItemPrendaDTO(); for (ItemPrendaEntity ipE :
		 * prenda.getItemsPrenda()) {
		 * ipDTO.setCantidadMinima(ipE.getCantidadMinima());
		 * ipDTO.setCantidadProducir(ipE.getCantidadProducir());
		 * ipDTO.setCantidadStock(ipE.getCantidadStock()); switch
		 * (ipE.getColor()) { case BLANCO: ipDTO.setColor(ColorDTO.BLANCO);
		 * break; case NEGRO: ipDTO.setColor(ColorDTO.NEGRO); break; case ROJO:
		 * ipDTO.setColor(ColorDTO.ROJO); break; case AZUL:
		 * ipDTO.setColor(ColorDTO.AZUL); break; case AMARILLO:
		 * ipDTO.setColor(ColorDTO.AMARILLO); break; }
		 * ipDTO.setSubcodigo(ipE.getSubcodigo()); switch (ipE.getTalle()) {
		 * case XS: ipDTO.setTalle(TalleDTO.XS); break; case S:
		 * ipDTO.setTalle(TalleDTO.S); break; case M:
		 * ipDTO.setTalle(TalleDTO.M); break; case L:
		 * ipDTO.setTalle(TalleDTO.L); break; case XL:
		 * ipDTO.setTalle(TalleDTO.XL); break; } listItemPrendaDTO.add(ipDTO); }
		 * prendaDTO.setItemsPrenda(listItemPrendaDTO);
		 * prendaDTO.setMolde(prenda.getMolde());
		 * prendaDTO.setPrecioVenta(prenda.getPrecioVenta()); switch
		 * (prenda.getTemporada()) { case OTONIO_INVIERNO:
		 * prendaDTO.setTemporada(TemporadaDTO.OTONIO_INVIERNO); break; case
		 * PRIMAVERA_VERANO:
		 * prendaDTO.setTemporada(TemporadaDTO.PRIMAVERA_VERANO); break; }
		 */
		return prendaDTO;
	}

	public static PrendaEntity prendaToEntity(PrendaDTO prenda) {

		PrendaEntity prendaEntity = new PrendaEntity();
		/*
		 * prendaEntity.setCodigo(prenda.getCodigo());
		 * prendaEntity.setCostoActual(prenda.getCostoActual());
		 * prendaEntity.setDescripcion(prenda.getDescripcion());
		 * prendaEntity.setEstado(prenda.getEstado()); List<ItemPrendaEntity>
		 * listItemPrendaEntity = new ArrayList<ItemPrendaEntity>();
		 * ItemPrendaEntity ipE = new ItemPrendaEntity(); for (ItemPrendaDTO
		 * ipDTO : prenda.getItemsPrenda()) {
		 * ipE.setCantidadMinima(ipDTO.getCantidadMinima());
		 * ipE.setCantidadProducir(ipDTO.getCantidadProducir());
		 * ipE.setCantidadStock(ipDTO.getCantidadStock()); switch
		 * (ipDTO.getColor()) { case BLANCO: ipE.setColor(ColorEntity.BLANCO);
		 * break; case NEGRO: ipE.setColor(ColorEntity.NEGRO); break; case ROJO:
		 * ipE.setColor(ColorEntity.ROJO); break; case AZUL:
		 * ipE.setColor(ColorEntity.AZUL); break; case AMARILLO:
		 * ipE.setColor(ColorEntity.AMARILLO); break; }
		 * ipE.setSubcodigo(ipDTO.getSubcodigo()); switch (ipDTO.getTalle()) {
		 * case XS: ipE.setTalle(TalleEntity.XS); break; case S:
		 * ipE.setTalle(TalleEntity.S); break; case M:
		 * ipE.setTalle(TalleEntity.M); break; case L:
		 * ipE.setTalle(TalleEntity.L); break; case XL:
		 * ipE.setTalle(TalleEntity.XL); break; } listItemPrendaEntity.add(ipE);
		 * } prendaEntity.setItemsPrenda(listItemPrendaEntity);
		 * prendaEntity.setMolde(prenda.getMolde());
		 * prendaEntity.setPrecioVenta(prenda.getPrecioVenta()); switch
		 * (prenda.getTemporada()) { case OTONIO_INVIERNO:
		 * prendaEntity.setTemporada(TemporadaEntity.OTONIO_INVIERNO); break;
		 * case PRIMAVERA_VERANO:
		 * prendaEntity.setTemporada(TemporadaEntity.PRIMAVERA_VERANO); break; }
		 */
		return prendaEntity;
	}
}
