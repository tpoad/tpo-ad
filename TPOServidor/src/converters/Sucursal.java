package converters;

import java.util.List;

public class Sucursal {
	
	private Integer numero;
	private String nombre; //es necesario?
	private String direccion;
	private String horarios;
	private Empleado gerente;
	private List<Empleado> empleados;
	private List<Ubicacion> ubicaciones;
	
	public Sucursal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sucursal(Integer numero, String nombre, String direccion, String horarios, Empleado gerente,
			List<Empleado> empleados, List<Ubicacion> ubicaciones) {
		super();
		this.numero = numero;
		this.nombre = nombre;
		this.direccion = direccion;
		this.horarios = horarios;
		this.gerente = gerente;
		this.empleados = empleados;
		this.ubicaciones = ubicaciones;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarios() {
		return horarios;
	}

	public void setHorarios(String horarios) {
		this.horarios = horarios;
	}

	public Empleado getGerente() {
		return gerente;
	}

	public void setGerente(Empleado gerente) {
		this.gerente = gerente;
	}

	public List<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}

	public List<Ubicacion> getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(List<Ubicacion> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}

}
