package converters;

import dto.ClienteDTO;
import entities.ClienteEntity;

public class ClienteConverter {

	public static ClienteDTO clienteToDTO(ClienteEntity cliente) {
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setCondicionesPago(cliente.getCondicionesPago());
		clienteDTO.setCuit(cliente.getCuit());
		clienteDTO.setDireccion(cliente.getDireccion());
		clienteDTO.setInscripcionAFIP(cliente.getInscripcionAFIP());
		clienteDTO.setLimiteCredito(cliente.getLimiteCredito());
		clienteDTO.setNumeroCliente(cliente.getNumeroCliente());
		clienteDTO.setRazonSocial(cliente.getRazonSocial());
		clienteDTO.setSaldo(cliente.getSaldo());
		return clienteDTO;
	}

	public static ClienteEntity clienteToEntity(ClienteDTO cliente) {
		ClienteEntity clienteEntity = new ClienteEntity();
		clienteEntity.setCondicionesPago(cliente.getCondicionesPago());
		clienteEntity.setCuit(cliente.getCuit());
		clienteEntity.setDireccion(cliente.getDireccion());
		clienteEntity.setInscripcionAFIP(cliente.getInscripcionAFIP());
		clienteEntity.setLimiteCredito(cliente.getLimiteCredito());
		clienteEntity.setNumeroCliente(cliente.getNumeroCliente());
		clienteEntity.setRazonSocial(cliente.getRazonSocial());
		clienteEntity.setSaldo(cliente.getSaldo());
		return clienteEntity;
	}

}
