package converters;

public class Ubicacion {

	private String calle;
	private Integer bloque;
	private Integer estante;
	private Integer posicion;
	private boolean estado;
	
	public Ubicacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Ubicacion(String calle, Integer bloque, Integer estante, Integer posicion, boolean estado) {
		super();
		this.calle = calle;
		this.bloque = bloque;
		this.estante = estante;
		this.posicion = posicion;
		this.estado = estado;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getBloque() {
		return bloque;
	}

	public void setBloque(Integer bloque) {
		this.bloque = bloque;
	}

	public Integer getEstante() {
		return estante;
	}

	public void setEstante(Integer estante) {
		this.estante = estante;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
