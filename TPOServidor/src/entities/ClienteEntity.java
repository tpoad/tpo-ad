package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author lvommaro Se debe realizar el mapeo de la clase hacia la tabla
 *         correspondiente
 */

@Entity
@Table(name = "Clientes")
public class ClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer numeroCliente;
	private String razonSocial;
	private String cuit;
	private String inscripcionAFIP;
	private String direccion;
	@OneToMany(cascade={CascadeType.PERSIST, CascadeType.MERGE})
//	@LazyCollection(LazyCollectionOption.FALSE)
	private List<MovimientoEntity> cuentaCorriente;
	private Float saldo;
	private Float limiteCredito;
	private String condicionesPago;

	public ClienteEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * public ClienteEntity(Integer numeroCliente, String razonSocial, String
	 * cuit, String inscripcionAFIP, String direccion, Float saldo, Float
	 * limiteCredito, String condicionesPago) { super(); this.numeroCliente =
	 * numeroCliente; this.razonSocial = razonSocial; this.cuit = cuit;
	 * this.inscripcionAFIP = inscripcionAFIP; this.direccion = direccion;
	 * this.saldo = saldo; this.limiteCredito = limiteCredito;
	 * this.condicionesPago = condicionesPago; }
	 */

	public Integer getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(Integer numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getInscripcionAFIP() {
		return inscripcionAFIP;
	}

	public void setInscripcionAFIP(String inscripcionAFIP) {
		this.inscripcionAFIP = inscripcionAFIP;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Float getSaldo() {
		return saldo;
	}

	public void setSaldo(Float saldo) {
		this.saldo = saldo;
	}

	public Float getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Float limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getCondicionesPago() {
		return condicionesPago;
	}

	public void setCondicionesPago(String condicionesPago) {
		this.condicionesPago = condicionesPago;
	}

	public List<MovimientoEntity> getCuentaCorriente() {
		return cuentaCorriente;
	}

	public void setCuentaCorriente(List<MovimientoEntity> cuentaCorriente) {
		this.cuentaCorriente = cuentaCorriente;
	}

	public void addMovimiento(MovimientoEntity m) {
		cuentaCorriente.add(m);
	}

}
