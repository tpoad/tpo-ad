package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Remitos")
public class RemitoEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int numero;
	@ManyToOne
	@PrimaryKeyJoinColumn
	private ClienteEntity Cliente;
	private Date fecha;
	@OneToMany(cascade = CascadeType.ALL)
	private List<ItemRemitoEntity> itemsRemito;
	private String direccionEntrega;

	public RemitoEntity() {
		super();
		itemsRemito = new ArrayList<ItemRemitoEntity>();
	}

	public RemitoEntity(ClienteEntity cliente, Date fecha, List<ItemRemitoEntity> itemsRemito, String direccionEntrega) {
		super();
		Cliente = cliente;
		this.fecha = fecha;
		this.itemsRemito = itemsRemito;
		this.direccionEntrega = direccionEntrega;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public ClienteEntity getCliente() {
		return Cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		Cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<ItemRemitoEntity> getItemsRemito() {
		return itemsRemito;
	}

	public void setItemsRemito(List<ItemRemitoEntity> itemsRemito) {
		this.itemsRemito = itemsRemito;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getDireccionEntrega() {
		return direccionEntrega;
	}

	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}

}
