package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "ItemsFacturas")
public class ItemFacturaEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private ItemPrendaEntity itemPrenda;
	private Integer cantidad;
	private float precioUnitario;

	public ItemFacturaEntity() {
		super();
	}

	public ItemFacturaEntity(ItemPrendaEntity itemPrenda, Integer cantidad, float precioUnitario) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precioUnitario = precioUnitario;
	}

	public ItemPrendaEntity getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaEntity itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public float getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(float precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
