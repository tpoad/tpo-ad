package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import enums.MovimientoTipo;

@Entity
@Table(name="Movimientos")
public class MovimientoEntity implements Serializable
{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
    private ClienteEntity cliente;
	@Enumerated(EnumType.STRING)
	private MovimientoTipo concepto ;
	private float importe;
	private Date fecha;
	
	public MovimientoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovimientoEntity(MovimientoTipo concepto, float importe, Date fecha) {
		super();
		this.concepto = concepto;
		this.importe = importe;
		this.fecha = fecha;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MovimientoTipo getConcepto() {
		return concepto;
	}

	public void setConcepto(MovimientoTipo concepto) {
		this.concepto = concepto;
	}
	
}
