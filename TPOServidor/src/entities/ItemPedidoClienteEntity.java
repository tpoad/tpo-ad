package entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author lvommaro Se debe realizar el mapeo de la clase hacia la tabla
 *         correspondiente
 */

@Entity
@Table(name = "ItemsPedidosCliente")
public class ItemPedidoClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(cascade=CascadeType.ALL)
	private ItemPrendaEntity itemPrenda;
	private Integer cantidad;
	private Float precio;

	public ItemPedidoClienteEntity() {
		super();
	}

	public ItemPedidoClienteEntity(ItemPrendaEntity itemPrenda, Integer cantidad, Float precio) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public ItemPrendaEntity getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaEntity itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

}
