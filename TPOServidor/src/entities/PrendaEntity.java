package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import enums.Temporada;

@Entity
@Table(name = "Prendas")
public class PrendaEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codigo;
	private String descripcion;
	@Enumerated(EnumType.STRING)
	private Temporada temporada;
	private String molde;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "prenda")
	private List<ItemPrendaEntity> itemsPrenda;
	private Integer estado;
	@OneToMany
	private List<PasoProduccionEntity> pasosPrSoduccion;
	private Float costoActual;
	private Float precioVenta;
	private Date tiempoProduccion;

	public PrendaEntity() {
		itemsPrenda = new ArrayList<ItemPrendaEntity>();
		pasosPrSoduccion = new ArrayList<PasoProduccionEntity>();
	}

	public PrendaEntity(int codigo, String descripcion, Temporada temporada, String molde,
			List<ItemPrendaEntity> itemsPrenda, Integer estado, Float costoActual, Float precioVenta, Date tiempoProduccion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.temporada = temporada;
		this.molde = molde;
		this.itemsPrenda = itemsPrenda;
		this.estado = estado;
		this.costoActual = costoActual;
		this.precioVenta = precioVenta;
		this.tiempoProduccion = tiempoProduccion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Temporada getTemporada() {
		return temporada;
	}

	public void setTemporada(Temporada temporada) {
		this.temporada = temporada;
	}

	public String getMolde() {
		return molde;
	}

	public void setMolde(String molde) {
		this.molde = molde;
	}

	public List<ItemPrendaEntity> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrendaEntity> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public List<PasoProduccionEntity> getPasosPrSoduccion() {
		return pasosPrSoduccion;
	}

	public void setPasosPrSoduccion(List<PasoProduccionEntity> pasosPrSoduccion) {
		this.pasosPrSoduccion = pasosPrSoduccion;
	}

	public Float getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(Float costoActual) {
		this.costoActual = costoActual;
	}

	public Float getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Float precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Date getTiempoProduccion() {
		return tiempoProduccion;
	}

	public void setTiempoProduccion(Date tiempoProduccion) {
		this.tiempoProduccion = tiempoProduccion;
	}

}
