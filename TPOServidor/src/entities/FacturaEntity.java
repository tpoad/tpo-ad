package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Facturas")
public class FacturaEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int numero;
	@ManyToOne
	@PrimaryKeyJoinColumn
	private ClienteEntity Cliente;
	private Date fecha;
	@OneToMany(cascade = CascadeType.ALL)
	private List<ItemFacturaEntity> itemsFactura;
	private float precioTotal;

	public FacturaEntity() {
		super();
		itemsFactura = new ArrayList<ItemFacturaEntity>();
	}

	public FacturaEntity(ClienteEntity cliente, Date fecha, List<ItemFacturaEntity> itemsFactura, float precioTotal) {
		super();
		Cliente = cliente;
		this.fecha = fecha;
		this.itemsFactura = itemsFactura;
		this.precioTotal = precioTotal;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public ClienteEntity getCliente() {
		return Cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		Cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<ItemFacturaEntity> getItemsFactura() {
		return itemsFactura;
	}

	public void setItemsFactura(List<ItemFacturaEntity> itemsFactura) {
		this.itemsFactura = itemsFactura;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public float getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(float precioTotal) {
		this.precioTotal = precioTotal;
	}

}
