package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import enums.Color;
import enums.Talle;

@Entity
@Table(name="ItemsPrendas")
public class ItemPrendaEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int subcodigo;
	@Enumerated(EnumType.STRING)
	private Color color;
	@Enumerated(EnumType.STRING)
	private Talle talle;
	private Integer cantidadStock;
	private Integer cantidadMinima;
	private Integer cantidadProducir;
	@OneToMany
	private List<InsumoUsadoEntity> insumosUsados;
	@ManyToOne(cascade=CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private PrendaEntity prenda;

	public PrendaEntity getPrenda() {
		return prenda;
	}

	public void setPrenda(PrendaEntity prenda) {
		this.prenda = prenda;
	}

	public ItemPrendaEntity() {
		super();
	}

	public ItemPrendaEntity(int subcodigo, Color color, Talle talle, Integer cantidadStock,
			Integer cantidadMinima, Integer cantidadProducir) {
		super();
		this.subcodigo = subcodigo;
		this.color = color;
		this.talle = talle;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadProducir = cantidadProducir;
	}

	public ItemPrendaEntity(int subcodigo, Color color, Talle talle, Integer cantidadStock,
			Integer cantidadMinima, Integer cantidadProducir, PrendaEntity prenda) {
		super();
		this.subcodigo = subcodigo;
		this.color = color;
		this.talle = talle;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadProducir = cantidadProducir;
		this.prenda = prenda;
	}

	public int getSubcodigo() {
		return subcodigo;
	}

	public void setSubcodigo(int subcodigo) {
		this.subcodigo = subcodigo;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Talle getTalle() {
		return talle;
	}

	public void setTalle(Talle talle) {
		this.talle = talle;
	}

	public Integer getCantidadStock() {
		return cantidadStock;
	}

	public void setCantidadStock(Integer cantidadStock) {
		this.cantidadStock = cantidadStock;
	}

	public Integer getCantidadMinima() {
		return cantidadMinima;
	}

	public void setCantidadMinima(Integer cantidadMinima) {
		this.cantidadMinima = cantidadMinima;
	}

	public Integer getCantidadProducir() {
		return cantidadProducir;
	}

	public void setCantidadProducir(Integer cantidadProducir) {
		this.cantidadProducir = cantidadProducir;
	}

	public List<InsumoUsadoEntity> getInsumosUsados() {
		return insumosUsados;
	}

	public void setInsumosUsados(List<InsumoUsadoEntity> insumosUsados) {
		this.insumosUsados = insumosUsados;
	}

}
