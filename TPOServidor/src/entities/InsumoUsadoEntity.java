package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="InsumosUsados")
public class InsumoUsadoEntity  implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne
	private InsumoEntity insumo;
	private float cantidad;
	
	public InsumoUsadoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InsumoUsadoEntity(InsumoEntity insumo, float cantidad) {
		super();
		this.insumo = insumo;
		this.cantidad = cantidad;
	}

	public InsumoEntity getInsumo() {
		return insumo;
	}

	public void setInsumo(InsumoEntity insumo) {
		this.insumo = insumo;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
