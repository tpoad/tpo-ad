package entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Parcial")
public class OrdenProduccionParcialEntity extends OrdenProduccionEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public OrdenProduccionParcialEntity(List<ItemPrendaEntity> lista) {
		super();
		itemsPrenda = lista;
	}

	/*
	 * public OrdenProduccionParcialEntity(String loteProduccion, boolean
	 * tipoOrdenProduccion, Date fechaGeneracion, PedidoClienteDTO
	 * pedidoCliente, float costoTotal) { super(loteProduccion,
	 * tipoOrdenProduccion, fechaGeneracion, pedidoCliente, costoTotal);
	 * Auto-generated constructor stub }
	 */

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
