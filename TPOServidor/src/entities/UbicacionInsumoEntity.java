package entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("Insumo")
public class UbicacionInsumoEntity extends UbicacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
//	@ManyToOne(optional = true)
	@ManyToOne
	@JoinColumn(nullable = true)
	private InsumoEntity insumo;

	public UbicacionInsumoEntity() {
		super();
	}

	public InsumoEntity getInsumo() {
		return insumo;
	}

	public void setInsumo(InsumoEntity insumo) {
		this.insumo = insumo;
	}

}
