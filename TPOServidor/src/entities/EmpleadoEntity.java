package entities;

import java.io.Serializable;

import javax.persistence.*;

import enums.EmpleadoRol;

@Entity
@Table(name="Empleados")
public class EmpleadoEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int legajo;
	private String nombreCompleto;
	@Enumerated(EnumType.STRING)
	private EmpleadoRol rol;
	
	public EmpleadoEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	
	public EmpleadoEntity(int legajo, String nombreCompleto, EmpleadoRol rol) {
		super();
		this.legajo = legajo;
		this.nombreCompleto = nombreCompleto;
		this.rol = rol;
	}



	public int getLegajo() {
		return legajo;
	}
	
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public EmpleadoRol getRol() {
		return rol;
	}



	public void setRol(EmpleadoRol rol) {
		this.rol = rol;
	}

	
}
