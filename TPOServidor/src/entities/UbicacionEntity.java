package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "Ubicaciones")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipoUbicacion", discriminatorType = DiscriminatorType.STRING)
public class UbicacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@EmbeddedId
	protected UbicacionKey ubicacion;
	protected int cantidad;

	public UbicacionEntity() {
		super();
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public UbicacionKey getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(UbicacionKey ubicacion) {
		this.ubicacion = ubicacion;
	}

}
