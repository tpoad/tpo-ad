package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="Sucursales")
public class SucursalEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numero;
	private String nombre;
	private String direccion;
	private String horarios;
	@OneToMany
	private List<EmpleadoEntity> empleados;
	@OneToMany
	private List<ClienteEntity> clientes;
	
	public SucursalEntity() {
		super();
		empleados = new ArrayList<EmpleadoEntity>();
		clientes = new ArrayList<ClienteEntity>();
	}

	public SucursalEntity(Integer numero, String nombre, String direccion, String horarios, List<EmpleadoEntity> empleados,
			List<ClienteEntity> clientes) {
		super();
		this.numero = numero;
		this.nombre = nombre;
		this.direccion = direccion;
		this.horarios = horarios;
		this.empleados = empleados;
		this.clientes = clientes;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarios() {
		return horarios;
	}

	public void setHorarios(String horarios) {
		this.horarios = horarios;
	}

	public List<EmpleadoEntity> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<EmpleadoEntity> empleados) {
		this.empleados = empleados;
	}

	public List<ClienteEntity> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteEntity> clientes) {
		this.clientes = clientes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	


}
