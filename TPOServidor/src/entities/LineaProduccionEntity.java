package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Lineas_Produccion")
public class LineaProduccionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private String nombre;
	private boolean estado;
	@ManyToOne
	private ItemPrendaEntity itemPrenda;
	
	public LineaProduccionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LineaProduccionEntity(String nombre, boolean estado, ItemPrendaEntity itemPrenda) {
		super();
		this.nombre = nombre;
		this.estado = estado;
		this.itemPrenda = itemPrenda;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public ItemPrendaEntity getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaEntity itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
