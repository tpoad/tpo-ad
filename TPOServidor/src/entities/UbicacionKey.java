package entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Embeddable;

@Embeddable
public class UbicacionKey implements Serializable {

	private static final long serialVersionUID = 1L;
	private String calle;
	private int bloque;
	private int estante;
	private int posicion;

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getBloque() {
		return bloque;
	}

	public void setBloque(int bloque) {
		this.bloque = bloque;
	}

	public int getEstante() {
		return estante;
	}

	public void setEstante(int estante) {
		this.estante = estante;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof UbicacionKey)) {
			return false;
		}
		UbicacionKey aux = (UbicacionKey) o;
		return Objects.equals(calle, aux.calle) 
				&& bloque == aux.bloque 
				&& estante == aux.estante
				&& posicion == aux.posicion;
	}

	public int hashCode() {
		return Objects.hash(calle, bloque, estante, posicion);
	}

}
