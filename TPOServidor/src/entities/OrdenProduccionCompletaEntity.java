package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@DiscriminatorValue("Completa")
public class OrdenProduccionCompletaEntity extends OrdenProduccionEntity implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public OrdenProduccionCompletaEntity(){
		super();
	}
	
	public OrdenProduccionCompletaEntity(PrendaEntity p){
		super();
	}
}
