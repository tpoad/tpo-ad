package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import enums.EstadoOrdenProduccion;

@Entity
@Table(name="Ordenes_Produccion")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name="tipoOrden",
    discriminatorType=DiscriminatorType.STRING
)
public class OrdenProduccionEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int lote;
	private Date fechaGeneracion;
	@ManyToOne(cascade=CascadeType.ALL)
	//@JoinColumn(name="pedidocliente_fk")
	private PedidoClienteEntity pedidocliente;
	private float costoTotal;
	private EstadoOrdenProduccion estado;
	@OneToMany(cascade = CascadeType.MERGE)
    @JoinTable(name="OrdenParcial_ItemsPrenda",
        joinColumns = @JoinColumn(name="OrdenProduccionID"),
        inverseJoinColumns = @JoinColumn(name="ItemPrendaID"))
	protected List<ItemPrendaEntity> itemsPrenda;
	
	
	public EstadoOrdenProduccion getEstado() {
		return estado;
	}



	public void setEstado(EstadoOrdenProduccion estado) {
		this.estado = estado;
	}



	public OrdenProduccionEntity() {
		super();
		// TODO Auto-generated constructor stub
		itemsPrenda = new ArrayList<ItemPrendaEntity>();
	}



	/*public OrdenProduccionEntity(String loteProduccion, Date fechaGeneracion,
			PedidoClienteEntity pedidoCliente, float costoTotal) {
		super();
		this.loteProduccion = loteProduccion;
		this.fechaGeneracion = fechaGeneracion;
		this.setPedidoCliente(pedidoCliente);
		this.costoTotal = costoTotal;
	}*/



	public int getLote() {
		return lote;
	}

	public void setLote(int loteProduccion) {
		this.lote = loteProduccion;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}


	public float getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(float costoTotal) {
		this.costoTotal = costoTotal;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public PedidoClienteEntity getPedidoCliente() {
		return pedidocliente;
	}


	public void setPedidoCliente(PedidoClienteEntity pedidoCliente) {
		this.pedidocliente = pedidoCliente;
	}

	
}
