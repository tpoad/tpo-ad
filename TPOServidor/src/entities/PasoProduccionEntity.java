package entities;

import java.io.Serializable;
import java.sql.Time;

import javax.persistence.*;

@Entity
@Table(name="PasosProduccion")
public class PasoProduccionEntity implements Serializable{

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private AreaProduccionEntity areaProduccion;
	private Time tiempoConsumido;
	
	public PasoProduccionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PasoProduccionEntity(AreaProduccionEntity areaProduccion, Time tiempoConsumido) {
		super();
		this.areaProduccion = areaProduccion;
		this.tiempoConsumido = tiempoConsumido;
	}

	public AreaProduccionEntity getAreaProduccion() {
		return areaProduccion;
	}

	public void setAreaProduccion(AreaProduccionEntity areaProduccion) {
		this.areaProduccion = areaProduccion;
	}

	public Time getTiempoConsumido() {
		return tiempoConsumido;
	}

	public void setTiempoConsumido(Time tiempoConsumido) {
		this.tiempoConsumido = tiempoConsumido;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	 

}
