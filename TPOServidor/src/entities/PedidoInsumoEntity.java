package entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import enums.EstadoPedidoInsumo;

@Entity
@Table(name = "PedidosInsumos")
public class PedidoInsumoEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int lote;
	@OneToOne
	private ProveedorEntity proveedor;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	private float total;
	@ManyToOne
	private OrdenProduccionEntity ordenProduccion;

	public OrdenProduccionEntity getOrdenProduccion() {
		return ordenProduccion;
	}

	public void setOrdenProduccion(OrdenProduccionEntity ordenProduccion) {
		this.ordenProduccion = ordenProduccion;
	}

	private EstadoPedidoInsumo estado;

	@ManyToOne
	private InsumoEntity insumo;

	public InsumoEntity getInsumo() {
		return insumo;
	}

	public void setInsumo(InsumoEntity insumo) {
		this.insumo = insumo;
	}

	public PedidoInsumoEntity() {
		super();
	}

	public PedidoInsumoEntity(int loteInsumo, ProveedorEntity proveedor, Date fechaGeneracion, Date fechaProbable,
			Date fechaReal, float total, OrdenProduccionEntity ordenProduccion, EstadoPedidoInsumo estado) {
		super();
		this.lote = loteInsumo;
		this.proveedor = proveedor;
		this.fechaGeneracion = fechaGeneracion;
		this.fechaProbable = fechaProbable;
		this.fechaReal = fechaReal;
		this.setTotal(total);
		this.ordenProduccion = ordenProduccion;
		this.setEstado(estado);
	}

	public int getLote() {
		return lote;
	}

	public void setLoteInsumo(int loteInsumo) {
		this.lote = loteInsumo;
	}

	public ProveedorEntity getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorEntity proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public EstadoPedidoInsumo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoInsumo estado) {
		this.estado = estado;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

}
