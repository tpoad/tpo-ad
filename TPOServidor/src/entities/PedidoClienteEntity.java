package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import enums.EstadoPedidoCliente;
import enums.MotivoRechazo;

@Entity
@Table(name = "PedidosCliente")

public class PedidoClienteEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer numeroPedido;
	@ManyToOne(cascade=CascadeType.ALL)
	private ClienteEntity cliente;
	@ManyToOne(cascade=CascadeType.ALL)
	private SucursalEntity Sucursal;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;
	@OneToMany(cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ItemPedidoClienteEntity> itemsPedidoCliente;
	private Float total;
	@Enumerated(EnumType.STRING)
	private EstadoPedidoCliente estado;
	@Enumerated(EnumType.STRING)
	private MotivoRechazo motivo;
	@OneToMany(cascade=CascadeType.ALL, mappedBy = "pedidocliente")
//	@LazyCollection(LazyCollectionOption.FALSE)
	private List<OrdenProduccionEntity> ordenesDeProduccion;

	public PedidoClienteEntity() {
		super();
		itemsPedidoCliente = new ArrayList<ItemPedidoClienteEntity>();
		ordenesDeProduccion = new ArrayList<OrdenProduccionEntity>();
	}

	public PedidoClienteEntity(Integer numeroPedido, ClienteEntity cliente, SucursalEntity sucursal,
			Date fechaGeneracion, Date fechaProbable, Date fechaReal, List<ItemPedidoClienteEntity> itemsPedidoCliente,
			Float total, EstadoPedidoCliente estado, MotivoRechazo motivo,
			List<OrdenProduccionEntity> ordenesDeProduccion) {
		super();
		this.numeroPedido = numeroPedido;
		this.cliente = cliente;
		Sucursal = sucursal;
		this.fechaGeneracion = fechaGeneracion;
		this.fechaProbable = fechaProbable;
		this.fechaReal = fechaReal;
		this.itemsPedidoCliente = itemsPedidoCliente;
		this.total = total;
		this.estado = estado;
		this.motivo = motivo;
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

	public Integer getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(Integer numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public List<ItemPedidoClienteEntity> getItemsPedidoCliente() {
		return itemsPedidoCliente;
	}

	public void setItemsPedidoCliente(List<ItemPedidoClienteEntity> itemsPedidoCliente) {
		this.itemsPedidoCliente = itemsPedidoCliente;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public EstadoPedidoCliente getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoCliente estado) {
		this.estado = estado;
	}

	public MotivoRechazo getMotivo() {
		return motivo;
	}

	public void setMotivo(MotivoRechazo motivo) {
		this.motivo = motivo;
	}

	public SucursalEntity getSucursal() {
		return Sucursal;
	}

	public void setSucursal(SucursalEntity sucursal) {
		Sucursal = sucursal;
	}

	public List<OrdenProduccionEntity> getOrdenesDeProduccion() {
		return ordenesDeProduccion;
	}

	public void setOrdenesDeProduccion(List<OrdenProduccionEntity> ordenesDeProduccion) {
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

}