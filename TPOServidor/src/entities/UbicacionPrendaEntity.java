package entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue("Prenda")
public class UbicacionPrendaEntity extends UbicacionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
//	@ManyToOne(optional = true)
	@ManyToOne
	@JoinColumn(nullable = true)
	private ItemPrendaEntity item;

	public UbicacionPrendaEntity() {
		super();
	}

	public ItemPrendaEntity getItem() {
		return item;
	}

	public void setItem(ItemPrendaEntity item) {
		this.item = item;
	}

}
