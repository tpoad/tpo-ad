package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Areas_De_Produccion")
public class AreaProduccionEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	private String nombre;

	@OneToMany
	private List<LineaProduccionEntity> lineasProduccion;
	@OneToMany
	private List<OrdenProduccionEntity> ordenesPendientes;
	@OneToMany
	private List<ItemPrendaEntity> itemPrendaTerminados;

	public AreaProduccionEntity() {
		super();
		lineasProduccion = new ArrayList<LineaProduccionEntity>();
		ordenesPendientes = new ArrayList<OrdenProduccionEntity>();
		itemPrendaTerminados = new ArrayList<ItemPrendaEntity>();
	}

	public AreaProduccionEntity(String nombre, List<LineaProduccionEntity> lineasProduccion,
			List<OrdenProduccionEntity> ordenesPendiente) {
		super();
		this.nombre = nombre;
		this.lineasProduccion = lineasProduccion;
		ordenesPendientes = ordenesPendiente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<LineaProduccionEntity> getLineasProduccion() {
		return lineasProduccion;
	}

	public void setLineasProduccion(List<LineaProduccionEntity> lineasProduccion) {
		this.lineasProduccion = lineasProduccion;
	}

	public List<OrdenProduccionEntity> getOrdenesPendiente() {
		return ordenesPendientes;
	}

	public void setOrdenesPendiente(List<OrdenProduccionEntity> ordenesPendiente) {
		ordenesPendientes = ordenesPendiente;
	}

	public List<ItemPrendaEntity> getItemPrendaTerminados() {
		return itemPrendaTerminados;
	}

	public void setItemPrendaTerminados(List<ItemPrendaEntity> itemPrendaTerminados) {
		this.itemPrendaTerminados = itemPrendaTerminados;
	}

}
