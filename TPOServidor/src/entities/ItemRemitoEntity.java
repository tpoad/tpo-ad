package entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "ItemsRemitos")
public class ItemRemitoEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@ManyToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private ItemPrendaEntity itemPrenda;
	private Integer cantidad;

	public ItemRemitoEntity() {
		super();
	}

	public ItemRemitoEntity(ItemPrendaEntity itemPrenda, Integer cantidad) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
	}

	public ItemPrendaEntity getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaEntity itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
