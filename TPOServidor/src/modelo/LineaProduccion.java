package modelo;

import controlador.ControladorGral;
import dto.LineaProduccionDTO;
import entities.LineaProduccionEntity;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class LineaProduccion {
	
	private String nombre;
	private boolean estado; // se puede sacar
	private ItemPrenda itemPrenda;
	private AreaProduccion areaProduccion;

	public LineaProduccion() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public AreaProduccion getAreaProduccion() {
		return areaProduccion;
	}

	public void setAreaProduccion(AreaProduccion areaProduccion) {
		this.areaProduccion = areaProduccion;
	}

	public ItemPrenda getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrenda itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public LineaProduccion(LineaProduccionEntity e, AreaProduccion a) {
		nombre = e.getNombre();
		estado = e.getEstado();
		areaProduccion = a;
		try {
			itemPrenda = ControladorGral.getInstancia().obtenerItemPrenda(e.getItemPrenda().getSubcodigo());
		} catch (ItemPrendaException e1) {
			e1.printStackTrace();
			System.out.println("LineaProduccion: error en itemPrenda");
		}
	}

	public LineaProduccionEntity toEntity() {
		LineaProduccionEntity e = new LineaProduccionEntity();
		e.setNombre(nombre);
		e.setEstado(estado);
		if (itemPrenda != null)
			e.setItemPrenda(itemPrenda.toEntity());
		return e;
	}

	public LineaProduccionDTO toDTO() {
		LineaProduccionDTO l = new LineaProduccionDTO();
		l.setNombre(nombre);
		l.setEstado(estado);
		l.setAreaProduccion(areaProduccion.toDTO());
		return l;
	}

	public LineaProduccion(LineaProduccionDTO dto) throws SucursalException, ItemPrendaException {
		nombre = dto.getNombre();
		estado = dto.isEstado();
		itemPrenda = new ItemPrenda(dto.getItemPrenda());
		areaProduccion = new AreaProduccion(dto.getAreaProduccion());
	}

}
