package modelo;

import java.util.Date;

import controlador.ControladorGral;
import dto.PedidoInsumoDTO;
import entities.PedidoInsumoEntity;
import enums.EstadoPedidoInsumo;

public class PedidoInsumo
{
	private int lote;
	private Proveedor proveedor;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;
	private float total;
	private OrdenProduccion ordenProduccion;
	private EstadoPedidoInsumo estado;
	
	public PedidoInsumo() {
		
	}


	public int getLoteInsumo() {
		return lote;
	}

	public void setLoteInsumo(int lote) {
		this.lote = lote;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public OrdenProduccion getOrdenProduccion() {
		return ordenProduccion;
	}

	public void setOrdenProduccion(OrdenProduccion ordenProduccion) {
		this.ordenProduccion = ordenProduccion;
	}

	public EstadoPedidoInsumo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoInsumo estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "PedidoInsumoDTO [loteInsumo=" + lote + ", proveedor=" + proveedor + ", fechaGeneracion="
				+ fechaGeneracion + ", fechaProbable=" + fechaProbable + ", fechaReal=" + fechaReal + ", total=" + total
				+ ", ordenProduccion=" + ordenProduccion + ", estado=" + estado + "]";
	}
	
	public PedidoInsumo(PedidoInsumoEntity e){
		lote = e.getLote();
		proveedor = ControladorGral.getInstancia().getProveedor(e.getProveedor().getRazonSocial());
		fechaGeneracion = e.getFechaGeneracion();
		fechaProbable = e.getFechaProbable();
		fechaReal = e.getFechaProbable();
		total = e.getTotal();
		ordenProduccion  = ControladorGral.getInstancia().getOrdenProduccion(e.getOrdenProduccion().getLote());
		estado = e.getEstado();
		proveedor = ControladorGral.getInstancia().getProveedor(e.getProveedor().getRazonSocial());
	}
	
	public PedidoInsumoEntity toEntity(){
		PedidoInsumoEntity e = new PedidoInsumoEntity();
		e.setLoteInsumo(lote);
		e.setProveedor(proveedor.toEntity());
		e.setFechaGeneracion(fechaGeneracion);
		e.setFechaProbable(fechaProbable);
		e.setFechaReal(fechaReal);
		e.setTotal(total);
		e.setOrdenProduccion(ordenProduccion.toEntity());
		e.setEstado(estado);
		e.setProveedor(proveedor.toEntity());
		return e;
	}
	
	public PedidoInsumoDTO toDTO()
	{
		PedidoInsumoDTO p=new PedidoInsumoDTO();
		p.setLoteInsumo(lote);
		p.setProveedor(proveedor.toDTO());
		p.setFechaGeneracion(fechaGeneracion);
		p.setFechaProbable(fechaProbable);
		p.setFechaReal(fechaReal);
		p.setTotal(total);
		p.setOrdenProduccion(ordenProduccion.toDTO());
		p.setEstado(estado);
		p.setProveedor(proveedor.toDTO());
		return p;
	}
	

}
