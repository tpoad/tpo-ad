package modelo;

import java.sql.Time;

import dto.PasoProduccionDTO;
import entities.PasoProduccionEntity;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class PasoProduccion {
	
	private AreaProduccion areaProduccion;
	private Time tiempoConsumido;

	public PasoProduccion() {
		super();
	}

	public PasoProduccion(AreaProduccion areaProduccion, Time tiempoConsumido) {
		super();
		this.areaProduccion = areaProduccion;
		this.tiempoConsumido = tiempoConsumido;
	}

	public AreaProduccion getAreaProduccion() {
		return areaProduccion;
	}

	public void setAreaProduccion(AreaProduccion areaProduccion) {
		this.areaProduccion = areaProduccion;
	}

	public Time getTiempoConsumido() {
		return tiempoConsumido;
	}

	public void setTiempoConsumido(Time tiempoConsumido) {
		this.tiempoConsumido = tiempoConsumido;
	}

	@Override
	public String toString() {
		return "PasoProduccionDTO [areaProduccion=" + areaProduccion + ", tiempoConsumido=" + tiempoConsumido + "]";
	}

	public PasoProduccion(PasoProduccionEntity entity) {
		tiempoConsumido = entity.getTiempoConsumido();
		areaProduccion = Produccion.getInstancia().obtenerAreaProduccion(entity.getAreaProduccion().getNombre());
	}

	public PasoProduccionEntity toEntity() {
		PasoProduccionEntity e = new PasoProduccionEntity();
		e.setTiempoConsumido(tiempoConsumido);
		e.setAreaProduccion(areaProduccion.toEntity());
		return e;
	}

	public PasoProduccionDTO toDTO() {
		PasoProduccionDTO p = new PasoProduccionDTO();
		p.setTiempoConsumido(tiempoConsumido);
		p.setAreaProduccion(areaProduccion.toDTO());
		return p;
	}

	public PasoProduccion(PasoProduccionDTO dto) throws SucursalException, ItemPrendaException {
		areaProduccion = new AreaProduccion(dto.getAreaProduccion());
		tiempoConsumido = dto.getTiempoConsumido();
	}

}
