package modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dto.ClienteDTO;
import entities.ClienteEntity;
import entities.MovimientoEntity;

public class Cliente {

	private int numeroCliente;
	private String razonSocial;
	private String cuit;
	private String inscripcionAFIP;
	private String direccion;
	private List <Movimiento> cuentaCorriente;
	private Float saldo;
	private Float limiteCredito;
	private String condicionesPago;

	public Cliente() {
		super();
		cuentaCorriente = new ArrayList<Movimiento>();
	}

	public Cliente(Integer numeroCliente, String razonSocial, String cuit, String inscripcionAFIP, String direccion,
			Float saldo, Float limiteCredito, String condicionesPago) {
		super();
		this.numeroCliente = numeroCliente;
		this.razonSocial = razonSocial;
		this.cuit = cuit;
		this.inscripcionAFIP = inscripcionAFIP;
		this.direccion = direccion;
		this.saldo = saldo;
		this.limiteCredito = limiteCredito;
		this.condicionesPago = condicionesPago;
		cuentaCorriente = new ArrayList<Movimiento>();
	}

	public Integer getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(Integer numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getInscripcionAFIP() {
		return inscripcionAFIP;
	}

	public void setInscripcionAFIP(String inscripcionAFIP) {
		this.inscripcionAFIP = inscripcionAFIP;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Float getSaldo() {
		return saldo;
	}

	public void setSaldo(Float saldo) {
		this.saldo = saldo;
	}

	public Float getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Float limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getCondicionesPago() {
		return condicionesPago;
	}

	public void setCondicionesPago(String condicionesPago) {
		this.condicionesPago = condicionesPago;
	}

	@Override
	public String toString() {
		return "ClienteDTO [numeroCliente=" + numeroCliente + ", razonSocial=" + razonSocial + ", cuit=" + cuit
				+ ", inscripcionAFIP=" + inscripcionAFIP + ", direccion=" + direccion + ", saldo=" + saldo
				+ ", limiteCredito=" + limiteCredito + ", condicionesPago=" + condicionesPago + "]";
	}
	
	public Cliente(ClienteDTO dto){
		numeroCliente = dto.getNumeroCliente();
		razonSocial = dto.getRazonSocial();
		cuit = dto.getCuit();
		inscripcionAFIP = dto.getInscripcionAFIP();
		direccion = dto.getDireccion();
		saldo = dto.getSaldo();
		limiteCredito = dto.getLimiteCredito();
		condicionesPago = dto.getCondicionesPago();
		cuentaCorriente = new ArrayList<Movimiento>();
//		for(MovimientoDTO m : dto.getCuentaCorriente()){
//			cuentaCorriente.add(new Movimiento(m));
//		}
	}
	
	public Cliente(ClienteEntity entity){
		numeroCliente = entity.getNumeroCliente();
		razonSocial = entity.getRazonSocial();
		cuit = entity.getCuit();
		inscripcionAFIP = entity.getInscripcionAFIP();
		direccion = entity.getDireccion();
		saldo = entity.getSaldo();
		limiteCredito = entity.getLimiteCredito();
		condicionesPago = entity.getCondicionesPago();
		cuentaCorriente = new ArrayList<Movimiento>();
		for(MovimientoEntity m : entity.getCuentaCorriente()){
			cuentaCorriente.add(new Movimiento(m));
		}
	}
	
	public ClienteDTO toDTO(){
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setCondicionesPago(getCondicionesPago());
		clienteDTO.setCuit(getCuit());
		clienteDTO.setDireccion(getDireccion());
		clienteDTO.setInscripcionAFIP(getInscripcionAFIP());
		clienteDTO.setLimiteCredito(getLimiteCredito());
		clienteDTO.setNumeroCliente(getNumeroCliente());
		clienteDTO.setRazonSocial(getRazonSocial());
		clienteDTO.setSaldo(getSaldo());
		for(Movimiento m : cuentaCorriente){
			clienteDTO.addMovimiento(m.toDTO());
		}
		return clienteDTO;
	}
	
	public ClienteEntity toEntity(){
		ClienteEntity clienteEntity = new ClienteEntity();
		clienteEntity.setCondicionesPago(getCondicionesPago());
		clienteEntity.setCuit(getCuit());
		clienteEntity.setDireccion(getDireccion());
		clienteEntity.setInscripcionAFIP(getInscripcionAFIP());
		clienteEntity.setLimiteCredito(getLimiteCredito());
		clienteEntity.setNumeroCliente(getNumeroCliente());
		clienteEntity.setRazonSocial(getRazonSocial());
		clienteEntity.setSaldo(getSaldo());
		for(Movimiento m : cuentaCorriente){
			clienteEntity.addMovimiento(m.toEntity());
		}
		return clienteEntity;
	}
	
	public boolean verificarSaldo(Float total) {
		return saldo >= total;
	}

	public Float restarSaldo(Float total) {
		cuentaCorriente.add(new Movimiento(-total, new Date()));
		return saldo -= total;
	}
	

}
