package modelo;

import controlador.ControladorGral;
import dto.UbicacionInsumoDTO;
import entities.UbicacionEntity;
import entities.UbicacionInsumoEntity;

public class UbicacionInsumo extends Ubicacion {

	private Insumo insumo;

	public UbicacionInsumo() {
		super();
	}

	public UbicacionInsumo(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super(calle, bloque, estante, posicion, cantidad);
	}

	public Insumo getInsumo() {
		return insumo;
	}

	public void setInsumo(Insumo insumo) {
		this.insumo = insumo;
	}

	@Override
	public String toString() {
		return "UbicacionInsumoDTO [insumo=" + insumo + "]";
	}

	public UbicacionInsumo(UbicacionInsumoEntity e) {
		super((UbicacionEntity) e);
		if (e.getInsumo() != null) {
			insumo = ControladorGral.getInstancia().obtenerInsumo(e.getInsumo().getCodigoInterno());
		} else {
			insumo = null;
		}
	}

	@Override
	public Boolean estaVacia() {
		return insumo == null;
	}

	@Override
	public UbicacionEntity toEntity() {
		UbicacionInsumoEntity e = new UbicacionInsumoEntity();
		e.setCantidad(cantidad);
		if (insumo != null)
			e.setInsumo(insumo.toEntity());
		e.setUbicacion(getKey());
		return e;
	}

	public UbicacionInsumoDTO toDTO() {
		UbicacionInsumoDTO u = new UbicacionInsumoDTO();
		u.setInsumo(insumo.toDTO());
		return u;
	}
}
