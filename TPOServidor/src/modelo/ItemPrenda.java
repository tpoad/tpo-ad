package modelo;

import java.util.ArrayList;
import java.util.List;

import dto.ItemPrendaDTO;
import dto.PrendaDTO;
import entities.InsumoUsadoEntity;
import entities.ItemPrendaEntity;
import entities.PrendaEntity;
import enums.Color;
import enums.Talle;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class ItemPrenda {
	
	private int subcodigo;
	private Color color;
	private Talle talle;
	private Integer cantidadStock;
	private Integer cantidadMinima;
	private Integer cantidadProducir;
	private List<InsumoUsado> insumosUsados;
	private Prenda prenda;

	public ItemPrenda() {
		super();
		insumosUsados = new ArrayList<InsumoUsado>();
	}

	public ItemPrenda(int subcodigo, Color color, Talle talle, Integer cantidadStock, Integer cantidadMinima,
			Integer cantidadProducir) {
		super();
		this.subcodigo = subcodigo;
		this.color = color;
		this.talle = talle;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadProducir = cantidadProducir;
		insumosUsados = new ArrayList<InsumoUsado>();
	}

	public ItemPrenda(int subcodigo, Color color, Talle talle, Integer cantidadStock, Integer cantidadMinima,
			Integer cantidadProducir, Prenda prenda) {
		super();
		this.subcodigo = subcodigo;
		this.color = color;
		this.talle = talle;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadProducir = cantidadProducir;
		insumosUsados = new ArrayList<InsumoUsado>();
		this.prenda = prenda;
	}

	public ItemPrenda(ItemPrenda item) {
		super();
		this.subcodigo = item.getSubcodigo();
		this.color = item.getColor();
		this.talle = item.getTalle();
		this.cantidadStock = item.getCantidadStock();
		this.cantidadMinima = item.getCantidadMinima();
		this.cantidadProducir = item.getCantidadProducir();
		insumosUsados = new ArrayList<InsumoUsado>();
		this.prenda = item.getPrenda();
	}

	public int getSubcodigo() {
		return subcodigo;
	}

	public void setSubcodigo(int subcodigo) {
		this.subcodigo = subcodigo;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Talle getTalle() {
		return talle;
	}

	public void setTalle(Talle talle) {
		this.talle = talle;
	}

	public Integer getCantidadStock() {
		return cantidadStock;
	}

	public void setCantidadStock(Integer cantidadStock) {
		this.cantidadStock = cantidadStock;
	}

	public Integer getCantidadMinima() {
		return cantidadMinima;
	}

	public void setCantidadMinima(Integer cantidadMinima) {
		this.cantidadMinima = cantidadMinima;
	}

	public Integer getCantidadProducir() {
		return cantidadProducir;
	}

	public void setCantidadProducir(Integer cantidadProducir) {
		this.cantidadProducir = cantidadProducir;
	}

	public List<InsumoUsado> getInsumosUsados() {
		return insumosUsados;
	}

	public void setInsumosUsados(List<InsumoUsado> insumosUsados) {
		this.insumosUsados = insumosUsados;
	}

	public Prenda getPrenda() {
		return prenda;
	}

	public void setPrenda(Prenda prenda) {
		this.prenda = prenda;
	}

	public ItemPrenda(ItemPrendaEntity entity, Prenda p) {
		subcodigo = entity.getSubcodigo();
		color = entity.getColor();
		talle = entity.getTalle();
		cantidadStock = entity.getCantidadStock();
		cantidadMinima = entity.getCantidadMinima();
		cantidadProducir = entity.getCantidadProducir();
		insumosUsados = new ArrayList<InsumoUsado>();
		for (InsumoUsadoEntity ie : entity.getInsumosUsados()) {
			insumosUsados.add(new InsumoUsado(ie));
		}
		prenda = p;
	}

	public ItemPrenda(ItemPrendaEntity entity) {
		subcodigo = entity.getSubcodigo();
		color = entity.getColor();
		talle = entity.getTalle();
		cantidadStock = entity.getCantidadStock();
		cantidadMinima = entity.getCantidadMinima();
		cantidadProducir = entity.getCantidadProducir();
		insumosUsados = new ArrayList<InsumoUsado>();
		for (InsumoUsadoEntity ie : entity.getInsumosUsados()) {
			insumosUsados.add(new InsumoUsado(ie));
		}
		Prenda p = new Prenda();
		p.setCodigo(entity.getPrenda().getCodigo());
		p.setDescripcion(entity.getPrenda().getDescripcion());
		p.setMolde(entity.getPrenda().getMolde());
		p.setEstado(entity.getPrenda().getEstado());
		p.setCostoActual(entity.getPrenda().getCostoActual());
		p.setPrecioVenta(entity.getPrenda().getPrecioVenta());
		p.setTemporada(entity.getPrenda().getTemporada());
		this.setPrenda(p);
	}

	public ItemPrendaEntity toEntity() {
		ItemPrendaEntity ent = new ItemPrendaEntity();
		ent.setSubcodigo(subcodigo);
		ent.setColor(color);
		ent.setTalle(talle);
		ent.setCantidadStock(cantidadStock);
		ent.setCantidadProducir(cantidadProducir);
		ent.setCantidadMinima(cantidadMinima);
		for (InsumoUsado i : insumosUsados) {
			ent.getInsumosUsados().add(i.toEntity());
		}
		PrendaEntity pe = new PrendaEntity();
		pe.setCodigo(prenda.getCodigo());
		pe.setDescripcion(prenda.getDescripcion());
		pe.setMolde(prenda.getMolde());
		pe.setCostoActual(prenda.getCostoActual());
		pe.setPrecioVenta(prenda.getPrecioVenta());
		pe.setEstado(prenda.getEstado());
		pe.setTemporada(prenda.getTemporada());
		pe.setTiempoProduccion(prenda.getTiempoProduccion());
		ent.setPrenda(pe);
		return ent;
	}

	public ItemPrendaDTO toDTOsinPrendaSinInsumos() {
		ItemPrendaDTO dto = new ItemPrendaDTO();
		dto.setSubcodigo(subcodigo);
		dto.setColor(color);
		dto.setTalle(talle);
		dto.setCantidadMinima(cantidadMinima);
		dto.setCantidadProducir(cantidadProducir);
		dto.setCantidadStock(cantidadStock);
		PrendaDTO prendaDTO = new PrendaDTO();
		prendaDTO.setCodigo(prenda.getCodigo());
		prendaDTO.setDescripcion(prenda.getDescripcion());
		prendaDTO.setMolde(prenda.getMolde());
		prendaDTO.setEstado(prenda.getEstado());
		prendaDTO.setCostoActual(prenda.getCostoActual());
		prendaDTO.setPrecioVenta(prenda.getPrecioVenta());
		prendaDTO.setTiempoProduccion(prenda.getTiempoProduccion());
		dto.setPrenda(prendaDTO);
		return dto;
	}

	public ItemPrendaDTO toDTO() {
		ItemPrendaDTO i = new ItemPrendaDTO();
		i.setSubcodigo(subcodigo);
		i.setColor(color);
		i.setTalle(talle);
		i.setCantidadMinima(cantidadMinima);
		i.setCantidadProducir(cantidadProducir);
		i.setCantidadStock(cantidadStock);
		i.setPrenda(prenda.toDTObobo());
		return i;
	}

	public ItemPrenda (ItemPrendaDTO dto) throws SucursalException, ItemPrendaException {
		subcodigo = dto.getSubcodigo();
		color = dto.getColor();
		talle = dto.getTalle();
		cantidadStock = dto.getCantidadStock();
		cantidadMinima = dto.getCantidadMinima();
		cantidadProducir = dto.getCantidadProducir();
		insumosUsados = new ArrayList<InsumoUsado>();
		prenda = new Prenda(dto.getPrenda());
	}

	@Override
	public String toString() {
		return "ItemPrenda [subcodigo=" + subcodigo + ", color=" + color + ", talle=" + talle + ", cantidadStock="
				+ cantidadStock + ", cantidadMinima=" + cantidadMinima + ", cantidadProducir=" + cantidadProducir
				+ ", insumosUsados=" + insumosUsados + ", prenda=" + prenda + "]";
	}

}
