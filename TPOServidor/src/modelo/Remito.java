package modelo;

import java.util.Date;
import java.util.List;

import controlador.ControladorGral;
import dto.RemitoDTO;
import entities.ItemRemitoEntity;
import entities.RemitoEntity;

public class Remito {
	private Cliente cliente;
	private Date fecha;
	private List<ItemRemito> itemsRemito;
	private String direccionEntrega;

	public Remito() {
		super();
	}

	public Remito(Cliente cliente, Date fecha, List<ItemRemito> itemsRemito, String direccionEntrega) {
		super();
		this.cliente = cliente;
		this.fecha = fecha;
		this.itemsRemito = itemsRemito;
		this.direccionEntrega = direccionEntrega;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<ItemRemito> getItemsRemito() {
		return itemsRemito;
	}

	public void setItemsRemito(List<ItemRemito> itemsRemito) {
		this.itemsRemito = itemsRemito;
	}

	public String getDireccionEntrega() {
		return direccionEntrega;
	}

	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}

	@Override
	public String toString() {
		return "RemitoDTO [Cliente=" + cliente + ", fecha=" + fecha + ", itemRemito=" + itemsRemito
				+ ", direccionEntrega=" + direccionEntrega + "]";
	}

	public Remito(RemitoEntity e) {
		cliente = ControladorGral.getInstancia().obtenerCliente(e.getCliente().getCuit());
		fecha = e.getFecha();
		for (ItemRemitoEntity itemRemito : e.getItemsRemito()) {
			itemsRemito.add(new ItemRemito(itemRemito));
		}
		direccionEntrega = e.getDireccionEntrega();
	}

	public RemitoEntity toEntity() {
		RemitoEntity e = new RemitoEntity();
		e.setCliente(cliente.toEntity());
		e.setFecha(fecha);
		for (ItemRemito i : itemsRemito) {
			e.getItemsRemito().add(i.toEntity());
		}
		e.setDireccionEntrega(direccionEntrega);
		return e;
	}

	public RemitoDTO toDTO() {
		RemitoDTO r = new RemitoDTO();
		r.setCliente(cliente.toDTO());
		r.setFecha(fecha);
		for (ItemRemito i : itemsRemito) {
			r.addItemsRemito(i.toDTO());
		}
		return r;
	}
}
