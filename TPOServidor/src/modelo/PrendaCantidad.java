package modelo;

public class PrendaCantidad 
{	
	private int cantidad;
	private Prenda prenda;
	
	public PrendaCantidad(){
		cantidad = 0;
	}
	
	public PrendaCantidad(int cantidad, Prenda prenda) {
		super();
		this.cantidad = cantidad;
		this.prenda = prenda;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Prenda getPrenda() {
		return prenda;
	}
	public void setPrenda(Prenda prenda) {
		this.prenda = prenda;
	}

	@Override
	public String toString() {
		return "PrendaCantidad [cantidad=" + cantidad + ", prenda=" + prenda
				+ "]";
	}
	
	
}
