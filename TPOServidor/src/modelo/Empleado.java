package modelo;

import java.io.Serializable;

import entities.EmpleadoEntity;
import enums.EmpleadoRol;
import dto.*;

public class Empleado implements Serializable {

	private static final long serialVersionUID = 1L;
	private int legajo;
	private String nombreCompleto;
	private EmpleadoRol rol;

	public Empleado() {
		super();
	}

	public Empleado(int legajo, String nombreCompleto) {
		super();
		this.legajo = legajo;
		this.nombreCompleto = nombreCompleto;
	}

	public int getLegajo() {
		return legajo;
	}

	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "EmpleadoDTO [legajo=" + legajo + ", nombreCompleto=" + nombreCompleto + "]";
	}

	public EmpleadoRol getRol() {
		return rol;
	}

	public void setRol(EmpleadoRol rol) {
		this.rol = rol;
	}

	public EmpleadoEntity toEntity() {
		EmpleadoEntity entity = new EmpleadoEntity();
		entity.setLegajo(legajo);
		entity.setNombreCompleto(nombreCompleto);
		entity.setRol(rol);
		return entity;
	}

	public Empleado(EmpleadoEntity e) {
		legajo = e.getLegajo();
		nombreCompleto = e.getNombreCompleto();
		rol = e.getRol();
	}

	public Empleado(EmpleadoDTO e) {
		legajo = e.getLegajo();
		nombreCompleto = e.getNombreCompleto();
		rol = e.getRol();
	}

	public EmpleadoDTO toDTO() {
		EmpleadoDTO e = new EmpleadoDTO();
		e.setLegajo(legajo);
		e.setNombreCompleto(nombreCompleto);
		e.setRol(rol);
		return e;
	}

}
