package modelo;

import dto.UbicacionDTO;
import entities.UbicacionEntity;
import entities.UbicacionKey;

public class Ubicacion {

	protected String calle;
	protected int bloque;
	protected int estante;
	protected int posicion;
	protected int cantidad;

	public Ubicacion() {
		super();
	}

	public Ubicacion(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super();
		this.calle = calle;
		this.bloque = bloque;
		this.estante = estante;
		this.posicion = posicion;
		this.cantidad = cantidad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getBloque() {
		return bloque;
	}

	public void setBloque(int bloque) {
		this.bloque = bloque;
	}

	public int getEstante() {
		return estante;
	}

	public void setEstante(int estante) {
		this.estante = estante;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "UbicacionDTO [calle=" + calle + ", bloque=" + bloque + ", estante=" + estante + ", posicion=" + posicion
				+ ", cantidad=" + cantidad + "]";
	}

	public Boolean estaVacia() {
		System.out.println("Pasa por el padre estaVacia");
		return null;
	}
	
	public Ubicacion(UbicacionEntity e) {
		calle = e.getUbicacion().getCalle();
		bloque = e.getUbicacion().getBloque();
		estante = e.getUbicacion().getEstante();
		posicion = e.getUbicacion().getPosicion();
		cantidad = e.getCantidad();
	}

	public UbicacionEntity toEntity() {
		return null;
	}

	public UbicacionKey getKey() {
		UbicacionKey u = new UbicacionKey();
		u.setBloque(bloque);
		u.setCalle(calle);
		u.setEstante(estante);
		u.setPosicion(posicion);
		return u;
	}

	public UbicacionDTO toDTO() {
		UbicacionDTO u = new UbicacionDTO();
		u.setCalle(calle);
		u.setBloque(bloque);
		u.setEstante(estante);
		u.setPosicion(posicion);
		u.setCantidad(cantidad);
		return u;
	}

	public Ubicacion(UbicacionDTO e) {
		calle = e.getCalle();
		bloque = e.getBloque();
		estante = e.getEstante();
		posicion = e.getPosicion();
		cantidad = e.getCantidad();
	}

}
