package modelo;

import controlador.ControladorGral;
import dto.InsumoUsadoDTO;
import entities.InsumoUsadoEntity;

public class InsumoUsado {
	
	private Insumo insumo;
	private float cantidad;

	public InsumoUsado() {
		super();
	}

	public InsumoUsado(Insumo insumo, float cantidad) {
		super();
		this.insumo = insumo;
		this.cantidad = cantidad;
	}

	public Insumo getInsumo() {
		return insumo;
	}

	public void setInsumo(Insumo insumo) {
		this.insumo = insumo;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "InsumoUsadoDTO [insumo=" + insumo + ", cantidad=" + cantidad + "]";
	}

	public InsumoUsado(InsumoUsadoEntity entity) {
		cantidad = entity.getCantidad();
		insumo = ControladorGral.getInstancia().obtenerInsumo(entity.getInsumo().getCodigoInterno());
	}

	public InsumoUsadoEntity toEntity() {
		InsumoUsadoEntity ent = new InsumoUsadoEntity();
		ent.setCantidad(cantidad);
		ent.setInsumo(insumo.toEntity());
		return ent;
	}

	public InsumoUsadoDTO toDTO() {
		InsumoUsadoDTO dto = new InsumoUsadoDTO();
		dto.setInsumo(insumo.toDTO());
		dto.setCantidad(cantidad);
		return dto;
	}

	public InsumoUsado(InsumoUsadoDTO dto) {
		cantidad = dto.getCantidad();
		// insumo =
		// ControladorGral.getInstancia().obtenerInsumo(dto.getInsumo().getCodigoInterno());
		Insumo i = new Insumo(dto.getInsumo());
		insumo = i;
	}

}
