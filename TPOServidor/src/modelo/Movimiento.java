package modelo;

import java.util.Date;

import dto.MovimientoDTO;
import entities.MovimientoEntity;
import enums.MovimientoTipo;

public class Movimiento 
{
	private float importe;
	private Date fecha;
	private MovimientoTipo concepto ;
	
	public MovimientoTipo getConcepto() {
		return concepto;
	}

	public void setConcepto(MovimientoTipo concepto) {
		this.concepto = concepto;
	}

	public Movimiento() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Movimiento(float importe, Date fecha) {
		super();
		this.importe = importe;
		this.fecha = fecha;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public MovimientoDTO toDTO(){
		MovimientoDTO dto = new MovimientoDTO();
		dto.setFecha(fecha);
		dto.setImporte(importe);
		dto.setConcepto(concepto);
		return dto;
	}
	
	public MovimientoEntity toEntity(){
		MovimientoEntity e = new MovimientoEntity();
		e.setConcepto(concepto);
		e.setFecha(fecha);
		e.setImporte(importe);
		return e;
	}
	
	public Movimiento(MovimientoDTO dto){
		concepto = dto.getConcepto();
		fecha = dto.getFecha();
		importe = dto.getImporte();
	}
	
	public Movimiento(MovimientoEntity entity){
		concepto = entity.getConcepto();
		fecha = entity.getFecha();
		importe = entity.getImporte();
	}
}
