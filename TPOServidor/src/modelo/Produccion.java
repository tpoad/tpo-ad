package modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dao.AreaProduccionDAO;
import dao.OrdenProduccionDAO;
import enums.EstadoOrdenProduccion;
import enums.EstadoPedidoCliente;
import exceptions.AreaProduccionException;
import exceptions.OrdenProduccionException;

public class Produccion {
	private List<OrdenProduccion> ordenesPendientes;
	private List<AreaProduccion> areas;

	static Produccion instancia;

	static public Produccion getInstancia() {
		if (instancia == null) {
			instancia = new Produccion();
		}
		return instancia;
	}

	private Produccion() {
		ordenesPendientes = new ArrayList<OrdenProduccion>();
		try {
			areas = AreaProduccionDAO.getInstancia().obtenerAreas();
			ordenesPendientes = OrdenProduccionDAO.getInstancia().obtenerOrdenesCopletas();

		} catch (AreaProduccionException e) {
			e.printStackTrace();
		} catch (OrdenProduccionException e) {
			e.printStackTrace();
		}

	}

	public Produccion(List<OrdenProduccion> ordenesPendientes, List<AreaProduccion> areas) {
		super();
		this.ordenesPendientes = ordenesPendientes;
		this.areas = areas;
	}

	public List<OrdenProduccion> getOrdenesPendientes() {
		return ordenesPendientes;
	}

	public void setOrdenesPendientes(List<OrdenProduccion> ordenesPendientes) {
		this.ordenesPendientes = ordenesPendientes;
	}

	public List<AreaProduccion> getAreas() {
		return areas;
	}

	public void setAreas(List<AreaProduccion> areas) {
		this.areas = areas;
	}

	@Override
	public String toString() {
		return "Produccion";
	}

	public AreaProduccion obtenerAreaProduccion(String area) {
		AreaProduccion aux = null;
		try {
			// primero me fijo si lo tengo en memoria, si lo tengo lo devuelvo
			for (AreaProduccion s : areas) {
				if (s != null && s.getNombre() == area) {
					aux = s;
					return aux;
				}
			}
			// si no lo tengo lo busco en la base de datos, si lo encuentra lo agrego a memoria
			aux = AreaProduccionDAO.getInstancia().obtenerArea(area);
			if (aux != null) {
				areas.add(aux);
			}
		} catch (AreaProduccionException e) {
			e.printStackTrace();
		}
		return aux;
	}

	public void InicializarAreasProduccion() {
		try {
			AreaProduccion area1 = new AreaProduccion();
			area1.setNombre("Marcado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area1);
			areas.add(area1);

			AreaProduccion area2 = new AreaProduccion();
			area2.setNombre("Corte");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area2);
			areas.add(area2);

			AreaProduccion area3 = new AreaProduccion();
			area3.setNombre("Habilitado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area3);
			areas.add(area3);

			AreaProduccion area4 = new AreaProduccion();
			area4.setNombre("Costura");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area4);
			areas.add(area4);

			AreaProduccion area5 = new AreaProduccion();
			area5.setNombre("Estampado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area5);
			areas.add(area5);

			AreaProduccion area6 = new AreaProduccion();
			area6.setNombre("Acabado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area6);
			areas.add(area6);

			AreaProduccion area7 = new AreaProduccion();
			area7.setNombre("Planchado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area7);
			areas.add(area7);

			AreaProduccion area8 = new AreaProduccion();
			area8.setNombre("Empacado");
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(area8);
			areas.add(area8);

			System.out.println("Areas de produccion inicializadas");

		} catch (AreaProduccionException e) {
			e.printStackTrace();
			System.out.println("No se pudo inicializar las areas de produccion");
		}
	}

	public static Date addDays(Date date, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); // minus number would decrement the days
		return cal.getTime();
	}
//
//	public void recibirPedido(PedidoCliente p) throws ClienteException, UbicacionException {
//		List<ItemPedidoCliente> items = p.getItemsPedidoCliente();
//
//		List<ItemPedidoCliente> itemsCompletos = new ArrayList<ItemPedidoCliente>();
//		List<ItemPedidoCliente> itemsIncompletos = new ArrayList<ItemPedidoCliente>();
//
//		for (ItemPedidoCliente i : items) {
//			if (i.getItemPrenda().getCantidadStock()/* lo que tengo */ >= i.getCantidad()/* lo pedido */) {
//				itemsCompletos.add(i);
//			} else {
//				itemsIncompletos.add(i);
//			}
//		}
//
//		if (itemsIncompletos.size() <= 0) {
//			// marcar pedido como terminado
//			p.setEstado(EstadoPedidoCliente.TERMINADO);
//			// poner fecha aprox
//			int prob = 5;
//			p.setFechaProbable(addDays(p.getFechaGeneracion(), prob));
//			// M.S: para generar la fechas pasar a despacho para que descuente el stock y arme el pedido
//			Despacho.getInstancia().generarDespacho(p);
//		} else { // existen incompletos.
//			// si faltan tres o mas itemprenda de la misma prenda emito orden!!!
//			// completar ???
//
//			List<PrendaCantidad> faltantePorPrenda = new ArrayList<PrendaCantidad>();
//			for (ItemPedidoCliente i : itemsIncompletos) {
//				boolean encontro = false;
//				for (PrendaCantidad pc : faltantePorPrenda) {
//					if (i.getItemPrenda().getPrenda() == pc.getPrenda()) {
//						pc.setCantidad(pc.getCantidad() + 1);
//						encontro = true;
//						break;
//					}
//				}
//				if (encontro == false) {
//					PrendaCantidad pc = new PrendaCantidad();
//					pc.setPrenda(i.getItemPrenda().getPrenda());
//					pc.setCantidad(1);
//				}
//			}
//
//			// aca tengo la lista de faltantes por prenda
//
//			for (PrendaCantidad pc : faltantePorPrenda) {
//				if (pc.getCantidad() > 2) {
//					// TODO
//					// generar orden completa ---> chequear si tengo insumos,
//					// generar por cada uno.
//					@SuppressWarnings("unused")
//					PedidoInsumo pedinsumo = new PedidoInsumo();
//					List<Insumo> insumos = new ArrayList<Insumo>(); // este
//																	// vector lo
//																	// agrego a
//																	// pedido
//																	// insumo.
//
//					for (ItemPrenda ip : pc.getPrenda().getItemsPrenda()) {
//						for (InsumoUsado iu : ip.getInsumosUsados()) {
//							if (iu.getInsumo().getCantidadStock() < (pc.getCantidad() * iu.getCantidad())) {
//								// si los insumos que tengo en stock es menor a
//								// lo que tengo que hacer, lo agrego al vector.
//								insumos.add(iu.getInsumo());
//							}
//						}
//					}
//
//					OrdenProduccionCompleta opc = new OrdenProduccionCompleta();
//					// setearle todos los datos.
//					float costoTotal = 0;
//					List<ItemPrenda> it1 = new ArrayList<ItemPrenda>();
//					for (ItemPedidoCliente ipc : itemsCompletos) {
//						costoTotal = costoTotal + (ipc.getPrecio() * ipc.getCantidad());
//						it1.add(ipc.getItemPrenda());
//					}
//					opc.setItemsPrenda(it1);
//					opc.setCostoTotal(costoTotal);
//					opc.setPedidoCliente(p);
//
//					try {
//						OrdenProduccionDAO.getInstancia().guardarOrdenProduccion(opc);
//						siguientePaso(null, opc);
//
//					} catch (OrdenProduccionException e) {
//						System.out.println("No se pudo generar la orden");
//						e.printStackTrace();
//					}
//
//				} else {
//					// TODO generar orden parcial
//					OrdenProduccionParcial opp = new OrdenProduccionParcial();
//					// hacer un metodo que busque los itemprenda del pedido con
//					// esa prenda y generar la orden parcial
//					List<ItemPrenda> it2 = new ArrayList<ItemPrenda>();
//
//					for (ItemPedidoCliente in : itemsIncompletos) {
//						it2.add(in.getItemPrenda());
//					}
//					opp.setItemsPrenda(it2);
//					opp.setPedidoCliente(p);
//
//					try {
//						OrdenProduccionDAO.getInstancia().guardarOrdenProduccion(opp);
//						siguientePaso(null, opp);
//
//					} catch (OrdenProduccionException e) {
//						System.out.println("No se pudo generar la orden");
//						e.printStackTrace();
//					}
//				}
//			}
//
//			// settear la fecha de entrega ---> la REAL o la PROBABLE ??
//			// PROBABLE esta arriba.
//			int prob = 0;
//			for (@SuppressWarnings("unused") ItemPedidoCliente aux : itemsIncompletos) {
//				prob += 2; // Le sumo 2 dias por cada item que no este
//			}
//			p.setFechaProbable(addDays(p.getFechaGeneracion(), prob)); // M.S:
//																		// para
//																		// generar
//																		// la
//																		// fechas
//
//			// le ponemos, no se, 2 dias por cada item en faltantePorPrenda o
//			// algo asi
//		}
//	}

	public void siguientePaso(AreaProduccion area, OrdenProduccion orden) {
		if (area == null) {
			orden.setEstado(EstadoOrdenProduccion.En_produccion);
			obtenerAreaProduccion("Marcado").agregarOrden(orden);
		} else if (area.getNombre() == "Marcado") {
			obtenerAreaProduccion("Corte").agregarOrden(orden);
		} else if (area.getNombre() == "Corte") {
			obtenerAreaProduccion("Habilitado").agregarOrden(orden);
		} else if (area.getNombre() == "Habilitado") {
			obtenerAreaProduccion("Costura").agregarOrden(orden);
		} else if (area.getNombre() == "Costura") {
			obtenerAreaProduccion("Estampado").agregarOrden(orden);
		} else if (area.getNombre() == "Estapado") {
			obtenerAreaProduccion("Acabado").agregarOrden(orden);
		} else if (area.getNombre() == "Acabado") {
			obtenerAreaProduccion("Planchado").agregarOrden(orden);
		} else if (area.getNombre() == "Planchado") {
			obtenerAreaProduccion("Empacado").agregarOrden(orden);
		} else if (area.getNombre() == "Empacado") {
			ordenesPendientes.add(orden);
			orden.setEstado(EstadoOrdenProduccion.Completada);
			ordenTerminada(orden);
		}
	}

	public void ordenTerminada(OrdenProduccion orden) {
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// mandarlas las prendas terminadas a deposito
		//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		for (OrdenProduccion o : orden.getPedidoCliente().getOrdenesDeProduccion()) {
			if (o.getEstado() != EstadoOrdenProduccion.Completada)
				return;
		}
		// si llego aca esta terminada
		orden.getPedidoCliente().setEstado(EstadoPedidoCliente.TERMINADO);
		for (OrdenProduccion o : orden.getPedidoCliente().getOrdenesDeProduccion()) {
			ordenesPendientes.remove(o);
		}

		@SuppressWarnings("unused")
		PedidoCliente pedidoListo = orden.getPedidoCliente();
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		// pedidoListo esta listo, ahora solo hay que mandarlo a despacho o lo
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// que
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// sea
		//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	public List<OrdenProduccion> obtenerOrdenesArea(String area) {
		return obtenerAreaProduccion(area).getOrdenesPendiente();
	}

	public void completarOrden(String area, int orden) {
		obtenerAreaProduccion(area).completarOrden(orden);
	}

}
