package modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controlador.ControladorGral;
import dto.ItemPedidoClienteDTO;
import dto.OrdenProduccionDTO;
import dto.PedidoClienteDTO;
import entities.ItemPedidoClienteEntity;
import entities.OrdenProduccionEntity;
import entities.PedidoClienteEntity;
import enums.EstadoPedidoCliente;
import enums.MotivoRechazo;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class PedidoCliente {

	private Integer numeroPedido;
	private Cliente cliente;
	private Sucursal sucursal;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;
	private List<ItemPedidoCliente> itemsPedidoCliente;
	private Float total;
	private EstadoPedidoCliente estado;
	private MotivoRechazo motivo;
	private List<OrdenProduccion> ordenesDeProduccion;

	public PedidoCliente() {
		ordenesDeProduccion = new ArrayList<OrdenProduccion>();
		itemsPedidoCliente = new ArrayList<ItemPedidoCliente>();
	}

	public PedidoCliente(Integer numeroPedido, Cliente cliente, Sucursal sucursal, Date fechaGeneracion,
			Date fechaProbable, Date fechaReal, List<ItemPedidoCliente> itemsPedidoCliente, Float total,
			EstadoPedidoCliente estado, MotivoRechazo motivo, List<OrdenProduccion> ordenesDeProduccion) {
		super();
		this.numeroPedido = numeroPedido;
		this.cliente = cliente;
		this.sucursal = sucursal;
		this.fechaGeneracion = fechaGeneracion;
		this.fechaProbable = fechaProbable;
		this.fechaReal = fechaReal;
		this.itemsPedidoCliente = itemsPedidoCliente;
		this.total = total;
		this.estado = estado;
		this.motivo = motivo;
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

	public PedidoCliente(PedidoCliente pc) {
		super();
		this.numeroPedido = pc.getNumeroPedido();
		this.cliente = pc.getCliente();
		this.sucursal = pc.getSucursal();
		this.fechaGeneracion = pc.getFechaGeneracion();
		this.fechaProbable = pc.getFechaProbable();
		this.fechaReal = pc.getFechaReal();
		itemsPedidoCliente = new ArrayList<ItemPedidoCliente>();
		for (ItemPedidoCliente ip : pc.getItemsPedidoCliente()) {
			this.itemsPedidoCliente.add(new ItemPedidoCliente(ip));
		}
		this.total = pc.getTotal();
		this.estado = pc.getEstado();
		this.motivo = pc.getMotivo();
		this.ordenesDeProduccion = new ArrayList<OrdenProduccion>();
		for (OrdenProduccion op : pc.getOrdenesDeProduccion()) {
			ordenesDeProduccion.add(new OrdenProduccion(op));
		}
	}

	public Integer getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(Integer numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public List<ItemPedidoCliente> getItemsPedidoCliente() {
		return itemsPedidoCliente;
	}

	public void setItemsPedidoCliente(List<ItemPedidoCliente> itemsPedidoCliente) {
		this.itemsPedidoCliente = itemsPedidoCliente;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public EstadoPedidoCliente getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoCliente estado) {
		this.estado = estado;
	}

	public MotivoRechazo getMotivo() {
		return motivo;
	}

	public void setMotivo(MotivoRechazo motivo) {
		this.motivo = motivo;
	}

	public Sucursal getSucursal() {
		return sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public List<OrdenProduccion> getOrdenesDeProduccion() {
		return ordenesDeProduccion;
	}

	public void setOrdenesDeProduccion(List<OrdenProduccion> ordenesDeProduccion) {
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

	@Override
	public String toString() {
		return "PedidoClienteDTO [numeroPedido=" + numeroPedido + ", cliente=" + cliente + ", Sucursal=" + sucursal
				+ ", fechaGeneracion=" + fechaGeneracion + ", fechaProbable=" + fechaProbable + ", fechaReal="
				+ fechaReal + ", itemsPedidoCliente=" + itemsPedidoCliente + ", total=" + total + ", estado=" + estado
				+ ", motivo=" + motivo + ", ordenesDeProduccion=" + ordenesDeProduccion + "]";
	}

	public PedidoCliente(PedidoClienteEntity e) {
		this.numeroPedido = e.getNumeroPedido();
		this.cliente = ControladorGral.getInstancia().obtenerCliente(e.getCliente().getCuit());
		this.fechaGeneracion = e.getFechaGeneracion();
		this.fechaProbable = e.getFechaProbable();
		this.fechaReal = e.getFechaGeneracion();
		this.total = e.getTotal();
		this.estado = e.getEstado();
		this.motivo = e.getMotivo();
		itemsPedidoCliente = new ArrayList<ItemPedidoCliente>();
		for (ItemPedidoClienteEntity i : e.getItemsPedidoCliente()) {
			try {
				itemsPedidoCliente.add(new ItemPedidoCliente(i));
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("PedidoCliente: tal vez el error est� con itemPrenda");
			}
		}

		ControladorGral.getInstancia().getPedidosCliente().add(this);

		ordenesDeProduccion = new ArrayList<OrdenProduccion>();
		for (OrdenProduccionEntity o : e.getOrdenesDeProduccion()) {
			ordenesDeProduccion.add(new OrdenProduccion(o));
		}

		if (e.getSucursal() != null)
			sucursal = ControladorGral.getInstancia().getSucursal(e.getSucursal().getNumero());
	}

	public PedidoClienteEntity toEntity() {
		PedidoClienteEntity e = new PedidoClienteEntity();
		for (OrdenProduccion o : ordenesDeProduccion) {
			e.getOrdenesDeProduccion().add(o.toEntity());
		}
		e.setCliente(cliente.toEntity());
		e.setEstado(estado);
		e.setFechaGeneracion(fechaGeneracion);
		e.setFechaProbable(fechaProbable);
		e.setFechaReal(fechaReal);
		e.setMotivo(motivo);
		e.setNumeroPedido(numeroPedido);
		e.setSucursal(sucursal.toEntity());
		e.setTotal(total);
		for (ItemPedidoCliente i : itemsPedidoCliente) {
			e.getItemsPedidoCliente().add(i.toEntity());
		}
		return e;
	}

	public PedidoClienteDTO toDTO() {
		PedidoClienteDTO dto = new PedidoClienteDTO();
		dto.setNumeroPedido(numeroPedido);
		if (cliente != null)
			dto.setCliente(cliente.toDTO());
		// no necesita la sucursal
		// no necesita las ordenes de produccion
		dto.setFechaGeneracion(fechaGeneracion);
		dto.setFechaProbable(fechaProbable);
		dto.setFechaReal(fechaReal);
		for (ItemPedidoCliente i : itemsPedidoCliente) {
			dto.getItemsPedidoCliente().add(i.toDTO());
		}
		dto.setTotal(total);
		dto.setEstado(estado);
		dto.setMotivo(motivo);

		return dto;
	}

	public PedidoCliente(PedidoClienteDTO dto) throws SucursalException, ItemPrendaException {
		numeroPedido = dto.getNumeroPedido();
		cliente = new Cliente(dto.getCliente());
		sucursal = new Sucursal(dto.getSucursal());
		fechaGeneracion = dto.getFechaGeneracion();
		fechaProbable = dto.getFechaProbable();
		fechaReal = dto.getFechaReal();
		itemsPedidoCliente = new ArrayList<ItemPedidoCliente>();
		for (ItemPedidoClienteDTO ipdto : dto.getItemsPedidoCliente()) {
			itemsPedidoCliente.add(new ItemPedidoCliente(ipdto));
		}
		total = dto.getTotal();
		estado = dto.getEstado();
		motivo = dto.getMotivo();
		ordenesDeProduccion = new ArrayList<OrdenProduccion>();
		for (OrdenProduccionDTO opdto : dto.getOrdenesDeProduccion()) {
			ordenesDeProduccion.add(new OrdenProduccion(opdto));
		}

	}

	// public PedidoCliente(PedidoClienteDTO dto) throws SucursalException,
	// ItemPrendaException {
	// ordenesDeProduccion = new ArrayList<OrdenProduccion>();
	// itemsPedidoCliente = new ArrayList<ItemPedidoCliente>();
	// Cliente cliente =
	// ControladorGral.getInstancia().obtenerCliente(dto.getCliente().getCuit());
	// this.cliente = cliente;
	// this.setFechaGeneracion(new Date());
	// this.setFechaReal(null);
	// this.setSucursal(ControladorGral.getInstancia().getSucursal((SucursalDAO.getInstancia().obtenerSucursalCliente(cliente.getCuit()).getNumero())));
	// this.setEstado(EstadoPedidoCliente.PENDIENTE_DE_CHECKEO);
	// this.setTotal(0f);
	// for (ItemPedidoClienteDTO item : dto.getItemsPedidoCliente()) {
	// ItemPedidoCliente i = new ItemPedidoCliente();
	// i.setCantidad(item.getCantidad());
	// i.setItemPrenda(ControladorGral.getInstancia().obtenerItemPrenda(item.getItemPrenda().getSubcodigo()));
	// i.setPrecio(i.getCantidad() *
	// i.getItemPrenda().getPrenda().getPrecioVenta());
	// this.setTotal(this.getTotal() + i.getPrecio() * i.getCantidad());
	// }
	// this.setFechaProbable(new Date()); // TODO HACER METODO EN EL
	// // CONTROLADOR QUE ESTIME FECHAS DE
	// // ENTREGA
	// }
	//
}
