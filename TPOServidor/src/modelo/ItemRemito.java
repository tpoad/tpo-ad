package modelo;

import controlador.ControladorGral;
import dto.ItemRemitoDTO;
import entities.ItemRemitoEntity;
import exceptions.ItemPrendaException;

public class ItemRemito {
	private ItemPrenda itemPrenda;
	private int cantidad;

	public ItemRemito() {
		super();
	}

	public ItemRemito(ItemPrenda itemPrenda, int cantidad) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
	}

	public ItemPrenda getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrenda itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public ItemRemitoEntity toEntity() {
		ItemRemitoEntity ent = new ItemRemitoEntity();
		ent.setItemPrenda(itemPrenda.toEntity());
		ent.setCantidad(cantidad);
		return ent;
	}

	public ItemRemito(ItemRemitoEntity ent) {
		cantidad = ent.getCantidad();
		try {
			itemPrenda = ControladorGral.getInstancia().obtenerItemPrenda(ent.getItemPrenda().getSubcodigo());
		} catch (ItemPrendaException e) {
			e.printStackTrace();
			System.out.println("ItemRemito: error en itemPrenda");
		}
	}

	public ItemRemitoDTO toDTO() {
		ItemRemitoDTO i = new ItemRemitoDTO();
		i.setItemPrenda(itemPrenda.toDTO());
		i.setCantidad(cantidad);
		return i;
	}

}
