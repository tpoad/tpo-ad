package modelo;

import java.util.ArrayList;
import java.util.List;

import dao.ItemPrendaDAO;
import dao.PedidoClienteDAO;
import dao.UbicacionDAO;
import enums.EstadoPedidoCliente;
import exceptions.PedidoClienteException;
import exceptions.UbicacionException;

public class Almacen {
	// private List<Ubicacion> ubicaciones;
	private List<UbicacionPrenda> ubicacionesPrendas;
	private List<UbicacionInsumo> ubicacionesInsumos;
	static private Almacen instancia;

	static final int CantidadMaximaPrendasPorUbicacion = 5;

	static public Almacen getInstancia() {
		if (instancia == null)
			instancia = new Almacen();
		return instancia;
	}

	private Almacen() {
		ubicacionesPrendas = new ArrayList<UbicacionPrenda>();
		ubicacionesInsumos = new ArrayList<UbicacionInsumo>();
		try {
			ubicacionesPrendas = UbicacionDAO.getInstancia().obtenerUbicacionesPrendas();
			ubicacionesInsumos = UbicacionDAO.getInstancia().obtenerUbicacionesInsumos();
		} catch (UbicacionException e) {
			e.printStackTrace();
		}
	}

	public List<UbicacionPrenda> getUbicacionesPrendas() {
		return ubicacionesPrendas;
	}

	public void setUbicacionesPrendas(List<UbicacionPrenda> ubicacionesPrendas) {
		this.ubicacionesPrendas = ubicacionesPrendas;
	}

	public List<UbicacionInsumo> getUbicacionesInsumos() {
		return ubicacionesInsumos;
	}

	public void setUbicacionesInsumos(List<UbicacionInsumo> ubicacionesInsumos) {
		this.ubicacionesInsumos = ubicacionesInsumos;
	}

	public static int getCantidadmaximaprendasporubicacion() {
		return CantidadMaximaPrendasPorUbicacion;
	}

	public void inicializar() throws UbicacionException {
		System.out.println("Inicializando almacen");
		int bloques = 5;
		int estante = 5;
		int posicion = 5;
		if (ubicacionesPrendas == null || ubicacionesPrendas.isEmpty()) {
			System.out.println("ubicacionesPrendas == null || ubicacionesPrendas.isEmpty()");
			String[] callesPrendas = { "A", "B", "C", "D", "E", "F", "G" };
			for (String p : callesPrendas) {
				for (int b = 0; b < bloques; b++) {
					for (int e = 0; e < estante; e++) {
						for (int pos = 0; pos < posicion; pos++) {
							UbicacionPrenda ubicacion = new UbicacionPrenda();
							ubicacion.setCalle(p);
							ubicacion.setBloque(b);
							ubicacion.setEstante(e);
							ubicacion.setPosicion(pos);
							// ubicacion.setItem(new ItemPrenda());
							// ubicacion.setItem(null);
							ubicacionesPrendas.add(ubicacion);
							UbicacionDAO.getInstancia().guardarUbicacion(ubicacion);
						}
					}
				}
			}
		}
		if (ubicacionesInsumos == null || ubicacionesInsumos.isEmpty()) {
			System.out.println("ubicacionesInsumos == null || ubicacionesInsumos.isEmpty()");
			String[] callesInsumos = { "H", "I", "J", "K", "L", "M", "N", "O", "P" };
			for (String p : callesInsumos) {
				for (int b = 0; b < bloques; b++) {
					for (int e = 0; e < estante; e++) {
						for (int pos = 0; pos < posicion; pos++) {
							UbicacionInsumo ubicacion = new UbicacionInsumo();
							ubicacion.setCalle(p);
							ubicacion.setBloque(b);
							ubicacion.setEstante(e);
							ubicacion.setPosicion(pos);
							// ubicacion.setInsumo(null);
							ubicacionesInsumos.add(ubicacion);
							UbicacionDAO.getInstancia().guardarUbicacion(ubicacion);
						}
					}
				}
			}
		}
//		System.out.println("\nubicacionesPrendas: " + ubicacionesPrendas);
		System.out.println("Almacen inicializado");
	}

	// LV
	public boolean almacenar(ItemPrenda item) throws UbicacionException {
		int cantidad = item.getCantidadStock();
		int cantidadBulto = 1;
		if (cantidad > CantidadMaximaPrendasPorUbicacion)
//			cantidadBulto = 1;
//		else
			cantidadBulto += (item.getCantidadStock() / CantidadMaximaPrendasPorUbicacion); //ESTA CUENTA NO EST� BIEN
		while (cantidadBulto > 0) {
			System.out.println("Almacenar item prenda");
			UbicacionPrenda up = (UbicacionPrenda) ObtenerProximaUbicacionVaciaPrenda();
			if (up != null) {
				up.setItem(item);
				if (cantidad < CantidadMaximaPrendasPorUbicacion) {
					up.setCantidad(cantidad);
				} else {
					up.setCantidad(CantidadMaximaPrendasPorUbicacion);
					cantidad -= CantidadMaximaPrendasPorUbicacion;
				}
				UbicacionDAO.getInstancia().modificarUbicacion(up);
				cantidadBulto--;
			} else {
				return false;
			}
		}
		return true;
	}

	// LV
	public List<UbicacionPrenda> obtenerUbicacionesPrendas() {
		List<UbicacionPrenda> lista = new ArrayList<UbicacionPrenda>();
		for (UbicacionPrenda u : ubicacionesPrendas) {
			if (u.estaVacia()) {
				lista.add(u);
			}
		}
		return lista;
	}

	// LV
	public List<UbicacionPrenda> obtenerUbicacionesPrendas(ItemPrenda item) {
		List<UbicacionPrenda> lista = new ArrayList<UbicacionPrenda>();
		for (UbicacionPrenda u : ubicacionesPrendas) {
			if (!u.estaVacia()){
				if(u.getItem().getSubcodigo() == item.getSubcodigo()) {
					lista.add(u);
				}
			}
		}
		return lista;
	}

	public List<UbicacionInsumo> obtenerUbicacionesInsumos(Insumo insumo) {
		List<UbicacionInsumo> lista = new ArrayList<UbicacionInsumo>();
		for (UbicacionInsumo u : ubicacionesInsumos) {
			if (u.getInsumo() == insumo) {
				lista.add(u);
			}
		}
		return lista;
	}

	public List<UbicacionCantidad> almacenar(Insumo insumo, int cantidad) {
		List<UbicacionInsumo> ubicacionesExistentes = obtenerUbicacionesInsumos(insumo);
		List<UbicacionCantidad> almacenarEn = new ArrayList<UbicacionCantidad>();
		for (UbicacionInsumo u : ubicacionesExistentes) {
			if (u.getCantidad() < CantidadMaximaPrendasPorUbicacion) {
				int cantidadGuardar = Math.min(cantidad, CantidadMaximaPrendasPorUbicacion - u.getCantidad());
				u.setCantidad(u.getCantidad() + cantidadGuardar);
				cantidad -= cantidadGuardar;
				UbicacionCantidad ubicant = new UbicacionCantidad();
				ubicant.setCantidad(cantidadGuardar);
				ubicant.setUbicacion(u);
				almacenarEn.add(ubicant);
				if (cantidad == 0) {
					break;
				}
			}
		}
		if (cantidad > 0)
			while (cantidad > 0) {
				UbicacionInsumo u = (UbicacionInsumo) ObtenerProximaUbicacionVaciaInsumo();
				if (u.getCantidad() < CantidadMaximaPrendasPorUbicacion) {
					u.setInsumo(insumo);
					int cantidadGuardar = Math.min(cantidad, CantidadMaximaPrendasPorUbicacion - u.getCantidad());
					u.setCantidad(u.getCantidad() + cantidadGuardar);
					cantidad -= cantidadGuardar;
					UbicacionCantidad ubicant = new UbicacionCantidad();
					ubicant.setCantidad(cantidadGuardar);
					ubicant.setUbicacion(u);
					almacenarEn.add(ubicant);
				}
			}
		return almacenarEn;
	}

	public List<UbicacionCantidad> almacenar(ItemPrenda item, int cantidad) {
		List<UbicacionPrenda> ubicacionesExistentes = obtenerUbicacionesPrendas(item);
		List<UbicacionCantidad> almacenarEn = new ArrayList<UbicacionCantidad>();
		for (UbicacionPrenda u : ubicacionesExistentes) {
			if (u.getCantidad() < CantidadMaximaPrendasPorUbicacion) {
				int cantidadGuardar = Math.min(cantidad, CantidadMaximaPrendasPorUbicacion - u.getCantidad());
				u.setCantidad(u.getCantidad() + cantidadGuardar);
				cantidad -= cantidadGuardar;
				UbicacionCantidad ubicant = new UbicacionCantidad();
				ubicant.setCantidad(cantidadGuardar);
				ubicant.setUbicacion(u);
				almacenarEn.add(ubicant);
				if (cantidad == 0) {
					break;
				}
			}
		}
		if (cantidad > 0)
			while (cantidad > 0) {
				UbicacionPrenda u = (UbicacionPrenda) ObtenerProximaUbicacionVaciaPrenda();
				if (u.getCantidad() < CantidadMaximaPrendasPorUbicacion) {
					u.setItem(item);
					int cantidadGuardar = Math.min(cantidad, CantidadMaximaPrendasPorUbicacion - u.getCantidad());
					u.setCantidad(u.getCantidad() + cantidadGuardar);
					cantidad -= cantidadGuardar;
					UbicacionCantidad ubicant = new UbicacionCantidad();
					ubicant.setCantidad(cantidadGuardar);
					ubicant.setUbicacion(u);
					almacenarEn.add(ubicant);
				}
			}
		return almacenarEn;
	}

	private Ubicacion ObtenerProximaUbicacionVaciaPrenda() {
		for (Ubicacion u : ubicacionesPrendas) {
			if (u.estaVacia()) {
				return u;
			}
		}
		return null;
	}

	private Ubicacion ObtenerProximaUbicacionVaciaInsumo() {
		for (Ubicacion u : ubicacionesInsumos) {
			if (u.estaVacia()) {
				return u;
			}
		}
		return null;
	}

	/**
	 * La idea es enviar un pedido para que se verifique si hay existencias
	 * 
	 * @param PedidoCliente
	 * @return true: El pedido se puede hacer
	 * @return false: El pedido no puede ser completado por falta de existencias
	 */
	public boolean verificarPedido(PedidoCliente pc) {
		for (ItemPedidoCliente i : pc.getItemsPedidoCliente()) {
			if (i.getCantidad() > obtenerExistenciasPrenda(i.getItemPrenda())) {
				return false;
			}
		}
		return true;
	}

	// LV
	public int obtenerExistenciasPrenda(ItemPrenda item) {
		int cantidad = 0;
		for (UbicacionPrenda u : obtenerUbicacionesPrendas(item)) {
			if(u != null){
				if (u.getItem().getSubcodigo() == item.getSubcodigo()) {
					cantidad += u.getCantidad();
				}
			}
		}
		return cantidad;
	}

	public int obtenerExistenciasInsumo(Insumo insumo) {
		int cantidad = 0;
		for (UbicacionInsumo u : obtenerUbicacionesInsumos(insumo)) {
			if (u.getInsumo() == insumo) {
				cantidad += u.getCantidad();
			}
		}
		return cantidad;
	}

	// LV
	public void procesarPedidoCliente(PedidoCliente pc) throws UbicacionException, PedidoClienteException {
		System.out.println("Almacen: procesarPedidoCliente");
		if (pc != null) {
			for (ItemPedidoCliente ipc : pc.getItemsPedidoCliente()) {
				retirarItems(ipc.getItemPrenda(), ipc.getCantidad());
			}
//			pc.setEstado(EstadoPedidoCliente.TERMINADO);
			PedidoCliente aux = PedidoClienteDAO.getInstancia().obtenerPedidoCliente(pc.getNumeroPedido());
			aux.setEstado(EstadoPedidoCliente.TERMINADO);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(aux);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
//			PedidoClienteDAO.getInstancia().modificarPedidoCliente(pc);
		} else {
			System.out.println("Almacen - procesarPedidoCliente: Error en el pedido cliente");
		}
	}

	// LV
	public void retirarItems(ItemPrenda item, int cantidad) throws UbicacionException {
		System.out.println("Almacen: retirar Items");
		ItemPrenda ip = new ItemPrenda(item);
		ip.setCantidadStock(item.getCantidadStock() - cantidad);
		for (UbicacionPrenda u : obtenerUbicacionesPrendas(item)) {
			if (u.getCantidad() <= cantidad) {
				u.setItem(null);
				cantidad -= u.getCantidad();
//				ubicacionesPrendas.remove(item);
			} else {
				u.setCantidad(u.getCantidad() - cantidad);
				cantidad = 0;
			}
			UbicacionDAO.getInstancia().modificarUbicacion(u);
			//TODO
			//QU� PASA CON LAS UBICACIONES EN MEMORIA?
		}
		ItemPrendaDAO.getInstancia().modificarItemPrenda(ip);
	}

	public ItemPrenda retirarItemsPrenda(ItemPrenda item, int cantidad) {
		ItemPrenda ip = new ItemPrenda();
		ip.setPrenda(item.getPrenda());
		ip.setSubcodigo(item.getSubcodigo());
		ip.setColor(item.getColor());
		ip.setTalle(item.getTalle());
		for (UbicacionPrenda u : obtenerUbicacionesPrendas(item)) {
			if (u.getCantidad() <= cantidad) {
				u.setItem(null);
				cantidad -= u.getCantidad();
			} else {
				u.setCantidad(u.getCantidad() - cantidad);
				cantidad = 0;
			}
			if (cantidad == 0) {
				return null;
			}
		}
		ip.setCantidadStock(cantidad);
		return ip;
	}

	public List<UbicacionCantidad> retirarPrendas(ItemPrenda item, int cantidad) {
		List<UbicacionPrenda> ubicacionesExistentes = obtenerUbicacionesPrendas(item);
		List<UbicacionCantidad> retirarDe = new ArrayList<UbicacionCantidad>();
		for (UbicacionPrenda u : ubicacionesExistentes) {
			int cantidadRetirar = Math.min(u.getCantidad(), u.getCantidad() - cantidad);
			u.setCantidad(u.getCantidad() - cantidadRetirar);
			cantidad -= cantidadRetirar;
			if (u.getCantidad() == 0) {
				u.setItem(null);
			}
			UbicacionCantidad ubicant = new UbicacionCantidad();
			ubicant.setCantidad(cantidadRetirar);
			ubicant.setUbicacion(u);
			retirarDe.add(ubicant);
			if (cantidad == 0) {
				break;
			}

		}
		return retirarDe;
	}

	public List<UbicacionCantidad> retirarInsumos(Insumo insumo, int cantidad) {
		List<UbicacionInsumo> ubicacionesExistentes = obtenerUbicacionesInsumos(insumo);
		List<UbicacionCantidad> retirarDe = new ArrayList<UbicacionCantidad>();
		for (UbicacionInsumo u : ubicacionesExistentes) {
			int cantidadRetirar = Math.min(u.getCantidad(), u.getCantidad() - cantidad);
			u.setCantidad(u.getCantidad() - cantidadRetirar);
			cantidad -= cantidadRetirar;
			if (u.getCantidad() == 0) {
				u.setInsumo(null);
			}
			UbicacionCantidad ubicant = new UbicacionCantidad();
			ubicant.setCantidad(cantidadRetirar);
			ubicant.setUbicacion(u);
			retirarDe.add(ubicant);
			if (cantidad == 0) {
				break;
			}

		}
		return retirarDe;
	}

}
