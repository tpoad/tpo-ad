package modelo;

import java.util.List;

import entities.OrdenProduccionParcialEntity;

public class OrdenProduccionParcial extends OrdenProduccion {

	public OrdenProduccionParcial() {
		super();
	}

	public OrdenProduccionParcial(OrdenProduccionParcialEntity entity) {
		super(entity);
	}

	public void setItemsPrenda(List<ItemPrenda> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	@Override
	public String toString() {
		return "OrdenProduccionParcial [itemsPrenda=" + itemsPrenda + "]";
	}

}
