
package modelo;

import java.util.ArrayList;

import entities.OrdenProduccionCompletaEntity;

public class OrdenProduccionCompleta extends OrdenProduccion
{
	public OrdenProduccionCompleta(){
		super();
	}
	
	public OrdenProduccionCompleta(Prenda p){
		super();
		itemsPrenda = new ArrayList<ItemPrenda>();
		for(ItemPrenda i : p.getItemsPrenda()){
			itemsPrenda.add(i);
		}
	}

	public OrdenProduccionCompleta(OrdenProduccionCompletaEntity entity) {
		super(entity);
	}
	
}
