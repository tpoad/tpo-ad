package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import controlador.ControladorGral;
import dao.AreaProduccionDAO;
import dto.AreaProduccionDTO;
import dto.ItemPrendaDTO;
import dto.LineaProduccionDTO;
import dto.OrdenProduccionDTO;
import entities.AreaProduccionEntity;
import entities.ItemPrendaEntity;
import entities.LineaProduccionEntity;
import entities.OrdenProduccionEntity;
import exceptions.AreaProduccionException;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class AreaProduccion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String nombre;
	private List<LineaProduccion> lineasProduccion;
	private List<OrdenProduccion> ordenesPendientes;
	private List<ItemPrenda> itemPrendaTerminados;
	
	public AreaProduccion() {
		super();
		lineasProduccion = new ArrayList<LineaProduccion>();
		ordenesPendientes = new ArrayList<OrdenProduccion>();
		itemPrendaTerminados= new ArrayList<ItemPrenda>();
	}

	public AreaProduccion(String nombre, List<LineaProduccion> lineasProduccion,
			List<OrdenProduccion> ordenesPendiente, List<ItemPrenda> itemPrendaTerminado) {
		super();
		this.nombre = nombre;
		this.lineasProduccion = lineasProduccion;
		ordenesPendientes = ordenesPendiente;
		this.itemPrendaTerminados = itemPrendaTerminado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<LineaProduccion> getLineasProduccion() {
		return lineasProduccion;
	}

	public void setLineasProduccion(List<LineaProduccion> lineasProduccion) {
		this.lineasProduccion = lineasProduccion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<OrdenProduccion> getOrdenesPendiente() {
		return ordenesPendientes;
	}

	public void setOrdenesPendiente(List<OrdenProduccion> ordenesPendiente) {
		ordenesPendientes = ordenesPendiente;
	}

	public List<ItemPrenda> getItemPrendaTerminado() {
		return itemPrendaTerminados;
	}

	public void setItemPrendaTerminado(List<ItemPrenda> itemPrendaTerminado) {
		this.itemPrendaTerminados = itemPrendaTerminado;
	}
	
	@Override
	public String toString() {
		return "[Area] " + nombre;
	}

	public AreaProduccion(AreaProduccionEntity entity) throws SucursalException, ItemPrendaException {
		nombre = entity.getNombre();
		lineasProduccion = new ArrayList<LineaProduccion>();
		ordenesPendientes = new ArrayList<OrdenProduccion>();
		itemPrendaTerminados= new ArrayList<ItemPrenda>();
		for(LineaProduccionEntity l : entity.getLineasProduccion()){
			lineasProduccion.add(new LineaProduccion(l, this));
		}
		for(OrdenProduccionEntity o : entity.getOrdenesPendiente()){
			ordenesPendientes.add(ControladorGral.getInstancia().getOrdenProduccion(o.getLote()));
		}
		for(ItemPrendaEntity i : entity.getItemPrendaTerminados()){
			itemPrendaTerminados.add(ControladorGral.getInstancia().obtenerItemPrenda(i.getSubcodigo()));
		}	
	}
	
	public AreaProduccionEntity toEntity() {
		AreaProduccionEntity e = new AreaProduccionEntity();
		e.setNombre(nombre);
		for(LineaProduccion l : lineasProduccion){
			e.getLineasProduccion().add(l.toEntity());
		}
		for(OrdenProduccion o : ordenesPendientes){
			e.getOrdenesPendiente().add(o.toEntity());
		}
		for(ItemPrenda i : itemPrendaTerminados){
			e.getItemPrendaTerminados().add(i.toEntity());
		}		
		return e;
	}

	public AreaProduccionDTO toDTO() {
		AreaProduccionDTO a=new AreaProduccionDTO();
		a.setNombre(nombre);
		for(LineaProduccion l: lineasProduccion){
			a.addLineasProduccion(l.toDTO());
		}
		for(OrdenProduccion o:ordenesPendientes){
			a.addOrdenesPendiente(o.toDTO());
		}
		for(ItemPrenda i: itemPrendaTerminados){
			a.addItemPrenda(i.toDTO());
		}
		return a;
	}	

	public AreaProduccion(AreaProduccionDTO dto) throws SucursalException, ItemPrendaException {
		nombre = dto.getNombre();
		lineasProduccion = new ArrayList<LineaProduccion>();
		for(LineaProduccionDTO l : dto.getLineasProduccion()){
			lineasProduccion.add(new LineaProduccion(l));
		}
		ordenesPendientes = new ArrayList<OrdenProduccion>();
		for(OrdenProduccionDTO o : dto.getOrdenesPendiente()){
			ordenesPendientes.add(new OrdenProduccion(o));
		}
		itemPrendaTerminados= new ArrayList<ItemPrenda>();
		for(ItemPrendaDTO i : dto.getItemPrendaTerminado()){
			itemPrendaTerminados.add(new ItemPrenda(i));
		}
	}
	
	//TODO
	public void agregarOrden(OrdenProduccion orden){
		ordenesPendientes.add(orden);
		try {
			AreaProduccionDAO.getInstancia().guardarAreaProduccion(this);
		} catch (AreaProduccionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void completarOrden(int id){
		for(OrdenProduccion o : ordenesPendientes){
			if(o.getLoteProduccion() == id){
				ordenesPendientes.remove(o);
				try {
					AreaProduccionDAO.getInstancia().guardarAreaProduccion(this);
				} catch (AreaProduccionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Produccion.getInstancia().siguientePaso(this, o);
				
			}
		}
		//guardar
	}

}
