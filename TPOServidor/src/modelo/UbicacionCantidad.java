package modelo;

import dto.UbicacionCantidadDTO;

public class UbicacionCantidad {
	
	public Ubicacion ubicacion;
	public int cantidad;

	public UbicacionCantidad() {
		ubicacion = null;
		cantidad = 0;
	}

	public Ubicacion getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public UbicacionCantidadDTO toDTO() {
		UbicacionCantidadDTO u = new UbicacionCantidadDTO();
		u.setUbicacion(ubicacion.toDTO());
		u.setCantidad(cantidad);
		return u;
	}

}
