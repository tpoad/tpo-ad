package modelo;

import controlador.ControladorGral;
import dto.ItemFacturaDTO;
import entities.ItemFacturaEntity;
import exceptions.ItemPrendaException;

public class ItemFactura {
	private ItemPrenda itemPrenda;
	private Integer cantidad;
	private float precioUnitario;

	public ItemFactura() {
		super();
	}

	public ItemFactura(ItemPrenda itemPrenda, Integer cantidad, float precioUnitario) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precioUnitario = precioUnitario;
	}

	public ItemPrenda getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrenda itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public float getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(float precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	@Override
	public String toString() {
		return "itemsFacturaDTO [itemPrenda=" + itemPrenda + ", cantidad=" + cantidad + ", precioUnitario="
				+ precioUnitario + "]";
	}

	public ItemFacturaEntity toEntity() {
		ItemFacturaEntity ent = new ItemFacturaEntity();
		ent.setItemPrenda(itemPrenda.toEntity());
		ent.setCantidad(cantidad);
		ent.setPrecioUnitario(precioUnitario);
		return ent;
	}

	public ItemFactura(ItemFacturaEntity ent) {
		precioUnitario = ent.getPrecioUnitario();
		cantidad = ent.getCantidad();
		try {
			itemPrenda = ControladorGral.getInstancia().obtenerItemPrenda(ent.getItemPrenda().getSubcodigo());
		} catch (ItemPrendaException e) {
			e.printStackTrace();
			System.out.println("ItemFactura: error en itemPrenda");
		}
	}

	public ItemFacturaDTO toDTO() {
		ItemFacturaDTO i = new ItemFacturaDTO();
		i.setItemPrenda(itemPrenda.toDTO());
		i.setCantidad(cantidad);
		i.setPrecioUnitario(precioUnitario);
		return i;
	}

}
