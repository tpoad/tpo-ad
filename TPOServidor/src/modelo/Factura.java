package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import controlador.ControladorGral;
import dto.FacturaDTO;
import entities.FacturaEntity;
import entities.ItemFacturaEntity;

public class Factura implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Cliente cliente;
	private Date fecha;
	private List<ItemFactura> itemsFactura;
	private float precioTotal;

	public Factura() {
		super();
	}

	public Factura(Cliente cliente, Date fecha, List<ItemFactura> itemsFactura, float precioTotal) {
		super();
		this.cliente = cliente;
		this.fecha = fecha;
		this.itemsFactura = itemsFactura;
		this.precioTotal = precioTotal;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<ItemFactura> getItemsFactura() {
		return itemsFactura;
	}

	public void setItemsFactura(List<ItemFactura> itemsFactura) {
		this.itemsFactura = itemsFactura;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public float getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(float precioTotal) {
		this.precioTotal = precioTotal;
	}

	@Override
	public String toString() {
		return "FacturaDTO [Cliente=" + cliente + ", fecha=" + fecha + ", itemFactura=" + itemsFactura
				+ ", precioTotal=" + precioTotal + "]";
	}

	public Factura(FacturaEntity e) {
		cliente = ControladorGral.getInstancia().obtenerCliente(e.getCliente().getCuit());
		fecha = e.getFecha();
		for (ItemFacturaEntity i : e.getItemsFactura()) {
			try {
				itemsFactura.add(new ItemFactura(i));
			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("Factura: tal vez el error est� en item prenda");
			}
		}
		precioTotal = e.getPrecioTotal();
	}

	public FacturaEntity toEntity() {
		FacturaEntity e = new FacturaEntity();
		e.setCliente(cliente.toEntity());
		e.setFecha(fecha);
		for (ItemFactura i : itemsFactura) {
			e.getItemsFactura().add(i.toEntity());
		}
		return e;
	}

	public FacturaDTO toDTO() {
		FacturaDTO f = new FacturaDTO();
		f.setCliente(cliente.toDTO());
		f.setFecha(fecha);
		for (ItemFactura i : itemsFactura) {
			f.addItemFactura(i.toDTO());
		}
		return f;
	}

}
