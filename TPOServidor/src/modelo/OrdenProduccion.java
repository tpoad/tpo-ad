package modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controlador.ControladorGral;
import dto.ItemPrendaDTO;
import dto.OrdenProduccionDTO;
import entities.OrdenProduccionEntity;
import enums.EstadoOrdenProduccion;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class OrdenProduccion{
	
	protected int loteProduccion;
	protected Date fechaGeneracion;
	protected PedidoCliente pedidoCliente;
	protected float costoTotal;
	protected EstadoOrdenProduccion estado;
	public EstadoOrdenProduccion getEstado() {
		return estado;
	}

	public void setEstado(EstadoOrdenProduccion estado) {
		this.estado = estado;
	}

	protected List<ItemPrenda> itemsPrenda;
	
	public OrdenProduccion() {
		super();
		itemsPrenda = new ArrayList<ItemPrenda>();
		estado = EstadoOrdenProduccion.Generada;
		
	}

	public OrdenProduccion(int loteProduccion, Date fechaGeneracion,
			PedidoCliente pedidoCliente, float costoTotal) {
		super();
		this.loteProduccion = loteProduccion;
		this.fechaGeneracion = fechaGeneracion;
		this.pedidoCliente = pedidoCliente;
		this.costoTotal = costoTotal;
	}

	public OrdenProduccion(OrdenProduccion op) {
		super();
		this.loteProduccion = op.getLoteProduccion();
		this.fechaGeneracion = op.getFechaGeneracion();
		this.pedidoCliente = op.getPedidoCliente();
		this.costoTotal = op.getCostoTotal();
	}

	public int getLoteProduccion() {
		return loteProduccion;
	}

	public void setLoteProduccion(int loteProduccion) {
		this.loteProduccion = loteProduccion;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public PedidoCliente getPedidoCliente() {
		return pedidoCliente;
	}

	public void setPedidoCliente(PedidoCliente pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}

	public float getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(float costoTotal) {
		this.costoTotal = costoTotal;
	}

	@Override
	public String toString() {
		return "OrdenProduccionDTO [loteProduccion=" + loteProduccion
				+ ", fechaGeneracion=" + fechaGeneracion + ", pedidoCliente=" + pedidoCliente + ", costoTotal="
				+ costoTotal + "]";
	}

	public List<ItemPrenda> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrenda> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}
	
	public OrdenProduccion(OrdenProduccionEntity o){
		loteProduccion = o.getLote();
		fechaGeneracion = o.getFechaGeneracion();
		pedidoCliente = ControladorGral.getInstancia().getPedidoCliente(o.getLote()); //ojo por ahi pincha
		costoTotal = o.getCostoTotal();
		estado = o.getEstado();
	}
	
	public OrdenProduccionEntity toEntity(){
		OrdenProduccionEntity e = new OrdenProduccionEntity();
		e.setCostoTotal(costoTotal);
		e.setEstado(estado);
		e.setFechaGeneracion(fechaGeneracion);
		e.setLote(loteProduccion);
		e.setPedidoCliente(pedidoCliente.toEntity());
		return e;
	}

	public OrdenProduccionDTO toDTO() {
		OrdenProduccionDTO o=new OrdenProduccionDTO();
		o.setLoteProduccion(loteProduccion);
		o.setFechaGeneracion(fechaGeneracion);
		o.setPedidoCliente(pedidoCliente.toDTO());
		o.setCostoTotal(costoTotal);
		o.setEstado(estado);
		//aca no falta el itemPrenda???
		return o;
	}
	
	public OrdenProduccion(OrdenProduccionDTO dto) throws SucursalException, ItemPrendaException {
		loteProduccion = dto.getLoteProduccion();
		fechaGeneracion = dto.getFechaGeneracion();
		pedidoCliente = new PedidoCliente(dto.getPedidoCliente());
		costoTotal = dto.getCostoTotal();
		estado = dto.getEstado();
		itemsPrenda = new ArrayList<ItemPrenda>();
		for(ItemPrendaDTO ipdto : dto.getItemsPrenda())
			itemsPrenda.add(new ItemPrenda(ipdto));
		
	}

}
