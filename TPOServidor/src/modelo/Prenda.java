package modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dto.ItemPrendaDTO;
import dto.PasoProduccionDTO;
import dto.PrendaDTO;
import entities.ItemPrendaEntity;
import entities.PasoProduccionEntity;
import entities.PrendaEntity;
import enums.Temporada;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class Prenda {

	private int codigo;
	private String descripcion;
	private Temporada temporada;
	private String molde;
	private List<ItemPrenda> itemsPrenda;
	private Integer estado;
	private List<PasoProduccion> pasosProduccion;
	private Float costoActual;
	private Float precioVenta;
	private Date tiempoProduccion;

	public Prenda() {
		super();
		itemsPrenda = new ArrayList<ItemPrenda>();
		pasosProduccion = new ArrayList<PasoProduccion>();

	}

	public Prenda(int codigo, String descripcion, Temporada temporada, String molde, List<ItemPrenda> itemsPrenda,
			Integer estado, List<PasoProduccion> pasosProduccion, Float costoActual, Float precioVenta,
			Date tiempoProduccion) {
		super();
		itemsPrenda = new ArrayList<ItemPrenda>();
		pasosProduccion = new ArrayList<PasoProduccion>();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.temporada = temporada;
		this.molde = molde;
		this.itemsPrenda = itemsPrenda;
		this.estado = estado;
		this.pasosProduccion = pasosProduccion;
		this.costoActual = costoActual;
		this.precioVenta = precioVenta;
		this.tiempoProduccion = tiempoProduccion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Temporada getTemporada() {
		return temporada;
	}

	public void setTemporada(Temporada temporada) {
		this.temporada = temporada;
	}

	public String getMolde() {
		return molde;
	}

	public void setMolde(String molde) {
		this.molde = molde;
	}

	public List<ItemPrenda> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrenda> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public List<PasoProduccion> getPasosProduccion() {
		return pasosProduccion;
	}

	public void setPasosProduccion(List<PasoProduccion> pasosProduccion) {
		this.pasosProduccion = pasosProduccion;
	}

	public Float getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(Float costoActual) {
		this.costoActual = costoActual;
	}

	public Float getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Float precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Date getTiempoProduccion() {
		return tiempoProduccion;
	}

	public void setTiempoProduccion(Date tiempoProduccion) {
		this.tiempoProduccion = tiempoProduccion;
	}

	@Override
	public String toString() {
		return "PrendaDTO [codigo=" + codigo + ", descripcion=" + descripcion + ", temporada=" + temporada + ", molde="
				+ molde + ", itemsPrenda=" + itemsPrenda + ", estado=" + estado + ", pasosProduccion=" + pasosProduccion
				+ ", costoActual=" + costoActual + ", precioVenta=" + precioVenta 
				+ ", tiempoProduccion=" + tiempoProduccion + "]";
	}

	public Prenda(PrendaEntity entity) {
		this.codigo = entity.getCodigo();
		this.descripcion = entity.getDescripcion();
		this.temporada = entity.getTemporada();
		this.molde = entity.getMolde();
		this.estado = entity.getEstado();
		this.costoActual = entity.getCostoActual();
		this.precioVenta = entity.getPrecioVenta();
		this.tiempoProduccion = entity.getTiempoProduccion();
		itemsPrenda = new ArrayList<ItemPrenda>();
		for (ItemPrendaEntity ie : entity.getItemsPrenda()) {
			itemsPrenda.add(new ItemPrenda(ie, this));
		}
		pasosProduccion = new ArrayList<PasoProduccion>();
		for (PasoProduccionEntity pe : entity.getPasosPrSoduccion()) {
			pasosProduccion.add(new PasoProduccion(pe));
		}

	}

	public PrendaEntity toEntity() {
		PrendaEntity e = new PrendaEntity();
		e.setCodigo(codigo);
		e.setDescripcion(descripcion);
		e.setTemporada(temporada);
		e.setMolde(molde);
		e.setEstado(estado);
		e.setCostoActual(costoActual);
		e.setPrecioVenta(precioVenta);
		e.setTiempoProduccion(tiempoProduccion);
		for (ItemPrenda i : itemsPrenda) {
			e.getItemsPrenda().add(i.toEntity());
		}
		for (PasoProduccion p : pasosProduccion) {
			e.getPasosPrSoduccion().add(p.toEntity());
		}
		return e;
	}

	public PrendaDTO toDTObobo() {
		PrendaDTO dto = new PrendaDTO();
		dto.setCodigo(codigo);
		dto.setDescripcion(descripcion);
		dto.setMolde(molde);
		dto.setEstado(estado);
		dto.setCostoActual(costoActual);
		dto.setPrecioVenta(precioVenta);
		dto.setTiempoProduccion(tiempoProduccion);
		dto.setTemporada(temporada);
		for (ItemPrenda i : itemsPrenda) {
			dto.getItemsPrenda().add(i.toDTOsinPrendaSinInsumos());
		}
		return dto;
	}

	public PrendaDTO toDTO() {
		return null;
	}

	public Prenda(PrendaDTO dto) throws SucursalException, ItemPrendaException {
		codigo = dto.getCodigo();
		descripcion = dto.getDescripcion();
		temporada = dto.getTemporada();
		molde = dto.getMolde();
		itemsPrenda = new ArrayList<ItemPrenda>();
		for(ItemPrendaDTO ipdto : dto.getItemsPrenda()){
			itemsPrenda.add(new ItemPrenda(ipdto));
		}
		estado = dto.getEstado();
		pasosProduccion = new ArrayList<PasoProduccion>();
		for(PasoProduccionDTO ppdto : dto.getPasosProduccion()){
			pasosProduccion.add(new PasoProduccion(ppdto));
		}
		costoActual = dto.getCostoActual();
		precioVenta = dto.getPrecioVenta();
		tiempoProduccion = dto.getTiempoProduccion();
	}

}
