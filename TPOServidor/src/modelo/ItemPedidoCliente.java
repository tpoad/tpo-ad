package modelo;

import java.io.Serializable;

import controlador.ControladorGral;
import dto.ItemPedidoClienteDTO;
import entities.ItemPedidoClienteEntity;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class ItemPedidoCliente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private ItemPrenda itemPrenda;
	private Integer cantidad;
	private Float precio;

	public ItemPedidoCliente() {
		super();
	}

	public ItemPedidoCliente(ItemPrenda itemPrenda, Integer cantidad, Float precio) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public ItemPedidoCliente(ItemPedidoCliente ip) {
		super();
		this.itemPrenda = ip.getItemPrenda();
		this.cantidad = ip.getCantidad();
		this.precio = ip.getPrecio();
	}

	public ItemPrenda getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrenda itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public ItemPedidoClienteEntity toEntity() {
		ItemPedidoClienteEntity ent = new ItemPedidoClienteEntity();
		ent.setCantidad(cantidad);
		ent.setPrecio(precio);
		ent.setItemPrenda(itemPrenda.toEntity());
		return ent;
	}

	public ItemPedidoCliente(ItemPedidoClienteEntity ent) {
		cantidad = ent.getCantidad();
		precio = ent.getPrecio();
		try {
			itemPrenda = ControladorGral.getInstancia().obtenerItemPrenda(ent.getItemPrenda().getSubcodigo());
		} catch (ItemPrendaException e) {
			e.printStackTrace();
			System.out.println("ItemPedidoCliente: error en itemPrenda");
		}
	}

	public ItemPedidoClienteDTO toDTO() {
		ItemPedidoClienteDTO dto = new ItemPedidoClienteDTO();
		dto.setCantidad(cantidad);
		dto.setPrecio(precio);
		dto.setItemPrenda(itemPrenda.toDTOsinPrendaSinInsumos());
		return dto;
	}

	public ItemPedidoCliente(ItemPedidoClienteDTO dto) throws SucursalException, ItemPrendaException {
		cantidad = dto.getCantidad();
		precio = dto.getPrecio();
		itemPrenda = new ItemPrenda(dto.getItemPrenda());
	}

	@Override
	public String toString() {
		return "ItemPedidoCliente [itemPrenda=" + itemPrenda + ", cantidad=" + cantidad + ", precio=" + precio + "]";
	}

}
