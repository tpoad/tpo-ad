package modelo;

import java.util.ArrayList;
import java.util.List;

import controlador.ControladorGral;
import dto.ClienteDTO;
import dto.EmpleadoDTO;
import dto.SucursalDTO;
import entities.ClienteEntity;
import entities.EmpleadoEntity;
import entities.SucursalEntity;

public class Sucursal {
	
	private int numero;
	private String nombre;
	private String direccion;
	private String horarios;
	private List<Empleado> empleados;
	private List<Cliente> clientes;

	public Sucursal() {
		super();
		empleados = new ArrayList<Empleado>();
		clientes = new ArrayList<Cliente>();
	}

	public Sucursal(int numero, String nombre, String direccion, String horarios, List<Empleado> empleados,
			List<Cliente> clientes) {
		super();
		this.numero = numero;
		this.nombre = nombre;
		this.direccion = direccion;
		this.horarios = horarios;
		this.empleados = empleados;
		this.clientes = clientes;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarios() {
		return horarios;
	}

	public void setHorarios(String horarios) {
		this.horarios = horarios;
	}

	public List<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	@Override
	public String toString() {
		return "SucursalDTO [numero=" + numero + ", nombre=" + nombre + ", direccion=" + direccion + ", horarios="
				+ horarios + ", empleados=" + empleados + ", clientes=" + clientes + "]";
	}

	public SucursalEntity toEntity() {
		SucursalEntity e = new SucursalEntity();
		e.setDireccion(direccion);
		e.setHorarios(horarios);
		e.setNombre(nombre);
		e.setNumero(numero);
		for (Empleado i : empleados) {
			e.getEmpleados().add(i.toEntity());
		}
		for (Cliente i : clientes) {
			e.getClientes().add(i.toEntity());
		}
		return e;
	}

	public Sucursal(SucursalEntity e) {
		direccion = e.getDireccion();
		horarios = e.getHorarios();
		nombre = e.getNombre();
		numero = e.getNumero();
		empleados = new ArrayList<Empleado>();
		clientes = new ArrayList<Cliente>();
		for (ClienteEntity i : e.getClientes()) {
			clientes.add(ControladorGral.getInstancia().obtenerCliente(i.getCuit()));
		}
		for (EmpleadoEntity i : e.getEmpleados()) {
			empleados.add(new Empleado(i));
		}
	}

	public SucursalDTO toDTO() {
		SucursalDTO s = new SucursalDTO();
		s.setDireccion(direccion);
		s.setHorarios(horarios);
		s.setNombre(nombre);
		s.setNumero(numero);
		for (Empleado e : empleados) {
			s.addEmpleados(e.toDTO());
		}
		for (Cliente c : clientes) {
			s.addClientes(c.toDTO());
		}
		return s;
	}

	public Sucursal(SucursalDTO dto) {
		numero = dto.getNumero();
		nombre = dto.getNombre();
		direccion = dto.getDireccion();
		horarios = dto.getHorarios();
		empleados = new ArrayList<Empleado>();
		for(EmpleadoDTO edto : dto.getEmpleados()){
			empleados.add(new Empleado(edto));
		}
		clientes = new ArrayList<Cliente>();
		for(ClienteDTO cdto : dto.getClientes()){
			clientes.add(new Cliente(cdto));
		}

	}

}
