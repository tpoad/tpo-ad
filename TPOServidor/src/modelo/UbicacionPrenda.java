package modelo;

import controlador.ControladorGral;
import dto.UbicacionPrendaDTO;
import entities.UbicacionEntity;
import entities.UbicacionPrendaEntity;
import exceptions.ItemPrendaException;
import exceptions.SucursalException;

public class UbicacionPrenda extends Ubicacion {

	private ItemPrenda item;

	public UbicacionPrenda() {
		super();
	}

	public UbicacionPrenda(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super(calle, bloque, estante, posicion, cantidad);
	}

	public ItemPrenda getItem() {
		return item;
	}

	public void setItem(ItemPrenda item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "UbicacionPrenda [item=" + item + ", calle=" + calle + ", bloque=" + bloque + ", estante=" + estante
				+ ", posicion=" + posicion + ", cantidad=" + cantidad + "]";
	}

	@Override
	public Boolean estaVacia() {
		return item == null;
	}

	public UbicacionPrenda(UbicacionPrendaEntity e) {
		super((UbicacionEntity) e);
		if (e.getItem() != null) {
			try {
				item = ControladorGral.getInstancia().obtenerItemPrenda(e.getItem().getSubcodigo());
			} catch (ItemPrendaException e1) {
				e1.printStackTrace();
				System.out.println("UbicacionPrenda: tal vez el error est� con itemPrenda");
			}
		} else{
			item = null;
		}
	}

	@Override
	public UbicacionEntity toEntity() {
		UbicacionPrendaEntity e = new UbicacionPrendaEntity();
		e.setCantidad(cantidad);
		if (item != null)
			e.setItem(item.toEntity());
		e.setUbicacion(getKey());
		return e;
	}

	public UbicacionPrenda(UbicacionPrendaDTO dto) throws SucursalException, ItemPrendaException {
		item = new ItemPrenda(dto.getItem());
	}

	/*
	 * public UbicacionPrendaDTO() { UbicacionPrendaDTO u=new
	 * UbicacionPrendaDTO(); u.setItem(item.toDTO()); }
	 */

}
