package modelo;

import dto.InsumoDTO;
import entities.InsumoEntity;

public class Insumo {
	
	private int codigoInterno;
	private String nombre;
	private Integer cantidadStock;
	private Integer cantidadMinima;
	private Integer cantidadComprar;
	private float costoActual;

	public Insumo() {
		super();
	}

	public Insumo(int codigoInterno, String nombre, Integer cantidadStock, Integer cantidadMinima,
			Integer cantidadComprar, float costoActual) {
		super();
		this.codigoInterno = codigoInterno;
		this.nombre = nombre;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadComprar = cantidadComprar;
		this.costoActual = costoActual;
	}
	
	public int getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(int codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCantidadStock() {
		return cantidadStock;
	}

	public void setCantidadStock(Integer cantidadStock) {
		this.cantidadStock = cantidadStock;
	}

	public Integer getCantidadMinima() {
		return cantidadMinima;
	}

	public void setCantidadMinima(Integer cantidadMinima) {
		this.cantidadMinima = cantidadMinima;
	}

	public Integer getCantidadComprar() {
		return cantidadComprar;
	}

	public void setCantidadComprar(Integer cantidadComprar) {
		this.cantidadComprar = cantidadComprar;
	}

	public float getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(float costoActual) {
		this.costoActual = costoActual;
	}

	@Override
	public String toString() {
		return "InsumoDTO [codigoInterno=" + codigoInterno + ", nombre=" + nombre + ", cantidadStock=" + cantidadStock
				+ ", cantidadMinima=" + cantidadMinima + ", cantidadComprar=" + cantidadComprar + ", costoActual="
				+ costoActual + "]";
	}

	public Insumo(InsumoEntity entity) {
		this.codigoInterno = entity.getCodigoInterno();
		this.nombre = entity.getNombre();
		this.cantidadStock = entity.getCantidadStock();
		this.cantidadMinima = entity.getCantidadMinima();
		this.cantidadComprar = entity.getCantidadComprar();
		this.costoActual = entity.getCostoActual();
	}

	public InsumoEntity toEntity() {
		InsumoEntity entity = new InsumoEntity();
		entity.setCodigoInterno(codigoInterno);
		entity.setNombre(nombre);
		entity.setCantidadStock(cantidadStock);
		entity.setCantidadMinima(cantidadMinima);
		entity.setCantidadComprar(cantidadComprar);
		entity.setCostoActual(costoActual);
		return entity;
	}

	public Insumo(InsumoDTO dto) {
		this.codigoInterno = dto.getCodigoInterno();
		this.nombre = dto.getNombre();
		this.cantidadStock = dto.getCantidadStock();
		this.cantidadMinima = dto.getCantidadMinima();
		this.cantidadComprar = dto.getCantidadComprar();
		this.costoActual = dto.getCostoActual();
	}

	public InsumoDTO toDTO() {
		InsumoDTO i = new InsumoDTO();
		i.setCodigoInterno(codigoInterno);
		i.setNombre(nombre);
		i.setCantidadStock(cantidadStock);
		i.setCantidadMinima(cantidadMinima);
		i.setCantidadComprar(cantidadComprar);
		i.setCostoActual(costoActual);
		return i;
	}

/**/public InsumoDTO toDTObobo() {
		InsumoDTO dto = new InsumoDTO();
		dto.setCodigoInterno(codigoInterno);
		dto.setCantidadComprar(cantidadComprar);
		dto.setCantidadMinima(cantidadMinima);
		dto.setCantidadStock(cantidadStock);
		dto.setCostoActual(costoActual);
		dto.setNombre(nombre);
		return dto;
	}

}
