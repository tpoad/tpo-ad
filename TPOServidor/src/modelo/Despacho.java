package modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import controlador.ControladorGral;
import dao.ClienteDAO;
import dao.FacturaDAO;
import dao.PedidoClienteDAO;
import dao.RemitoDAO;
import enums.EstadoPedidoCliente;
import exceptions.ClienteException;
import exceptions.FacturaException;
import exceptions.PedidoClienteException;
import exceptions.RemitoException;
import exceptions.UbicacionException;

public class Despacho {

	private List<PedidoCliente> pedidosClienteCompletos;
	static private Despacho instancia;

	private Despacho() {
		pedidosClienteCompletos = new ArrayList<PedidoCliente>();
		pedidosClienteCompletos = ControladorGral.getInstancia()
				.obtenerPedidosClienteEstado(EstadoPedidoCliente.TERMINADO);
	}

	static public Despacho getInstancia() {
		if (instancia == null)
			instancia = new Despacho();
		return instancia;
	}

	public Despacho(List<PedidoCliente> pedidosClienteCompletos) {
		this.pedidosClienteCompletos = pedidosClienteCompletos;
	}

	public List<PedidoCliente> getPedidosClienteCompletos() {
		return pedidosClienteCompletos;
	}

	public void setPedidosClienteCompletos(List<PedidoCliente> pedidosClienteCompletos) {
		this.pedidosClienteCompletos = pedidosClienteCompletos;
	}

	@Override
	public String toString() {
		return "Despacho]";
	}

	/**
	 * La idea es que haga el env�o (cambiar de estado al pedido), y luego el
	 * remito y la factura
	 * 
	 * @param idPedido
	 * @return
	 * @throws ClienteException
	 * @throws UbicacionException
	 * @throws PedidoClienteException 
	 */
	public void generarDespacho(PedidoCliente pc) throws ClienteException, UbicacionException, PedidoClienteException {
		System.out.println("Despacho: generarDespacho");
		if (pc != null) {
			generarFactura(pc);
			generarRemito(pc);
			Cliente cliente = new Cliente(pc.getCliente().toDTO());
			cliente.setSaldo(pc.getCliente().restarSaldo(pc.getTotal()));
			ClienteDAO.getInstancia().modificarCliente(cliente);
//			PedidoClienteDAO.getInstancia().altaPedidoCliente(p);
			PedidoCliente aux = PedidoClienteDAO.getInstancia().obtenerPedidoCliente(pc.getNumeroPedido());
			aux.setEstado(EstadoPedidoCliente.DESPACHADO);
			PedidoClienteDAO.getInstancia().modificarPedidoCliente(aux);
		} else {
			System.out.println("Despacho - generarDespacho: Error en el pedido cliente");
		}
	}

	// public List<UbicacionCantidad> generarDespacho(int idPedido) {
	// List<UbicacionCantidad> lista = new ArrayList<UbicacionCantidad>();
	// PedidoCliente p =
	// ControladorGral.getInstancia().getPedidoCliente(idPedido);
	// for (ItemPedidoCliente i : p.getItemsPedidoCliente()) {
	// lista.addAll(Almacen.getInstancia().retirarPrendas(i.getItemPrenda(),
	// i.getCantidad()));
	// }
	// return lista;
	// }

	public Factura generarFactura(PedidoCliente p) {
		System.out.println("Despacho: factura");
		Factura f = new Factura();
		try {
			f.setCliente(p.getCliente());
			Calendar hoy = Calendar.getInstance();
			hoy.set(Calendar.HOUR_OF_DAY, 0);
			f.setFecha(hoy.getTime());
			List<ItemFactura> lif = new ArrayList<ItemFactura>();
			for (ItemPedidoCliente ipc : p.getItemsPedidoCliente()) {
				lif.add(new ItemFactura(ipc.getItemPrenda(), ipc.getCantidad(), ipc.getPrecio()));
			}
			f.setItemsFactura(lif);
			f.setPrecioTotal(p.getTotal());
			// Llamar al DAO para persistir la factura
			FacturaDAO.getInstancia().guardarFactura(f);
		} catch (FacturaException e) {
			e.printStackTrace();
		}
		return f;
	}

	public Remito generarRemito(PedidoCliente p) {
		System.out.println("Despacho: remito");
		Remito r = new Remito();
		try {
			r.setCliente(p.getCliente());
			r.setDireccionEntrega(p.getCliente().getDireccion());
			Calendar hoy = Calendar.getInstance();
			hoy.set(Calendar.HOUR_OF_DAY, 0);
			r.setFecha(hoy.getTime());
			List<ItemRemito> lir = new ArrayList<ItemRemito>();
			for (ItemPedidoCliente ipc : p.getItemsPedidoCliente()) {
				lir.add(new ItemRemito(ipc.getItemPrenda(), ipc.getCantidad()));
			}
			r.setItemsRemito(lir);
			// Llamar al DAO para persistir el remito
			RemitoDAO.getInstancia().guardarRemito(r);
		} catch (RemitoException e) {
			e.printStackTrace();
		}
		return r;
	}

	public void cambiarEstadoPedidoCliente(PedidoCliente pc, EstadoPedidoCliente estado) {
		pc.setEstado(estado);
	}

//	public DespachoDTO toDTO() {
//		DespachoDTO d = new DespachoDTO();
//		for (PedidoCliente p : pedidosClienteCompletos) {
//			d.addPedidosClienteCompleto(p.toDTO());
//		}
//		return d;
//	}

}
