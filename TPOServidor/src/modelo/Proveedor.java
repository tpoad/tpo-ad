package modelo;

import dto.ProveedorDTO;
import entities.ProveedorEntity;

public class Proveedor 
{
	private String razonSocial;
	private String cuit;
	private String direccion;
	private String condicionesCobro;
	
	public Proveedor() {
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCondicionesCobro() {
		return condicionesCobro;
	}

	public void setCondicionesCobro(String condicionesCobro) {
		this.condicionesCobro = condicionesCobro;
	}

	@Override
	public String toString() {
		return "ProveedorDTO [razonSocial=" + razonSocial + ", cuit=" + cuit + ", direccion=" + direccion
				+ ", condicionesCobro=" + condicionesCobro + "]";
	}
	
	public ProveedorEntity toEntity(){
		ProveedorEntity e = new ProveedorEntity();
		e.setCondicionesCobro(condicionesCobro);
		e.setCuit(cuit);
		e.setDireccion(direccion);
		e.setRazonSocial(razonSocial);
		return e;
	}
	
	public Proveedor(ProveedorEntity e){
		cuit = e.getCuit();
		condicionesCobro = e.getCondicionesCobro();
		direccion = e.getDireccion();
		razonSocial = e.getRazonSocial();
	}

	public ProveedorDTO toDTO() {
		ProveedorDTO p=new ProveedorDTO();
		p.setCuit(cuit);
		p.setCondicionesCobro(condicionesCobro);
		p.setDireccion(direccion);
		p.setRazonSocial(razonSocial);
		return p;
	}
	
	
	
}
