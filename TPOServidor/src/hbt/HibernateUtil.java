package hbt;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import entities.*;

/**
 * @author lvommaro
 *
 */
public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			AnnotationConfiguration config = new AnnotationConfiguration();

			config.addAnnotatedClass(AreaProduccionEntity.class);
			config.addAnnotatedClass(ClienteEntity.class);
			config.addAnnotatedClass(EmpleadoEntity.class);
			config.addAnnotatedClass(FacturaEntity.class);
			config.addAnnotatedClass(InsumoEntity.class);
			config.addAnnotatedClass(InsumoUsadoEntity.class);
			config.addAnnotatedClass(ItemFacturaEntity.class);
			config.addAnnotatedClass(ItemPedidoClienteEntity.class);
			config.addAnnotatedClass(ItemPrendaEntity.class);
			config.addAnnotatedClass(ItemRemitoEntity.class);
			config.addAnnotatedClass(LineaProduccionEntity.class);
			config.addAnnotatedClass(MovimientoEntity.class);
			config.addAnnotatedClass(OrdenProduccionCompletaEntity.class);
			config.addAnnotatedClass(OrdenProduccionEntity.class);
			config.addAnnotatedClass(OrdenProduccionParcialEntity.class);
			config.addAnnotatedClass(PasoProduccionEntity.class);
			config.addAnnotatedClass(PedidoClienteEntity.class);
			config.addAnnotatedClass(PedidoInsumoEntity.class);
			config.addAnnotatedClass(PrendaEntity.class);
			config.addAnnotatedClass(ProveedorEntity.class);
			config.addAnnotatedClass(RemitoEntity.class);
			config.addAnnotatedClass(SucursalEntity.class);
			config.addAnnotatedClass(UbicacionEntity.class);
			config.addAnnotatedClass(UbicacionInsumoEntity.class);
			config.addAnnotatedClass(UbicacionKey.class);
			config.addAnnotatedClass(UbicacionPrendaEntity.class);
			

			sessionFactory = config.buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Error en creacion de SessionFactory: " + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

}
