package dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import entities.PedidoInsumoEntity;
import exceptions.PedidoInsumoException;
import hbt.HibernateUtil;
import modelo.PedidoInsumo;

public class PedidoInsumoDAO {
	private static PedidoInsumoDAO instancia;
	private static SessionFactory sf = null;
	
	private PedidoInsumoDAO() {
		super();
	}

	public static PedidoInsumoDAO getInstancia() {
		if (instancia == null){
			sf = HibernateUtil.getSessionFactory();
			instancia = new PedidoInsumoDAO();
			}
		return instancia;
	}

	public PedidoInsumo obtenerPedidoInsumo(int lote) throws PedidoInsumoException{
		PedidoInsumo p = null;
		try{
			PedidoInsumoEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM PedidoInsumoEntity c WHERE c.lote = :lote");
			query.setInteger("lote", lote);
			entity = (PedidoInsumoEntity) query.list().get(0);
			if(entity != null){
				p = new PedidoInsumo(entity);
			}
			session.flush();
			session.close();
			
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new PedidoInsumoException("No se pudo obtener el pedido de insumo.");
		}
		return p;
	}
	
	public void guardarPedidoInsumo(PedidoInsumo item) throws PedidoInsumoException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.close();
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new PedidoInsumoException("No se pudo guardar el pedido de insumo.");
		}
	}
	
}
