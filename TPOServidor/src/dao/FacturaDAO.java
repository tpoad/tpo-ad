
package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.FacturaEntity;
import exceptions.FacturaException;
import hbt.HibernateUtil;
import modelo.Factura;

public class FacturaDAO {
	private static FacturaDAO instancia;
	private static SessionFactory sf = null;

	private FacturaDAO() {
		super();
	}

	public static FacturaDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new FacturaDAO();
		}
		return instancia;
	}

	
	@SuppressWarnings("unchecked")
	public List<Factura> obtenerFacturasCliente(String cuit) throws FacturaException{
		List<Factura> resultado = new ArrayList<Factura>();
		try{
			List<FacturaEntity> listadoQuery = new ArrayList<FacturaEntity>();
			
			Session sesion = sf.openSession();
			Transaction tran = sesion.beginTransaction();
			Query query = (Query) sesion.createQuery("FROM FacturaEntity f WHERE f.cliente.cuit = :cuit");
			query.setString("cuit", cuit);
			listadoQuery = (ArrayList<FacturaEntity>) query.list();
			tran.commit();
			sesion.close();
			
			for (FacturaEntity p : listadoQuery) {
				resultado.add(new Factura(p));
			}
			
			return resultado;
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new FacturaException("No se pudo obtener las facturas.");
		}
	}
	
	public void guardarFactura(Factura factura) throws FacturaException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(factura.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		}
		catch(Exception e){
			System.out.println(e);
			throw new FacturaException("Factura DAO: No se pudo guardar la factura");
		}
	}
	
}
