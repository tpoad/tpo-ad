package dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import converters.ItemPedidoClienteConverter;
import dto.ItemPedidoClienteDTO;
import entities.ItemPedidoClienteEntity;
import hbt.HibernateUtil;

/**
 * @author zuki Ac� se hacen las CRUD en HQL (create, read, update, and delete)
 */
public class ItemPedidoClienteDAO {

	private static SessionFactory sf = null;
	private static ItemPedidoClienteDAO instancia;

	private ItemPedidoClienteDAO() {
		super();
	}

	public static ItemPedidoClienteDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new ItemPedidoClienteDAO();
		}
		return instancia;
	}

	public boolean altaItemPedidoCliente(ItemPedidoClienteDTO itemPedidoClienteDTO) {
		Session session = sf.openSession();
		ItemPedidoClienteEntity itemPedidoCliente = new ItemPedidoClienteEntity();
		itemPedidoCliente = ItemPedidoClienteConverter.itemPedidoClienteToEntity(itemPedidoClienteDTO);
		session.save(itemPedidoCliente);
		session.getTransaction().commit();
		session.close();
		return true;
	}
	
	

}
