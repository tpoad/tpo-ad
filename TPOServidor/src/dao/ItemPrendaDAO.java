package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import controlador.ControladorGral;
import entities.ItemPrendaEntity;
import exceptions.ItemPrendaException;
import hbt.HibernateUtil;
import modelo.ItemPrenda;
import modelo.Prenda;

/**
 * @author zuki Ac� se hacen las CRUD en HQL (create, read, update, and delete)
 */
public class ItemPrendaDAO {

	private static SessionFactory sf = null;
	private static ItemPrendaDAO instancia;

	private ItemPrendaDAO() {
		super();
	}

	public static ItemPrendaDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new ItemPrendaDAO();
		}
		return instancia;
	}

	@SuppressWarnings("unchecked")
	public List<ItemPrenda> obtenerItemsPrenda(Prenda prenda) throws ItemPrendaException{
		List<ItemPrenda> item = new ArrayList<ItemPrenda>();
		try{
			List<ItemPrendaEntity> entity = new ArrayList<ItemPrendaEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ItemPrendaEntity c WHERE c.prenda.codigo = :codigo");
			query.setInteger("codigo", prenda.getCodigo());
			entity = (ArrayList<ItemPrendaEntity>) query.list();
			for (ItemPrendaEntity p : entity) {
				item.add(new ItemPrenda(p,ControladorGral.getInstancia().obtenerPrenda(p.getPrenda().getCodigo())));
			}	
			tran.commit();
			session.flush();
			session.close();
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new ItemPrendaException("ItemPrendaDAO: No se pudo obtener los items prenda para una prenda");
		}
		return item;
	}

	@SuppressWarnings("unchecked")
	public List<ItemPrenda> obtenerTodosItemsPrenda() throws ItemPrendaException{
		List<ItemPrenda> resultado = new ArrayList<ItemPrenda>();
		try{
			List<ItemPrendaEntity> listadoQuery = new ArrayList<ItemPrendaEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ItemPrendaEntity");
			listadoQuery = (ArrayList<ItemPrendaEntity>) query.list();
			for (ItemPrendaEntity i : listadoQuery) {
				resultado.add(new ItemPrenda(i));
			}
			tran.commit();
			session.flush();
			session.close();
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new ItemPrendaException("ItemPrendaDAO: No se pudo obtener todos los items prenda");
		}
		return resultado;
	}

	public ItemPrenda obtenerItemPrenda(int subcodigo) throws ItemPrendaException{
		ItemPrenda item = null;
		try{
			ItemPrendaEntity entity = null;
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ItemPrendaEntity c WHERE c.subcodigo = :subcodigo");
			query.setInteger("subcodigo", subcodigo);
			entity = (ItemPrendaEntity) query.list().get(0);
			if(entity != null){
				item = new ItemPrenda(entity, ControladorGral.getInstancia().obtenerPrenda(entity.getPrenda().getCodigo()));
			}
			tran.commit();
			session.flush();
			session.close();
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new ItemPrendaException("ItemPrendaDAO: No se pudo obtener el ItemPrenda.");
		}
		return item;
	}
	
	public void guardarItemPrenda(ItemPrenda item) throws ItemPrendaException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new ItemPrendaException("ItemPrendaDAO: No se pudo guardar el itemprenda.");
		}
	}
	
	public void eliminarItemPrenda(int subcodigo) {
		try {
			ItemPrendaEntity entity = null;
			Session session = sf.openSession();
			session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ItemPrendaEntity i WHERE i.subcodigo = :subcodigo");
			query.setInteger("subcodigo", subcodigo);
			entity = (ItemPrendaEntity) query.list().get(0);
			session.delete(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("ItemPrendaDAO. No se pudo eliminar el item prenda");
		}
	}

	public void modificarItemPrenda(ItemPrenda itemPrenda) {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			ItemPrendaEntity entity = new ItemPrendaEntity();
			entity = itemPrenda.toEntity();
			session.update(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("ItemPrendaDAO: No se pudo modificar el item prenda");
		}
	}

	public ItemPrenda obtenerUltimoItemPrenda() {
		ItemPrenda item = null;
		try{
			ItemPrendaEntity entity = null;
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ItemPrendaEntity ORDER BY subcodigo DESC");
			query.setMaxResults(1);
			entity = (ItemPrendaEntity) query.uniqueResult();
			if(entity != null){
				item = new ItemPrenda(entity, ControladorGral.getInstancia().obtenerPrenda(entity.getPrenda().getCodigo()));
			}
			tran.commit();
			session.flush();
			session.close();
		}
		catch (Exception e){
			System.out.println(e);
			System.out.println("ItemPrendaDAO: No se pudo obtener el �ltimo item prenda");
		}
		return item;
	}
	
}
