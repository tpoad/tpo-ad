package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.InsumoEntity;
import exceptions.InsumoException;
import hbt.HibernateUtil;
import modelo.Insumo;

public class InsumoDAO {

	private static InsumoDAO instancia;
	private static SessionFactory sf = null;

	private InsumoDAO() {
		super();
	}

	public static InsumoDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new InsumoDAO();
		}
		return instancia;
	}

	public void guardarInsumo(Insumo insumo) throws InsumoException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(insumo.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new InsumoException("InsumoDAO: No se pudo guardar el insumo");
		}
	}

	@SuppressWarnings({ "unchecked" })
	public List<Insumo> obtenerInsumos() throws InsumoException {
		List<Insumo> resultado = new ArrayList<Insumo>();
		try {
			List<InsumoEntity> listadoQuery = new ArrayList<InsumoEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM InsumoEntity");
			listadoQuery = (ArrayList<InsumoEntity>) query.list();
			for (InsumoEntity p : listadoQuery) {
				resultado.add(new Insumo(p));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new InsumoException("InsumoDAO: No se pudo obtener los insumos");
		}
	}

	public Insumo obtenerInsumo(int id) throws InsumoException {
		Insumo p = null;
		try {
			InsumoEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM InsumoEntity e WHERE e.id = :id");
			query.setInteger("id", id);
			entity = (InsumoEntity) query.list().get(0);
			if (entity != null) {
				p = new Insumo(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new InsumoException("InsumoDAO: No se pudo obtener el insumo");
		}
		return p;
	}

}
