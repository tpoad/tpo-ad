package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.AreaProduccionEntity;
import exceptions.AreaProduccionException;
import hbt.HibernateUtil;
import modelo.AreaProduccion;

public class AreaProduccionDAO {


	private static AreaProduccionDAO instancia;
	private static SessionFactory sf = null;

	private AreaProduccionDAO() {
		super();
	}

	public static AreaProduccionDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new AreaProduccionDAO();
		}
		return instancia;
	}
	
	
	public AreaProduccion obtenerArea(String nombre) throws AreaProduccionException{
		AreaProduccion p = null;
		try{
			AreaProduccionEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM AreaProduccionEntity a WHERE a.nombre = :nombre");
			query.setString("nombre", nombre);
			entity = (AreaProduccionEntity) query.list().get(0);
			if(entity != null){
				p = new AreaProduccion(entity);
			}
			session.flush();
			session.close();
			
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new AreaProduccionException("No se pudo obtener el area.");
		}
		return p;
	}
	
	public void guardarAreaProduccion(AreaProduccion area) throws AreaProduccionException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			AreaProduccionEntity entity = area.toEntity();
			session.saveOrUpdate(entity);
			session.getTransaction().commit();
			session.close();
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new AreaProduccionException("No se pudo guardar el area.");
		}
	}

	@SuppressWarnings("unchecked")
	public List<AreaProduccion> obtenerAreas() throws AreaProduccionException {
		List<AreaProduccion> resultado = new ArrayList<AreaProduccion>();
		try {
			List<AreaProduccionEntity> listadoQuery = new ArrayList<AreaProduccionEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM AreaProduccionEntity");
			listadoQuery = (ArrayList<AreaProduccionEntity>) query.list();
			for (AreaProduccionEntity p : listadoQuery) {
				resultado.add(new AreaProduccion(p));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new AreaProduccionException("AreaProduccionDAO: No se pudo obtener las areas");
		}
	}


	 
}
