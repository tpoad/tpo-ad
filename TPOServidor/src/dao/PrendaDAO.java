package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.PrendaEntity;
import exceptions.PrendaException;
import hbt.HibernateUtil;
import modelo.Prenda;

/**
 * @author zuki Ac� se hacen las CRUD en HQL (create, read, update, and delete)
 */
public class PrendaDAO {

	private static PrendaDAO instancia;
	private static SessionFactory sf = null;

	private PrendaDAO() {
		super();
	}

	public static PrendaDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new PrendaDAO();
		}
		return instancia;
	}

	@SuppressWarnings("unchecked")
	public List<Prenda> obtenerPrendas() throws PrendaException {
		List<Prenda> resultado = new ArrayList<Prenda>();
		try {
			List<PrendaEntity> listadoQuery = new ArrayList<PrendaEntity>();
			Session sesion = sf.openSession();
			Transaction tran = sesion.beginTransaction();
			Query query = (Query) sesion.createQuery("FROM PrendaEntity");
			listadoQuery = (ArrayList<PrendaEntity>) query.list();
			for (PrendaEntity p : listadoQuery) {
				resultado.add(new Prenda(p));
			}
			tran.commit();
			sesion.close();
			return resultado;
		}

		catch (Exception e) {
			System.out.println(e);
			throw new PrendaException("PrendaDAO: No se pudo listar las prendas");
		}
	}

	public Prenda obtenerPrenda(int codigo) throws PrendaException {
		Prenda item = null;
		try {
			PrendaEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM PrendaEntity c WHERE c.codigo = :codigo");
			query.setInteger("codigo", codigo);
			entity = (PrendaEntity) query.list().get(0);
			if (entity != null) {
				item = new Prenda(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new PrendaException("PrendaDAO: No se pudo obtener la prenda");
		}
		return item;
	}

	public void guardarPrenda(Prenda item) throws PrendaException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new PrendaException("PrendaDAO: No se pudo guardar la prenda");
		}
	}

	public boolean altaPrenda(Prenda prenda) throws PrendaException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(prenda.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
			return true;
		} catch (Exception e) {
			System.out.println(e);
			throw new PrendaException("PrendaDAO: No se pudo guardar la prenda");
		}
	}

	public void eliminarPrenda(int codigo) throws PrendaException {
//		Prenda p = null;
		try {
			PrendaEntity entity = null;
			Session session = sf.openSession();
			session.beginTransaction();
			Query query = (Query) session.createQuery("FROM PrendaEntity p WHERE p.codigo = :codigo");
			query.setInteger("codigo", codigo);
			entity = (PrendaEntity) query.list().get(0);
//			if (entity != null) {
//				p = new Prenda(entity);
//			}
			session.delete(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("PrendaDAO. No se pudo eliminar la prenda");
		}
	}

	public void modificarPrenda(Prenda prenda) throws PrendaException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			PrendaEntity entity = new PrendaEntity();
			entity = prenda.toEntity();
			session.update(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("PrendaDAO. No se pudo modificar la prenda");
		}
	}

}
