package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.OrdenProduccionCompletaEntity;
import entities.OrdenProduccionEntity;
import entities.OrdenProduccionParcialEntity;
import exceptions.OrdenProduccionException;
import hbt.HibernateUtil;
import modelo.OrdenProduccion;
import modelo.OrdenProduccionCompleta;
import modelo.OrdenProduccionParcial;

public class OrdenProduccionDAO {

	private static OrdenProduccionDAO instancia;
	private static SessionFactory sf = null;

	private OrdenProduccionDAO() {
		super();
	}

	public static OrdenProduccionDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new OrdenProduccionDAO();
		}
		return instancia;
	}
	
	public OrdenProduccion obtenerOrden(int lote) throws OrdenProduccionException{
		OrdenProduccion p = null;
		try{
			OrdenProduccionEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM OrdenProduccionEntity c WHERE c.lote = :lote");
			query.setInteger("lote", lote);
			entity = (OrdenProduccionEntity) query.list().get(0);
			if(entity != null){
				//me va a devolver el tipo del padre, tengo que ver a qu� tipo de hijo corresponde
				if(entity.getClass() == OrdenProduccionCompletaEntity.class){
					p = new OrdenProduccionCompleta((OrdenProduccionCompletaEntity)entity);
				}
				else if(entity.getClass() == OrdenProduccionParcialEntity.class){
					p = new OrdenProduccionParcial((OrdenProduccionParcialEntity)entity);
				}
			}
			session.flush();
			session.close();
			
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new OrdenProduccionException("No se pudo obtener la orden de produccion.");
		}
		return p;
	}
	
	public void guardarOrdenProduccion(OrdenProduccion item) throws OrdenProduccionException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.saveOrUpdate(item.toEntity());
			session.getTransaction().commit();
			session.close();
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new OrdenProduccionException("No se pudo guardar la orden de produccion.");
		}
	}

	@SuppressWarnings("unchecked")
	public List<OrdenProduccion> obtenerOrdenesCopletas() throws OrdenProduccionException {
		List<OrdenProduccion> resultado = new ArrayList<OrdenProduccion>();
		try {
			List<OrdenProduccionEntity> listadoQuery = new ArrayList<OrdenProduccionEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM OrdenProduccionEntity o WHERE o.estado = 'Completada'");
			listadoQuery = (ArrayList<OrdenProduccionEntity>) query.list();
			for (OrdenProduccionEntity p : listadoQuery) {
				OrdenProduccion orden = null;
				if(p.getClass() == OrdenProduccionCompletaEntity.class){
					orden = new OrdenProduccionCompleta((OrdenProduccionCompletaEntity)p);
				}
				else if(p.getClass() == OrdenProduccionParcialEntity.class){
					orden = new OrdenProduccionParcial((OrdenProduccionParcialEntity)p);
				}
				resultado.add(orden);
			}
			
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new OrdenProduccionException("No se pudo obtener las ordenes completadas");
		}
	}
}
