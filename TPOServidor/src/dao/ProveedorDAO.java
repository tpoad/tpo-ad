package dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import entities.ProveedorEntity;
import exceptions.ProveedorException;
import hbt.HibernateUtil;
import modelo.Proveedor;

public class ProveedorDAO {
	
	private static ProveedorDAO instancia;
	private static SessionFactory sf = null;

	private ProveedorDAO() {
		super();
	}

	public static ProveedorDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new ProveedorDAO();
		}
		return instancia;
	}
	
	
	public Proveedor obtenerProveedor(String razonSocial) throws ProveedorException{
		Proveedor p = null;
		try{
			ProveedorEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM ProveedorEntity c WHERE c.razonSocial = :razonSocial");
			query.setString("razonSocial", razonSocial);
			entity = (ProveedorEntity) query.list().get(0);
			if(entity != null){
				p = new Proveedor(entity);
			}
			session.flush();
			session.close();
			
		}
		catch (Exception e){
			System.out.println(e.getMessage());	
			throw new ProveedorException("No se pudo obtener la sucursal.");
		}
		return p;
	}
	
	public void guardarProveedor(Proveedor item) throws ProveedorException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.close();
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new ProveedorException("No se pudo guardar el proveedor.");
		}
	}
	
}
