package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.RemitoEntity;
import exceptions.RemitoException;
import hbt.HibernateUtil;
import modelo.Remito;

public class RemitoDAO {

	private static RemitoDAO instancia;
	private static SessionFactory sf = null;

	private RemitoDAO() {
		super();
	}

	public static RemitoDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new RemitoDAO();
		}
		return instancia;
	}

	
	@SuppressWarnings("unchecked")
	public List<Remito> obtenerRemitosCliente(String cuit) throws RemitoException{
		List<Remito> resultado = new ArrayList<Remito>();
		try{
			List<RemitoEntity> listadoQuery = new ArrayList<RemitoEntity>();
			
			Session sesion = sf.openSession();
			Transaction tran = sesion.beginTransaction();
			Query query = (Query) sesion.createQuery("FROM RemitoEntity f WHERE f.cliente.cuit = :cuit");
			query.setString("cuit", cuit);
			listadoQuery = (ArrayList<RemitoEntity>) query.list();
			tran.commit();
			sesion.close();
			
			for (RemitoEntity p : listadoQuery) {
				resultado.add(new Remito(p));
			}
			
			return resultado;
		}
		
		catch(Exception e){
			System.out.println(e);
			throw new RemitoException("No se pudo obtener los remitos.");
		}
	}
	
	public void guardarRemito(Remito remito) throws RemitoException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(remito.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		}
		catch(Exception e){
			System.out.println(e);
			throw new RemitoException("Remito DAO: No se pudo guardar el remito");
		}
	}

}
