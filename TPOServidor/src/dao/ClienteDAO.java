package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.ClienteEntity;
import exceptions.ClienteException;
import hbt.HibernateUtil;
import modelo.Cliente;

public class ClienteDAO {

	private static ClienteDAO instancia;
	private static SessionFactory sf = null;

	private ClienteDAO() {
		super();
	}

	public static ClienteDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new ClienteDAO();
		}
		return instancia;
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> obtenerClientes() throws ClienteException {
		List<Cliente> resultado = new ArrayList<Cliente>();
		try {
			List<ClienteEntity> listadoQuery = new ArrayList<ClienteEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ClienteEntity");
			listadoQuery = (ArrayList<ClienteEntity>) query.list();
			for (ClienteEntity p : listadoQuery) {
				resultado.add(new Cliente(p));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new ClienteException("ClienteDAO: No se pudo obtener los clientes");
		}
	}

	public Cliente obtenerCliente(String cuit) throws ClienteException {
		Cliente p = null;
		try {
			ClienteEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM ClienteEntity c WHERE c.cuit = :cuit");
			query.setString("cuit", cuit);
			entity = (ClienteEntity) query.list().get(0);
			if (entity != null) {
				p = new Cliente(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new ClienteException("ClienteDAO: No se pudo obtener al cliente");
		}
		return p;
	}

	public void guardarCliente(Cliente cliente) throws ClienteException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(cliente.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new ClienteException("ClienteDAO: No se pudo guardar el cliente");
		}
	}

	@SuppressWarnings("unused")
	public void eliminarCliente(String cuit) throws ClienteException {
		Cliente p = null;
		try {
			ClienteEntity entity = null;
			Session session = sf.openSession();
			session.beginTransaction();
			Query query = (Query) session.createQuery("FROM ClienteEntity c WHERE c.cuit = :cuit");
			query.setString("cuit", cuit);
			entity = (ClienteEntity) query.list().get(0);
			if (entity != null) {
				p = new Cliente(entity);
			}
			session.delete(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("ClienteDAO. No se pudo eliminar al cliente");
		}
	}

	public void modificarCliente(Cliente cliente) throws ClienteException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			ClienteEntity entity = new ClienteEntity();
			entity = cliente.toEntity();
			session.update(entity);
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public Cliente obtenerClienteRazonSocial(String nombre) throws ClienteException {
		Cliente p = null;
		try {
			ClienteEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM ClienteEntity c WHERE c.razonSocial LIKE :nombre");
			query.setString("nombre", nombre);
			entity = (ClienteEntity) query.list().get(0);
			if (entity != null) {
				p = new Cliente(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new ClienteException("ClienteDAO: No se pudo obtener al cliente");
		}
		return p;
	}


}
