package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.SucursalEntity;
import exceptions.SucursalException;
import hbt.HibernateUtil;
import modelo.Sucursal;

public class SucursalDAO {

	private static SucursalDAO instancia;
	private static SessionFactory sf = null;

	private SucursalDAO() {
		super();
	}

	public static SucursalDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new SucursalDAO();
		}
		return instancia;
	}

	@SuppressWarnings("unchecked")
	public List<Sucursal> obtenerSucursales() throws SucursalException {
		List<Sucursal> resultado = new ArrayList<Sucursal>();
		try {
			List<SucursalEntity> listadoQuery = new ArrayList<SucursalEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM SucursalEntity");
			listadoQuery = (ArrayList<SucursalEntity>) query.list();
			for (SucursalEntity s : listadoQuery) {
				resultado.add(new Sucursal(s));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new SucursalException("SucursalDAO: No se pudo obtener las sucursales");
		}
	}

	public Sucursal obtenerSucursal(int nro) throws SucursalException {
		Sucursal p = null;
		try {
			SucursalEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("FROM SucursalEntity c WHERE c.numero = :numero");
			query.setInteger("numero", nro);
			entity = (SucursalEntity) query.list().get(0);
			if (entity != null) {
				p = new Sucursal(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SucursalException("No se pudo obtener la sucursal.");
		}
		return p;
	}

	public void guardarSucursal(Sucursal sucursal) throws SucursalException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(sucursal.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new SucursalException("SucursalDAO: No se pudo guardar la sucursal");
		}
	}

	public Sucursal obtenerSucursalCliente(String cuit) throws SucursalException {
		Sucursal p = null;
		try {
			SucursalEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session.createQuery("SELECT s FROM SucursalEntity s INNER JOIN s.clientes c WHERE c.cuit = :cuit");
			query.setString("cuit", cuit);
			entity = (SucursalEntity) query.list().get(0);
			if (entity != null) {
				p = new Sucursal(entity);
			}
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SucursalException("No se pudo obtener la sucursal.");
		}
		return p;
	}
	
	
	
	
	
	
	
	
	
	
}
