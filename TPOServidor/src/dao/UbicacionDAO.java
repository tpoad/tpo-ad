package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.UbicacionEntity;
import entities.UbicacionInsumoEntity;
import entities.UbicacionKey;
import entities.UbicacionPrendaEntity;
import exceptions.UbicacionException;
import hbt.HibernateUtil;
import modelo.Ubicacion;
import modelo.UbicacionInsumo;
import modelo.UbicacionPrenda;

public class UbicacionDAO {
	
	private static UbicacionDAO instancia;
	private static SessionFactory sf = null;

	private UbicacionDAO() {
		super();
	}

	public static UbicacionDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new UbicacionDAO();
		}
		return instancia;
	}
	
	@SuppressWarnings("unchecked")
	public List<UbicacionPrenda> obtenerUbicacionesPrendas() throws UbicacionException{
//	public List<Ubicacion> obtenerUbicacionesPrendas() throws UbicacionException{
		List<UbicacionPrenda> resultado = new ArrayList<UbicacionPrenda>();
//		List<Ubicacion> resultado = new ArrayList<Ubicacion>();
		try{
			List<UbicacionPrendaEntity> listadoQuery = new ArrayList<UbicacionPrendaEntity>();
//			List<UbicacionEntity> listadoQuery = new ArrayList<UbicacionEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM UbicacionPrendaEntity");
			listadoQuery = (ArrayList<UbicacionPrendaEntity>) query.list();
//			listadoQuery = (ArrayList<UbicacionEntity>) query.list();
			for (UbicacionPrendaEntity entity : listadoQuery) {
//			for (UbicacionEntity entity : listadoQuery) {
				resultado.add(new UbicacionPrenda(entity));
//				resultado.add(new Ubicacion(entity));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch(Exception e){
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo obtener las ubicaciones de las prendas");
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<UbicacionInsumo> obtenerUbicacionesInsumos() throws UbicacionException{
		List<UbicacionInsumo> resultado = new ArrayList<UbicacionInsumo>();
		try{
			List<UbicacionInsumoEntity> listadoQuery = new ArrayList<UbicacionInsumoEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM UbicacionInsumoEntity");
			listadoQuery = (ArrayList<UbicacionInsumoEntity>) query.list();
			for (UbicacionInsumoEntity entity : listadoQuery) {
				resultado.add(new UbicacionInsumo(entity));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		}
		catch(Exception e){
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo obtener las ubicaciones de los insumos");
		}
	}
	
	public Ubicacion ObtenerUbicacion(String calle, int bloque, int estante, int posicion) throws UbicacionException{		
		Ubicacion ubi = null;
		try{
			UbicacionKey key = new UbicacionKey();
			key.setCalle(calle);
			key.setBloque(bloque);
			key.setEstante(estante);
			key.setPosicion(posicion);
			
			Session session = sf.openSession();
			UbicacionEntity entity = (UbicacionEntity) session.get(UbicacionEntity.class, key);
			
			if(entity != null){
				if(entity.getClass() == UbicacionPrendaEntity.class){
					ubi = new UbicacionPrenda((UbicacionPrendaEntity)entity) ;
				}
				else if(entity.getClass() == UbicacionInsumoEntity.class){
					ubi = new UbicacionInsumo((UbicacionInsumoEntity)entity);
				}
			}
			session.flush();
			session.close();
		}
		catch(Exception e){
			System.out.println(e.getStackTrace());
			throw new UbicacionException("Ubicación DAO: No se puede obtener la ubicacion");
		}
		return ubi;
	}
	
	/*
	public void guardarUbicacion(Ubicacion item) throws UbicacionException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.close();
		}
		catch(Exception e){
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo guardar la ubicacion");
		}
	}
	*/
	
	public void guardarUbicacion(UbicacionPrenda item) throws UbicacionException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
//			UbicacionPrendaEntity ubicacion = (UbicacionPrendaEntity) item.toEntity();
//			session.save(ubicacion);
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		}
		catch(Exception e){
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo guardar la ubicacion de la prenda");
		}
	}
	
	public void guardarUbicacion(UbicacionInsumo item) throws UbicacionException {
		try{
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		}
		catch(Exception e){
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo guardar la ubicacion del insumo");
		}
	}

	public void modificarUbicacion(UbicacionPrenda item) throws UbicacionException {
		try {
			Session session = sf.openSession();
			session.beginTransaction();
			session.update(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new UbicacionException("Ubicación DAO: No se pudo actualizar la ubicacion de la prenda");
		}
	}



}
