package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;

import entities.PedidoClienteEntity;
import enums.EstadoPedidoCliente;
import exceptions.PedidoClienteException;
import hbt.HibernateUtil;
import modelo.PedidoCliente;

/**
 * @author zuki Ac� se hacen las CRUD en HQL (create, read, update, and delete)
 */
public class PedidoClienteDAO {
	private static PedidoClienteDAO instancia;
	private static SessionFactory sf = null;

	private PedidoClienteDAO() {
		super();
	}

	public static PedidoClienteDAO getInstancia() {
		if (instancia == null) {
			sf = HibernateUtil.getSessionFactory();
			instancia = new PedidoClienteDAO();
		}
		return instancia;
	}

	@SuppressWarnings("unchecked")
	public List<PedidoCliente> obtenerPedidos() throws PedidoClienteException {
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		try {
			List<PedidoClienteEntity> listadoQuery = new ArrayList<PedidoClienteEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM PedidoClienteEntity");
			listadoQuery = (ArrayList<PedidoClienteEntity>) query.list();
			for (PedidoClienteEntity pc : listadoQuery) {
				resultado.add(new PedidoCliente(pc));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo obtener todos los pedidos");
		}
	}

	@SuppressWarnings("unchecked")
	public List<PedidoCliente> obtenerPedidosCliente(String cuit) throws PedidoClienteException {
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		try {
			List<PedidoClienteEntity> listadoQuery = new ArrayList<PedidoClienteEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM PedidoClienteEntity c WHERE c.cliente.cuit = :cuit");
			query.setString("cuit", cuit);
			listadoQuery = (ArrayList<PedidoClienteEntity>) query.list();
			for (PedidoClienteEntity pc : listadoQuery) {
				resultado.add(new PedidoCliente(pc));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo obtener todos los pedidos del cliente + " + cuit);
		}
	}

	@SuppressWarnings("unchecked")
	public List<PedidoCliente> obtenerPedidosSucursal(int sucursal) throws PedidoClienteException {
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		try {
			List<PedidoClienteEntity> listadoQuery = new ArrayList<PedidoClienteEntity>();

			Session sesion = sf.openSession();
			Transaction tran = sesion.beginTransaction();
			Query query = (Query) sesion.createQuery("FROM PedidoClienteEntity c WHERE c.Sucursal.numero = :nro");
			query.setInteger("nro", sucursal);
			listadoQuery = (ArrayList<PedidoClienteEntity>) query.list();

			for (PedidoClienteEntity p : listadoQuery) {
				resultado.add(new PedidoCliente(p));
			}

			tran.commit();
			sesion.close();

			return resultado;
		}

		catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException(
					"PedidoClienteDAO: No se pudo obtener todos los pedidos de clientes de la sucursal " + sucursal);
		}
	}

	@SuppressWarnings("unchecked")
	public List<PedidoCliente> obtenerPedidosEstado() throws PedidoClienteException {
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		try {
			List<PedidoClienteEntity> listadoQuery = new ArrayList<PedidoClienteEntity>();
			Session session = sf.openSession();
			Transaction tran = session.beginTransaction();
			Query query = (Query) session.createQuery("FROM PedidoClienteEntity");
			listadoQuery = (ArrayList<PedidoClienteEntity>) query.list();
			for (PedidoClienteEntity p : listadoQuery) {
				resultado.add(new PedidoCliente(p));
			}
			tran.commit();
			session.flush();
			session.close();
			return resultado;
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo obtener todos los pedidos de clientes");
		}
	}

	@SuppressWarnings("unchecked")
	public List<PedidoCliente> obtenerPedidosEstado(EstadoPedidoCliente estado) throws PedidoClienteException {
		List<PedidoCliente> resultado = new ArrayList<PedidoCliente>();
		try {
			List<PedidoClienteEntity> listadoQuery = new ArrayList<PedidoClienteEntity>();

			Session sesion = sf.openSession();
			Transaction tran = sesion.beginTransaction();
			Query query = (Query) sesion.createQuery("FROM PedidoClienteEntity c WHERE c.estado = :estado");
			query.setString("estado", estado.toString());
			listadoQuery = (ArrayList<PedidoClienteEntity>) query.list();

			for (PedidoClienteEntity p : listadoQuery) {
				resultado.add(new PedidoCliente(p));
			}

			tran.commit();
			sesion.close();

			return resultado;
		}

		catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException(
					"PedidoClienteDAO: No se pudo obtener todos los pedidos de clientes en el estado " + estado);
		}
	}

	public PedidoCliente obtenerPedidoCliente(int numeroPedido) throws PedidoClienteException {
		PedidoCliente p = null;
		try {
			PedidoClienteEntity entity = null;
			Session session = sf.openSession();
			Query query = (Query) session
					.createQuery("FROM PedidoClienteEntity c WHERE c.numeroPedido = :numeroPedido");
			query.setInteger("numeroPedido", numeroPedido);
			entity = (PedidoClienteEntity) query.list().get(0);
			if (entity != null) {
				p = new PedidoCliente(entity);
			}
			session.flush();
			session.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo obtener el pedido de cliente");
		}
		return p;
	}

	public void altaPedidoCliente(PedidoCliente item) throws PedidoClienteException {
		try {
			System.out.println("\nALTA PEDIDO");
			Session session = sf.openSession();
			session.beginTransaction();
			session.saveOrUpdate(item.toEntity());
			session.getTransaction().commit();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo crear (alta) el pedido de cliente");
		}
	}

	public void guardarPedidoCliente(PedidoCliente item) throws PedidoClienteException {
		try {
			System.out.println("GUARDAR PEDIDO");
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(item.toEntity());
			session.getTransaction().commit();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo crear (guardar) el pedido de cliente");
		}
	}
	
	public void modificarPedidoCliente(PedidoCliente item) throws PedidoClienteException {
		try {
			System.out.println("\nMODIFICAR PEDIDO");
			Session session = sf.openSession();
			session.beginTransaction();
//			session.saveOrUpdate(item.toEntity());
			session.update(item.toEntity());
			session.getTransaction().commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			System.out.println(e);
			throw new PedidoClienteException("PedidoClienteDAO: No se pudo modificar el pedido de cliente");
		}
	}

}
