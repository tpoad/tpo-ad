package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bd.BusinessDelegate;
import dto.SucursalDTO;
import enums.MotivoRechazo;

/**
 * Servlet implementation class Produccion
 */
@WebServlet("/Produccion")
public class Produccion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public Produccion() {
        super();

    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		dispatch("/LoginProduccion.jsp", request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
        if ((action == null) || (action.length() < 1))
        {
            action = "default";
        }
        else if("login".equals(action)){
        	//setteo la sucursal en la sesion
        	
        	String area = request.getParameter("area");
        	
        	HttpSession s = request.getSession();
        	s.setAttribute("AREA", area);
        	request.setAttribute("ORDENES", BusinessDelegate.getInstancia().obtenerOrdenesArea(area));
        
			
			dispatch("/DashboardProduccion.jsp", request, response);
        }
        
        else{
        	int lote = Integer.parseInt(request.getParameter("lote").toString());
        	HttpSession s = request.getSession();
        	String area = s.getAttribute("AREA").toString();
        	
        	   if("aceptar".equals(action)){
        		   BusinessDelegate.getInstancia().completarOrden(area, lote);
               		System.out.println("Lote "+ lote +" completado en area "+ area);
               }
        	   
           	s.setAttribute("AREA", area);
           	request.setAttribute("ORDENES", BusinessDelegate.getInstancia().obtenerOrdenesArea(area));
           
   			
   			dispatch("/DashboardProduccion.jsp", request, response);
        }
	}
	
	protected void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (jsp != null) {
			/*
			 * Env�a el control al JSP que pasamos como par�metro, y con los
			 * request / response cargados con los par�metros
			 */
			RequestDispatcher rd = request.getRequestDispatcher(jsp);
			rd.forward(request, response);
		}
	}


}
