package Servlets;

import java.io.IOException;
import java.rmi.RemoteException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bd.BusinessDelegate;
import dto.ClienteDTO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String jspPage = "/IngresarCliente.jsp";
		dispatch(jspPage, request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// busco el cliente
		String pagename = request.getParameter("pageName").toString();
		try {
			String cuit = request.getParameter("cuit");
			ClienteDTO cliente = BusinessDelegate.getInstancia().obtenerCliente(cuit);
			System.out.println(cliente);
			if (cliente != null) {
				HttpSession s = request.getSession();
				s.setAttribute("PEDIDOSDTO",
						BusinessDelegate.getInstancia().obtenerPedidosCliente(request.getParameter("cuit")));
				s.setAttribute("CLIENTEDTO", cliente);

				dispatch("/Dashboard.jsp", request, response);
			} else {
				// regreso
				doGet(request, response);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	protected void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (jsp != null) {
			/*
			 * Env�a el control al JSP que pasamos como par�metro, y con los
			 * request / response cargados con los par�metros
			 */
			RequestDispatcher rd = request.getRequestDispatcher(jsp);
			rd.forward(request, response);
		}
	}

}
