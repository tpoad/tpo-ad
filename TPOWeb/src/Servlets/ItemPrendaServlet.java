package Servlets;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bd.BusinessDelegate;
import cache.CachePedidos;
import dto.ClienteDTO;
import dto.ItemPedidoClienteDTO;
import dto.ItemPrendaDTO;
import dto.PedidoClienteDTO;
import enums.EstadoPedidoCliente;

/**
 * Servlet implementation class ItemPrendaServlet
 */
@WebServlet("/ItemPrendaServlet")
public class ItemPrendaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemPrendaServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unused")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("accion");

		if ("cargarItemsPrenda".equals(action)) {
			String prenda = request.getParameter("prenda");
			int id = Integer.parseInt(prenda);

			List<ItemPrendaDTO> items = BusinessDelegate.getInstancia().obtenerItemsPrenda(id);

			Map<String, String> options = new LinkedHashMap<>();
			for (ItemPrendaDTO i : items) {
				options.put(String.valueOf(i.getSubcodigo()), i.toString());
			}

			String json = new Gson().toJson(options);

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		} else if ("agregarItem".equals(action)) {
			String prenda = request.getParameter("prenda");

		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession s = request.getSession();
		ClienteDTO cliente = (ClienteDTO) s.getAttribute("CLIENTEDTO");

		String accion = request.getParameter("accion");
		String jspPage = "/NuevoPedido.jsp";

		if ("addItem".equals(accion)) {
			String cant = request.getParameter("cantidad");
			String pren = request.getParameter("prenda");
			String ipren = request.getParameter("itemPrenda");

			int cantidad = Integer.parseInt(cant);
			int idPrenda = Integer.parseInt(pren);
			int idItemPrenda = Integer.parseInt(ipren);

			List<ItemPrendaDTO> items = BusinessDelegate.getInstancia().obtenerItemsPrenda(idPrenda);
			Float precio = 0F;
			for (ItemPrendaDTO i : items) {
				if (i.getSubcodigo() == idItemPrenda) {
					ItemPedidoClienteDTO item = new ItemPedidoClienteDTO();
//					precio += i.getPrenda().getPrecioVenta();
					item.setCantidad(cantidad);
					item.setItemPrenda(i);
//					item.setPrecio(precio);
					item.setPrecio(i.getPrenda().getPrecioVenta());
					item.getItemPrenda().setPrenda(BusinessDelegate.getInstancia().getPrendaBoba(idPrenda));

					CachePedidos.getInstancia().agregarItem(item, cliente);
					break;
				}
			}

			request.setAttribute("PRENDASDTO", BusinessDelegate.getInstancia().obtenerPrendas());
			request.setAttribute("ITEMSPEDIDODTO", CachePedidos.getInstancia().obtenerPedido(cliente).getItemsPedidoCliente());
			jspPage = "/NuevoPedido.jsp";
		}

		if ("confirmar".equals(accion)) {
			// obtengo el pedido
			PedidoClienteDTO pedido = CachePedidos.getInstancia().obtenerPedido(cliente);
			System.out.println(pedido);
			pedido.setFechaGeneracion(new Date ());
			pedido.setFechaProbable(new Date((new Date().getTime()) + (14 * 24 * 3600 * 1000)));
			Float total = 0F;
			for(ItemPedidoClienteDTO ipcdto : pedido.getItemsPedidoCliente()){
				total += (ipcdto.getPrecio() * ipcdto.getCantidad());
			}
			pedido.setTotal(total);
			pedido.setEstado(EstadoPedidoCliente.PENDIENTE_DE_CHECKEO);
			pedido.setCliente(cliente);
			pedido.setSucursal(BusinessDelegate.getInstancia().getSucursal(1));
			
			// TODO
			// completar el metodo de alta pedido cliente, basarse en el
			// pedido solo tiene los id y la cantidad
			BusinessDelegate.getInstancia().altaPedidoCliente(pedido);

			// enviar a dashboard
			s.setAttribute("PEDIDOSDTO", BusinessDelegate.getInstancia().obtenerPedidosCliente(cliente.getCuit()));
			s.setAttribute("CLIENTEDTO", cliente);
			jspPage = "/Dashboard.jsp";

			System.out.println("NUEVO PEDIDO REALIZADO");
			
//			BusinessDelegate.getInstancia().procesarPedidoCliente(pedido);

			// dispatch("/Dashboard.jsp", request, response);
		}
		dispatch(jspPage, request, response);

	}

	protected void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (jsp != null) {
			/*
			 * Env�a el control al JSP que pasamos como par�metro, y con los
			 * request / response cargados con los par�metros
			 */
			RequestDispatcher rd = request.getRequestDispatcher(jsp);
			rd.forward(request, response);
		}
	}
}
