package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bd.BusinessDelegate;
import dto.ClienteDTO;
import dto.SucursalDTO;
import enums.MotivoRechazo;

/**
 * Servlet implementation class Sucursal
 */
@WebServlet("/Sucursal")
public class Sucursal extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Sucursal() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setAttribute("SUCURSALES", BusinessDelegate.getInstancia().getSucursales());
		
		dispatch("/LoginSucursal.jsp", request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String action = request.getParameter("action");
		
        if ((action == null) || (action.length() < 1))
        {
            action = "default";
        }
        else if("login".equals(action)){
        	//setteo la sucursal en la sesion
        	
        	HttpSession s = request.getSession();
        	s.setAttribute("SUCURSAL", BusinessDelegate.getInstancia().getSucursal(Integer.parseInt(request.getParameter("sucursal"))));
        	request.setAttribute("PEDIDOS", BusinessDelegate.getInstancia().obtenerPedidosClienteSucursal(Integer.parseInt(request.getParameter("sucursal"))));
        
			
			dispatch("/DashboardSucursal.jsp", request, response);
        }
        
        else{
        	int nro = Integer.parseInt(request.getParameter("idpedido").toString());
        	   if("aceptar".equals(action)){
        		   BusinessDelegate.getInstancia().aprobarPedidoCliente(nro);
               		System.out.println("Pedido "+ nro +" aceptado");
               }
               else if("rechazar".equals(action)){
               		MotivoRechazo motivo = MotivoRechazo.valueOf(request.getParameter("motivo").toString());
               		BusinessDelegate.getInstancia().rechazarPedidoCliente(nro, motivo);
               		System.out.println("Pedido "+ nro +" rechazado, motivo: "+ motivo.toString());
               }
        	   
        	   HttpSession s = request.getSession();  	   
        	   SucursalDTO sucursal = (SucursalDTO) s.getAttribute("SUCURSAL");
           	   request.setAttribute("PEDIDOS", BusinessDelegate.getInstancia().obtenerPedidosClienteSucursal(sucursal.getNumero()));
 
        	   dispatch("/DashboardSucursal.jsp", request, response);
        }
     
		
	}
	
	protected void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (jsp != null) {
			/*
			 * Env�a el control al JSP que pasamos como par�metro, y con los
			 * request / response cargados con los par�metros
			 */
			RequestDispatcher rd = request.getRequestDispatcher(jsp);
			rd.forward(request, response);
		}
	}

}
