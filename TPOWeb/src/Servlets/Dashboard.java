package Servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bd.BusinessDelegate;
import cache.CachePedidos;
import dto.ClienteDTO;

/**
 * Servlet implementation class Dashboard
 */
@WebServlet("/Dashboard")
public class Dashboard extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Dashboard() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String jspPage = "/Dashboard.jsp";
		String action = request.getParameter("action");
		
		HttpSession s=request.getSession();
		ClienteDTO cliente = (ClienteDTO)s.getAttribute("CLIENTEDTO");

        if ((action == null) || (action.length() < 1))
        {
            action = "default";
        }
        else if("aceptarPedido".equals(action)){
        	int nroPedido = Integer.parseInt(request.getParameter("id"));
        	System.out.println("DASHBOARD - Nro Pedido: " + nroPedido);
        	BusinessDelegate.getInstancia().aceptarPedidoCliente(nroPedido);
        	System.out.println("Pedido aceptado");
        	s.setAttribute("PEDIDOSDTO", BusinessDelegate.getInstancia().obtenerPedidosCliente(cliente.getCuit()));
        }
        else if("cancelarPedido".equals(action)){
        	int nroPedido = Integer.parseInt(request.getParameter("id"));
        	BusinessDelegate.getInstancia().cancelarPedidoCliente(nroPedido);
        	System.out.println("Pedido cancelado");
        	s.setAttribute("PEDIDOSDTO", BusinessDelegate.getInstancia().obtenerPedidosCliente(cliente.getCuit()));
        }
        else if("nuevoPedido".equals(action)){   		
        	request.setAttribute("PRENDASDTO", BusinessDelegate.getInstancia().obtenerPrendas());
        	request.setAttribute("ITEMSPEDIDODTO", CachePedidos.getInstancia().obtenerPedido(cliente).getItemsPedidoCliente());
			jspPage = "/NuevoPedido.jsp";
		}
        else if("default".equals(action)){
			jspPage = "/Dashboard.jsp";
		}
		
		dispatch(jspPage, request, response);
	}
	
	 protected void dispatch(String jsp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
     {
         if (jsp != null)
         {
         	/*Env�a el control al JSP que pasamos como par�metro, y con los 
         	 * request / response cargados con los par�metros */
             RequestDispatcher rd = request.getRequestDispatcher(jsp);
             rd.forward(request, response);
         }
     }

}
