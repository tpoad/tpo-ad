package cache;

import java.util.ArrayList;
import java.util.List;

import dto.ClienteDTO;
import dto.ItemPedidoClienteDTO;
import dto.PedidoClienteDTO;
import webaux.AuxClientePedidoDTO;

public class CachePedidos {
	private List<AuxClientePedidoDTO> pedidos;
	
	static private CachePedidos instancia;
	
	private CachePedidos(){
		pedidos = new ArrayList<AuxClientePedidoDTO>();
	}
	
	static public CachePedidos getInstancia(){
		if(instancia == null){
			instancia = new CachePedidos();
		}
		return instancia;
	}
	
	public PedidoClienteDTO obtenerPedido(ClienteDTO cliente){
		PedidoClienteDTO pedido = null;
		for(AuxClientePedidoDTO aux : pedidos){
			if(aux.getCliente().getCuit() == cliente.getCuit()){
				pedido = aux.getPedido();
				break;
			}
		}
		if(pedido == null){
			pedido = new PedidoClienteDTO();
			AuxClientePedidoDTO aux = new AuxClientePedidoDTO();
			aux.setCliente(cliente);
			aux.setPedido(pedido);		
			pedidos.add(aux);
		}
		return pedido;
	}
	
	public void agregarItem (ItemPedidoClienteDTO item, ClienteDTO cliente){
		PedidoClienteDTO pedido = obtenerPedido(cliente);
		//si ya tengo el item le sumo la cantidad, sino lo agrego
		Boolean nuevo = false;
		for(ItemPedidoClienteDTO aux : pedido.getItemsPedidoCliente()){
			if(aux.getItemPrenda().getSubcodigo() == item.getItemPrenda().getSubcodigo()){
				aux.setCantidad(aux.getCantidad()+item.getCantidad());
				nuevo = true;
			}
		}
		if(nuevo == false){
			pedido.getItemsPedidoCliente().add(item);
		}
	}
	
}
