<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import= "dto.PrendaDTO"%>
<%@ page import= "dto.ItemPedidoClienteDTO"%>
<%@ page import= "dto.ItemPrendaDTO"%>
<%@ page import= "java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo Pedido</title>

<script src="jquery-3.2.1.min.js"></script>
<script type="text/javascript">
function agregar() {

  var elemento = document.createElement("li");
  
  var ddprenda = document.getElementById("dropdownprenda");
  var strPrenda = ddprenda.options[ddprenda.selectedIndex];
  
  var texto = document.createTextNode(strPrenda);
  
  elemento.appendChild(strPrenda);
 
  var lista = document.getElementById("lista");
  lista.appendChild(elemento);
}
</script>

<script>

window.onload = function() {
				
	var ddprenda = document.getElementById("dropdownprenda");
	var idPrenda = ddprenda.options[ddprenda.selectedIndex].value;
	var params = {
   		 prenda: idPrenda,
   		 accion: "cargarItemsPrenda"
	};
								               // When HTML DOM "click" event is invoked on element with ID "somebutton", execute the following function...
    $.get("ItemPrendaServlet", $.param(params), function(responseJson) {                 // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
        var $select = $("#dropdownitemprenda");                           // Locate HTML DOM element with ID "someselect".
        $select.find("option").remove();                          // Find all child elements with tag name "option" and remove them (just to prevent duplicate options when button is pressed again).
        $.each(responseJson, function(key, value) {               // Iterate over the JSON object.
            $("<option>").val(key).text(value).appendTo($select); // Create HTML <option> element, set its value with currently iterated key and its text content with currently iterated item and finally append it to the <select>.
        });
    });
};

</script>

<script type="text/javascript">
$(document).on("change", "#dropdownprenda", function() {
				
	var ddprenda = document.getElementById("dropdownprenda");
	var idPrenda = ddprenda.options[ddprenda.selectedIndex].value;
	var params = {
   		 prenda: idPrenda,
   		 accion: "cargarItemsPrenda"
	};
								               // When HTML DOM "click" event is invoked on element with ID "somebutton", execute the following function...
    $.get("ItemPrendaServlet", $.param(params), function(responseJson) {                 // Execute Ajax GET request on URL of "someservlet" and execute the following function with Ajax response JSON...
        var $select = $("#dropdownitemprenda");                           // Locate HTML DOM element with ID "someselect".
        $select.find("option").remove();                          // Find all child elements with tag name "option" and remove them (just to prevent duplicate options when button is pressed again).
        $.each(responseJson, function(key, value) {               // Iterate over the JSON object.
            $("<option>").val(key).text(value).appendTo($select); // Create HTML <option> element, set its value with currently iterated key and its text content with currently iterated item and finally append it to the <select>.
        });
    });
});

</script>

</head>
<body>
Nuevo pedido:
<br>
<ul id="lista">
</ul>

<table id="tabla" cellspacing="3" cellpadding="3" border="1" width="100%">
<tr>
  <td><strong>#prenda</strong></td>
  <td><strong>Modelo</strong></td>
  <td><strong>#itemprenda</strong></td>
  <td><strong>talle/color</strong></td>
   <td><strong>cantidad</strong></td>
</tr>

<% List<ItemPedidoClienteDTO> items = (List<ItemPedidoClienteDTO>)request.getAttribute("ITEMSPEDIDODTO");
 %>
 
 <% for (int i = 0; i < items.size(); i++) { %>
    <tr>
 	<td><%= String.valueOf(items.get(i).getItemPrenda().getPrenda().getCodigo()) %></td>
  <td><%= items.get(i).getItemPrenda().getPrenda().getDescripcion() %></td>
  <td><%= String.valueOf(items.get(i).getItemPrenda().getSubcodigo()) %></td>
  <td><%= items.get(i).getItemPrenda().toString() %></td>
  <td><%= items.get(i).getCantidad() %></td>
</tr>
<% } %>

</table>


<form action="ItemPrendaServlet" method="POST">

<% List<PrendaDTO> prendas = (List<PrendaDTO>)request.getAttribute("PRENDASDTO");
 %>

<br><select id="dropdownprenda" name = "prenda" style="width: 134px;">
<% for (int i = 0; i < prendas.size(); i++) { %>

 <option value="<%= prendas.get(i).getCodigo()%>"> <%= prendas.get(i).getDescripcion().toString() %></option>

  <%  }%>
</select>
<select id="dropdownitemprenda" name="itemPrenda" style="width: 134px; ">
</select>

<input type="text" name="cantidad" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width: 48px; ">
<input type="hidden" name="accion" value="addItem"/>
<input type="submit" value="Agregar"></input> 

</form>

<form action="ItemPrendaServlet" method="POST">
<input type="hidden" name="accion" value="confirmar"/>
  <input type="submit" value="Confirmar">
</form>


</body>
</html>