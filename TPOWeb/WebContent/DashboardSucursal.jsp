<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import= "dto.PedidoClienteDTO"%>
        <%@ page import= "dto.SucursalDTO"%>
<%@ page import= "enums.EstadoPedidoCliente"%>
<%@ page import= "enums.MotivoRechazo"%>
<%@ page import=  "java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SUCURSAL</title>
</head>
<body>

Sucursal <%= ((SucursalDTO)session.getAttribute("SUCURSAL")).getNombre() %><br><br>Pedidos:<br><table cellspacing="3" cellpadding="3" border="1" width="100%">
<tr>
  <td><strong>#</strong></td>
  <td><strong>Total de prendas</strong></td>
  <td><strong>Costo</strong></td>
  <td><strong>Estado</strong></td>
  <td><strong>Fecha estimada</strong></td>
  <td><strong>Fecha real</strong></td>
  <td><strong>Accion</strong></td>
</tr>

<% List<PedidoClienteDTO> pedidos = (List<PedidoClienteDTO>)request.getAttribute("PEDIDOS");
 %>


<% for (int i = 0; i < pedidos.size(); i++) { %>
    <tr>
 	<td><%= pedidos.get(i).getNumeroPedido().toString() %></td>
  <td><%= pedidos.get(i).getItemsPedidoCliente().size() %></td>
  <td><%= pedidos.get(i).getTotal() %></td>
  <td><%= pedidos.get(i).getEstado() %></td>
  <td><%= pedidos.get(i).getFechaProbable() %></td>
  <td><%= pedidos.get(i).getFechaReal() %></td>
  <% if (pedidos.get(i).getEstado() == EstadoPedidoCliente.PENDIENTE_DE_CHECKEO){ %>
  <td> 
	<form action="Sucursal" method="POST">
		<input type="hidden" name="action" value= "aceptar">
		<input type="hidden" name="idpedido" value= "<%= pedidos.get(i).getNumeroPedido() %>">
		<input type="submit" value="aceptar">
	</form>
	<br>
	<form action="Sucursal" method="POST">
		<select name="motivo" >
			<option value="<%=MotivoRechazo.SALDO_INSUFICIENTE %>"> Saldo insuficiente</option>
			 <option value="<%=MotivoRechazo.PRENDAS_DISCONTINUAS %>"> Prendas discontinuas</option>
		</select>
		<br>
		<input type="hidden" name="action" value= "rechazar">
		<input type="hidden" name="idpedido" value= "<%= pedidos.get(i).getNumeroPedido() %>">
		<input type="submit" value="rechazar">
	</form>

  </td>
  <%  }%>
</tr>
<% } %>




</body>
</html>