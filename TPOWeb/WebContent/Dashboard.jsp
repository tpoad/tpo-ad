<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import= "dto.ClienteDTO"%>
<%@ page import= "dto.PedidoClienteDTO"%>
<%@ page import= "enums.EstadoPedidoCliente"%>
<%@ page import=  "java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gut Gekleidet Wurst</title>

</head>
<body>
<%
	ClienteDTO c = (ClienteDTO)session.getAttribute("CLIENTEDTO");
%><br>Bienvenido <%=c.getRazonSocial()%><br><br><a href="/TPOWeb/Dashboard?action=nuevoPedido">Nuevo Pedido</a> - Log out<br><br>Pedidos:

<table cellspacing="3" cellpadding="3" border="1" width="100%">
<tr>
  <td><strong>#</strong></td>
  <td><strong>Total de prendas</strong></td>
  <td><strong>Costo</strong></td>
  <td><strong>Estado</strong></td>
  <td><strong>Fecha estimada</strong></td>
  <td><strong>Fecha real</strong></td>
  <td><strong>Accion</strong></td>
</tr>

<% List<PedidoClienteDTO> pedidos = (List<PedidoClienteDTO>)session.getAttribute("PEDIDOSDTO");
 %>


<% for (int i = 0; i < pedidos.size(); i++) { %>
    <tr>
 	<td><%= pedidos.get(i).getNumeroPedido().toString() %></td>
  <td><%= pedidos.get(i).getItemsPedidoCliente().size() %></td>
  <td><%= pedidos.get(i).getTotal() %></td>
  <td><%= pedidos.get(i).getEstado() %></td>
  <td><%= pedidos.get(i).getFechaProbable() %></td>
  <td><%= pedidos.get(i).getFechaReal() %></td>
  <% if (pedidos.get(i).getEstado() == EstadoPedidoCliente.PENDIENTE_DE_ACEPTACION_CLIENTE){ %>
  <td> <a href="/TPOWeb/Dashboard?action=aceptarPedido&id=<%= pedidos.get(i).getNumeroPedido() %>">Aceptar</a> /
  <a href="/TPOWeb/Dashboard?action=cancelarPedido&id=<%= pedidos.get(i).getNumeroPedido() %>">Cancelar</a>
  </td>
  <%  }%>
</tr>
<% } %>


 
 </table>
 
 </body>
</html>