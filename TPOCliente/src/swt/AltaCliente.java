package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import controladorCliente.ControladorCliente;

public class AltaCliente extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtRazonSocial;
	private JTextField txtCUIT;
	private JTextField txtInscripcion;
	private JTextField txtDireccion;
	private JFormattedTextField txtSaldo;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxCondiciones;
	private JFormattedTextField txtCredito;
	private JButton aceptar;
	private JButton cancelar;

	public AltaCliente() {
		inicializarComponentes();
		crearEventos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void inicializarComponentes() {
		this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
		this.setBounds(100, 100, 450, 469);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Cargar Cliente");
		titulo.setFont(new Font("Verdana", Font.BOLD, 20));
		titulo.setBounds(10, 11, 182, 26);
		this.getContentPane().add(titulo);

		JPanel General = new JPanel();
		General.setBounds(10, 42, 414, 156);
		General.setBorder(new TitledBorder(null, "Datos particulares", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(General);
		General.setLayout(null);

		JLabel lblRaznSocial = new JLabel("Raz\u00F3n Social");
		lblRaznSocial.setBounds(10, 30, 72, 14);
		General.add(lblRaznSocial);

		txtRazonSocial = new JTextField();
		txtRazonSocial.setBounds(80, 27, 324, 20);
		General.add(txtRazonSocial);
		txtRazonSocial.setColumns(10);

		JLabel lblCuit = new JLabel("CUIT");
		lblCuit.setBounds(10, 58, 72, 14);
		General.add(lblCuit);

		txtCUIT = new JTextField();
		txtCUIT.setBounds(80, 55, 162, 20);
		General.add(txtCUIT);
		txtCUIT.setColumns(10);

		JLabel lblDireccion = new JLabel("Direcci\u00F3n");
		lblDireccion.setBounds(10, 121, 72, 14);
		General.add(lblDireccion);

		txtDireccion = new JTextField();
		txtDireccion.setBounds(80, 118, 324, 20);
		General.add(txtDireccion);
		txtDireccion.setColumns(10);

		JLabel lblInscripcion = new JLabel("Inscripci\u00F3n");
		lblInscripcion.setBounds(10, 88, 72, 14);
		General.add(lblInscripcion);

		txtInscripcion = new JTextField();
		txtInscripcion.setColumns(10);
		txtInscripcion.setBounds(80, 86, 162, 20);
		General.add(txtInscripcion);

		JPanel CuentaCorriente = new JPanel();
		CuentaCorriente.setBounds(10, 229, 414, 156);
		CuentaCorriente.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Cuenta Corriente",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(CuentaCorriente);
		CuentaCorriente.setLayout(null);

		JLabel lblSaldo = new JLabel("Saldo");
		lblSaldo.setBounds(10, 28, 72, 14);
		CuentaCorriente.add(lblSaldo);

		txtSaldo = new JFormattedTextField();
		txtSaldo.setBounds(80, 25, 162, 20);
//		txtSaldo.setValue(new Float(10000));
		CuentaCorriente.add(txtSaldo);

		JLabel lblCondiciones = new JLabel("Condiciones");
		lblCondiciones.setBounds(10, 59, 72, 14);
		CuentaCorriente.add(lblCondiciones);

		cbxCondiciones = new JComboBox();
		cbxCondiciones.setModel(new DefaultComboBoxModel(new String[] { "Contado", "Transferencia", "30 d�as", "60 d�as" }));
		cbxCondiciones.setBounds(80, 56, 162, 20);
		CuentaCorriente.add(cbxCondiciones);

		JLabel lblCredito = new JLabel("Cr\u00E9dito");
		lblCredito.setBounds(10, 91, 72, 14);
		CuentaCorriente.add(lblCredito);

		txtCredito = new JFormattedTextField();
		txtCredito.setBounds(80, 88, 162, 20);
		CuentaCorriente.add(txtCredito);

		aceptar = new JButton("Aceptar");
		aceptar.setBounds(240, 389, 89, 23);
		this.getContentPane().add(aceptar);

		cancelar = new JButton("Cancelar");
		cancelar.setBounds(335, 389, 89, 23);
		this.getContentPane().add(cancelar);

	}

	private void crearEventos() {
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validacion()) {
					String textField = txtSaldo.getText();
					float floatSaldo = 0F;
					if (textField != null && !textField.isEmpty()) {
					    floatSaldo = Float.parseFloat(textField);
					}
					textField = txtCredito.getText();
					float floatCredito = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatCredito = Float.parseFloat(textField);
					}
					boolean flag = ControladorCliente.getInstancia().altaCliente(new Integer(0),
							txtRazonSocial.getText(), txtCUIT.getText(), txtInscripcion.getText(),
							txtDireccion.getText(), floatSaldo, floatCredito, cbxCondiciones.getSelectedItem().toString());
					if (flag) {
						JOptionPane.showMessageDialog(null,
								"Se ha generado el cliente: " + txtRazonSocial.getText(),
								"Cliente generado con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaCliente c = new ConsultaCliente();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido generar al cliente", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaCliente c = new ConsultaCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

	}

	public boolean validacion() {

		if (txtRazonSocial.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la raz�n social", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtRazonSocial.requestFocus();
			return false;
		} else if (txtCUIT.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el CUIT", "Atenci�n", JOptionPane.WARNING_MESSAGE);
			txtCUIT.requestFocus();
			return false;
		} else if (txtInscripcion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la inscripci�n en AFIP", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtInscripcion.requestFocus();
			return false;
		} else if (txtDireccion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la direcci�n", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtDireccion.requestFocus();
			return false;
		} else if (txtSaldo.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el saldo", "Atenci�n", JOptionPane.WARNING_MESSAGE);
			txtSaldo.requestFocus();
			return false;
		} else if (cbxCondiciones.getSelectedItem().toString().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor seleccione la forma de pago.", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			cbxCondiciones.requestFocus();
			return false;
		} else if (txtCredito.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el l�mite de cr�dito.", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtCredito.requestFocus();
			return false;
		}
		return true;
	}



}