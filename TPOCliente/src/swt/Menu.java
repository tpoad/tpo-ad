package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR	
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class Menu extends JFrame {

	private static final long serialVersionUID = 1L;
	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private JPanel jPanel1;
	private JMenuBar jMenuBar;
	private JMenu jMenuSistema;
	private JMenu jMenuAdministracion;
	private JMenu jMenuVentas;
	private JMenu jMenuProduccion;
	private JMenu jMenuAlmacen;

	private JMenuItem jMenuItemExit;
	private JMenuItem jMenuItemSucursales;
	private JMenuItem jMenuItemEmpleados;
	private JMenuItem jMenuItemPrendas;
	private JMenuItem jMenuItemInsumos;
	private JMenuItem jMenuItemClientes;
	private JMenuItem jMenuItemProveedores;
	private JMenuItem jMenuItemConsultarVentas;
	private JMenuItem jMenuItemConsultarProduccion;
	private JMenuItem jMenuItemConsultarAlmacen;

//	private JLabel jLabelAcceso;
//	private JLabel jLabelSucursal;
//	private JLabel jLabelRol;
//	private JLabel jLabelUsuario;
	private JLabel jLabelTitulo;
	private JLabel jLabelLogo;

	// private DTO_Sucursal sucursalActual;
	// private DTO_Usuario usuario;

	// public Menu(DTO_Usuario usuario, DTO_Sucursal sucursal) {
	public Menu() {
		super();

		// this.sucursalActual = sucursal;
		// this.usuario = usuario;
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			{
				this.setResizable(false);
				this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
				this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
			getContentPane().setLayout(null);
			{
				jPanel1 = new JPanel();
				jPanel1.setBounds(0, 0, 431, 294);
				getContentPane().add(jPanel1);
				jPanel1.setPreferredSize(new java.awt.Dimension(402, 284));
				{
					jLabelLogo = new JLabel();
					jLabelLogo.setBounds(131, 51, 166, 121);
					jLabelLogo.setIcon(new ImageIcon(getClass().getClassLoader().getResource("recursos/compras.jpg")));
					jLabelLogo.setLayout(null);
				}
				{
					jLabelTitulo = new JLabel();
					jLabelTitulo.setBounds(111, 11, 209, 29);
					jLabelTitulo.setText("Gut Gekleidet Wurst");
					jLabelTitulo.setFont(new java.awt.Font("Tahoma", 1, 20));
					jLabelTitulo.setLayout(null);
				}
				jPanel1.setLayout(null);
				jPanel1.add(jLabelTitulo);
				jPanel1.add(jLabelLogo);
			}
			this.setSize(437, 344);
			{
				jMenuBar = new JMenuBar();
				setJMenuBar(jMenuBar);
				{
					jMenuSistema = new JMenu();
					jMenuBar.add(jMenuSistema);
					jMenuSistema.setText("Sistema");
					{
						jMenuItemExit = new JMenuItem();
						jMenuSistema.add(jMenuItemExit);
						jMenuItemExit.setText("Salir");
					}
				}

				{
					jMenuAdministracion = new JMenu();
					jMenuBar.add(jMenuAdministracion);
					jMenuAdministracion.setText("Administración");
					{
						jMenuItemClientes = new JMenuItem();
						jMenuAdministracion.add(jMenuItemClientes);
						jMenuItemClientes.setText("Clientes");
						jMenuItemClientes.setVisible(true);
					}
					{
						jMenuItemSucursales = new JMenuItem();
						jMenuAdministracion.add(jMenuItemSucursales);
						jMenuItemSucursales.setText("Sucursales");
						jMenuItemSucursales.setVisible(true);
					}
					{
						jMenuItemInsumos = new JMenuItem();
						jMenuAdministracion.add(jMenuItemInsumos);
						jMenuItemInsumos.setText("Insumos");
						jMenuItemInsumos.setVisible(true);
					}
					{
						jMenuItemPrendas = new JMenuItem();
						jMenuAdministracion.add(jMenuItemPrendas);
						jMenuItemPrendas.setText("Prendas");
						jMenuItemPrendas.setVisible(true);
					}
					{
						jMenuItemEmpleados = new JMenuItem();
						jMenuAdministracion.add(jMenuItemEmpleados);
						jMenuItemEmpleados.setText("Empleados");
						jMenuItemEmpleados.setVisible(false);
					}
					{
						jMenuItemProveedores = new JMenuItem();
						jMenuAdministracion.add(jMenuItemProveedores);
						jMenuItemProveedores.setText("Proveedores");
						jMenuItemProveedores.setVisible(false);
					}
				}

				{
					jMenuVentas = new JMenu();
					jMenuBar.add(jMenuVentas);
					jMenuVentas.setText("Ventas");
					jMenuVentas.setVisible(true);
					{
						jMenuItemConsultarVentas = new JMenuItem();
						jMenuVentas.add(jMenuItemConsultarVentas);
						jMenuItemConsultarVentas.setText("Consultar ventas");
						jMenuItemConsultarVentas.setVisible(true);
					}
				}
				{
					jMenuProduccion = new JMenu();
					jMenuBar.add(jMenuProduccion);
					jMenuProduccion.setText("Producción");
					jMenuProduccion.setVisible(false);
					{
						jMenuItemConsultarProduccion = new JMenuItem();
						jMenuProduccion.add(jMenuItemConsultarProduccion);
						jMenuItemConsultarProduccion.setText("Consultar produccion");
						jMenuItemConsultarProduccion.setVisible(false);
					}
				}
				{
					jMenuAlmacen = new JMenu();
					jMenuBar.add(jMenuAlmacen);
					jMenuAlmacen.setText("Almacen");
					jMenuAlmacen.setVisible(false);
					{
						jMenuItemConsultarAlmacen = new JMenuItem();
						jMenuAlmacen.add(jMenuItemConsultarAlmacen);
						jMenuItemConsultarAlmacen.setText("Consultar almacen");
						jMenuItemConsultarAlmacen.setVisible(false);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void crearEventos() {

		jMenuItemExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});

		jMenuItemSucursales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConsultaSucursal c = new ConsultaSucursal();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

		jMenuItemEmpleados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});

		jMenuItemPrendas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConsultaPrenda c = new ConsultaPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

		jMenuItemInsumos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConsultaInsumo c = new ConsultaInsumo();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

		jMenuItemClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConsultaCliente c = new ConsultaCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

		jMenuItemProveedores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});

		jMenuItemConsultarVentas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConsultaPedidoCliente c = new ConsultaPedidoCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

		jMenuItemConsultarProduccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});

		jMenuItemConsultarAlmacen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});

	}

}
