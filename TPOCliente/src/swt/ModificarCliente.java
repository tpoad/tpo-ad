package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controladorCliente.ControladorCliente;
import dto.ClienteDTO;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ModificarCliente extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;
	private JPanel jPanel2;

	private JLabel lblCUIT;
	private JTextField txtCUIT;
	private JButton btnBuscar;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JLabel lblRazonSocial;
	private JTextField txtRazonSocial;
	private JLabel lblInscripcion;
	private JTextField txtInscripcion;
	private JLabel lblDireccion;
	private JTextField txtDireccion;
	private JLabel lblSaldo;
	private JFormattedTextField txtSaldo;
	private JLabel lblCondiciones;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxCondiciones;
	private JLabel lblCredito;
	private JFormattedTextField txtCredito;

	private ClienteDTO clienteDTO;

	public ModificarCliente() {
		super();
		inicializarComponentes();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Modificar Cliente");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(10, 0, 231, 40);
			}
			{
				{
					lblCUIT = new JLabel();
					getContentPane().add(lblCUIT);
					getContentPane().add(getBtnBuscar());
					lblCUIT.setText("CUIT");
					lblCUIT.setFont(new java.awt.Font("Verdana", 0, 11));
					lblCUIT.setBounds(12, 54, 80, 14);
					getContentPane().add(getBtnAceptar());
					getContentPane().add(getBtnCancelar());
				}
				{
					txtCUIT = new JTextField();
					getContentPane().add(txtCUIT);
					txtCUIT.setBounds(118, 52, 143, 21);
					txtCUIT.requestFocus();
				}
				jPanel1 = new JPanel();
				jPanel2 = new JPanel();
				getContentPane().add(jPanel1);
				getContentPane().add(jPanel2);
				jPanel1.setLayout(null);
				jPanel1.setBounds(12, 128, 372, 115);
				jPanel1.setBorder(BorderFactory.createTitledBorder("Datos del cliente"));
				{
					jPanel1.add(getRazonSocialLabel());
					jPanel1.add(getRazonSocialTXT());
					jPanel1.add(getInscripcionLabel());
					jPanel1.add(getInscripcionTXT());
					jPanel1.add(getDireccionLabel());
					jPanel1.add(getDireccionTXT());
					txtRazonSocial.setEnabled(false);
					txtInscripcion.setEnabled(false);
					txtDireccion.setEnabled(false);
				}
				jPanel2.setLayout(null);
				jPanel2.setBounds(12, 254, 372, 115);
				jPanel2.setBorder(BorderFactory.createTitledBorder("Cuenta Corriente"));
				{
					jPanel2.add(getSaldoLabel());
					jPanel2.add(getSaldoTXT());
					jPanel2.add(getCondicionesLabel());
					jPanel2.add(getCondicionesCBX());
					jPanel2.add(getCreditoLabel());
					jPanel2.add(getCreditoTXT());
					txtSaldo.setEnabled(false);
					cbxCondiciones.setEnabled(false);
					txtCredito.setEnabled(false);
				}
			}

			this.setSize(409, 500);
			// pack();
		} catch (Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton();
			btnBuscar.setText("Buscar");
			btnBuscar.setBounds(269, 84, 115, 27);
			btnBuscar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					clienteDTO = ControladorCliente.getInstancia().obtenerCliente(txtCUIT.getText());
					if (clienteDTO != null) {
						txtRazonSocial.setText(clienteDTO.getRazonSocial());
						txtInscripcion.setText(clienteDTO.getInscripcionAFIP());
						txtDireccion.setText(clienteDTO.getDireccion());
						txtSaldo.setValue(clienteDTO.getSaldo());
						cbxCondiciones.setSelectedItem(clienteDTO.getCondicionesPago());
						txtCredito.setValue(clienteDTO.getLimiteCredito());
						btnBuscar.setEnabled(false);
						txtCUIT.setEnabled(false);
						txtRazonSocial.setEnabled(false);
						txtInscripcion.setEnabled(true);
						txtDireccion.setEnabled(true);
						txtSaldo.setEnabled(true);
						cbxCondiciones.setEnabled(true);
						txtCredito.setEnabled(true);
					} else {
						JOptionPane.showMessageDialog(null, "No se ha encontrado al cliente", "Error",
								JOptionPane.ERROR_MESSAGE);
						txtCUIT.setEnabled(true);
						txtCUIT.setText("");
						txtCUIT.requestFocus();
					}
				}
			});
		}
		return btnBuscar;
	}

	private JLabel getRazonSocialLabel() {
		if (lblRazonSocial == null) {
			lblRazonSocial = new JLabel();
			lblRazonSocial.setText("Raz\u00F3n Social");
			lblRazonSocial.setFont(new java.awt.Font("Verdana", 0, 11));
			lblRazonSocial.setBounds(10, 25, 73, 14);
		}
		return lblRazonSocial;
	}

	private JTextField getRazonSocialTXT() {
		if (txtRazonSocial == null) {
			txtRazonSocial = new JTextField();
			txtRazonSocial.setBounds(85, 22, 277, 21);
		}
		return txtRazonSocial;
	}

	private JLabel getInscripcionLabel() {
		if (lblInscripcion == null) {
			lblInscripcion = new JLabel();
			lblInscripcion.setText("Inscripci�n");
			lblInscripcion.setFont(new java.awt.Font("Verdana", 0, 11));
			lblInscripcion.setBounds(10, 50, 70, 14);
		}
		return lblInscripcion;
	}

	private JTextField getInscripcionTXT() {
		if (txtInscripcion == null) {
			txtInscripcion = new JTextField();
			txtInscripcion.setBounds(85, 47, 143, 21);
		}
		return txtInscripcion;
	}

	private JLabel getDireccionLabel() {
		if (lblDireccion == null) {
			lblDireccion = new JLabel();
			lblDireccion.setText("Direcci�n");
			lblDireccion.setFont(new java.awt.Font("Verdana", 0, 11));
			lblDireccion.setBounds(10, 75, 70, 14);
		}
		return lblDireccion;
	}

	private JTextField getDireccionTXT() {
		if (txtDireccion == null) {
			txtDireccion = new JTextField();
			txtDireccion.setBounds(85, 72, 277, 21);
		}
		return txtDireccion;
	}

	private JLabel getSaldoLabel() {
		if (lblSaldo == null) {
			lblSaldo = new JLabel();
			lblSaldo.setText("Saldo");
			lblSaldo.setFont(new java.awt.Font("Verdana", 0, 11));
			lblSaldo.setBounds(10, 25, 70, 14);
		}
		return lblSaldo;
	}

	private JTextField getSaldoTXT() throws ParseException {
		if (txtSaldo == null) {
			txtSaldo = new JFormattedTextField();
			txtSaldo.setBounds(85, 23, 143, 21);
			// mascaraValidar = new MaskFormatter("##.##");
		}
		return txtSaldo;
	}

	private JLabel getCondicionesLabel() {
		if (lblCondiciones == null) {
			lblCondiciones = new JLabel();
			lblCondiciones.setText("Condiciones");
			lblCondiciones.setFont(new java.awt.Font("Verdana", 0, 11));
			lblCondiciones.setBounds(10, 50, 70, 14);
		}
		return lblCondiciones;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private JComboBox getCondicionesCBX() {
		if (cbxCondiciones == null) {
			cbxCondiciones = new JComboBox();
			cbxCondiciones.setModel(
					new DefaultComboBoxModel(new String[] { "Contado", "Transferencia", "30 d�as", "60 d�as" }));
			cbxCondiciones.setBounds(85, 47, 143, 21);
		}
		return cbxCondiciones;
	}

	private JLabel getCreditoLabel() {
		if (lblCredito == null) {
			lblCredito = new JLabel();
			lblCredito.setText("Cr�dito");
			lblCredito.setFont(new java.awt.Font("Verdana", 0, 11));
			lblCredito.setBounds(10, 75, 70, 14);
		}
		return lblCredito;
	}

	private JTextField getCreditoTXT() {
		if (txtCredito == null) {
			txtCredito = new JFormattedTextField();
			txtCredito.setBounds(85, 72, 143, 21);
		}
		return txtCredito;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton();
			btnAceptar.setText("Aceptar");
			btnAceptar.setBounds(144, 402, 115, 27);
			btnAceptar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (validacion()) {
						String textField = txtSaldo.getText();
						float floatSaldo = 0F;
						if (textField != null && !textField.isEmpty()) {
							floatSaldo = Float.parseFloat(textField);
						}
						textField = txtCredito.getText();
						float floatCredito = 0F;
						if (textField != null && !textField.isEmpty()) {
							floatCredito = Float.parseFloat(textField);
						}
						boolean flag = ControladorCliente.getInstancia().modificarCliente(clienteDTO.getNumeroCliente(),
								clienteDTO.getRazonSocial(), clienteDTO.getCuit(), txtInscripcion.getText(),
								txtDireccion.getText(), floatSaldo, floatCredito,
								cbxCondiciones.getSelectedItem().toString());
						if (flag) {
							JOptionPane.showMessageDialog(null,
									"Se ha modificado el cliente: " + txtRazonSocial.getText(),
									"Cliente modificado con �xito!", JOptionPane.INFORMATION_MESSAGE);
							ConsultaCliente c = new ConsultaCliente();
							c.setLocationRelativeTo(null);
							c.setVisible(true);
							dispose();
						} else {
							JOptionPane.showMessageDialog(null, "No se ha podido modificar al cliente", "Error",
									JOptionPane.ERROR_MESSAGE);
							txtCUIT.setFocusable(true);
						}
					}
				}
			});
		}
		return btnAceptar;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton();
			btnCancelar.setText("Cancelar");
			btnCancelar.setBounds(269, 402, 115, 27);
			btnCancelar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ConsultaCliente c = new ConsultaCliente();
					c.setLocationRelativeTo(null);
					c.setVisible(true);
					dispose();
				}
			});
		}
		return btnCancelar;
	}

	public boolean validacion() {
		if (txtCUIT.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor realice una b�squeda", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtCUIT.requestFocus();
			return false;
		} else if (txtInscripcion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la inscripci�n en AFIP", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtInscripcion.requestFocus();
			return false;
		} else if (txtDireccion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la direcci�n", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtDireccion.requestFocus();
			return false;
		} else if (txtSaldo.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el saldo", "Atenci�n", JOptionPane.WARNING_MESSAGE);
			txtSaldo.requestFocus();
			return false;
		} else if (cbxCondiciones.getSelectedItem().toString().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor seleccione la forma de pago.", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			cbxCondiciones.requestFocus();
			return false;
		} else if (txtCredito.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el l�mite de cr�dito.", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtCredito.requestFocus();
			return false;
		}
		return true;
	}

}
