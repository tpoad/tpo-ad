package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import controladorCliente.ControladorCliente;

public class AltaInsumo extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;
	private JFormattedTextField txtStock;
	private JFormattedTextField txtMinimo;
	private JFormattedTextField txtCompra;
	private JFormattedTextField txtCosto;
	private JButton aceptar;
	private JButton cancelar;

	public AltaInsumo() {
		inicializarComponentes();
		crearEventos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void inicializarComponentes() {
		this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
		this.setBounds(100, 100, 450, 469);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Cargar Insumo");
		titulo.setFont(new Font("Verdana", Font.BOLD, 20));
		titulo.setBounds(10, 11, 182, 26);
		this.getContentPane().add(titulo);

		JPanel General = new JPanel();
		General.setBounds(10, 42, 414, 229);
		General.setBorder(
				new TitledBorder(null, "Datos particulares", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(General);
		General.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 30, 72, 14);
		General.add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.setBounds(80, 27, 324, 20);
		General.add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(10, 57, 72, 14);
		General.add(lblStock);

		txtStock = new JFormattedTextField();
		txtStock.setColumns(10);
		txtStock.setBounds(80, 55, 162, 20);
		General.add(txtStock);

		JLabel lblMinimo = new JLabel("M\u00EDnimo");
		lblMinimo.setBounds(10, 90, 72, 14);
		General.add(lblMinimo);

		txtMinimo = new JFormattedTextField();
		txtMinimo.setBounds(80, 87, 162, 20);
		General.add(txtMinimo);
		txtMinimo.setColumns(10);

		JLabel lblCompra = new JLabel("Compra");
		lblCompra.setBounds(10, 121, 72, 14);
		General.add(lblCompra);

		txtCompra = new JFormattedTextField();
		txtCompra.setColumns(10);
		txtCompra.setBounds(80, 118, 162, 20);
		General.add(txtCompra);

		JLabel lblCosto = new JLabel("Costo");
		lblCosto.setBounds(10, 152, 72, 14);
		General.add(lblCosto);

		txtCosto = new JFormattedTextField();
		txtCosto.setColumns(10);
		txtCosto.setBounds(80, 149, 162, 20);
		General.add(txtCosto);

		aceptar = new JButton("Aceptar");
		aceptar.setBounds(240, 389, 89, 23);
		this.getContentPane().add(aceptar);

		cancelar = new JButton("Cancelar");
		cancelar.setBounds(335, 389, 89, 23);
		this.getContentPane().add(cancelar);

	}

	private void crearEventos() {

		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validacion()) {
					String textField = txtStock.getText();
					int intStock = 0;
					if (textField != null && !textField.isEmpty()) {
						intStock = Integer.parseInt(textField);
					}
					textField = txtMinimo.getText();
					int intMinimo = 0;
					if (textField != null && !textField.isEmpty()) {
						intMinimo = Integer.parseInt(textField);
					}
					textField = txtCompra.getText();
					int intCompra = 0;
					if (textField != null && !textField.isEmpty()) {
						intCompra = Integer.parseInt(textField);
					}
					textField = txtCosto.getText();
					Float floatCosto = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatCosto = Float.parseFloat(textField);
					}
					boolean flag;
					flag = ControladorCliente.getInstancia().altaInsumo(0, txtNombre.getText(), intStock, intMinimo,
							intCompra, floatCosto);
					if (flag) {
						JOptionPane.showMessageDialog(null, "Se ha generado el insumo: " + txtNombre.getText(),
								"Insumo generado con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaInsumo c = new ConsultaInsumo();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido generar el insumo", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaInsumo c = new ConsultaInsumo();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

	}

	public boolean validacion() {

		if (txtNombre.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el nombre del insumo", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtNombre.requestFocus();
			return false;
		} else if (txtStock.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el stock", "Atenci�n", JOptionPane.WARNING_MESSAGE);
			txtStock.requestFocus();
			return false;
		} else if (txtMinimo.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el m�nimo en almacen", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtMinimo.requestFocus();
			return false;
		} else if (txtCompra.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la cantidad a comprar", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtCompra.requestFocus();
			return false;
		} else if (txtCosto.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el costo", "Atenci�n", JOptionPane.WARNING_MESSAGE);
			txtCompra.requestFocus();
			return false;
		}
		return true;
	}

}
