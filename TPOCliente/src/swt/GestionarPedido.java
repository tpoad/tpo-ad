package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.ItemPedidoClienteDTO;
import dto.PedidoClienteDTO;

public class GestionarPedido extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;

	private JLabel lblNroPedido;
	private JFormattedTextField txtNroPedido;
	private JButton btnBuscar;
	private JLabel lblRazonSocial;
	private JTextField txtRazonSocial;
	private JLabel lblGeneracion;
	private JFormattedTextField txtGeneracion;
	private JLabel lblEstimada;
	private JFormattedTextField txtEstimada;
	private JLabel lblReal;
	private JFormattedTextField txtReal;
	private JLabel lblEstado;
	private JTextField txtEstado;
	private JLabel lblMonto;
	private JFormattedTextField txtMonto;

	JScrollPane scrollPaneListadoItemsPrenda;
	private DefaultTableModel jTableListadoModel;
	private JTable jTableListado;

	private JButton btnAceptar;
	private JButton btnRechazar;
	private JButton btnVolver;

	private PedidoClienteDTO pedidoClienteDTO;
	private int intNroPedido;

	public GestionarPedido() {
		super();
		inicializarComponentes();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Gestionar Pedido");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(10, 0, 383, 40);
			}
			{
				{
					lblNroPedido = new JLabel();
					getContentPane().add(lblNroPedido);
					{
						txtNroPedido = new JFormattedTextField();
						getContentPane().add(txtNroPedido);
						txtNroPedido.setBounds(118, 52, 143, 21);
						txtNroPedido.requestFocus();
					}
					getContentPane().add(getBtnBuscar());
					lblNroPedido.setText("N\u00FAmero Pedido");
					lblNroPedido.setFont(new java.awt.Font("Verdana", 0, 11));
					lblNroPedido.setBounds(12, 54, 96, 14);

				}
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1);
				jPanel1.setLayout(null);
				jPanel1.setBounds(12, 128, 372, 177);
				jPanel1.setBorder(BorderFactory.createTitledBorder("Datos del pedido"));

				lblRazonSocial = new JLabel("Raz\u00F3n Social");
				lblRazonSocial.setBounds(10, 27, 72, 14);
				jPanel1.add(lblRazonSocial);

				txtRazonSocial = new JTextField();
				txtRazonSocial.setEditable(false);
				txtRazonSocial.setColumns(10);
				txtRazonSocial.setBounds(93, 24, 269, 20);
				jPanel1.add(txtRazonSocial);
				{

					lblGeneracion = new JLabel();
					lblGeneracion.setText("Generaci\u00F3n");
					lblGeneracion.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblGeneracion.setBounds(10, 55, 83, 14);
					jPanel1.add(lblGeneracion);

					txtGeneracion = new JFormattedTextField();
					txtGeneracion.setBounds(93, 52, 143, 21);
					txtGeneracion.setEnabled(false);
					jPanel1.add(txtGeneracion);

					lblEstimada = new JLabel();
					lblEstimada.setText("Estimada");
					lblEstimada.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblEstimada.setBounds(10, 84, 83, 14);
					jPanel1.add(lblEstimada);

					txtEstimada = new JFormattedTextField();
					txtEstimada.setBounds(93, 81, 143, 21);
					txtEstimada.setEnabled(false);
					jPanel1.add(txtEstimada);

					lblReal = new JLabel();
					lblReal.setText("Real");
					lblReal.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblReal.setBounds(10, 112, 83, 14);
					jPanel1.add(lblReal);

					txtReal = new JFormattedTextField();
					txtReal.setBounds(93, 110, 143, 21);
					txtReal.setEnabled(false);
					jPanel1.add(txtReal);

					lblEstado = new JLabel();
					lblEstado.setText("Estado");
					lblEstado.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblEstado.setBounds(10, 141, 83, 14);
					jPanel1.add(lblEstado);

					txtEstado = new JTextField();
					txtEstado.setEnabled(false);
					txtEstado.setBounds(93, 139, 143, 21);
					jPanel1.add(txtEstado);

					lblMonto = new JLabel();
					lblMonto.setText("Monto Total");
					lblMonto.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblMonto.setHorizontalAlignment(SwingConstants.CENTER);
					lblMonto.setBounds(246, 112, 116, 14);
					jPanel1.add(lblMonto);

					txtMonto = new JFormattedTextField();
					txtMonto.setEnabled(false);
					txtMonto.setBounds(246, 139, 116, 21);
					jPanel1.add(txtMonto);
				}
			}

			scrollPaneListadoItemsPrenda = new JScrollPane();
			scrollPaneListadoItemsPrenda.setBounds(10, 313, 374, 209);
			getContentPane().add(scrollPaneListadoItemsPrenda);
			{
				jTableListadoModel = new DefaultTableModel();
				jTableListadoModel.addColumn("Prenda");
				jTableListadoModel.addColumn("C");
				jTableListadoModel.addColumn("T");
				jTableListadoModel.addColumn("Cant");
				jTableListadoModel.addColumn("$ un.");
				jTableListadoModel.addColumn("$ Final");

				jTableListado = new JTable(jTableListadoModel);
				scrollPaneListadoItemsPrenda.setViewportView(jTableListado);
				jTableListado.setModel(jTableListadoModel);
			}

			getContentPane().add(getBtnAceptar());
			getContentPane().add(getBtnRechazar());
			getContentPane().add(getBtnVolver());

			this.setSize(409, 600);
			// pack();
		} catch (Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton();
			btnBuscar.setText("Buscar");
			btnBuscar.setBounds(269, 84, 115, 27);
			btnBuscar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String textField = txtNroPedido.getText();
					intNroPedido = 0;
					if (textField != null && !textField.isEmpty()) {
						intNroPedido = Integer.parseInt(textField);
					}
					pedidoClienteDTO = ControladorCliente.getInstancia().getPedidoCliente(intNroPedido);
					if (pedidoClienteDTO != null) {
						btnBuscar.setEnabled(false);
						txtRazonSocial.setText(pedidoClienteDTO.getCliente().getRazonSocial());
						SimpleDateFormat formato = new SimpleDateFormat("dd - MMM / yyyy");
						// (pedidoClienteDTO.getFechaGeneracion() != null) ?
						// txtGeneracion.setText(formato.format(pedidoClienteDTO.getFechaGeneracion()))
						// : txtGeneracion.setText("-");
						if (pedidoClienteDTO.getFechaGeneracion() != null) {
							txtGeneracion.setText(formato.format(pedidoClienteDTO.getFechaGeneracion()));
						} else {
							txtGeneracion.setText("-");
						}
						if (pedidoClienteDTO.getFechaProbable() != null) {
							txtEstimada.setText(formato.format(pedidoClienteDTO.getFechaProbable()));
						} else {
							txtEstimada.setText("-");
						}
						if (pedidoClienteDTO.getFechaReal() != null) {
							txtReal.setText(formato.format(pedidoClienteDTO.getFechaReal()));
						} else {
							txtReal.setText("-");
						}
						txtEstado.setText(pedidoClienteDTO.getEstado().toString());
						txtMonto.setValue(pedidoClienteDTO.getTotal());
						for (ItemPedidoClienteDTO ipcdto : pedidoClienteDTO.getItemsPedidoCliente()) {
							jTableListadoModel
									.addRow(new Object[] { ipcdto.getItemPrenda().getPrenda().getDescripcion(),
											ipcdto.getItemPrenda().getColor(), ipcdto.getItemPrenda().getTalle(),
											ipcdto.getCantidad() + "un",
											"$ " + ipcdto.getItemPrenda().getPrenda().getPrecioVenta(),
											"$ " + (ipcdto.getCantidad()
													* ipcdto.getItemPrenda().getPrenda().getPrecioVenta()) });
						}
					} else {
						JOptionPane.showMessageDialog(null, "No se ha encontrado el pedido", "Error",
								JOptionPane.ERROR_MESSAGE);
						txtNroPedido.setEnabled(true);
						txtNroPedido.setText("");
						txtNroPedido.requestFocus();
					}
				}
			});
		}
		return btnBuscar;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton();
			btnAceptar.setText("Aceptar");
			btnAceptar.setBounds(153, 533, 115, 27);
			btnAceptar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					boolean flag;
//					flag = ControladorCliente.getInstancia().aprobarPedidoCliente(intNroPedido);
					flag = ControladorCliente.getInstancia().procesarPedidoCliente(intNroPedido);
					if (flag) {
						JOptionPane.showMessageDialog(null,
								"Se ha dado curso al pedido " + pedidoClienteDTO.getNumeroPedido()
										+ " para el cliente " + pedidoClienteDTO.getCliente().getRazonSocial(),
								"Pedido confirmado!!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaPedidoCliente c = new ConsultaPedidoCliente();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se pudo gestionar el pedido", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnAceptar;
	}

	private JButton getBtnRechazar() {
		if (btnRechazar == null) {
			btnRechazar = new JButton();
			btnRechazar.setText("Rechazar");
			btnRechazar.setBounds(278, 533, 115, 27);
			btnRechazar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnRechazar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					boolean flag;
					flag = ControladorCliente.getInstancia().cancelarPedidoCliente(intNroPedido);
					if (flag) {
						JOptionPane.showMessageDialog(null,
								"Se ha rechazado al pedido " + pedidoClienteDTO.getNumeroPedido()
										+ " para el cliente " + pedidoClienteDTO.getCliente().getRazonSocial(),
								"Pedido rechazado!!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaPedidoCliente c = new ConsultaPedidoCliente();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido rechazar el pedido", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnRechazar;
	}
	
	private JButton getBtnVolver() {
		if(btnVolver == null){
			btnVolver = new JButton();
			btnVolver.setText("Volver");
			btnVolver.setBounds(10, 533, 115, 27);
			btnVolver.setFont(new java.awt.Font("Verdana", 0, 11));
			btnVolver.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ConsultaPedidoCliente c = new ConsultaPedidoCliente();
					c.setLocationRelativeTo(null);
					c.setVisible(true);
					dispose();
				}
			});
		}
		return btnVolver;
	}
	
}
