package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.ClienteDTO;

public class ConsultaCliente extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoClientes;
	private JTable jTableListado;
	private JButton alta;
	private JButton baja;
	private JButton modificar;
	private JButton volver;

	public ConsultaCliente() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Clientes");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 245, 35);
			}
			{
				jScrollPaneListadoClientes = new JScrollPane();
				getContentPane().add(jScrollPaneListadoClientes);
				jScrollPaneListadoClientes.setBounds(12, 53, 799, 311);
				{
					List<ClienteDTO> clienteDTO = ControladorCliente.getInstancia().getClientes();
//					List<ClienteDTO> clienteDTO = new ArrayList<ClienteDTO>();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("Nro");
					jTableListadoModel.addColumn("Razon Social");
					jTableListadoModel.addColumn("CUIT");
					jTableListadoModel.addColumn("Inscripci�n");
					jTableListadoModel.addColumn("Direccion");
					jTableListadoModel.addColumn("Saldo");
					jTableListadoModel.addColumn("Cr�dito");
					jTableListadoModel.addColumn("Pago");
					for (ClienteDTO c : clienteDTO) {
						jTableListadoModel.addRow(new Object[] { 
								c.getNumeroCliente(), 
								c.getRazonSocial(), 
								c.getCuit(),
								c.getInscripcionAFIP(), 
								c.getDireccion(), 
								c.getSaldo(), 
								c.getLimiteCredito(),
								c.getCondicionesPago() });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoClientes.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			alta = new JButton("Nuevo");
			alta.setBounds(235, 389, 89, 23);
			this.getContentPane().add(alta);
			baja = new JButton("Eliminar");
			baja.setBounds(335, 389, 89, 23);
			this.getContentPane().add(baja);
			modificar = new JButton("Modificar");
			modificar.setBounds(435, 389, 89, 23);
			this.getContentPane().add(modificar);
			volver = new JButton("Volver");
			volver.setBounds(335, 419, 89, 23);
			this.getContentPane().add(volver);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a los clientes", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaCliente c = new AltaCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		baja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BajaCliente c = new BajaCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarCliente c = new ModificarCliente();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}

}
