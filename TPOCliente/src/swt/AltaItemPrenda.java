package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import controladorCliente.ControladorCliente;
import dto.InsumoDTO;
import dto.PrendaDTO;
import enums.Color;
import enums.Talle;
import javax.swing.JTextField;

public class AltaItemPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings("rawtypes")
	private JComboBox cbxPrenda;
	private JTextField txtDescripcion;
	
	@SuppressWarnings("rawtypes")
	private JComboBox cbxColor;
	
	@SuppressWarnings("rawtypes")
	private JComboBox cbxInsumo;
	
	@SuppressWarnings("rawtypes")
	private JComboBox cbxTalle;
	private JFormattedTextField txtStock;
	private JFormattedTextField txtMinimo;
	private JFormattedTextField txtProduccion;
	private JButton aceptar;
	private JButton cancelar;

	public AltaItemPrenda() {
		inicializarComponentes();
		crearEventos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void inicializarComponentes() {
		this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
		this.setBounds(100, 100, 450, 469);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Cargar Item Prenda");
		titulo.setFont(new Font("Verdana", Font.BOLD, 20));
		titulo.setBounds(10, 11, 414, 26);
		this.getContentPane().add(titulo);

		JPanel General = new JPanel();
		General.setBounds(10, 42, 414, 249);
		General.setBorder(
				new TitledBorder(null, "Datos particulares", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(General);
		General.setLayout(null);
//+
		JLabel lblPrenda = new JLabel("Prenda");
		lblPrenda.setBounds(10, 30, 72, 14);
		General.add(lblPrenda);

		cbxPrenda = new JComboBox();
		List<PrendaDTO> prendaDTO = ControladorCliente.getInstancia().obtenerPrendas();
		List<String> listaPrendas = new ArrayList<String>();
		for (PrendaDTO p : prendaDTO) {
			listaPrendas.add(new Integer(p.getCodigo()).toString());
		}
		
		cbxPrenda.setModel(new DefaultComboBoxModel(listaPrendas.toArray()));
		cbxPrenda.setBounds(80, 27, 80, 20);
		General.add(cbxPrenda);
		// cbxPrenda.setColumns(10);
		cbxPrenda.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
				String textField = cbxPrenda.getSelectedItem().toString();
				int intPrenda = 0;
				if (textField != null && !textField.isEmpty()) {
					intPrenda = Integer.parseInt(textField);
				}
				txtDescripcion.setText(ControladorCliente.getInstancia().getPrendaBoba(intPrenda).getDescripcion());
		    }
		});

		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(170, 27, 234, 20);
		txtDescripcion.setEnabled(false);
		General.add(txtDescripcion);
//+
		JLabel lblColor = new JLabel("Color");
		lblColor.setBounds(10, 58, 72, 14);
		General.add(lblColor);

		cbxColor = new JComboBox();
	    List<Color> listColor = Arrays.asList(Color.values());
		cbxColor.setModel(new DefaultComboBoxModel(listColor.toArray()));
		cbxColor.setBounds(80, 55, 162, 20);
		General.add(cbxColor);
		// cbxColor.setColumns(10);

		JLabel lblTalle = new JLabel("Talle");
		lblTalle.setBounds(10, 88, 72, 14);
		General.add(lblTalle);

		cbxTalle = new JComboBox();
	    List<Talle> listTalle = Arrays.asList(Talle.values());
		cbxTalle.setModel(new DefaultComboBoxModel(listTalle.toArray()));
		cbxTalle.setBounds(80, 85, 162, 20);
		General.add(cbxTalle);
		// cbxTalle.setColumns(10);

		JLabel lblStock = new JLabel("Stock");
		lblStock.setBounds(10, 121, 72, 14);
		General.add(lblStock);

		txtStock = new JFormattedTextField();
		txtStock.setBounds(80, 118, 162, 20);
		General.add(txtStock);
		txtStock.setColumns(10);

		JLabel lblMinimo = new JLabel("Minimo");
		lblMinimo.setBounds(10, 152, 72, 14);
		General.add(lblMinimo);

		txtMinimo = new JFormattedTextField();
		txtMinimo.setColumns(10);
		txtMinimo.setBounds(80, 149, 162, 20);
		General.add(txtMinimo);

		JLabel lblProduccion = new JLabel("Producci\u00F3n");
		lblProduccion.setBounds(10, 183, 72, 14);
		General.add(lblProduccion);

		txtProduccion = new JFormattedTextField();
		txtProduccion.setColumns(10);
		txtProduccion.setBounds(80, 180, 162, 20);
		General.add(txtProduccion);

//+		
//+
/**/			JLabel lblInsumo = new JLabel("Insumo");
/**/			lblInsumo.setBounds(10, 214, 72, 14);
/**/			General.add(lblInsumo);
/**/
/*cbxPrenda*/	cbxInsumo = new JComboBox();
/**/				List<InsumoDTO> insumoDTO = ControladorCliente.getInstancia().obtenerInsumos(); 
/**/				List<String> listaInsumos = new ArrayList<String>();
/**/				for (InsumoDTO i : insumoDTO) {
/**/					listaInsumos.add(new Integer(i.getCodigoInterno()).toString());
/**/				}
/**/				cbxInsumo.setModel(new DefaultComboBoxModel(listaInsumos.toArray()));
/**/				cbxInsumo.setBounds(80, 211, 162, 20);
/**/				General.add(cbxInsumo);
/**/				// cbxPrenda.setColumns(10);
/**/				cbxInsumo.addActionListener (new ActionListener () {
/**/				    public void actionPerformed(ActionEvent e) {
/**/						String textField = cbxInsumo.getSelectedItem().toString();
/**/						int intInsumo = 0;
/**/						if (textField != null && !textField.isEmpty()) {
/**/							intInsumo = Integer.parseInt(textField);
/**/						}
/**/						txtDescripcion.setText(ControladorCliente.getInstancia().getInsumo(intInsumo).getNombre());
/**/				    }
				});

/**/		txtDescripcion = new JTextField();
/**/		txtDescripcion.setColumns(10);
/**/		txtDescripcion.setBounds(170, 27, 234, 20);
/**/		txtDescripcion.setEnabled(false);
/**/		General.add(txtDescripcion);
//+
//+
				
		aceptar = new JButton("Aceptar");
		aceptar.setBounds(240, 389, 89, 23);
		this.getContentPane().add(aceptar);

		cancelar = new JButton("Cancelar");
		cancelar.setBounds(335, 389, 89, 23);
		this.getContentPane().add(cancelar);

	}

	private void crearEventos() {
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validacion()) {
					String textField = txtStock.getText();
					int intStock = 0;
					if (textField != null && !textField.isEmpty()) {
						intStock = Integer.parseInt(textField);
					}
					textField = txtMinimo.getText();
					int intMinimo = 0;
					if (textField != null && !textField.isEmpty()) {
						intMinimo = Integer.parseInt(textField);
					}
					textField = txtProduccion.getText();
					int intProduccion = 0;
					if (textField != null && !textField.isEmpty()) {
						intProduccion = Integer.parseInt(textField);
					}
					textField = cbxPrenda.getSelectedItem().toString();
					int intPrenda = 0;
					if (textField != null && !textField.isEmpty()) {
						intPrenda = Integer.parseInt(textField);
					}
/*****
 * ALTA ITEM
 *  * ****/			PrendaDTO prendaDTO = ControladorCliente.getInstancia().getPrendaBoba(intPrenda);
					boolean flag = ControladorCliente.getInstancia().altaItemPrenda(0,
							Color.valueOf(cbxColor.getSelectedItem().toString()),
							Talle.valueOf(cbxTalle.getSelectedItem().toString()), intStock, intMinimo, intProduccion,
							prendaDTO);
					if (flag) {
						JOptionPane.showMessageDialog(null,
								"Se ha generado el item prenda para: " + intPrenda + " ["
										+ Color.valueOf(cbxColor.getSelectedItem().toString()) + "/"
										+ Talle.valueOf(cbxTalle.getSelectedItem().toString()) + "]",
								"Item prenda generado con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaItemPrenda c = new ConsultaItemPrenda();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido generar el item prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaItemPrenda c = new ConsultaItemPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

	}

	public boolean validacion() {
		if (txtStock.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el stock", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtStock.requestFocus();
			return false;
		} else if (txtMinimo.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el m�nimo de alerta", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtMinimo.requestFocus();
			return false;
		} else if (txtProduccion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la cantidad a producir", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtMinimo.requestFocus();
			return false;
		}
		return true;
	}
}