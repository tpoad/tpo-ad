package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controladorCliente.ControladorCliente;
import dto.ItemPrendaDTO;
import enums.Color;
import enums.Talle;

public class BajaItemPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;

	private JLabel lblSubcodigo;
	private JFormattedTextField txtSubcodigo;
	private JButton btnBuscar;
	private JLabel lblCodigo;
	private JFormattedTextField txtCodigo;
	private JTextField txtDescripcion;
	private JLabel lblColor;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxColor;
	private JLabel lblTalle;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxTalle;
	private JLabel lblStock;
	private JFormattedTextField txtStock;
	private JLabel lblMinima;
	private JFormattedTextField txtMinima;
	private JLabel lblProduccion;
	private JFormattedTextField txtProduccion;

	private JButton btnAceptar;
	private JButton btnCancelar;

	private ItemPrendaDTO itemPrendaDTO;

	public BajaItemPrenda() {
		super();
		inicializarComponentes();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Eliminar Item Prenda");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(10, 0, 383, 40);
			}
			{
				{
					lblSubcodigo = new JLabel();
					getContentPane().add(lblSubcodigo);
					{
						txtSubcodigo = new JFormattedTextField();
						getContentPane().add(txtSubcodigo);
						txtSubcodigo.setBounds(118, 52, 143, 21);
						txtSubcodigo.requestFocus();
					}
					getContentPane().add(getBtnBuscar());
					lblSubcodigo.setText("SubC\u00F3digo");
					lblSubcodigo.setFont(new java.awt.Font("Verdana", 0, 11));
					lblSubcodigo.setBounds(12, 54, 80, 14);

				}
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1);
				jPanel1.setLayout(null);
				jPanel1.setBounds(12, 128, 372, 248);
				jPanel1.setBorder(BorderFactory.createTitledBorder("Datos del item prenda"));
				{
					lblCodigo = new JLabel();
					lblCodigo.setText("Prenda");
					lblCodigo.setFont(new java.awt.Font("Verdana", 0, 11));
					lblCodigo.setBounds(10, 27, 83, 14);
					jPanel1.add(lblCodigo);

					txtCodigo = new JFormattedTextField();
					txtCodigo.setBounds(93, 25, 75, 21);
					txtCodigo.setEnabled(false);
					jPanel1.add(txtCodigo);

					txtDescripcion = new JTextField();
					txtDescripcion.setBounds(178, 25, 184, 21);
					txtDescripcion.setEnabled(false);
					jPanel1.add(txtDescripcion);

					lblColor = new JLabel();
					lblColor.setText("Color");
					lblColor.setFont(new java.awt.Font("Verdana", 0, 11));
					lblColor.setBounds(10, 86, 83, 14);
					jPanel1.add(lblColor);

					cbxColor = new JComboBox();
					cbxColor.setModel(
							new DefaultComboBoxModel(new String[] { "BLANCO", "NEGRO", "ROJO", "AZUL", "AMARILLO" }));
					cbxColor.setBounds(93, 83, 143, 21);
					cbxColor.setEnabled(false);
					jPanel1.add(cbxColor);

					lblTalle = new JLabel();
					lblTalle.setText("Talle");
					lblTalle.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblTalle.setBounds(10, 114, 83, 14);
					jPanel1.add(lblTalle);

					cbxTalle = new JComboBox();
					cbxTalle.setModel(new DefaultComboBoxModel(new String[] { "XS", "S", "M", "L", "XL" }));
					cbxTalle.setBounds(93, 112, 143, 21);
					cbxTalle.setEnabled(false);
					jPanel1.add(cbxTalle);

					lblStock = new JLabel();
					lblStock.setText("Stock");
					lblStock.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblStock.setBounds(10, 144, 83, 14);
					jPanel1.add(lblStock);

					txtStock = new JFormattedTextField();
					txtStock.setBounds(93, 141, 143, 21);
					txtStock.setEnabled(false);
					jPanel1.add(txtStock);

					lblMinima = new JLabel();
					lblMinima.setText("Cant M\u00EDn");
					lblMinima.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblMinima.setBounds(10, 173, 83, 14);
					jPanel1.add(lblMinima);

					txtMinima = new JFormattedTextField();
					txtMinima.setBounds(93, 170, 143, 21);
					txtMinima.setEnabled(false);
					jPanel1.add(txtMinima);

					lblProduccion = new JLabel();
					lblProduccion.setText("Cant Prod");
					lblProduccion.setFont(new Font("Verdana", Font.PLAIN, 11));
					lblProduccion.setBounds(10, 201, 83, 14);
					jPanel1.add(lblProduccion);

					txtProduccion = new JFormattedTextField();
					txtProduccion.setBounds(93, 199, 143, 21);
					txtProduccion.setEnabled(false);
					jPanel1.add(txtProduccion);
				}
			}
			getContentPane().add(getBtnAceptar());
			getContentPane().add(getBtnCancelar());

			this.setSize(409, 500);
			// pack();
		} catch (Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton();
			btnBuscar.setText("Buscar");
			btnBuscar.setBounds(269, 84, 115, 27);
			btnBuscar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String textField = txtSubcodigo.getText();
					int intSubcodigo = 0;
					if (textField != null && !textField.isEmpty()) {
						intSubcodigo = Integer.parseInt(textField);
					}
					itemPrendaDTO = ControladorCliente.getInstancia().obtenerItemPrenda(intSubcodigo);
					if (itemPrendaDTO != null) {
						txtCodigo.setValue(itemPrendaDTO.getPrenda().getCodigo());
						txtDescripcion.setText(itemPrendaDTO.getPrenda().getDescripcion());
						cbxColor.setSelectedItem(itemPrendaDTO.getColor().toString());
						cbxTalle.setSelectedItem(itemPrendaDTO.getTalle().toString());
						txtStock.setValue(itemPrendaDTO.getCantidadStock());
						txtMinima.setValue(itemPrendaDTO.getCantidadMinima());
						txtProduccion.setValue(itemPrendaDTO.getCantidadProducir());
						btnBuscar.setEnabled(false);
					} else {
						JOptionPane.showMessageDialog(null, "No se ha encontrado el item prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
						txtSubcodigo.setEnabled(true);
						txtSubcodigo.setText("");
						txtSubcodigo.requestFocus();
					}
				}
			});
		}
		return btnBuscar;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton();
			btnAceptar.setText("Aceptar");
			btnAceptar.setBounds(144, 402, 115, 27);
			btnAceptar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					boolean flag = ControladorCliente.getInstancia().eliminarPrenda(itemPrendaDTO.getSubcodigo());
					if (flag) {
						JOptionPane.showMessageDialog(null,
								// "Se ha eliminado el item prenda para: " + itemPrendaDTO.getPrenda() + " ["
								"Se ha eliminado el item prenda para: " + itemPrendaDTO.getPrenda().getCodigo() + " ["
										+ Color.valueOf(cbxColor.getSelectedItem().toString()) + "/"
										+ Talle.valueOf(cbxTalle.getSelectedItem().toString()) + "]",
								"Item prenda eliminado con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaItemPrenda c = new ConsultaItemPrenda();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido eliminar el item prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnAceptar;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton();
			btnCancelar.setText("Cancelar");
			btnCancelar.setBounds(269, 402, 115, 27);
			btnCancelar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ConsultaItemPrenda c = new ConsultaItemPrenda();
					c.setLocationRelativeTo(null);
					c.setVisible(true);
					dispose();
				}
			});
		}
		return btnCancelar;
	}

}
