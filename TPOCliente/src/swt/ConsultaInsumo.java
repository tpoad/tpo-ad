package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.InsumoDTO;

public class ConsultaInsumo extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoPrendas;
	private JTable jTableListado;
	private JButton alta;
	private JButton baja;
	private JButton modificar;
	private JButton volver;

	public ConsultaInsumo() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Insumos");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 799, 35);
			}
			{
				jScrollPaneListadoPrendas = new JScrollPane();
				getContentPane().add(jScrollPaneListadoPrendas);
				jScrollPaneListadoPrendas.setBounds(12, 53, 799, 311);
				{
					List<InsumoDTO> insumoDTO = ControladorCliente.getInstancia().obtenerInsumos();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("C�digo");
					jTableListadoModel.addColumn("Nombre");
					jTableListadoModel.addColumn("Stock");
					jTableListadoModel.addColumn("M�nimo");
					jTableListadoModel.addColumn("Comprar");
					jTableListadoModel.addColumn("Costo");
					for (InsumoDTO i : insumoDTO) {
						jTableListadoModel.addRow(new Object[] { 
								i.getCodigoInterno(),
								i.getNombre(),
								i.getCantidadStock(),
								i.getCantidadMinima(),
								i.getCantidadComprar(),
								i.getCostoActual() });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoPrendas.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			alta = new JButton("Nuevo");
			alta.setBounds(235, 389, 89, 23);
			this.getContentPane().add(alta);
			baja = new JButton("Eliminar");
			baja.setBounds(335, 389, 89, 23);
			this.getContentPane().add(baja);
			modificar = new JButton("Modificar");
			modificar.setBounds(435, 389, 89, 23);
			this.getContentPane().add(modificar);
			
			volver = new JButton("Volver");
			volver.setBounds(335, 420, 89, 23);
			getContentPane().add(volver);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a los items prenda", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaInsumo a = new AltaInsumo();
				a.setLocationRelativeTo(null);
				a.setVisible(true);
				setVisible(false);
			}
		});
		
		baja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BajaInsumo b = new BajaInsumo();
				b.setLocationRelativeTo(null);
				b.setVisible(true);
				setVisible(false);
			}
		});
		
		modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarInsumo m = new ModificarInsumo();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
		
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}
}
