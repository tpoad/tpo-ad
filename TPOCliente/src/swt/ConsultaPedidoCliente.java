package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.PedidoClienteDTO;

public class ConsultaPedidoCliente extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoClientes;
	private JTable jTableListado;
	private JButton gestionar;
//	private JButton baja;
//	private JButton modificar;
	private JButton volver;

	public ConsultaPedidoCliente() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Pedidos Cliente");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 799, 35);
			}
			{
				jScrollPaneListadoClientes = new JScrollPane();
				getContentPane().add(jScrollPaneListadoClientes);
				jScrollPaneListadoClientes.setBounds(12, 53, 799, 311);
				{
					List<PedidoClienteDTO> lpcdto = ControladorCliente.getInstancia().getPedidosCliente();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("Nro");
					jTableListadoModel.addColumn("Razon Social");
					jTableListadoModel.addColumn("Monto");
					jTableListadoModel.addColumn("Generación");
					jTableListadoModel.addColumn("Estimada");
					jTableListadoModel.addColumn("Real");
					jTableListadoModel.addColumn("Estado");
					for (PedidoClienteDTO pcdto : lpcdto) {
						SimpleDateFormat formato = new SimpleDateFormat("dd - MMM / yyyy");
						jTableListadoModel.addRow(new Object[] {
								pcdto.getNumeroPedido(),
								pcdto.getCliente().getRazonSocial(), 
								pcdto.getTotal(),
								(pcdto.getFechaGeneracion() != null) ? formato.format(pcdto.getFechaGeneracion()) : "-",
								(pcdto.getFechaProbable() != null) ? formato.format(pcdto.getFechaProbable()) : "-",
								(pcdto.getFechaReal() != null) ? formato.format(pcdto.getFechaReal()) : "-",
								pcdto.getEstado() });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoClientes.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			gestionar = new JButton("Gestionar");
//			gestionar.setBounds(235, 389, 89, 23);
			gestionar.setBounds(335, 389, 89, 23);
			this.getContentPane().add(gestionar);
//			baja = new JButton("Eliminar");
//			baja.setBounds(335, 389, 89, 23);
//			this.getContentPane().add(baja);
//			modificar = new JButton("Modificar");
//			modificar.setBounds(435, 389, 89, 23);
//			this.getContentPane().add(modificar);
			volver = new JButton("Volver");
			volver.setBounds(335, 419, 89, 23);
			this.getContentPane().add(volver);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a los pedidos de los clientes", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		gestionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GestionarPedido g = new GestionarPedido();
				g.setLocationRelativeTo(null);
				g.setVisible(true);
				setVisible(false);
			}
		});
//		baja.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				Menu m = new Menu();
//				m.setLocationRelativeTo(null);
//				m.setVisible(true);
//				setVisible(false);
//			}
//		});
//		modificar.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				Menu m = new Menu();
//				m.setLocationRelativeTo(null);
//				m.setVisible(true);
//				setVisible(false);
//			}
//		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}

}
