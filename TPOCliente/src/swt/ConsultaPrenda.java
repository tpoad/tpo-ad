package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.PrendaDTO;

public class ConsultaPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoPrendas;
	private JTable jTableListado;
	private JButton items;
	private JButton alta;
	private JButton baja;
	private JButton modificar;
	private JButton volver;

	public ConsultaPrenda() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Prendas");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 245, 35);
			}
			{
				jScrollPaneListadoPrendas = new JScrollPane();
				getContentPane().add(jScrollPaneListadoPrendas);
				jScrollPaneListadoPrendas.setBounds(12, 53, 799, 311);
				{
					List<PrendaDTO> prendaDTO = ControladorCliente.getInstancia().obtenerPrendas();
//					List<PrendaDTO> prendaDTO = new ArrayList<PrendaDTO>();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("C�digo");
					jTableListadoModel.addColumn("Descripci�n");
					jTableListadoModel.addColumn("Temporada");
					jTableListadoModel.addColumn("Molde");
					jTableListadoModel.addColumn("T Producci�n");
					jTableListadoModel.addColumn("Costo");
					jTableListadoModel.addColumn("Precio Venta");
					jTableListadoModel.addColumn("Ganancia");
					jTableListadoModel.addColumn("Estado");
					for (PrendaDTO c : prendaDTO) {
						String estado;
						if(c.getEstado() == 1)
							estado = "Activa";
						else
							estado = "Inactiva";
						jTableListadoModel.addRow(new Object[] { 
								c.getCodigo(),
								c.getDescripcion(),
								c.getTemporada(),
								c.getMolde(),
								c.getTiempoProduccion().getTime() + " hs",
								"$ " + c.getCostoActual(),
								"$ " + c.getPrecioVenta(),
								"$ " + new Float(c.getPrecioVenta() - c.getCostoActual()),
								estado });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoPrendas.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			alta = new JButton("Nuevo");
			alta.setBounds(235, 389, 89, 23);
			this.getContentPane().add(alta);
			baja = new JButton("Eliminar");
			baja.setBounds(335, 389, 89, 23);
			this.getContentPane().add(baja);
			modificar = new JButton("Modificar");
			modificar.setBounds(435, 389, 89, 23);
			this.getContentPane().add(modificar);
			volver = new JButton("Volver");
			volver.setBounds(335, 419, 89, 23);
			this.getContentPane().add(volver);
			items = new JButton("Items");
			items.setBounds(12, 389, 89, 23);
			getContentPane().add(items);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a las prendas", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		items.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ConsultaItemPrenda c = new ConsultaItemPrenda(); //SE PUEDE PASAR UN VALOR (OBJ CONTENEDOR) Y EL MODO (LECTURA, ESCRITURA, ETC.)?!
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		
		alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaPrenda c = new AltaPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		baja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BajaPrenda c = new BajaPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarPrenda c = new ModificarPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				setVisible(false);
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}
}
