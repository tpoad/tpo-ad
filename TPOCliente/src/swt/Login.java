package swt;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class Login extends JFrame {

	private static final long serialVersionUID = 1L;
	private static Login instancia = new Login();
	private JLabel jLabelLogo;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;
	private JLabel jLabelUsuario;
	private JTextField jTextFieldUsuario;
	private JLabel jLabelContrasena;
	private JPasswordField contrasena;
	private JButton jButtonAceptar;
	private JButton jButtonCancelar;
	private JLabel jLabelMje;

	{
		// Set Look & Feel
		try {
			javax.swing.UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Login inst = new Login();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	private Login() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	public static Login getInstancia() {
		return instancia;
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			GroupLayout thisLayout = new GroupLayout(getContentPane());
			getContentPane().setLayout(thisLayout);
			{
				jPanel1 = new JPanel();
				{
					jLabelLogo = new JLabel();
					jLabelLogo.setBounds(123, 48, 168, 124);
					jLabelLogo.setIcon(new ImageIcon(getClass().getClassLoader().getResource("recursos/compras.jpg")));
					jLabelLogo.setLayout(null);
				}
				{
					jLabelTitulo = new JLabel();
					jLabelTitulo.setBounds(100, 11, 205, 26);
					jLabelTitulo.setText("Gut Gekleidet Wurst");
					jLabelTitulo.setLayout(null);
					jLabelTitulo.setFont(new java.awt.Font("Tahoma", 1, 20));
				}
				{
					jLabelUsuario = new JLabel();
					jLabelUsuario.setBounds(10, 230, 81, 14);
					jLabelUsuario.setText("Usuario");
					jLabelUsuario.setLayout(null);
					jLabelUsuario.setFont(new java.awt.Font("Tahoma", 1, 11));
				}
				{
					jLabelContrasena = new JLabel();
					jLabelContrasena.setBounds(10, 256, 81, 14);
					jLabelContrasena.setText("Contrase�a");
					jLabelContrasena.setFont(new java.awt.Font("Tahoma", 1, 11));
					jLabelContrasena.setLayout(null);
				}
				{
					jLabelMje = new JLabel();
					jLabelMje.setBounds(123, 284, 269, 14);
					jLabelMje.setText("El usuario y/o contrase�a no es v�lido");
					jLabelMje.setFont(new java.awt.Font("Tahoma", 1, 11));
					jLabelMje.setLayout(null);
					jLabelMje.setForeground(Color.RED);
					jLabelMje.setVisible(false);
				}
				{
					jTextFieldUsuario = new JTextField();
					jTextFieldUsuario.setBounds(123, 227, 269, 20);
				}
				{
					contrasena = new JPasswordField();
					contrasena.setBounds(123, 253, 269, 20);
				}
				{
					jButtonCancelar = new JButton();
					jButtonCancelar.setBounds(310, 312, 75, 23);
					jButtonCancelar.setText("Cancelar");
				}
				{
					jButtonAceptar = new JButton();
					jButtonAceptar.setBounds(231, 312, 71, 23);
					jButtonAceptar.setText("Aceptar");
				}
			}
			thisLayout.setVerticalGroup(
					thisLayout.createSequentialGroup().addComponent(jPanel1, 0, 346, Short.MAX_VALUE));
			thisLayout.setHorizontalGroup(
					thisLayout.createSequentialGroup().addComponent(jPanel1, 0, 402, Short.MAX_VALUE));
			jPanel1.setLayout(null);
			jPanel1.add(jLabelUsuario);
			jPanel1.add(jLabelContrasena);
			jPanel1.add(jLabelTitulo);
			jPanel1.add(jButtonCancelar);
			jPanel1.add(jLabelLogo);
			jPanel1.add(jTextFieldUsuario);
			jPanel1.add(contrasena);
			jPanel1.add(jLabelMje);
			jPanel1.add(jButtonAceptar);

			pack();
			this.setSize(436, 375);
		} catch (

		Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	private boolean validacionEntrada() {
		if (jTextFieldUsuario.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese un usuario", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			jTextFieldUsuario.requestFocus();
			return false;
		} else if (contrasena.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el password", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			contrasena.requestFocus();
			return false;
		}
		return true;
	}

	private void crearEventos() {
		jButtonAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				if (validacionEntrada()) {
					// @SuppressWarnings("deprecation")
					// DTO_Usuario u =
					// controladorAdmin.getInstancia().validarUsuario(jTextFieldUsuario.getText(),
					// Contrasena.getText());
					// if (u != null) {
					// DTO_Empleado e =
					// controladorAdmin.getInstancia().getEmpleado(u.getIdEmpleado());
					// if (e != null) {
					// DTO_Sucursal s =
					// controladorAdmin.getInstancia().getSucursal(e.getIdSucursal());
					// if (s != null) {
					// Muestro menu
					// Menu m = new Menu(u, s);
					Menu m = new Menu();
					m.setLocationRelativeTo(null);
					m.setVisible(true);
					// controladorAdmin.getInstancia().ingresoUsuario(u.getUsuario());
					// Cierro la ventana del login
					dispose();
					// }
					// }
					// } else {
					// jLabel2.setVisible(true);
					// }
				}
			}
		});

		jButtonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				System.exit(0);
			}
		});
		
	}

}
