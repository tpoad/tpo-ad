package swt;

import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controladorCliente.ControladorCliente;
import dto.PrendaDTO;
import enums.Temporada;

public class ModificarPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;

	private JLabel lblCodigo;
	private JFormattedTextField txtCodigo;
	private JButton btnBuscar;
	private JLabel lblDescripcion;
	private JTextField txtDescripcion;
	private JLabel lblTemporada;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxTemporada;
	private JLabel lblMolde;
	private JTextField txtMolde;
	private JLabel lblCosto;
	private JFormattedTextField txtCosto;
	private JLabel lblPrecio;
	private JFormattedTextField txtPrecio;
	private JLabel lblProduccion;
	private JFormattedTextField txtProduccion;
	private JButton btnAceptar;
	private JButton btnCancelar;

	private PrendaDTO prendaDTO;

	public ModificarPrenda() {
		super();
		inicializarComponentes();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Modificar Prenda");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(10, 0, 231, 40);
			}
			{
				{
					lblCodigo = new JLabel();
					getContentPane().add(lblCodigo);
					getContentPane().add(getBtnBuscar());
					getContentPane().add(getBtnAceptar());
					getContentPane().add(getBtnCancelar());
					lblCodigo.setText("C\u00F3digo");
					lblCodigo.setFont(new java.awt.Font("Verdana", 0, 11));
					lblCodigo.setBounds(12, 54, 80, 14);
				}
				{
					txtCodigo = new JFormattedTextField();
					getContentPane().add(txtCodigo);
					txtCodigo.setBounds(118, 52, 143, 21);
					txtCodigo.requestFocus();
				}
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1);
				jPanel1.setLayout(null);
				jPanel1.setBounds(12, 128, 372, 211);
				jPanel1.setBorder(BorderFactory.createTitledBorder("Datos de la prenda"));
				{
					jPanel1.add(getDescripcionLabel());
					jPanel1.add(getDescripcionTXT());
					jPanel1.add(getTemporadaLabel());
					jPanel1.add(getTemporadaCBX());
					jPanel1.add(getMoldeLabel());
					jPanel1.add(getMoldeTXT());
					jPanel1.add(getCostoLabel());
					jPanel1.add(getCostoTXT());
					jPanel1.add(getTxtPrecio());
					jPanel1.add(getLblPrecio());
					jPanel1.add(getProduccionLabel());
					jPanel1.add(getProduccionTXT());
					txtDescripcion.setEnabled(false);
					cbxTemporada.setEnabled(false);
					txtMolde.setEnabled(false);
					txtCosto.setEnabled(false);
					txtPrecio.setEnabled(false);
					txtProduccion.setEnabled(false);
				}
				
			}

			this.setSize(409, 500);
			// pack();
		} catch (Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton();
			btnBuscar.setText("Buscar");
			btnBuscar.setBounds(269, 84, 115, 27);
			btnBuscar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnBuscar.addActionListener(new ActionListener() {
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public void actionPerformed(ActionEvent arg0) {
					String textField = txtCodigo.getText();
					int intCodigo = 0;
					if (textField != null && !textField.isEmpty()) {
						intCodigo = Integer.parseInt(textField);
					}
					prendaDTO = ControladorCliente.getInstancia().getPrendaBoba(intCodigo);
					if (prendaDTO != null) {
						txtDescripcion.setText(prendaDTO.getDescripcion());
					    List<Temporada> listTemporada = Arrays.asList(Temporada.values());
						cbxTemporada.setModel(new DefaultComboBoxModel(listTemporada.toArray()));
						cbxTemporada.setSelectedItem(prendaDTO.getTemporada().toString());
						txtMolde.setText(prendaDTO.getMolde());
						txtCosto.setValue(prendaDTO.getCostoActual());
						txtPrecio.setValue(prendaDTO.getPrecioVenta());
						txtProduccion.setValue(prendaDTO.getTiempoProduccion().getTime());
						btnBuscar.setEnabled(false);
						txtCodigo.setEnabled(false);
						txtDescripcion.setEnabled(true);
						cbxTemporada.setEnabled(true);
						txtMolde.setEnabled(true);
						txtCosto.setEnabled(true);
						txtPrecio.setEnabled(true);
						txtProduccion.setEnabled(true);
					} else {
						JOptionPane.showMessageDialog(null, "No se ha encontrado la prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
						txtCodigo.setEnabled(true);
						txtCodigo.setText("");
						txtCodigo.requestFocus();
					}
				}
			});
		}
		return btnBuscar;
	}

	private JLabel getDescripcionLabel() {
		if (lblDescripcion == null) {
			lblDescripcion = new JLabel();
			lblDescripcion.setText("Descripci\u00F3n");
			lblDescripcion.setFont(new java.awt.Font("Verdana", 0, 11));
			lblDescripcion.setBounds(10, 28, 73, 14);
		}
		return lblDescripcion;
	}

	private JTextField getDescripcionTXT() {
		if (txtDescripcion == null) {
			txtDescripcion = new JTextField();
			txtDescripcion.setBounds(85, 26, 277, 21);
		}
		return txtDescripcion;
	}

	@SuppressWarnings("rawtypes")
	private Component getTemporadaCBX() {
		cbxTemporada = new JComboBox();
		cbxTemporada.setBounds(85, 57, 143, 20);
		jPanel1.add(cbxTemporada);
		return cbxTemporada;
	}

	private Component getTemporadaLabel() {
		lblTemporada = new JLabel("Temporada");
		lblTemporada.setBounds(8, 60, 72, 14);
		jPanel1.add(lblTemporada);
		return lblTemporada;
	}

	private JLabel getMoldeLabel() {
		if (lblMolde == null) {
			lblMolde = new JLabel();
			lblMolde.setText("Molde");
			lblMolde.setFont(new java.awt.Font("Verdana", 0, 11));
			lblMolde.setBounds(10, 89, 70, 14);
		}
		return lblMolde;
	}

	private JTextField getMoldeTXT() {
		if (txtMolde == null) {
			txtMolde = new JTextField();
			txtMolde.setBounds(85, 87, 143, 21);
		}
		return txtMolde;
	}

	private JLabel getCostoLabel() {
		if (lblCosto == null) {
			lblCosto = new JLabel();
			lblCosto.setText("Costo");
			lblCosto.setFont(new java.awt.Font("Verdana", 0, 11));
			lblCosto.setBounds(10, 120, 70, 14);
		}
		return lblCosto;
	}

	private JTextField getCostoTXT() {
		if (txtCosto == null) {
			txtCosto = new JFormattedTextField();
			txtCosto.setBounds(85, 118, 143, 21);
		}
		return txtCosto;
	}

	private JTextField getTxtPrecio() {
		if (txtPrecio == null) {
			txtPrecio = new JFormattedTextField();
			txtPrecio.setEnabled(false);
			txtPrecio.setBounds(85, 149, 143, 21);
		}
		return txtPrecio;
	}

	private JLabel getLblPrecio() {
		if (lblPrecio == null) {
			lblPrecio = new JLabel();
			lblPrecio.setText("Precio");
			lblPrecio.setFont(new Font("Verdana", Font.PLAIN, 11));
			lblPrecio.setBounds(10, 151, 70, 14);
		}
		return lblPrecio;
	}

	private Component getProduccionLabel() {
		lblProduccion = new JLabel("T. Producci\u00F3n");
		lblProduccion.setBounds(10, 183, 72, 14);
		jPanel1.add(lblProduccion);
		return lblProduccion;
	}

	private Component getProduccionTXT() {
		txtProduccion = new JFormattedTextField();
		txtProduccion.setColumns(10);
		txtProduccion.setBounds(85, 180, 143, 20);
		jPanel1.add(txtProduccion);
		return txtProduccion;
	}

	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton();
			btnAceptar.setText("Aceptar");
			btnAceptar.setBounds(144, 402, 115, 27);
			btnAceptar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String textField = txtPrecio.getText();
					Float floatPrecio = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatPrecio = Float.parseFloat(textField);
					}
					textField = txtCosto.getText();
					Float floatCosto = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatCosto = Float.parseFloat(textField);
					}
					textField = txtProduccion.getText();
					int intTiempo = 0;
					if (textField != null && !textField.isEmpty()) {
						intTiempo = Integer.parseInt(textField);
					}
					boolean flag = ControladorCliente.getInstancia().modificarPrenda(prendaDTO.getCodigo(),
							txtDescripcion.getText(), Temporada.valueOf(cbxTemporada.getSelectedItem().toString()), txtMolde.getText(), new Integer(1), floatCosto, floatPrecio,
							new Date(intTiempo));
					if (flag) {
						JOptionPane.showMessageDialog(null, "Se ha modificado la prenda: " + txtDescripcion.getText(),
								"Prenda modificada con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaPrenda c = new ConsultaPrenda();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido modificar la prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnAceptar;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton();
			btnCancelar.setText("Cancelar");
			btnCancelar.setBounds(269, 402, 115, 27);
			btnCancelar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ConsultaPrenda c = new ConsultaPrenda();
					c.setLocationRelativeTo(null);
					c.setVisible(true);
					dispose();
				}
			});
		}
		return btnCancelar;
	}
}
