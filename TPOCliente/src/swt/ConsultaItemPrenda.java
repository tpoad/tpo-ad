package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.ItemPrendaDTO;

public class ConsultaItemPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoPrendas;
	private JTable jTableListado;
	private JButton alta;
	private JButton baja;
	private JButton modificar;
	private JButton prendas;
	private JButton volver;

	public ConsultaItemPrenda() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Items Prenda");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 799, 35);
			}
			{
				jScrollPaneListadoPrendas = new JScrollPane();
				getContentPane().add(jScrollPaneListadoPrendas);
				jScrollPaneListadoPrendas.setBounds(12, 53, 799, 311);
				{
					List<ItemPrendaDTO> itemPrendaDTO = ControladorCliente.getInstancia().obtenerTodosItemsPrenda();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("Prenda");
					jTableListadoModel.addColumn("SubC�digo");
					jTableListadoModel.addColumn("Color");
					jTableListadoModel.addColumn("Talle");
					jTableListadoModel.addColumn("Stock");
					jTableListadoModel.addColumn("M�nimo");
					jTableListadoModel.addColumn("Producci�n");
					for (ItemPrendaDTO i : itemPrendaDTO) {
						jTableListadoModel.addRow(new Object[] { 
								i.getPrenda().getCodigo() + " - " + i.getPrenda().getDescripcion(),
								i.getSubcodigo(),
								i.getColor(),
								i.getTalle(),
								i.getCantidadStock(),
								i.getCantidadMinima(),
								i.getCantidadProducir() });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoPrendas.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			alta = new JButton("Nuevo");
			alta.setBounds(235, 389, 89, 23);
			this.getContentPane().add(alta);
			baja = new JButton("Eliminar");
			baja.setBounds(335, 389, 89, 23);
			this.getContentPane().add(baja);
			modificar = new JButton("Modificar");
			modificar.setBounds(435, 389, 89, 23);
			this.getContentPane().add(modificar);
			prendas = new JButton("Prendas");
			prendas.setBounds(283, 419, 89, 23);
			this.getContentPane().add(prendas);
			
			volver = new JButton("Volver");
			volver.setBounds(385, 419, 89, 23);
			getContentPane().add(volver);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a los items prenda", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaItemPrenda a = new AltaItemPrenda();
				a.setLocationRelativeTo(null);
				a.setVisible(true);
				setVisible(false);
			}
		});
		
		baja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BajaItemPrenda b = new BajaItemPrenda();
				b.setLocationRelativeTo(null);
				b.setVisible(true);
				setVisible(false);
			}
		});
		
		modificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ModificarItemPrenda m = new ModificarItemPrenda();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
		
		prendas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaPrenda p = new ConsultaPrenda();
				p.setLocationRelativeTo(null);
				p.setVisible(true);
				setVisible(false);
			}
		});
		
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}
}
