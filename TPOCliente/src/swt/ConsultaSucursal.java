package swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.SucursalDTO;

public class ConsultaSucursal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JScrollPane jScrollPaneListadoClientes;
	private JTable jTableListado;
	private JButton alta;
	private JButton volver;

	public ConsultaSucursal() {
		super();
		inicializarComponentes();
		crearEventos();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Listado Sucursales");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(12, 12, 245, 35);
			}
			{
				jScrollPaneListadoClientes = new JScrollPane();
				getContentPane().add(jScrollPaneListadoClientes);
				jScrollPaneListadoClientes.setBounds(12, 53, 799, 311);
				{
					List<SucursalDTO> sucursalDTO = ControladorCliente.getInstancia().obtenerSucursales();
					DefaultTableModel jTableListadoModel = new DefaultTableModel();
					jTableListadoModel.addColumn("Nro");
					jTableListadoModel.addColumn("Nombre");
					jTableListadoModel.addColumn("Direcci�n");
					jTableListadoModel.addColumn("Horario");
					for (SucursalDTO s : sucursalDTO) {
						jTableListadoModel.addRow(new Object[] { 
								s.getNumero(),
								s.getNombre(),
								s.getDireccion(),
								s.getHorarios() });
					}
					jTableListado = new JTable(jTableListadoModel);
					jScrollPaneListadoClientes.setViewportView(jTableListado);
					jTableListado.setModel(jTableListadoModel);
				}
			}
			alta = new JButton("Nuevo");
			alta.setBounds(335, 389, 89, 23);
			this.getContentPane().add(alta);
			volver = new JButton("Volver");
			volver.setBounds(335, 419, 89, 23);
			this.getContentPane().add(volver);
			this.setSize(839, 505);
//			pack();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se han podido cargar a las sucursales", "Error",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	private void crearEventos() {
		alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AltaSucursal s = new AltaSucursal();
				s.setLocationRelativeTo(null);
				s.setVisible(true);
				setVisible(false);
			}
		});
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu m = new Menu();
				m.setLocationRelativeTo(null);
				m.setVisible(true);
				setVisible(false);
			}
		});
	}
	
}
