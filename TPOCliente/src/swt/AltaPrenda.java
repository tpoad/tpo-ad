package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import controladorCliente.ControladorCliente;
import enums.Temporada;

public class AltaPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtDescripcion;
	@SuppressWarnings("rawtypes")
	private JComboBox cbxTemporada;
	private JTextField txtMolde;
	private JFormattedTextField txtCosto;
	private JFormattedTextField txtPrecio;
	private JFormattedTextField txtTiempo;
	private JButton aceptar;
	private JButton cancelar;

	public AltaPrenda() {
		inicializarComponentes();
		crearEventos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void inicializarComponentes() {
		this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
		this.setBounds(100, 100, 450, 469);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Cargar Prenda");
		titulo.setFont(new Font("Verdana", Font.BOLD, 20));
		titulo.setBounds(10, 11, 182, 26);
		this.getContentPane().add(titulo);

		JPanel General = new JPanel();
		General.setBounds(10, 42, 414, 229);
		General.setBorder(
				new TitledBorder(null, "Datos particulares", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(General);
		General.setLayout(null);

		JLabel lblDescripcion = new JLabel("Descripci\u00F3n");
		lblDescripcion.setBounds(10, 30, 72, 14);
		General.add(lblDescripcion);

		txtDescripcion = new JTextField();
		txtDescripcion.setBounds(80, 27, 324, 20);
		General.add(txtDescripcion);
		txtDescripcion.setColumns(10);

		JLabel lblTemporada = new JLabel("Temporada");
		lblTemporada.setBounds(10, 58, 72, 14);
		General.add(lblTemporada);

		cbxTemporada = new JComboBox();
	    List<Temporada> listTemporada = Arrays.asList(Temporada.values());
		cbxTemporada.setModel(new DefaultComboBoxModel(listTemporada.toArray()));
		cbxTemporada.setBounds(80, 55, 162, 20);
		General.add(cbxTemporada);

		JLabel lblMolde = new JLabel("Molde");
		lblMolde.setBounds(10, 88, 72, 14);
		General.add(lblMolde);

		txtMolde = new JTextField();
		txtMolde.setColumns(10);
		txtMolde.setBounds(80, 86, 162, 20);
		General.add(txtMolde);

		JLabel lblCosto = new JLabel("Costo");
		lblCosto.setBounds(10, 121, 72, 14);
		General.add(lblCosto);

		txtCosto = new JFormattedTextField();
		txtCosto.setBounds(80, 118, 162, 20);
		General.add(txtCosto);
		txtCosto.setColumns(10);

		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setBounds(10, 152, 72, 14);
		General.add(lblPrecio);

		txtPrecio = new JFormattedTextField();
		txtPrecio.setColumns(10);
		txtPrecio.setBounds(80, 149, 162, 20);
		General.add(txtPrecio);
		
		JLabel lblTiempo = new JLabel("T. Producci\u00F3n");
		lblTiempo.setBounds(10, 183, 72, 14);
		General.add(lblTiempo);
		
		txtTiempo = new JFormattedTextField();
		txtTiempo.setColumns(10);
		txtTiempo.setBounds(80, 180, 162, 20);
		General.add(txtTiempo);

		aceptar = new JButton("Aceptar");
		aceptar.setBounds(240, 389, 89, 23);
		this.getContentPane().add(aceptar);

		cancelar = new JButton("Cancelar");
		cancelar.setBounds(335, 389, 89, 23);
		this.getContentPane().add(cancelar);

	}

	private void crearEventos() {
		
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validacion()) {
					String textField = txtPrecio.getText();
					Float floatPrecio = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatPrecio = Float.parseFloat(textField);
					}
					textField = txtCosto.getText();
					Float floatCosto = 0F;
					if (textField != null && !textField.isEmpty()) {
						floatCosto = Float.parseFloat(textField);
					}
					textField = txtTiempo.getText();
					int intTiempo = 0;
					if (textField != null && !textField.isEmpty()) {
						intTiempo = Integer.parseInt(textField);
					}
					boolean flag = ControladorCliente.getInstancia().altaPrenda(0, txtDescripcion.getText(),
							Temporada.valueOf(cbxTemporada.getSelectedItem().toString()), txtMolde.getText(), new Integer(1), floatCosto, floatPrecio, new Date(intTiempo));
					if (flag) {
						JOptionPane.showMessageDialog(null, "Se ha generado la prenda: " + txtDescripcion.getText(),
								"Prenda generada con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaPrenda c = new ConsultaPrenda();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido generar la prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaPrenda c = new ConsultaPrenda();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

	}

	public boolean validacion() {

		if (txtDescripcion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la descripci�n", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtDescripcion.requestFocus();
			return false;
		} else if (txtMolde.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el molde", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtMolde.requestFocus();
			return false;
		} else if (txtCosto.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el costo actual", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtCosto.requestFocus();
			return false;
		} else if (txtPrecio.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el precio actual", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtPrecio.requestFocus();
			return false;
		}
		return true;
	}
}
