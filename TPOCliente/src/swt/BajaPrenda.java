package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import controladorCliente.ControladorCliente;
import dto.ItemPrendaDTO;
import dto.PrendaDTO;

public class BajaPrenda extends JFrame {

	private static final long serialVersionUID = 1L;
	private JLabel jLabelTitulo;
	private JPanel jPanel1;

	private JLabel lblCodigo;
	private JFormattedTextField txtCodigo;
	private JButton btnBuscar;
	private JLabel lblDescripcion;
	private JTextField txtDescripcion;
	private JLabel lblCosto;
	private JFormattedTextField txtCosto;
	private JLabel lblPrecio;
	private JFormattedTextField txtPrecio;
	private JScrollPane scrollPaneListadoItemsPrenda;
	private JTable jTableListado;
	private DefaultTableModel jTableListadoModel;
	private JButton btnAceptar;
	private JButton btnCancelar;

	private PrendaDTO prendaDTO;

	public BajaPrenda() {
		super();
		inicializarComponentes();
	}

	private void inicializarComponentes() {
		try {
			this.setResizable(false);
			this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
			this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			// setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			{
				jLabelTitulo = new JLabel();
				getContentPane().add(jLabelTitulo);
				jLabelTitulo.setText("Eliminar Prenda");
				jLabelTitulo.setFont(new java.awt.Font("Verdana", 1, 20));
				jLabelTitulo.setBounds(10, 0, 231, 40);
			}
			{
				{
					lblCodigo = new JLabel();
					getContentPane().add(lblCodigo);
					getContentPane().add(getBtnBuscar());
					getContentPane().add(getBtnAceptar());
					getContentPane().add(getBtnCancelar());
					lblCodigo.setText("C\u00F3digo");
					lblCodigo.setFont(new java.awt.Font("Verdana", 0, 11));
					lblCodigo.setBounds(12, 54, 80, 14);
				}
				{
					txtCodigo = new JFormattedTextField();
					getContentPane().add(txtCodigo);
					txtCodigo.setBounds(118, 52, 143, 21);
					txtCodigo.requestFocus();
				}
				jPanel1 = new JPanel();
				getContentPane().add(jPanel1);
				jPanel1.setLayout(null);
				jPanel1.setBounds(12, 128, 372, 148);
				jPanel1.setBorder(BorderFactory.createTitledBorder("Datos de la prenda"));
				{
					jPanel1.add(getDescripcionLabel());
					jPanel1.add(getDescripcionTXT());
					jPanel1.add(getCostoLabel());
					jPanel1.add(getCostoTXT());
					txtDescripcion.setEnabled(false);
					txtCosto.setEnabled(false);
				}
				jPanel1.add(getTxtPrecio());
				jPanel1.add(getLblPrecio());
			}
			
			scrollPaneListadoItemsPrenda = new JScrollPane();
			scrollPaneListadoItemsPrenda.setBounds(10, 287, 374, 107);
			getContentPane().add(scrollPaneListadoItemsPrenda);
			{
				jTableListadoModel = new DefaultTableModel();
				jTableListadoModel.addColumn("Color");
				jTableListadoModel.addColumn("Talle");
				jTableListadoModel.addColumn("Stock");
//				List<ItemPrendaDTO> itemPrendaDTO = ControladorCliente.getInstancia().obtenerItemsPrenda(intCodigo);
//				for (ItemPrendaDTO i : itemPrendaDTO) {
//					jTableListadoModel.addRow(new Object[] { 
//							i.getColor(),
//							i.getTalle(),
//							i.getCantidadStock() });
//				}
				jTableListado = new JTable(jTableListadoModel);
				scrollPaneListadoItemsPrenda.setViewportView(jTableListado);
				jTableListado.setModel(jTableListadoModel);
			}

			this.setSize(409, 500);
			// pack();
		} catch (Exception e) {
			// add your error handling code here
			e.printStackTrace();
		}
	}

	private JButton getBtnBuscar() {
		if (btnBuscar == null) {
			btnBuscar = new JButton();
			btnBuscar.setText("Buscar");
			btnBuscar.setBounds(269, 84, 115, 27);
			btnBuscar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String textField = txtCodigo.getText();
					int intCodigo = 0;
					if (textField != null && !textField.isEmpty()) {
						intCodigo = Integer.parseInt(textField);
					}
					prendaDTO = ControladorCliente.getInstancia().getPrendaBoba(intCodigo);
					txtCodigo.requestFocus();
					txtDescripcion.setEnabled(false);
					txtCosto.setEnabled(false);
					txtPrecio.setEnabled(false);
					if (prendaDTO != null) {
						btnBuscar.setEnabled(false);
						txtCodigo.setEnabled(false);
						txtDescripcion.setText(prendaDTO.getDescripcion());
						txtCosto.setValue(prendaDTO.getCostoActual());
						txtPrecio.setValue(prendaDTO.getPrecioVenta());
						List<ItemPrendaDTO> itemPrendaDTO = ControladorCliente.getInstancia().obtenerItemsPrenda(intCodigo);
						for (ItemPrendaDTO i : itemPrendaDTO) {
							jTableListadoModel.addRow(new Object[] { 
									i.getColor(),
									i.getTalle(),
									i.getCantidadStock() });
						}
					} else {
						JOptionPane.showMessageDialog(null, "No se ha encontrado la prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
						txtCodigo.setEnabled(true);
						txtCodigo.setText("");
						txtCodigo.requestFocus();
					}
				}
			});
		}
		return btnBuscar;
	}

	private JLabel getDescripcionLabel() {
		if (lblDescripcion == null) {
			lblDescripcion = new JLabel();
			lblDescripcion.setText("Descripci\u00F3n");
			lblDescripcion.setFont(new java.awt.Font("Verdana", 0, 11));
			lblDescripcion.setBounds(10, 25, 73, 14);
		}
		return lblDescripcion;
	}

	private JTextField getDescripcionTXT() {
		if (txtDescripcion == null) {
			txtDescripcion = new JTextField();
			txtDescripcion.setBounds(85, 22, 277, 21);
		}
		return txtDescripcion;
	}

	private JLabel getCostoLabel() {
		if (lblCosto == null) {
			lblCosto = new JLabel();
			lblCosto.setText("Costo");
			lblCosto.setFont(new java.awt.Font("Verdana", 0, 11));
			lblCosto.setBounds(10, 75, 70, 14);
		}
		return lblCosto;
	}

	private JTextField getCostoTXT() {
		if (txtCosto == null) {
			txtCosto = new JFormattedTextField();
			txtCosto.setBounds(85, 72, 143, 21);
		}
		return txtCosto;
	}

	private JTextField getTxtPrecio() {
		if (txtPrecio == null) {
			txtPrecio = new JFormattedTextField();
			txtPrecio.setEnabled(false);
			txtPrecio.setBounds(85, 98, 143, 21);
		}
		return txtPrecio;
	}

	private JLabel getLblPrecio() {
		if (lblPrecio == null) {
			lblPrecio = new JLabel();
			lblPrecio.setText("Precio");
			lblPrecio.setFont(new Font("Verdana", Font.PLAIN, 11));
			lblPrecio.setBounds(10, 101, 70, 14);
		}
		return lblPrecio;
	}
	
	private JButton getBtnAceptar() {
		if (btnAceptar == null) {
			btnAceptar = new JButton();
			btnAceptar.setText("Aceptar");
			btnAceptar.setBounds(144, 402, 115, 27);
			btnAceptar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnAceptar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					boolean flag = ControladorCliente.getInstancia().eliminarPrenda(prendaDTO.getCodigo());
					if (flag) {
						JOptionPane.showMessageDialog(null, "Se ha eliminado la prenda: " + txtDescripcion.getText(),
								"Prenda eliminada con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaPrenda c = new ConsultaPrenda();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido eliminar la prenda", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnAceptar;
	}

	private JButton getBtnCancelar() {
		if (btnCancelar == null) {
			btnCancelar = new JButton();
			btnCancelar.setText("Cancelar");
			btnCancelar.setBounds(269, 402, 115, 27);
			btnCancelar.setFont(new java.awt.Font("Verdana", 0, 11));
			btnCancelar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					ConsultaPrenda c = new ConsultaPrenda();
					c.setLocationRelativeTo(null);
					c.setVisible(true);
					dispose();
				}
			});
		}
		return btnCancelar;
	}
}
