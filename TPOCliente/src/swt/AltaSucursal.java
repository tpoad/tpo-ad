package swt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import controladorCliente.ControladorCliente;
import dto.ClienteDTO;

public class AltaSucursal extends JFrame {

	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;
	private JTextField txtHorarios;
	private JTextField txtDireccion;
	private JButton aceptar;
	private JButton cancelar;

	public AltaSucursal() {
		inicializarComponentes();
		crearEventos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void inicializarComponentes() {
		this.setTitle("Aplicaciones Distribuidas [TPO Grupo 5]");
		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("recursos/logo.jpg")).getImage());
		this.setBounds(100, 100, 450, 469);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Cargar Sucursal");
		titulo.setFont(new Font("Verdana", Font.BOLD, 20));
		titulo.setBounds(10, 11, 182, 26);
		this.getContentPane().add(titulo);

		JPanel General = new JPanel();
		General.setBounds(10, 42, 414, 134);
		General.setBorder(new TitledBorder(null, "Datos particulares", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.getContentPane().add(General);
		General.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 30, 72, 14);
		General.add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.setBounds(80, 27, 324, 20);
		General.add(txtNombre);
		txtNombre.setColumns(10);

		JLabel lblDireccion = new JLabel("Direcci\u00F3n");
		lblDireccion.setBounds(10, 93, 72, 14);
		General.add(lblDireccion);

		txtDireccion = new JTextField();
		txtDireccion.setBounds(80, 90, 324, 20);
		General.add(txtDireccion);
		txtDireccion.setColumns(10);

		JLabel lblHorarios = new JLabel("Horarios");
		lblHorarios.setBounds(10, 60, 72, 14);
		General.add(lblHorarios);

		txtHorarios = new JTextField();
		txtHorarios.setColumns(10);
		txtHorarios.setBounds(80, 58, 162, 20);
		General.add(txtHorarios);

		aceptar = new JButton("Aceptar");
		aceptar.setBounds(240, 389, 89, 23);
		this.getContentPane().add(aceptar);

		cancelar = new JButton("Cancelar");
		cancelar.setBounds(335, 389, 89, 23);
		this.getContentPane().add(cancelar);

	}

	private void crearEventos() {
		aceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (validacion()) {
					List<ClienteDTO> clientes = ControladorCliente.getInstancia().getClientes();
					boolean flag = ControladorCliente.getInstancia().altaSucursal(new Integer(0), txtNombre.getText(), txtDireccion.getText(), txtHorarios.getText(), clientes);
					if (flag) {
						JOptionPane.showMessageDialog(null,
								"Se ha generado la sucursal: " + txtNombre.getText(),
								"Sucursal generada con �xito!", JOptionPane.INFORMATION_MESSAGE);
						ConsultaSucursal c = new ConsultaSucursal();
						c.setLocationRelativeTo(null);
						c.setVisible(true);
						dispose();
					} else {
						JOptionPane.showMessageDialog(null, "No se ha podido generar la sucursal", "Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConsultaSucursal c = new ConsultaSucursal();
				c.setLocationRelativeTo(null);
				c.setVisible(true);
				dispose();
			}
		});

	}

	public boolean validacion() {

		if (txtNombre.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el nombre", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtNombre.requestFocus();
			return false;
		} else if (txtHorarios.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese el horario", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtHorarios.requestFocus();
			return false;
		} else if (txtDireccion.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Por favor ingrese la direcci�n", "Atenci�n",
					JOptionPane.WARNING_MESSAGE);
			txtDireccion.requestFocus();
			return false;
		}
		return true;
	}


}
