package controladorCliente;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import dto.ClienteDTO;
import dto.InsumoDTO;
import dto.ItemPrendaDTO;
import dto.PedidoClienteDTO;
import dto.PrendaDTO;
import dto.SucursalDTO;
import enums.Color;
import enums.Talle;
import enums.Temporada;
import interfazRemota.InterfazRemota;

//public class ControladorCliente {
//	ClienteDTO clienteDTO;
//	PedidoClienteDTO pedidoClienteDTO;
//	PrendaDTO prendaDTO;
//
//	public static void main(String[] args) throws Exception {
//		new ControladorCliente();
//	}
//
//	public ControladorCliente() throws RemoteException, ClienteException {
//		System.out.println("Crear cliente");
//		BusinessDelegate.getInstancia().altaCliente(clienteDTO);
//		System.out.println("Cliente: " + clienteDTO);
//	}
//}

public class ControladorCliente {

	// Interfaz Remota que esta en el Repositorio
	InterfazRemota objetoRemoto;
	private static ControladorCliente instancia;

	private ControladorCliente() {
		try {
			// Establezco el lugar a donde el cliente va a hacer el lookup para
			// conectarse con el server
			// objetoRemoto = (InterfazRemota)
			// Naming.lookup(InterfazRemota.url); //localhost/RecursoRMI
			objetoRemoto = (InterfazRemota) Naming.lookup("//localhost/ObjetoRemoto");
			// clienteDTO = BusinessDelegate.getInstancia().altaCliente();
			// System.out.println("Obtenido el servicio de la Instancia Remota "
			// + InterfazRemota.url);
			System.out.println("Obtenido el servicio de la Instancia Remota: //localhost/ObjetoRemoto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ControladorCliente getInstancia() {
		if (instancia == null)
			instancia = new ControladorCliente();
		return instancia;
	}

	/**************************************************************************************************************************/
	/** Administracion **/
	/**************************************************************************************************************************/
	// Empleados
	// public boolean altaEmpleado(String nombre, String apellido, Date
	// fechaIngreso, DTO_Sucursal sucursal, DTO_Empleado superior) {
	// try {
	//
	// DTO_Empleado empleado = new DTO_Empleado();
	// empleado.setNombre(nombre);
	// empleado.setApellido(apellido);
	// empleado.setFechaIngreso(fechaIngreso);
	// empleado.setIdSucursal(sucursal.getId());
	// empleado.setIdSuperior(superior.getId());
	//
	// objetoRemoto.altaEmpleado(empleado);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Empleado");
	// }
	// return false;
	// }
	//
	// public boolean modificarEmpleado(Integer id, String nombre, String
	// apellido, Date fechaIngreso, DTO_Sucursal sucursal, DTO_Empleado
	// superior) {
	// try {
	//
	// DTO_Empleado empleado = new DTO_Empleado();
	// empleado.setId(id);
	// empleado.setNombre(nombre);
	// empleado.setApellido(apellido);
	// empleado.setFechaIngreso(fechaIngreso);
	// empleado.setIdSucursal(sucursal.getId());
	// empleado.setIdSuperior(superior.getId());
	//
	// objetoRemoto.modificarEmpleado(empleado);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Modificar Empleado");
	// }
	// return false;
	// }
	//
	// public boolean bajaEmpleado(Integer id) {
	// try {
	// objetoRemoto.bajaEmpleado(id);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Baja Empleado");
	// }
	// return false;
	// }
	//
	// public DTO_Empleado getEmpleado(Integer id){
	// DTO_Empleado empleado = null;
	// try {
	// empleado = objetoRemoto.getEmpleado(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Empleado");
	// }
	// return empleado;
	// }
	//
	// //Sucursal
	// public boolean altaSucursal(String descripcion, String direccion, String
	// provincia, String localidad, String codigoPostal, String telefono, float
	// costoLocal, DTO_Empleado gerente, DTO_Empleado encargadoDeposito,
	// DTO_Empleado recepcionPedidos) {
	// try {
	//
	// DTO_Sucursal sucursal = new DTO_Sucursal();
	// sucursal.setDescripcion(descripcion);
	// sucursal.setDireccion(direccion);
	// sucursal.setProvincia(provincia);
	// sucursal.setLocalidad(localidad);
	// sucursal.setCodigoPostal(codigoPostal);
	// sucursal.setTelefono(telefono);
	// sucursal.setCostoLocal(costoLocal);
	// sucursal.setIdGerente(gerente.getId());
	// sucursal.setIdEncargadoDeposito(encargadoDeposito.getId());
	// sucursal.setIdRecepcionPedidos(recepcionPedidos.getId());
	// objetoRemoto.altaSucursal(sucursal);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Sucursal");
	// }
	// return false;
	// }
	//
	// public boolean modificarSucursal(Integer id, String descripcion, String
	// direccion, String provincia, String localidad, String codigoPostal,
	// String telefono, float costoLocal, DTO_Empleado gerente, DTO_Empleado
	// encargadoDeposito, DTO_Empleado recepcionPedidos) {
	// try {
	//
	// DTO_Sucursal sucursal = new DTO_Sucursal();
	// sucursal.setId(id);
	// sucursal.setDescripcion(descripcion);
	// sucursal.setDireccion(direccion);
	// sucursal.setProvincia(provincia);
	// sucursal.setLocalidad(localidad);
	// sucursal.setCodigoPostal(codigoPostal);
	// sucursal.setTelefono(telefono);
	// sucursal.setCostoLocal(costoLocal);
	// sucursal.setIdGerente(gerente.getId());
	// sucursal.setIdEncargadoDeposito(encargadoDeposito.getId());
	// sucursal.setIdRecepcionPedidos(recepcionPedidos.getId());
	// objetoRemoto.modificarSucursal(sucursal);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Modificar Sucursal");
	// }
	// return false;
	// }
	//
	// public boolean bajaSucursal(Integer id) {
	// try {
	// objetoRemoto.bajaSucursal(id);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Baja Sucursal");
	// }
	// return false;
	// }
	//
	// public DTO_Sucursal getSucursal(Integer id){
	// DTO_Sucursal s = null;
	// try {
	// s = objetoRemoto.getSucursal(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Empleado");
	// }
	// return s;
	// }
	//
	// //Sucursal
	// public boolean altaStock(String descripcion, Integer idSucursal) {
	// try {
	// DTO_Stock stock = new DTO_Stock();
	// stock.setDescripcion(descripcion);
	// stock.setIdSucursal(idSucursal);
	// objetoRemoto.altaStock(stock);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Stock");
	// }
	// return false;
	// }
	//
	// public boolean modificarStock(Integer id, String descripcion, Integer
	// idSucursal) {
	// try {
	// DTO_Stock stock = new DTO_Stock();
	// stock.setId(id);
	// stock.setDescripcion(descripcion);
	// stock.setIdSucursal(idSucursal);
	// objetoRemoto.modificarStock(stock);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Modificar Stock");
	// }
	// return false;
	// }
	//
	// public boolean bajaStock(Integer id) {
	// try {
	// objetoRemoto.bajaStock(id);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Baja Stock");
	// }
	// return false;
	// }
	//
	// public DTO_Stock getStock(Integer id){
	// DTO_Stock s = null;
	// try {
	// s = objetoRemoto.getStock(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Get stock");
	// }
	// return s;
	// }
	//
	// Usuario
	// public boolean altaUsuario(String usuario, String password, DTO_Empleado
	// empleado, List<DTO_Rol> roles){
	// try {
	// DTO_Usuario u = new DTO_Usuario();
	// u.setUsuario(usuario);
	// u.setPassword(password);
	// u.setFechaCreacion(new Date());
	// u.setIdEmpleado(empleado.getId());
	// u.setRoles(roles);
	// objetoRemoto.altaUsuario(u);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error validar usuario");
	// }
	// return false;
	// }
	//
	// public boolean bajaUsuario(String usuario) {
	// try {
	// objetoRemoto.bajaUsuario(usuario);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Baja usuario");
	// }
	// return false;
	// }
	//
	// public boolean cambiarPassword(String usuario, String oldPassword, String
	// newPassword){
	// try {
	// objetoRemoto.cambiarPassword(usuario, oldPassword, newPassword);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Cambiar password");
	// }
	// return false;
	// }
	//
	// //Login
	// public DTO_Usuario validarUsuario(String usuario, String password){
	// try {
	// return objetoRemoto.validarUsuario(usuario, password);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error validar usuario");
	// }
	// return null;
	// }
	//
	// public void ingresoUsuario(String usuario){
	// try {
	// objetoRemoto.ingresoUsuario(usuario);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Ingreso Usuario");
	// }
	// }
	//
	// Listados
	// public List<DTO_Rol> listarRoles(){
	// List<DTO_Rol> lista = null;
	// try {
	// lista = objetoRemoto.listarRoles();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar Roles");
	// }
	// return lista;
	// }
	//
	// public List<DTO_Rol> buscarRolesUsuario(String usuario){
	// try {
	// return objetoRemoto.buscarRolesUsuario(usuario);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Buscar Roles Usuario");
	// }
	// return null;
	// }
	//
	// public List<DTO_Empleado> listarEmpleados() {
	// List<DTO_Empleado> empleados = null;
	// try {
	// empleados = objetoRemoto.listarEmpleados();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar empleados");
	// }
	// return empleados;
	// }
	//
	// public List<DTO_Empleado> listarEmpleados(Integer idSucursal) {
	// List<DTO_Empleado> empleados = null;
	// try {
	// empleados = objetoRemoto.listarEmpleados(idSucursal);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar empleados sucursal");
	// }
	// return empleados;
	// }
	//
	// public List<DTO_Sucursal> listarSucursales() {
	// List<DTO_Sucursal> sucursales = null;
	// try {
	// sucursales = objetoRemoto.listarSucursales();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar sucursales");
	// }
	// return sucursales;
	// }
	//
	// public List<DTO_Stock> listarStock() {
	// List<DTO_Stock> stocks = null;
	// try {
	// stocks = objetoRemoto.listarStock();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar stocks");
	// }
	// return stocks;
	// }
	//
	// public List<DTO_Stock> listarStock(Integer idSucursal){
	// List<DTO_Stock> stocks = null;
	// try {
	// stocks = objetoRemoto.listarStock(idSucursal);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar stocks");
	// }
	// return stocks;
	// }
	//
	// public List<DTO_Usuario> listarUsuarios() {
	// List<DTO_Usuario> usuarios = null;
	// try {
	// usuarios = objetoRemoto.listarUsuarios();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar usuarios");
	// }
	// return usuarios;
	// }

	/**************************************************************************************************************************/
	/** Clientes */
	/**
	 *************************************************************************************************************************/
	public List<ClienteDTO> getClientes() {
		List<ClienteDTO> c = null;
		try {
			c = objetoRemoto.getClientes();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar clientes");
		}
		return c;
	}

	public ClienteDTO obtenerCliente(String cuit) {
		ClienteDTO c = null;
		try {
			c = objetoRemoto.obtenerCliente(cuit);
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error b˙squeda de cliente");
		}
		return c;
	}

	public boolean altaCliente(Integer numeroCliente, String razonSocial, String cuit, String inscripcionAFIP,
			String direccion, Float saldo, Float limiteCredito, String condicionesPago) {
		try {
			ClienteDTO dto = new ClienteDTO();
			dto.setCondicionesPago(condicionesPago);
			dto.setCuit(cuit);
			dto.setDireccion(direccion);
			dto.setInscripcionAFIP(inscripcionAFIP);
			dto.setLimiteCredito(limiteCredito);
			dto.setNumeroCliente(numeroCliente);
			dto.setRazonSocial(razonSocial);
			dto.setSaldo(saldo);

			// CuentaCorrienteDTO cc = new CuentaCorrienteDTO();
			// cc.setLimiteCredito(limiteCredito);
			// cc.setCredito(limiteCredito);
			// cc.setFormaPago(formaPago);
			//
			// cliente.setCuentaCorriente(cc);

			objetoRemoto.altaCliente(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error alta de cliente");
		}
		return false;
	}

	public boolean eliminarCliente(String cuit) {
		try {
			objetoRemoto.eliminarCliente(cuit);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error baja de cliente");
		}
		return false;
	}

	public boolean modificarCliente(Integer numeroCliente, String razonSocial, String cuit, String inscripcionAFIP,
			String direccion, Float saldo, Float limiteCredito, String condicionesPago) {
		try {
			ClienteDTO dto = new ClienteDTO();
			dto.setCondicionesPago(condicionesPago);
			dto.setCuit(cuit);
			dto.setDireccion(direccion);
			dto.setInscripcionAFIP(inscripcionAFIP);
			dto.setLimiteCredito(limiteCredito);
			dto.setNumeroCliente(numeroCliente);
			dto.setRazonSocial(razonSocial);
			dto.setSaldo(saldo);
			objetoRemoto.modificarCliente(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error modificar Cliente");
		}
		return false;
	}

	/**************************************************************************************************************************/
	/** Prendas */
	/**
	 *************************************************************************************************************************/
	public List<PrendaDTO> obtenerPrendas() {
		List<PrendaDTO> c = null;
		try {
			c = objetoRemoto.obtenerPrendas();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar prendas");
		}
		return c;
	}

	public PrendaDTO getPrendaBoba(int codigo) {
		PrendaDTO p = null;
		try {
			p = objetoRemoto.getPrendaBoba(codigo);
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error b˙squeda de prenda");
		}
		return p;
	}

	public boolean altaPrenda(int codigo, String descripcion, Temporada temporada, String molde, Integer estado,
			Float costoActual, Float precioVenta, Date date) {
		try {
			PrendaDTO dto = new PrendaDTO();
			dto.setCodigo(codigo);
			dto.setCostoActual(costoActual);
			dto.setDescripcion(descripcion);
			dto.setEstado(estado);
			dto.setMolde(molde);
			dto.setPrecioVenta(precioVenta);
			dto.setTemporada(temporada);
			dto.setTiempoProduccion(date);
			objetoRemoto.altaPrenda(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error alta de prenda");
		}
		return false;
	}

	public boolean eliminarPrenda(int codigo) {
		try {
			objetoRemoto.eliminarPrenda(codigo);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error baja de prenda");
		}
		return false;
	}

	public boolean modificarPrenda(int codigo, String descripcion, Temporada temporada, String molde, Integer estado,
			Float costoActual, Float precioVenta, Date tiempoProduccion) {
		try {
			PrendaDTO dto = new PrendaDTO();
			dto.setCodigo(codigo);
			dto.setCostoActual(costoActual);
			dto.setDescripcion(descripcion);
			dto.setEstado(estado);
			dto.setMolde(molde);
			dto.setPrecioVenta(precioVenta);
			dto.setTemporada(temporada);
			dto.setTiempoProduccion(tiempoProduccion);
			objetoRemoto.modificarPrenda(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error modificar prenda");
		}
		return false;
	}

	/* ITEMPRENDA---------------------------------- */
	public List<ItemPrendaDTO> obtenerItemsPrenda(int codigo) {
		List<ItemPrendaDTO> l = null;
		try {
			l = objetoRemoto.obtenerItemsPrenda(codigo);
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar items prenda por cÛdigo");
		}
		return l;
	}

	public List<ItemPrendaDTO> obtenerTodosItemsPrenda() {
		List<ItemPrendaDTO> l = null;
		try {
			l = objetoRemoto.obtenerTodosItemsPrenda();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar items prenda");
		}
		return l;
	}

	public ItemPrendaDTO obtenerItemPrenda(int subcodigo) {
		ItemPrendaDTO i = null;
		try {
			i = objetoRemoto.obtenerItemPrenda(subcodigo);
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error b˙squeda de item prenda");
		}
		return i;
	}

	public boolean altaItemPrenda(int subcodigo, Color color, Talle talle, int cantidadStock, int cantidadMinima,
			int cantidadProducir, PrendaDTO prenda) {
		try {
			objetoRemoto.altaItemPrenda(new ItemPrendaDTO(subcodigo, color, talle, cantidadStock, cantidadMinima,
					cantidadProducir, prenda));
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error alta de item prenda");
		}
		return false;
	}

	public boolean eliminarItemPrenda(int subCodigo) {
		try {
			objetoRemoto.eliminarItemPrenda(subCodigo);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error baja de item prenda");
		}
		return false;
	}

	public boolean modificarItemPrenda(int subcodigo, Color color, Talle talle, Integer cantidadStock,
			Integer cantidadMinima, Integer cantidadProducir) {
		try {
			ItemPrendaDTO dto = new ItemPrendaDTO();
			dto.setSubcodigo(subcodigo);
			dto.setColor(color);
			dto.setTalle(talle);
			dto.setCantidadStock(cantidadStock);
			dto.setCantidadMinima(cantidadMinima);
			dto.setCantidadProducir(cantidadProducir);
			objetoRemoto.modificarItemPrenda(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error modificar item prenda");
		}
		return false;
	}

	/**************************************************************************************************************************/
	/** Sucursales */
	/**
	 * @param clientes
	 *************************************************************************************************************************/
	public boolean altaSucursal(Integer numero, String nombre, String direccion, String horarios,
			List<ClienteDTO> clientes) {
		try {
			SucursalDTO dto = new SucursalDTO();
			dto.setNumero(numero);
			dto.setNombre(nombre);
			dto.setDireccion(direccion);
			dto.setHorarios(horarios);
			dto.setClientes(clientes);
			objetoRemoto.altaSucursal(dto);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error alta sucursal");
		}
		return false;
	}

	public List<SucursalDTO> obtenerSucursales() {
		List<SucursalDTO> l = null;
		try {
			l = objetoRemoto.getSucursales();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar sucursales");
		}
		return l;
	}

	/**************************************************************************************************************************/
	/** Pedido cliente */
	/**
	 * @throws RemoteException
	 *************************************************************************************************************************/
	public List<PedidoClienteDTO> getPedidosCliente() {
		List<PedidoClienteDTO> l = null;
		try {
			l = objetoRemoto.obtenerPedidosCliente();
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Controlador Cliente: Error listar pedidos para un cliente");
		}
		return l;
	}

	public List<PedidoClienteDTO> getPedidosCliente(String cuit) {
		List<PedidoClienteDTO> l = null;
		try {
			l = objetoRemoto.obtenerPedidosCliente(cuit);
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Controlador Cliente: Error listar pedidos para un cliente");
		}
		return l;
	}

	public PedidoClienteDTO getPedidoCliente(int intNroPedido) {
		PedidoClienteDTO pcdto = null;
		try {
			pcdto = objetoRemoto.obtenerPedidoCliente(intNroPedido);
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Controlador Cliente: Error en buscar un pedido de cliente");
		}
		return pcdto;
	}

	public boolean aprobarPedidoCliente(int intNroPedido) {
		try {
			objetoRemoto.aprobarPedidoCliente(intNroPedido);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error en aprobar un pedido");
		}
		return false;
	}

	public boolean cancelarPedidoCliente(int intNroPedido) {
		try {
			objetoRemoto.cancelarPedidoCliente(intNroPedido);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error en cancelar un pedido");
		}
		return false;
	}

	public boolean procesarPedidoCliente(int intNroPedido) {
		try {
			objetoRemoto.procesarPedidoCliente(intNroPedido);
			return true;
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error en procesar un pedido");
		}
		return false;
	}

//	public boolean despacharPedidoCliente(int intNroPedido) {
//		try {
//			objetoRemoto.despacharPedidoCliente(intNroPedido);
//			return true;
//		} catch (RemoteException e) {
//			System.out.println(e);
//			System.out.println("Controlador Cliente: Error en despachar un pedido");
//		}
//		return false;
//	}

	/**************************************************************************************************************************/
	/** Insumos */
	/**
	 *************************************************************************************************************************/

	public List<InsumoDTO> obtenerInsumos() {
		List<InsumoDTO> c = null;
		try {
			c = objetoRemoto.obtenerInsumos();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar insumos");
		}
		return c;
	}

	public List<InsumoDTO> obtenerInsumo() {
		List<InsumoDTO> c = null;
		try {
			c = objetoRemoto.obtenerInsumo();
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error listar insumo");
		}
		return c;
	}

	public InsumoDTO getInsumo(int codigo) {
		InsumoDTO i = null;
		try {
			i = objetoRemoto.getInsumo(codigo);
		} catch (RemoteException e) {
			System.out.println(e);
			System.out.println("Controlador Cliente: Error búsqueda de insumo");
		}
		return i;
	}

	public boolean altaInsumo(int codigo, String nombre, int stock, int minimo, int compra, Float costo) {
		InsumoDTO dto = new InsumoDTO();
		try {
			dto.setCodigoInterno(codigo);
			dto.setNombre(nombre);
			dto.setCantidadStock(stock);
			dto.setCantidadMinima(minimo);
			dto.setCantidadComprar(compra);
			dto.setCostoActual(costo);
			objetoRemoto.altaInsumo(dto);
			return true;
		} catch (RemoteException e) {
			e.printStackTrace();
			System.out.println("Controlador Cliente: Error alta de insumo");
		}
		return false;
	}

	/**************************************************************************************************************************/
	/** ? */
	/**
	 *************************************************************************************************************************/
	// Cuenta Corriente
	// public boolean actualizarCuentaCorriente(Integer id, float credito, float
	// limiteCredito, String formaPago) {
	// try {
	// DTO_CuentaCorriente cc = new DTO_CuentaCorriente();
	// cc.setId(id);
	// cc.setCredito(credito);
	// cc.setLimiteCredito(limiteCredito);
	// cc.setFormaPago(formaPago);
	//
	// objetoRemoto.actualizarCuentaCorriente(cc);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Actualizar Cuenta Corriente");
	// }
	// return false;
	//
	// }

	// Factura Cliente
	// public DTO_FacturaCliente getFacturaCliente(Integer id){
	// DTO_FacturaCliente f = null;
	// try {
	// f = objetoRemoto.getFacturaCliente(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Get Factura Cliente");
	// }
	// return f;
	// }
	//
	// public DTO_ReciboPago getReciboPago(Integer id){
	// DTO_ReciboPago r = null;
	// try {
	// r = objetoRemoto.getReciboPago(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Get Recibo Pago");
	// }
	// return r;
	// }
	//
	// Listados
	//
	// public List<DTO_FacturaCliente> listarFacturasClientePendientes() {
	// List<DTO_FacturaCliente> f = null;
	// try {
	// f = objetoRemoto.listarFacturasClientePendientes();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar facturas clientes pendientes");
	// }
	// return f;
	// }
	//
	// public List<DTO_FacturaCliente> listarFacturasClientePendientes(Integer
	// idCliente) {
	// List<DTO_FacturaCliente> f = null;
	// try {
	// f = objetoRemoto.listarFacturasClientePendientes(idCliente);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar facturas clientes pendientes por
	// cliente");
	// }
	// return f;
	// }
	//
	// public List<DTO_FacturaCliente> listarFacturasCliente(Integer idCliente)
	// {
	// List<DTO_FacturaCliente> f = null;
	// try {
	// f = objetoRemoto.listarFacturasCliente(idCliente);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar facturas clientes por cliente");
	// }
	// return f;
	// }
	//
	// public List<DTO_FacturaCliente> listarFacturasCliente() {
	// List<DTO_FacturaCliente> f = null;
	// try {
	// f = objetoRemoto.listarFacturasCliente();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar facturas clientes");
	// }
	// return f;
	// }
	//
	// public List<DTO_ReciboPago> listarRecibos() {
	// List<DTO_ReciboPago> r = null;
	// try {
	// r = objetoRemoto.listarRecibos();
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar recibos");
	// }
	// return r;
	// }
	//
	// public List<DTO_ReciboPago> listarRecibos(Integer idCliente) {
	// List<DTO_ReciboPago> r = null;
	// try {
	// r = objetoRemoto.listarRecibos(idCliente);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Listar recibos por cliente");
	// }
	// return r;
	// }
	//

	/**************************************************************************************************************************/
	/** Proveedor **/
	/**************************************************************************************************************************/
	// Proveedor
	// public boolean altaProveedor(String activo, String razonSocial, String
	// cuit, String categoria, String direccion, String codigoPostal, String
	// localidad, String provincia,
	// String pais, String email, String telefono, String tipo, String
	// metodoPago, String especialidad, String tallerOficial) {
	// try {
	// DTO_Proveedor proveedor = new DTO_Proveedor();
	// proveedor.setActivo(activo);
	// proveedor.setRazonSocial(razonSocial);
	// proveedor.setCuit(cuit);
	// proveedor.setCategoria(categoria);
	// proveedor.setDireccion(direccion);
	// proveedor.setCodigoPostal(codigoPostal);
	// proveedor.setLocalidad(localidad);
	// proveedor.setProvincia(provincia);
	// proveedor.setPais(pais);
	// proveedor.setEmail(email);
	// proveedor.setTelefono(telefono);
	// proveedor.setTipo(tipo);
	// if (tipo.equals("T")){
	// proveedor.setEspecialidad(especialidad);
	// proveedor.setTallerOficial(tallerOficial);
	// }
	// proveedor.setMetodoPago(metodoPago);
	//
	// objetoRemoto.altaProveedor(proveedor);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Proveedor");
	// }
	// return false;
	// }
	//
	// public boolean modificarProveedor(Integer id, String activo, String
	// razonSocial, String cuit, String categoria, String direccion, String
	// codigoPostal, String localidad, String provincia,
	// String pais, String email, String telefono, String tipo, String
	// metodoPago, String especialidad, String tallerOficial) {
	// try {
	// DTO_Proveedor proveedor = new DTO_Proveedor();
	// proveedor.setId(id);
	// proveedor.setActivo(activo);
	// proveedor.setRazonSocial(razonSocial);
	// proveedor.setCuit(cuit);
	// proveedor.setCategoria(categoria);
	// proveedor.setDireccion(direccion);
	// proveedor.setCodigoPostal(codigoPostal);
	// proveedor.setLocalidad(localidad);
	// proveedor.setProvincia(provincia);
	// proveedor.setPais(pais);
	// proveedor.setEmail(email);
	// proveedor.setTelefono(telefono);
	// proveedor.setTipo(tipo);
	// if (tipo.equals("T")){
	// proveedor.setEspecialidad(especialidad);
	// proveedor.setTallerOficial(tallerOficial);
	// }
	// proveedor.setMetodoPago(metodoPago);
	//
	// objetoRemoto.modificarProveedor(proveedor);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Modificar Proveedor");
	// }
	// return false;
	// }
	//
	// public boolean bajaProveedor(Integer id) {
	// try {
	// objetoRemoto.bajaProveedor(id);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Baja Proveedor");
	// }
	// return false;
	// }
	//
	// public DTO_Proveedor buscarProveedor(String cuit){
	// DTO_Proveedor p = null;
	// try {
	// p = objetoRemoto.buscarProveedor(cuit);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Proveedor");
	// }
	// return p;
	// }
	//
	// public DTO_Proveedor getProveedor(Integer id){
	// DTO_Proveedor p = null;
	// try {
	// p = objetoRemoto.getProveedor(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Proveedor");
	// }
	// return p;
	// }
	//
	// Factura Proveedor
	// public boolean altaFacturaProveedor(Integer idProveedor, String numero,
	// Date fecha, Date vencimiento, float monto){
	//
	// try {
	// DTO_FacturaProveedor factura = new DTO_FacturaProveedor();
	// factura.setNumero(numero);
	// factura.setFecha(fecha);
	// factura.setVencimiento(vencimiento);
	// factura.setMontoTotal(monto);
	// factura.setMontoPendiente(monto);
	// factura.setIdProveedor(idProveedor);
	// objetoRemoto.altaFacturaProveedor(factura);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Alta Factura Proveedor");
	// }
	//
	// return false;
	// }
	//
	// public boolean modificarFacturaProveedor(Integer id, Integer idProveedor,
	// String numero, Date fecha, Date vencimiento, float monto){
	//
	// try {
	// DTO_FacturaProveedor factura = new DTO_FacturaProveedor();
	// factura.setId(id);
	// factura.setNumero(numero);
	// factura.setFecha(fecha);
	// factura.setVencimiento(vencimiento);
	// factura.setMontoTotal(monto);
	// factura.setMontoPendiente(monto);
	// factura.setIdProveedor(idProveedor);
	// objetoRemoto.modificarFacturaProveedor(factura);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error Modificar Factura Proveedor");
	// }
	//
	// return false;
	// }
	//
	// public DTO_FacturaProveedor buscarFacturaProveedor(Integer id){
	// DTO_FacturaProveedor f = null;
	// try {
	// f = objetoRemoto.buscarFacturaProveedor(id);
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error buscar Factura Proveedor");
	// }
	// return f;
	// }
	//
	// public boolean pagarFacturaProveedor(Integer id){
	// try {
	// objetoRemoto.pagarFactura(id);
	// return true;
	// } catch (RemoteException e) {
	// System.out.println(e);
	// System.out.println("Error buscar Factura Proveedor");
	// }
	// return false;
	// }

}
