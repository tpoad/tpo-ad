package webaux;

import dto.ClienteDTO;
import dto.PedidoClienteDTO;

public class AuxClientePedidoDTO {
	private ClienteDTO cliente;
	private PedidoClienteDTO pedido;
	
	public PedidoClienteDTO getPedido() {
		return pedido;
	}
	public void setPedido(PedidoClienteDTO pedido) {
		this.pedido = pedido;
	}
	public ClienteDTO getCliente() {
		return cliente;
	}
	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}
	
	
}
