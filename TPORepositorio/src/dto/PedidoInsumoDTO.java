package dto;

import java.io.Serializable;
import java.util.Date;

import enums.EstadoPedidoInsumo;

public class PedidoInsumoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int loteInsumo;
	private ProveedorDTO proveedor;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;
	private float total;
	private OrdenProduccionDTO ordenProduccion;
	private EstadoPedidoInsumo estado;

	public PedidoInsumoDTO() {
		super();
	}

	public PedidoInsumoDTO(int loteInsumo, ProveedorDTO proveedor, Date fechaGeneracion, Date fechaProbable,
			Date fechaReal, float total, OrdenProduccionDTO ordenProduccion, EstadoPedidoInsumo estado) {
		super();
		this.loteInsumo = loteInsumo;
		this.proveedor = proveedor;
		this.fechaGeneracion = fechaGeneracion;
		this.fechaProbable = fechaProbable;
		this.fechaReal = fechaReal;
		this.total = total;
		this.ordenProduccion = ordenProduccion;
		this.estado = estado;
	}

	public int getLoteInsumo() {
		return loteInsumo;
	}

	public void setLoteInsumo(int loteInsumo) {
		this.loteInsumo = loteInsumo;
	}

	public ProveedorDTO getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorDTO proveedor) {
		this.proveedor = proveedor;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public OrdenProduccionDTO getOrdenProduccion() {
		return ordenProduccion;
	}

	public void setOrdenProduccion(OrdenProduccionDTO ordenProduccion) {
		this.ordenProduccion = ordenProduccion;
	}

	public EstadoPedidoInsumo getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoInsumo estado) {
		this.estado = estado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PedidoInsumoDTO [loteInsumo=" + loteInsumo + ", proveedor=" + proveedor + ", fechaGeneracion="
				+ fechaGeneracion + ", fechaProbable=" + fechaProbable + ", fechaReal=" + fechaReal + ", total=" + total
				+ ", ordenProduccion=" + ordenProduccion + ", estado=" + estado + "]";
	}

}
