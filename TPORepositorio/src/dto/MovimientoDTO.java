package dto;

import java.io.Serializable;
import java.util.Date;

import enums.MovimientoTipo;

public class MovimientoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private MovimientoTipo concepto ;
	public MovimientoTipo getConcepto() {
		return concepto;
	}

	public void setConcepto(MovimientoTipo concepto) {
		this.concepto = concepto;
	}

	private float importe;
	private Date fecha;
	
	public MovimientoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovimientoDTO(float importe, Date fecha) {
		super();
		this.importe = importe;
		this.fecha = fecha;
	}

	public float getImporte() {
		return importe;
	}

	public void setImporte(float importe) {
		this.importe = importe;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
