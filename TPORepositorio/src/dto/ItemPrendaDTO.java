package dto;

import java.io.Serializable;
import java.util.List;

import enums.Color;
import enums.Talle;

public class ItemPrendaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int subcodigo;
	private Color color;
	private Talle talle;
	private Integer cantidadStock;
	private Integer cantidadMinima;
	private Integer cantidadProducir;
	private PrendaDTO prenda;
	private List<InsumoUsadoDTO> insumosUsados;

	public ItemPrendaDTO() {
		super();
	}

	public ItemPrendaDTO(int subcodigo, Color color, Talle talle, Integer cantidadStock, Integer cantidadMinima,
			Integer cantidadProducir, PrendaDTO prenda) {
		super();
		this.subcodigo = subcodigo;
		this.color = color;
		this.talle = talle;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadProducir = cantidadProducir;
		this.prenda = prenda;
	}

	public int getSubcodigo() {
		return subcodigo;
	}

	public void setSubcodigo(int subcodigo) {
		this.subcodigo = subcodigo;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Talle getTalle() {
		return talle;
	}

	public void setTalle(Talle talle) {
		this.talle = talle;
	}

	public Integer getCantidadStock() {
		return cantidadStock;
	}

	public void setCantidadStock(Integer cantidadStock) {
		this.cantidadStock = cantidadStock;
	}

	public Integer getCantidadMinima() {
		return cantidadMinima;
	}

	public void setCantidadMinima(Integer cantidadMinima) {
		this.cantidadMinima = cantidadMinima;
	}

	public Integer getCantidadProducir() {
		return cantidadProducir;
	}

	public void setCantidadProducir(Integer cantidadProducir) {
		this.cantidadProducir = cantidadProducir;
	}

	public PrendaDTO getPrenda() {
		return prenda;
	}

	public void setPrenda(PrendaDTO prenda) {
		this.prenda = prenda;
	}

	public List<InsumoUsadoDTO> getInsumosUsados() {
		return insumosUsados;
	}

	public void setInsumosUsados(List<InsumoUsadoDTO> insumosUsados) {
		this.insumosUsados = insumosUsados;
	}

	@Override
	public String toString() {
		return color + " " + talle;
	}
}
