package dto;

import java.io.Serializable;

public class UbicacionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String calle;
	private Integer bloque;
	private Integer estante;
	private Integer posicion;
	private Integer cantidad;
	
	public UbicacionDTO() {
		super();
	}

	public UbicacionDTO(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super();
		this.calle = calle;
		this.bloque = bloque;
		this.estante = estante;
		this.posicion = posicion;
		this.cantidad = cantidad;
		
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getBloque() {
		return bloque;
	}

	public void setBloque(Integer bloque) {
		this.bloque = bloque;
	}

	public Integer getEstante() {
		return estante;
	}

	public void setEstante(Integer estante) {
		this.estante = estante;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "UbicacionDTO [calle=" + calle + ", bloque=" + bloque + ", estante=" + estante + ", posicion=" + posicion
				+ ", cantidad=" + cantidad + "]";
	}
	
}
