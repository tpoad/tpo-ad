package dto;


public class UbicacionCantidadDTO {
	public int cantidad;
	public UbicacionDTO ubicacion;
	
	public UbicacionDTO getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(UbicacionDTO ubicacion) {
		this.ubicacion = ubicacion;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public UbicacionCantidadDTO(){
		ubicacion = null;
		cantidad = 0;
	}
}
