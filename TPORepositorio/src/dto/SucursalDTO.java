package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SucursalDTO implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private Integer numero;
	private String nombre;
	private String direccion;
	private String horarios;
	private List<EmpleadoDTO> empleados;
	private List<ClienteDTO> clientes;
	
	public SucursalDTO() {
		super();
		// TODO Auto-generated constructor stub
		empleados = new ArrayList<EmpleadoDTO>();
		clientes = new ArrayList<ClienteDTO>();
	}

	public SucursalDTO(Integer numero, String nombre, String direccion, String horarios, List<EmpleadoDTO> empleados,
			List<ClienteDTO> clientes) {
		super();
		this.numero = numero;
		this.nombre = nombre;
		this.direccion = direccion;
		this.horarios = horarios;
		this.empleados = empleados;
		this.clientes = clientes;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getHorarios() {
		return horarios;
	}

	public void setHorarios(String horarios) {
		this.horarios = horarios;
	}

	public List<EmpleadoDTO> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<EmpleadoDTO> empleados) {
		this.empleados = empleados;
	}

	public List<ClienteDTO> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteDTO> clientes) {
		this.clientes = clientes;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SucursalDTO [numero=" + numero + ", nombre=" + nombre + ", direccion=" + direccion + ", horarios="
				+ horarios + ", empleados=" + empleados + ", clientes=" + clientes + "]";
	}

	public void addEmpleados(EmpleadoDTO dto)
	{
		empleados.add(dto);
	}
	
	public void addClientes(ClienteDTO dto)
	{
		clientes.add(dto);
	}

}
