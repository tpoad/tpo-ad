package dto;

import java.io.Serializable;
import java.util.List;

public class DespachoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private  List<PedidoClienteDTO> pedidosClienteCompletos;
	
	public DespachoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public DespachoDTO(List<PedidoClienteDTO> pedidosClienteCompletos) {
		super();
		this.pedidosClienteCompletos = pedidosClienteCompletos;
	}
	public List<PedidoClienteDTO> getPedidosClienteCompletos() {
		return pedidosClienteCompletos;
	}
	public void setPedidosClienteCompletos(List<PedidoClienteDTO> pedidosClienteCompletos) {
		this.pedidosClienteCompletos = pedidosClienteCompletos;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "DespachoDTO [pedidosClienteCompletos=" + pedidosClienteCompletos + "]";
	}
	
	public void addPedidosClienteCompleto(PedidoClienteDTO dto)
	{
		pedidosClienteCompletos.add(dto);
	}
	
}
