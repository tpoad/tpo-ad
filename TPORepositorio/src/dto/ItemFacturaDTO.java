package dto;

import java.io.Serializable;

public class ItemFacturaDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ItemPrendaDTO itemPrenda;
	private Integer cantidad;
	private float precioUnitario;
	
	public ItemFacturaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemFacturaDTO(ItemPrendaDTO itemPrenda, Integer cantidad, float precioUnitario) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precioUnitario = precioUnitario;
	}

	public ItemPrendaDTO getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaDTO itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public float getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(float precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "itemsFacturaDTO [itemPrenda=" + itemPrenda + ", cantidad=" + cantidad + ", precioUnitario="
				+ precioUnitario + "]";
	}
	
	

}
