package dto;

import java.io.Serializable;
import java.util.List;

public class AlmacenDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private List<UbicacionDTO> ubicaciones;
	
	public AlmacenDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AlmacenDTO(List<UbicacionDTO> ubicaciones) {
		super();
		this.ubicaciones = ubicaciones;
	}

	public List<UbicacionDTO> getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(List<UbicacionDTO> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}

	@Override
	public String toString() {
		return "AlmacenDTO [ubicaciones=" + ubicaciones + "]";
	}
	
}
