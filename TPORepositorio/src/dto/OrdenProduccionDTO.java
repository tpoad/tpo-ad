package dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import enums.EstadoOrdenProduccion;


public class OrdenProduccionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int loteProduccion;
	private Date fechaGeneracion;
	private PedidoClienteDTO pedidoCliente;
	private float costoTotal;
	private EstadoOrdenProduccion estado ;
	private List<ItemPrendaDTO> itemsPrenda;
	
	public OrdenProduccionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public OrdenProduccionDTO(int loteProduccion, Date fechaGeneracion,
			PedidoClienteDTO pedidoCliente, float costoTotal,
			EstadoOrdenProduccion estado, List<ItemPrendaDTO> itemsPrenda) {
		super();
		this.loteProduccion = loteProduccion;
		this.fechaGeneracion = fechaGeneracion;
		this.pedidoCliente = pedidoCliente;
		this.costoTotal = costoTotal;
		this.estado = estado;
		this.itemsPrenda = itemsPrenda;
	}



	public int getLoteProduccion() {
		return loteProduccion;
	}



	public void setLoteProduccion(int loteProduccion) {
		this.loteProduccion = loteProduccion;
	}



	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}



	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}



	public PedidoClienteDTO getPedidoCliente() {
		return pedidoCliente;
	}



	public void setPedidoCliente(PedidoClienteDTO pedidoCliente) {
		this.pedidoCliente = pedidoCliente;
	}



	public float getCostoTotal() {
		return costoTotal;
	}



	public void setCostoTotal(float costoTotal) {
		this.costoTotal = costoTotal;
	}



	public EstadoOrdenProduccion getEstado() {
		return estado;
	}



	public void setEstado(EstadoOrdenProduccion estado) {
		this.estado = estado;
	}



	public List<ItemPrendaDTO> getItemsPrenda() {
		return itemsPrenda;
	}



	public void setItemsPrenda(List<ItemPrendaDTO> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "OrdenProduccionDTO [loteProduccion=" + loteProduccion
				+ ", fechaGeneracion=" + fechaGeneracion + ", pedidoCliente="
				+ pedidoCliente + ", costoTotal=" + costoTotal + ", estado="
				+ estado + ", itemsPrenda=" + itemsPrenda + "]";
	}

	

	public void addItemPrenda(ItemPrendaDTO dto)
	{
		itemsPrenda.add(dto);
	}


}
