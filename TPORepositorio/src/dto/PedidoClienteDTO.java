package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import enums.EstadoPedidoCliente;
import enums.MotivoRechazo;

public class PedidoClienteDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer numeroPedido;
	private ClienteDTO cliente;
	private SucursalDTO Sucursal;
	private Date fechaGeneracion;
	private Date fechaProbable;
	private Date fechaReal;
	private List<ItemPedidoClienteDTO> itemsPedidoCliente;
	private Float total;
	private EstadoPedidoCliente estado;
	private MotivoRechazo motivo;
	private List<OrdenProduccionDTO> ordenesDeProduccion;

	public PedidoClienteDTO() {
		super();
		itemsPedidoCliente = new ArrayList<ItemPedidoClienteDTO>();
		ordenesDeProduccion = new ArrayList<OrdenProduccionDTO>();
	}

	public PedidoClienteDTO(Integer numeroPedido, ClienteDTO cliente, SucursalDTO sucursal, Date fechaGeneracion,
			Date fechaProbable, Date fechaReal, List<ItemPedidoClienteDTO> itemsPedidoCliente, Float total,
			EstadoPedidoCliente estado, MotivoRechazo motivo, List<OrdenProduccionDTO> ordenesDeProduccion) {
		super();
		this.numeroPedido = numeroPedido;
		this.cliente = cliente;
		Sucursal = sucursal;
		this.fechaGeneracion = fechaGeneracion;
		this.fechaProbable = fechaProbable;
		this.fechaReal = fechaReal;
		this.itemsPedidoCliente = itemsPedidoCliente;
		this.total = total;
		this.estado = estado;
		this.motivo = motivo;
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

	public Integer getNumeroPedido() {
		return numeroPedido;
	}

	public void setNumeroPedido(Integer numeroPedido) {
		this.numeroPedido = numeroPedido;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public Date getFechaProbable() {
		return fechaProbable;
	}

	public void setFechaProbable(Date fechaProbable) {
		this.fechaProbable = fechaProbable;
	}

	public Date getFechaReal() {
		return fechaReal;
	}

	public void setFechaReal(Date fechaReal) {
		this.fechaReal = fechaReal;
	}

	public List<ItemPedidoClienteDTO> getItemsPedidoCliente() {
		return itemsPedidoCliente;
	}

	public void setItemsPedidoCliente(List<ItemPedidoClienteDTO> itemsPedidoCliente) {
		this.itemsPedidoCliente = itemsPedidoCliente;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public EstadoPedidoCliente getEstado() {
		return estado;
	}

	public void setEstado(EstadoPedidoCliente estado) {
		this.estado = estado;
	}

	public MotivoRechazo getMotivo() {
		return motivo;
	}

	public void setMotivo(MotivoRechazo motivo) {
		this.motivo = motivo;
	}

	public SucursalDTO getSucursal() {
		return Sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		Sucursal = sucursal;
	}

	public List<OrdenProduccionDTO> getOrdenesDeProduccion() {
		return ordenesDeProduccion;
	}

	public void setOrdenesDeProduccion(List<OrdenProduccionDTO> ordenesDeProduccion) {
		this.ordenesDeProduccion = ordenesDeProduccion;
	}

	@Override
	public String toString() {
		return "PedidoClienteDTO [numeroPedido=" + numeroPedido + ", cliente=" + cliente + ", Sucursal=" + Sucursal
				+ ", fechaGeneracion=" + fechaGeneracion + ", fechaProbable=" + fechaProbable + ", fechaReal="
				+ fechaReal + ", itemsPedidoCliente=" + itemsPedidoCliente + ", total=" + total + ", estado=" + estado
				+ ", motivo=" + motivo + ", ordenesDeProduccion=" + ordenesDeProduccion + "]";
	}

}
