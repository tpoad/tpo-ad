package dto;

import java.io.Serializable;
import java.util.List;

public class ProduccionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private List<OrdenProduccionDTO> ordenesPendientes;
	private List<AreaProduccionDTO> areas;
	
	public ProduccionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProduccionDTO(List<OrdenProduccionDTO> ordenesPendientes, List<AreaProduccionDTO> areas) {
		super();
		this.ordenesPendientes = ordenesPendientes;
		this.areas = areas;
	}

	public List<OrdenProduccionDTO> getOrdenesPendientes() {
		return ordenesPendientes;
	}

	public void setOrdenesPendientes(List<OrdenProduccionDTO> ordenesPendientes) {
		this.ordenesPendientes = ordenesPendientes;
	}

	public List<AreaProduccionDTO> getAreas() {
		return areas;
	}

	public void setAreas(List<AreaProduccionDTO> areas) {
		this.areas = areas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProduccionDTO [ordenesPendientes=" + ordenesPendientes + ", areas=" + areas + "]";
	}
	
	public void addOrdenesPendientes(OrdenProduccionDTO dto)
	{
		ordenesPendientes.add(dto);
	}
	
	public void addAreaProduccion(AreaProduccionDTO dto)
	{
		areas.add(dto);
	}
}
