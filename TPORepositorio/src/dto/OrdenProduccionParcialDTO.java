package dto;

import java.util.List;

public class OrdenProduccionParcialDTO extends OrdenProduccionDTO {
	private static final long serialVersionUID = 1L;
	private List<ItemPrendaDTO> itemsPrenda;

	public OrdenProduccionParcialDTO() {
		super();
	}

	public OrdenProduccionParcialDTO(List<ItemPrendaDTO> itemsPrenda) {
		super();
		this.itemsPrenda = itemsPrenda;
	}

	public List<ItemPrendaDTO> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrendaDTO> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OrdenProduccionParcial [itemsPrenda=" + itemsPrenda + "]";
	}

	public void addItemsPrenda(ItemPrendaDTO dto) {
		itemsPrenda.add(dto);
	}

}
