package dto;

import java.io.Serializable;
import java.util.List;

public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer numeroCliente;
	private String razonSocial;
	private String cuit;
	private String inscripcionAFIP;
	private String direccion;
	private List <MovimientoDTO> cuentaCorriente;
	private Float saldo;
	private Float limiteCredito;
	private String condicionesPago;

	public ClienteDTO() {
		super();
	}

	public ClienteDTO(Integer numeroCliente, String razonSocial, String cuit, String inscripcionAFIP, String direccion,
			Float saldo, Float limiteCredito, String condicionesPago) {
		super();
		this.numeroCliente = numeroCliente;
		this.razonSocial = razonSocial;
		this.cuit = cuit;
		this.inscripcionAFIP = inscripcionAFIP;
		this.direccion = direccion;
		this.saldo = saldo;
		this.limiteCredito = limiteCredito;
		this.condicionesPago = condicionesPago;
	}

	public Integer getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(Integer numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getInscripcionAFIP() {
		return inscripcionAFIP;
	}

	public void setInscripcionAFIP(String inscripcionAFIP) {
		this.inscripcionAFIP = inscripcionAFIP;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Float getSaldo() {
		return saldo;
	}

	public void setSaldo(Float saldo) {
		this.saldo = saldo;
	}

	public Float getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Float limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public String getCondicionesPago() {
		return condicionesPago;
	}

	public void setCondicionesPago(String condicionesPago) {
		this.condicionesPago = condicionesPago;
	}

	@Override
	public String toString() {
		return "ClienteDTO [numeroCliente=" + numeroCliente + ", razonSocial=" + razonSocial + ", cuit=" + cuit + "]";
	}

	public List <MovimientoDTO> getCuentaCorriente() {
		return cuentaCorriente;
	}

	public void setCuentaCorriente(List <MovimientoDTO> cuentaCorriente) {
		this.cuentaCorriente = cuentaCorriente;
	}

	public void addMovimiento(MovimientoDTO dto){
		cuentaCorriente.add(dto);
	}
}
