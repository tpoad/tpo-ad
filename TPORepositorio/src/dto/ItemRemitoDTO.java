package dto;

import java.io.Serializable;

public class ItemRemitoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ItemPrendaDTO itemPrenda;
	private Integer cantidad;
	
	public ItemRemitoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ItemRemitoDTO(ItemPrendaDTO itemPrenda, Integer cantidad) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
	}
	public ItemPrendaDTO getItemPrenda() {
		return itemPrenda;
	}
	public void setItemPrenda(ItemPrendaDTO itemPrenda) {
		this.itemPrenda = itemPrenda;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

}
