package dto;

import java.io.Serializable;

public class UbicacionPrendaDTO extends UbicacionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private ItemPrendaDTO item;
	
	public UbicacionPrendaDTO() {
		super();
	}
	
	public UbicacionPrendaDTO(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super(calle, bloque, estante, posicion, cantidad);
	}
	
	public ItemPrendaDTO getItem() {
		return item;
	}
	
	public void setItems(ItemPrendaDTO item) {
		this.item = item;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "UbicacionPrendaDTO [item=" + item + "]";
	}
	
}
