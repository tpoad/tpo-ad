package dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import enums.Temporada;

public class PrendaDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private int codigo;
	private String descripcion;
	private Temporada temporada;
	private String molde;
	private List<ItemPrendaDTO> itemsPrenda;
	private Integer estado;
	private List <PasoProduccionDTO> pasosProduccion;
	private Float costoActual;
	private Float precioVenta;
	private Date tiempoProduccion;

	public PrendaDTO() {
		itemsPrenda = new ArrayList<ItemPrendaDTO>();
		pasosProduccion = new ArrayList <PasoProduccionDTO>();
	}

	public PrendaDTO(int codigo, String descripcion, Temporada temporada, String molde,
			List<ItemPrendaDTO> itemsPrenda, Integer estado, List<PasoProduccionDTO> pasosProduccion, Float costoActual,
			Float precioVenta, Date tiempoProduccion) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.temporada = temporada;
		this.molde = molde;
		this.itemsPrenda = itemsPrenda;
		this.estado = estado;
		this.pasosProduccion = pasosProduccion;
		this.costoActual = costoActual;
		this.precioVenta = precioVenta;
		this.tiempoProduccion = tiempoProduccion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Temporada getTemporada() {
		return temporada;
	}

	public void setTemporada(Temporada temporada) {
		this.temporada = temporada;
	}

	public String getMolde() {
		return molde;
	}

	public void setMolde(String molde) {
		this.molde = molde;
	}

	public List<ItemPrendaDTO> getItemsPrenda() {
		return itemsPrenda;
	}

	public void setItemsPrenda(List<ItemPrendaDTO> itemsPrenda) {
		this.itemsPrenda = itemsPrenda;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public List<PasoProduccionDTO> getPasosProduccion() {
		return pasosProduccion;
	}

	public void setPasosProduccion(List<PasoProduccionDTO> pasosProduccion) {
		this.pasosProduccion = pasosProduccion;
	}

	public Float getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(Float costoActual) {
		this.costoActual = costoActual;
	}

	public Float getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Float precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Date getTiempoProduccion() {
		return tiempoProduccion;
	}

	public void setTiempoProduccion(Date tiempoProduccion) {
		this.tiempoProduccion = tiempoProduccion;
	}

	@Override
	public String toString() {
		return "PrendaDTO [codigo=" + codigo + ", descripcion=" + descripcion + ", temporada=" + temporada + ", molde="
				+ molde + ", itemsPrenda=" + itemsPrenda + ", estado=" + estado + ", pasosProduccion=" + pasosProduccion
				+ ", costoActual=" + costoActual + ", precioVenta=" + precioVenta + ", tiempoProduccion="
				+ tiempoProduccion + "]";
	}
	
}
