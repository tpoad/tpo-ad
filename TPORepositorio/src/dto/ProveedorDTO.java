package dto;

import java.io.Serializable;

public class ProveedorDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String razonSocial;
	private String cuit;
	private String direccion;
	private String condicionesCobro;
	
	public ProveedorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProveedorDTO(String razonSocial, String cuit, String direccion, String condicionesCobro) {
		super();
		this.razonSocial = razonSocial;
		this.cuit = cuit;
		this.direccion = direccion;
		this.condicionesCobro = condicionesCobro;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCondicionesCobro() {
		return condicionesCobro;
	}

	public void setCondicionesCobro(String condicionesCobro) {
		this.condicionesCobro = condicionesCobro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ProveedorDTO [razonSocial=" + razonSocial + ", cuit=" + cuit + ", direccion=" + direccion
				+ ", condicionesCobro=" + condicionesCobro + "]";
	}
	
	
	
}
