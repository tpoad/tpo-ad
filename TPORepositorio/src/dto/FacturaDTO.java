package dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FacturaDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ClienteDTO Cliente;
	private Date fecha;
	private List<ItemFacturaDTO> itemsFactura;
	private float precioTotal;
	
	public FacturaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public FacturaDTO(ClienteDTO cliente, Date fecha, List<ItemFacturaDTO> itemsFactura, float precioTotal) {
		super();
		Cliente = cliente;
		this.fecha = fecha;
		this.itemsFactura = itemsFactura;
		this.precioTotal = precioTotal;
	}



	public ClienteDTO getCliente() {
		return Cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		Cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public List<ItemFacturaDTO> getItemsFactura() {
		return itemsFactura;
	}



	public void setItemsFactura(List<ItemFacturaDTO> itemsFactura) {
		this.itemsFactura = itemsFactura;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public float getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(float precioTotal) {
		this.precioTotal = precioTotal;
	}

	@Override
	public String toString() {
		return "FacturaDTO [Cliente=" + Cliente + ", fecha=" + fecha + ", itemFactura=" + itemsFactura + ", precioTotal="
				+ precioTotal + "]";
	}
	
	public void addItemFactura(ItemFacturaDTO dto)
	{
		itemsFactura.add(dto);
	}
}
