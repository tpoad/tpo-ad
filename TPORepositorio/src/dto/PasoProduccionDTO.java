package dto;

import java.io.Serializable;
import java.sql.Time;

public class PasoProduccionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private AreaProduccionDTO areaProduccion;
	private Time tiempoConsumido;
	
	public PasoProduccionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PasoProduccionDTO(AreaProduccionDTO areaProduccion, Time tiempoConsumido) {
		super();
		this.areaProduccion = areaProduccion;
		this.tiempoConsumido = tiempoConsumido;
	}

	public AreaProduccionDTO getAreaProduccion() {
		return areaProduccion;
	}

	public void setAreaProduccion(AreaProduccionDTO areaProduccion) {
		this.areaProduccion = areaProduccion;
	}

	public Time getTiempoConsumido() {
		return tiempoConsumido;
	}

	public void setTiempoConsumido(Time tiempoConsumido) {
		this.tiempoConsumido = tiempoConsumido;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PasoProduccionDTO [areaProduccion=" + areaProduccion + ", tiempoConsumido=" + tiempoConsumido + "]";
	}
	
	 

}
