package dto;

import java.io.Serializable;

public class UbicacionInsumoDTO extends UbicacionDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private InsumoDTO insumo;
	
	public UbicacionInsumoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UbicacionInsumoDTO(String calle, Integer bloque, Integer estante, Integer posicion, Integer cantidad) {
		super(calle, bloque, estante, posicion, cantidad);
		// TODO Auto-generated constructor stub
	}
	public InsumoDTO getInsumo() {
		return insumo;
	}
	public void setInsumo(InsumoDTO insumo) {
		this.insumo = insumo;
	}
	@Override
	public String toString() {
		return "UbicacionInsumoDTO [insumo=" + insumo + "]";
	}
	
	
	
}
