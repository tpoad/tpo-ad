package dto;

import java.io.Serializable;
import java.util.List;

public class AreaProduccionDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nombre;
	private List<LineaProduccionDTO> lineasProduccion;
	private List<OrdenProduccionDTO> OrdenesPendiente;
	private List<ItemPrendaDTO> itemPrendaTerminado;

	public AreaProduccionDTO() {
		super();
	}

	public AreaProduccionDTO(String nombre, List<LineaProduccionDTO> lineasProduccion,
			List<OrdenProduccionDTO> ordenesPendiente, List<ItemPrendaDTO> itemPrendaTerminado) {
		super();
		this.nombre = nombre;
		this.lineasProduccion = lineasProduccion;
		OrdenesPendiente = ordenesPendiente;
		this.itemPrendaTerminado = itemPrendaTerminado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<LineaProduccionDTO> getLineasProduccion() {
		return lineasProduccion;
	}

	public void setLineasProduccion(List<LineaProduccionDTO> lineasProduccion) {
		this.lineasProduccion = lineasProduccion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<OrdenProduccionDTO> getOrdenesPendiente() {
		return OrdenesPendiente;
	}

	public void setOrdenesPendiente(List<OrdenProduccionDTO> ordenesPendiente) {
		OrdenesPendiente = ordenesPendiente;
	}

	public List<ItemPrendaDTO> getItemPrendaTerminado() {
		return itemPrendaTerminado;
	}

	public void setItemPrendaTerminado(List<ItemPrendaDTO> itemPrendaTerminado) {
		this.itemPrendaTerminado = itemPrendaTerminado;
	}

	@Override
	public String toString() {
		return "AreaProduccionDTO [nombre=" + nombre + ", lineasProduccion=" + lineasProduccion + ", OrdenesPendiente="
				+ OrdenesPendiente + ", itemPrendaTerminado=" + itemPrendaTerminado + "]";
	}

	public void addLineasProduccion(LineaProduccionDTO dto) {
		lineasProduccion.add(dto);
	}

	public void addOrdenesPendiente(OrdenProduccionDTO dto) {
		OrdenesPendiente.add(dto);
	}

	public void addItemPrenda(ItemPrendaDTO dto) {
		itemPrendaTerminado.add(dto);
	}
}
