package dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RemitoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private ClienteDTO Cliente;
	private Date fecha;
	private List<ItemRemitoDTO> itemsRemito;
	private String direccionEntrega;
	
	public RemitoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	public RemitoDTO(ClienteDTO cliente, Date fecha, List<ItemRemitoDTO> itemsRemito, String direccionEntrega) {
		super();
		Cliente = cliente;
		this.fecha = fecha;
		this.itemsRemito = itemsRemito;
		this.direccionEntrega = direccionEntrega;
	}



	public ClienteDTO getCliente() {
		return Cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		Cliente = cliente;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public List<ItemRemitoDTO> getItemsRemito() {
		return itemsRemito;
	}



	public void setItemsRemito(List<ItemRemitoDTO> itemsRemito) {
		this.itemsRemito = itemsRemito;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public String getDireccionEntrega() {
		return direccionEntrega;
	}

	public void setDireccionEntrega(String direccionEntrega) {
		this.direccionEntrega = direccionEntrega;
	}

	@Override
	public String toString() {
		return "RemitoDTO [Cliente=" + Cliente + ", fecha=" + fecha + ", itemRemito=" + itemsRemito
				+ ", direccionEntrega=" + direccionEntrega + "]";
	}
	
	public void addItemsRemito(ItemRemitoDTO dto)
	{
		itemsRemito.add(dto);
	}
	
}
