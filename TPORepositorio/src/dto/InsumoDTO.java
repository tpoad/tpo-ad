package dto;

import java.io.Serializable;

public class InsumoDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private int codigoInterno;
	private String nombre;
	private Integer cantidadStock;
	private Integer cantidadMinima;
	private Integer cantidadComprar;
	private float costoActual;
	
	public InsumoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InsumoDTO(int codigoInterno, String nombre, Integer cantidadStock, Integer cantidadMinima,
			Integer cantidadComprar, float costoActual) {
		super();
		this.codigoInterno = codigoInterno;
		this.nombre = nombre;
		this.cantidadStock = cantidadStock;
		this.cantidadMinima = cantidadMinima;
		this.cantidadComprar = cantidadComprar;
		this.costoActual = costoActual;
	}

	public int getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(int codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getCantidadStock() {
		return cantidadStock;
	}

	public void setCantidadStock(Integer cantidadStock) {
		this.cantidadStock = cantidadStock;
	}

	public Integer getCantidadMinima() {
		return cantidadMinima;
	}

	public void setCantidadMinima(Integer cantidadMinima) {
		this.cantidadMinima = cantidadMinima;
	}

	public Integer getCantidadComprar() {
		return cantidadComprar;
	}

	public void setCantidadComprar(Integer cantidadComprar) {
		this.cantidadComprar = cantidadComprar;
	}

	public float getCostoActual() {
		return costoActual;
	}

	public void setCostoActual(float costoActual) {
		this.costoActual = costoActual;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InsumoDTO [codigoInterno=" + codigoInterno + ", nombre=" + nombre + ", cantidadStock=" + cantidadStock
				+ ", cantidadMinima=" + cantidadMinima + ", cantidadComprar=" + cantidadComprar + ", costoActual="
				+ costoActual + "]";
	}
	
	
	
}
