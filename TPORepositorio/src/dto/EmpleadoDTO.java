package dto;

import java.io.Serializable;

import enums.EmpleadoRol;

public class EmpleadoDTO implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private int legajo;
	private String nombreCompleto;
	private  EmpleadoRol rol;
	
	public EmpleadoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public EmpleadoDTO(int legajo, String nombreCompleto) {
		super();
		this.legajo = legajo;
		this.nombreCompleto = nombreCompleto;
	}
	
	public int getLegajo() {
		return legajo;
	}
	
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public EmpleadoRol getRol() {
		return rol;
	}



	public void setRol(EmpleadoRol rol) {
		this.rol = rol;
	}
	

	@Override
	public String toString() {
		return "EmpleadoDTO [legajo=" + legajo + ", nombreCompleto=" + nombreCompleto + "]";
	}
	

	
}
