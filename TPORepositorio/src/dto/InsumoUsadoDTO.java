package dto;

import java.io.Serializable;

public class InsumoUsadoDTO  implements Serializable
{
	private static final long serialVersionUID = 1L;
	private InsumoDTO insumo;
	private float cantidad;
	
	public InsumoUsadoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InsumoUsadoDTO(InsumoDTO insumo, float cantidad) {
		super();
		this.insumo = insumo;
		this.cantidad = cantidad;
	}

	public InsumoDTO getInsumo() {
		return insumo;
	}

	public void setInsumo(InsumoDTO insumo) {
		this.insumo = insumo;
	}

	public float getCantidad() {
		return cantidad;
	}

	public void setCantidad(float cantidad) {
		this.cantidad = cantidad;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "InsumoUsadoDTO [insumo=" + insumo + ", cantidad=" + cantidad + "]";
	}
	
	
	

}
