package dto;

import java.io.Serializable;

public class LineaProduccionDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String nombre;
	private boolean estado;
	// private ItemOrdenProduccionDTO ItemOrdenProduccion; //OJO con esto!no lo tenemos en el diagrama
	private ItemPrendaDTO itemPrenda;
	private AreaProduccionDTO AreaProduccion;
	
	public LineaProduccionDTO() {
		super();
	}

	public LineaProduccionDTO(String nombre, boolean estado, OrdenProduccionDTO ordenProduccion,
			AreaProduccionDTO AreaProduccion) {
		super();
		this.nombre = nombre;
		this.estado = estado;
		// this.ordenProduccion = ordenProduccion; //OJO con esto!no lo tenemos en el diagrama
		this.AreaProduccion = AreaProduccion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	//// OJO con esto!no lo tenemos en el diagrama
	/*
	 * public OrdenProduccionDTO getOrdenProduccion() { return ordenProduccion;
	 * }
	 * 
	 * public void setOrdenProduccion(OrdenProduccionDTO ordenProduccion) {
	 * this.ordenProduccion = ordenProduccion; }
	 */

	public AreaProduccionDTO getAreaProduccion() {
		return AreaProduccion;
	}

	public void setAreaProduccion(AreaProduccionDTO areaProduccion) {
		AreaProduccion = areaProduccion;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public ItemPrendaDTO getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaDTO itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

}
