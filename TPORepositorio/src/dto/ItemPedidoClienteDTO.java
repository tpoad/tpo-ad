package dto;

import java.io.Serializable;

public class ItemPedidoClienteDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private ItemPrendaDTO itemPrenda;
	private Integer cantidad;
	private Float precio;

	public ItemPedidoClienteDTO() {
		super();
	}

	public ItemPedidoClienteDTO(ItemPrendaDTO itemPrenda, Integer cantidad, Float precio) {
		super();
		this.itemPrenda = itemPrenda;
		this.cantidad = cantidad;
		this.precio = precio;
	}

	public ItemPrendaDTO getItemPrenda() {
		return itemPrenda;
	}

	public void setItemPrenda(ItemPrendaDTO itemPrenda) {
		this.itemPrenda = itemPrenda;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "ItemPedidoClienteDTO [itemPrenda=" + itemPrenda + ", cantidad=" + cantidad + ", precio=" + precio + "]";
	}
	
}
