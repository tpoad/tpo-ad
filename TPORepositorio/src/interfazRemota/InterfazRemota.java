package interfazRemota;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import dto.*;
import enums.MotivoRechazo;

/**
 * @author zuki Ac� van las firmas de los metodos a publicar con su
 *         correspondiente throws exception
 */
public interface InterfazRemota extends Remote{

	/* CLIENTE---------------------------------- */
	public List<ClienteDTO> getClientes() throws RemoteException;
	public ClienteDTO obtenerCliente(String cuit) throws RemoteException;
	public void altaCliente(ClienteDTO dto) throws RemoteException;
	public void eliminarCliente(String cuit) throws RemoteException;
	public void modificarCliente(ClienteDTO clienteDTO) throws RemoteException;
	
	/* PRENDA---------------------------------- */
	public List<PrendaDTO> obtenerPrendas() throws RemoteException;
	public PrendaDTO getPrendaBoba(int id) throws RemoteException;
	public void altaPrenda(PrendaDTO dto) throws RemoteException;
	public void eliminarPrenda(int codigo) throws RemoteException;
	public void modificarPrenda(PrendaDTO dto) throws RemoteException;

	/* ITEMPRENDA---------------------------------- */
	public List<ItemPrendaDTO> obtenerItemsPrenda(int codigo) throws RemoteException;
	public List<ItemPrendaDTO> obtenerTodosItemsPrenda() throws RemoteException;
	public ItemPrendaDTO obtenerItemPrenda(int subcodigo) throws RemoteException;
	public void altaItemPrenda(ItemPrendaDTO dto) throws RemoteException;
	public void eliminarItemPrenda(int subcodigo) throws RemoteException;
	public void modificarItemPrenda(ItemPrendaDTO dto) throws RemoteException;

	/* SUCURSAL---------------------------------- */
	public void altaSucursal(SucursalDTO dto) throws RemoteException;
	public SucursalDTO getSucursal(int nro) throws RemoteException;
	public List<SucursalDTO> getSucursales() throws RemoteException;
	
	/* PEDIDO CLIENTE---------------------------------- */
	public List<PedidoClienteDTO> obtenerPedidosCliente() throws RemoteException;
	public List<PedidoClienteDTO> obtenerPedidosCliente(String cuit) throws RemoteException;
	public List<PedidoClienteDTO> obtenerPedidosClienteSucursal(int nrosucursal) throws RemoteException;
	public List<PedidoClienteDTO> obtenerPedidosClientePendienteDeCheckeo() throws RemoteException;
	public PedidoClienteDTO obtenerPedidoCliente(int intNroPedido) throws RemoteException;
	public void altaPedidoCliente(PedidoClienteDTO pedidoCliente) throws RemoteException;
	public void aprobarPedidoCliente(int nroPedido) throws RemoteException;
	public void aceptarPedidoCliente(int nroPedido) throws RemoteException;
	public void rechazarPedidoCliente(int nroPedido, MotivoRechazo motivo) throws RemoteException;
	public void cancelarPedidoCliente(int nroPedido) throws RemoteException;
	public void procesarPedidoCliente(int nroPedido) throws RemoteException;
//	public void despacharPedidoCliente(int nroPedido) throws RemoteException;
	
	/* PRODUCCION---------------------------------- */
	public List<OrdenProduccionDTO> obtenerOrdenesArea(String area)throws RemoteException;
	public void completarOrden(String area, int orden)throws RemoteException;
	
	/* INSUMO---------------------------------- */
/**/   	public List<InsumoDTO> obtenerInsumos() throws RemoteException;
/**/	public List<InsumoDTO> obtenerInsumo() throws RemoteException;
/**/    public InsumoDTO getInsumo(int id) throws RemoteException;
		public void altaInsumo(InsumoDTO dto) throws RemoteException;

	/* ADMINISTRACI�N---------------------------------- */
//	public FacturaDTO generarFactura(int nroPedido) throws RemoteException;
//	public RemitoDTO generarRemito(int nroPedido) throws RemoteException;

}
