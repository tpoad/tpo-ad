package exceptions;

import java.rmi.RemoteException;

public class LineaProduccionException extends RemoteException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public LineaProduccionException (String message)
	{
		super(message);
	}
	

}
