package exceptions;

import java.rmi.RemoteException;

public class ItemPrendaException extends RemoteException {

	private static final long serialVersionUID = 1L;

	public ItemPrendaException(String message) {
		super(message);
	}

}
