package exceptions;

import java.rmi.RemoteException;

/**
 * @author zuki
 *
 */
public class PedidoException extends RemoteException {

	private static final long serialVersionUID = 1L;

	public PedidoException(String message) {
		super(message);
	}

}
