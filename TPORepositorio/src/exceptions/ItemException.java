package exceptions;

import java.rmi.RemoteException;

/**
 * @author zuki
 *
 */
public class ItemException extends RemoteException {

	private static final long serialVersionUID = 1L;

	public ItemException(String message) {
		super(message);
	}

}
