package exceptions;

import java.rmi.RemoteException;

public class PedidoClienteException extends RemoteException {

	private static final long serialVersionUID = 1L;

	public PedidoClienteException(String message) {
		super(message);
	}
}
