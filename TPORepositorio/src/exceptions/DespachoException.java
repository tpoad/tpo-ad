package exceptions;

import java.rmi.RemoteException;

public class DespachoException extends RemoteException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public DespachoException (String message)
	{
		super(message);
	}

}
