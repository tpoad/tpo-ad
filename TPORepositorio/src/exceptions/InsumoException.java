package exceptions;

import java.rmi.RemoteException;

public class InsumoException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InsumoException (String message)
	{
		super(message);
	}
}
