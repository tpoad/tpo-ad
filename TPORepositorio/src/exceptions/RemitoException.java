package exceptions;

import java.rmi.RemoteException;

public class RemitoException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public RemitoException(String message)
	{
		super(message);
	}

}
