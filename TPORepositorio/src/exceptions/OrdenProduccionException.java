package exceptions;

import java.rmi.RemoteException;

public class OrdenProduccionException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public OrdenProduccionException(String message)
	{
		super(message);
	}

}
