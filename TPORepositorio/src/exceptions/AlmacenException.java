package exceptions;

public class AlmacenException extends Exception
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AlmacenException (String message)
	{
		super(message);
	}
}
