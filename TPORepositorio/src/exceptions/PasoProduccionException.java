package exceptions;

import java.rmi.RemoteException;

public class PasoProduccionException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public PasoProduccionException (String message)
	{
		super(message);
	}

}
