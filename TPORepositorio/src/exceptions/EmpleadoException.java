package exceptions;

import java.rmi.RemoteException;

public class EmpleadoException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public EmpleadoException (String message)
	{
		super(message);
	}

}
