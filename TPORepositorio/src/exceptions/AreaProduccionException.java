package exceptions;

import java.rmi.RemoteException;

public class AreaProduccionException extends RemoteException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public AreaProduccionException(String message)
	{
		super(message);
	}

}
