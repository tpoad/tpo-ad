package exceptions;

import java.rmi.RemoteException;

public class MovimientoException extends RemoteException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MovimientoException (String message)
	{
		super(message);
	}
}
