package exceptions;

import java.rmi.RemoteException;

public class UbicacionException extends RemoteException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public UbicacionException (String message)
	{
		super(message);
	}
	
}
