package exceptions;

import java.rmi.RemoteException;

public class ProveedorException extends RemoteException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ProveedorException (String message)
	{
		super(message);
	}

}
