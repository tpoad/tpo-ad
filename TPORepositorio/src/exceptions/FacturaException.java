package exceptions;

import java.rmi.RemoteException;

public class FacturaException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public FacturaException (String message)
	{
		super(message);
	}

}
