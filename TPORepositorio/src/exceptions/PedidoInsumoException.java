package exceptions;

import java.rmi.RemoteException;

public class PedidoInsumoException extends RemoteException{

	private static final long serialVersionUID = 1L;
	public PedidoInsumoException (String message)
	{
		super(message);
	}
}