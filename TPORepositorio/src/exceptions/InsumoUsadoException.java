package exceptions;

import java.rmi.RemoteException;

public class InsumoUsadoException extends RemoteException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public InsumoUsadoException (String message)
	{
		super(message);
	}

}
