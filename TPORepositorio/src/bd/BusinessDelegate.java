package bd;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import dto.ClienteDTO;
import dto.ItemPrendaDTO;
import dto.OrdenProduccionDTO;
import dto.PedidoClienteDTO;
import dto.PrendaDTO;
import dto.SucursalDTO;
import enums.MotivoRechazo;
import interfazRemota.InterfazRemota;

/**
 * @author zuki Ac� voy a llamar a cada uno de los metodos definidos en la
 *         interfaz remota con su correspondiente throw Exception usando
 *         manejoNegocio
 */
public class BusinessDelegate extends UnicastRemoteObject {

	private static final long serialVersionUID = 1L;
	private static BusinessDelegate instancia;
	private InterfazRemota s;

	protected BusinessDelegate() throws RemoteException {
		super();
		try {
			s = (InterfazRemota) Naming.lookup("//localhost/ObjetoRemoto");
			System.out.println("Business Delegate lookup exitoso");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static BusinessDelegate getInstancia() throws RemoteException {
		if (instancia == null)
			instancia = new BusinessDelegate();
		return instancia;
	}

	/* CLIENTE---------------------------------- */
	public List<ClienteDTO> getClientes() throws RemoteException {
		return s.getClientes();
	}

	public ClienteDTO obtenerCliente(String cuit) throws RemoteException {
		return s.obtenerCliente(cuit);
	}

	public void altaCliente(ClienteDTO dto) throws RemoteException {
		s.altaCliente(dto);
	}

	public void eliminarCliente(String cuit) throws RemoteException {
		s.eliminarCliente(cuit);
	}

	public void modificarCliente(ClienteDTO cliente) throws RemoteException {
		s.modificarCliente(cliente);
	}

	/* PRENDA---------------------------------- */
	public List<PrendaDTO> obtenerPrendas() throws RemoteException {
		return s.obtenerPrendas();
	}

	public PrendaDTO getPrendaBoba(int id) throws RemoteException {
		return s.getPrendaBoba(id);
	}

	public void altaPrenda(PrendaDTO dto) throws RemoteException {
		s.altaPrenda(dto);
	}

	public void eliminarPrenda(int codigo) throws RemoteException {
		s.eliminarPrenda(codigo);
	}

	public void modificarPrenda(PrendaDTO dto) throws RemoteException {
		s.modificarPrenda(dto);
	}

	/* ITEM PRENDA---------------------------------- */
	public List<ItemPrendaDTO> obtenerItemsPrenda(int codigo) throws RemoteException {
		return s.obtenerItemsPrenda(codigo);
	}

	public List<ItemPrendaDTO> obtenerTodosItemsPrenda() throws RemoteException {
		return s.obtenerTodosItemsPrenda();
	}

	public ItemPrendaDTO obtenerItemPrenda(int subcodigo) throws RemoteException {
		return s.obtenerItemPrenda(subcodigo);
	}

	public void altaItemPrenda(ItemPrendaDTO dto) throws RemoteException {
		s.altaItemPrenda(dto);
	}

	public void eliminarItemPrenda(int subcodigo) throws RemoteException {
		s.eliminarItemPrenda(subcodigo);
	}

	public void modificarItemPrenda(ItemPrendaDTO dto) throws RemoteException {
		s.modificarItemPrenda(dto);
	}

	/* PEDIDO CLIENTE---------------------------------- */
	public void altaPedidoCliente(PedidoClienteDTO pedidoCliente) throws RemoteException {
		s.altaPedidoCliente(pedidoCliente);
	}

	public List<PedidoClienteDTO> obtenerPedidosClientePendienteDeCheckeo() throws RemoteException {
		return s.obtenerPedidosClientePendienteDeCheckeo();
	}

	public List<PedidoClienteDTO> obtenerPedidosCliente(String cuit) throws RemoteException {
		return s.obtenerPedidosCliente(cuit);
	}

	public List<PedidoClienteDTO> obtenerPedidosClienteSucursal(int nrosucursal) throws RemoteException {
		return s.obtenerPedidosClienteSucursal(nrosucursal);
	}

	public void aprobarPedidoCliente(int nroPedido) throws RemoteException {
		s.aprobarPedidoCliente(nroPedido);
	}

	public void aceptarPedidoCliente(int nroPedido) throws RemoteException {
		s.aceptarPedidoCliente(nroPedido);
	}

	public void rechazarPedidoCliente(int nroPedido, MotivoRechazo motivo) throws RemoteException {
		s.rechazarPedidoCliente(nroPedido, motivo);
	}

	public void cancelarPedidoCliente(int nroPedido) throws RemoteException {
		s.aceptarPedidoCliente(nroPedido);
	}

//	public void procesarPedidoCliente(PedidoClienteDTO dto) throws RemoteException{
//		s.procesarPedidoCliente(dto);
//	}
	
	public void procesarPedidoCliente(int nroPedido) throws RemoteException{
		s.procesarPedidoCliente(nroPedido);
	}
	
//	public void despacharPedidoCliente(int nroPedido) throws RemoteException {
//		s.despacharPedidoCliente(nroPedido);
//	}

//	public List<UbicacionCantidadDTO> despacharPedidoCliente(int nroPedido) throws RemoteException {
//		return s.despacharPedidoCliente(nroPedido);
//	}

	/* PRODUCCION-------------------- */
	
	public List<OrdenProduccionDTO> obtenerOrdenesArea(String area)throws RemoteException{
		return s.obtenerOrdenesArea(area);
	}
	
	public void completarOrden(String area, int orden)throws RemoteException{
		s.completarOrden(area, orden);
	}
	
	/* ADMINISTRACI�N---------------------------------- */
//	public FacturaDTO generarFactura(int nroPedido) throws RemoteException {
//		return s.generarFactura(nroPedido);
//	}
//
//	public RemitoDTO generarRemito(int nroPedido) throws RemoteException {
//		return s.generarRemito(nroPedido);
//	}
	
	public List<SucursalDTO> getSucursales() throws RemoteException{
		return s.getSucursales();
	}
	
	public SucursalDTO getSucursal(int nro) throws RemoteException {
		return  s.getSucursal(nro);
	}

}
