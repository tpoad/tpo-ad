package enums;

public enum EstadoPedidoInsumo {
	Pendiente, Aceptado, Rechazado
}
