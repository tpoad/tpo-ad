package enums;

public enum MotivoRechazo {
	SALDO_INSUFICIENTE, PRENDAS_DISCONTINUAS
}
